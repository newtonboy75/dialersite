$(document).ready(function(){
    //tooltip
    $('[data-toggle="tooltip"]').tooltip();

    //to top
    $(window).scroll(function () {
        if ($(this).scrollTop() > 500) {
            $('#back-to-top').slideDown('normal', 'easeOutBack');
        } else {
            $('#back-to-top').slideUp('normal', 'easeInBack');
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 200, 'easeOutQuint');
        return false;
    });

    // // favourite-toggle
    $('div[class="recent-favorite"]').click(function (){

        var selector = $(this);
        var slug = selector.data('slug');
        var id = selector.attr('rel');
        $.ajax({
            type : 'POST',
            url : '/save-ad-as-favorite',
            data : { id: id, slug : slug, action: 'add',  _token : '{{ csrf_token() }}' },
            success : function (data) {
                if (data.status == 1){
                  //console.log(data.msg);
                    if(!data.msg.includes("remove")){
                      $(selector).removeClass('recent-favorite-active');
                      $(selector).attr('data-original-title', 'Add to Favorites');
                      gtag('event', 'FavoriteRemoved', {
                      'event_category': 'Home  Page',
                      'event_label': slug,
                      });

                    }else{
                      $(selector).addClass('recent-favorite-active');
                      $(selector).attr('data-original-title', 'Remove from Favorites');
                      gtag('event', 'FavoriteAdded', {
                        'event_category': 'Home  Page',
                        'event_label': slug,
                      });
                    }
                }else {
                    if (data.redirect_url){
                        location.href= data.redirect_url;
                    }
                }
            }
        });
    });

    $('.featured-favorite').click(function (){

            var selector = $(this);
            var slug = selector.data('slug');

            $.ajax({
                type : 'POST',
                url : '/save-ad-as-favorite',
                data : { slug : slug, action: 'add',  _token : '{{ csrf_token() }}' },
                success : function (data) {
                  if (data.status == 1){
                    //console.log(data.msg);
                      if(!data.msg.includes("remove")){
                        $(selector).removeClass('featured-favorite-active');
                        $(selector).attr('data-original-title', 'Add to Favorites');
                        gtag('event', 'FavoriteRemoved', {
                          'event_category': 'Home  Page',
                          'event_label': slug,
                        });

                      }else{
                        $(selector).addClass('featured-favorite-active');
                        $(selector).attr('data-original-title', 'Remove from Favorites');
                        gtag('event', 'FavoriteAdded', {
                          'event_category': 'Home  Page',
                          'event_label': slug,
                        });
                      }
                  }else {
                      if (data.redirect_url){
                          location.href= data.redirect_url;
                      }
                  }
                }
            });
    });

    // owl carousel for featured items
    $('.owl-carousel').owlCarousel({
        loop:false,
        // margin:10,
        responsiveClass:true,
        // autoplay:true,
        dots:false,
        nav:true,
        fluidSpeed:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:3
            }
        }
    });
});
