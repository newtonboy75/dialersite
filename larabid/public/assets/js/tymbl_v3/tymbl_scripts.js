/*HEADER PRODUCTS*/
	$( "#tymbl_header_link_products" ).click(function() {
		if($('#tymbl_header_products_drop:visible').length) {
			tymbl_hide_products();
		} else {
			tymbl_show_products();
		}
	});
	$( "#tymbl_home_head" ).click(function() {
		if($('#tymbl_header_products_drop:visible').length) {
			tymbl_hide_products();
		}
	});

	function tymbl_hide_products() {
		tymbl_header_products_drop_height = $('#tymbl_header_products_drop').css('height');
		$('#tymbl_header_products_drop').animate({height:tymbl_header_products_drop_height},0);
		$('#tymbl_header_products_drop').animate({height:0},150);
		setTimeout(
			function() 
			{
				$('#tymbl_header_products_drop').hide();
				$('#tymbl_header_products_drop').animate({height:tymbl_header_products_drop_height},0);
			}, 150);	
	}
	function tymbl_show_products() {
		tymbl_header_products_drop_height = $('#tymbl_header_products_drop').css('height');
		$('#tymbl_header_products_drop').show();
		$('#tymbl_header_products_drop').animate({height:0},0);
		$('#tymbl_header_products_drop').animate({height:tymbl_header_products_drop_height},150);
	}

/*MOBILE MENU*/
	tymbl_header_mobile_width_original = 320;

	$( "#tymbl_header_hamburger" ).click(function() {
		if($('#tymbl_header_mobile:visible').length) {
			tymbl_hide_mobile_menu();
		} else {
			tymbl_show_mobile_menu();
		}
	});
	$( ".tymbl_header_mobile_products_drop_block" ).click(function() {
		tymbl_hide_mobile_menu();
		setTimeout(
			function() 
			{
				/*more precise, but don't work in WebKit: window.location.hash = window.location.hash;*/
				window.location.href = window.location.hash;
			}, 150);
	});
	$( "#tymbl_header_mobile_bg" ).click(function() {
		tymbl_hide_mobile_menu();
	});

	function tymbl_hide_mobile_menu() {
		tymbl_header_mobile_width = tymbl_header_mobile_width_original;
		$('#tymbl_header_mobile_bg').animate({opacity:0.6},0);
		$('#tymbl_header_mobile_bg').animate({opacity:0},150);
		$('#tymbl_header_mobile').animate({width:tymbl_header_mobile_width},0);
		$('#tymbl_header_mobile_btn').animate({width:tymbl_header_mobile_width},0);
		$('#tymbl_header_mobile').animate({width:0},150);
		$('#tymbl_header_mobile_btn').animate({width:0},150);
		setTimeout(
			function() 
			{
				$('#tymbl_header_mobile').hide();
				$('#tymbl_header_mobile_bg').hide();
				$('body').css({
					'position': 'relative',
					'height': 'auto',
					'top': 'auto'
				});
				$('#tymbl_header').css({
					'position': 'relative'
				});
				$('#tymbl_header_hamburger_img').attr('src', 'img/header_menu_open.svg');
				$('#tymbl_header_mobile').animate({width:tymbl_header_mobile_width},0);
				$('#tymbl_header_mobile_btn').animate({width:tymbl_header_mobile_width},0);
			}, 150);
	}
	function tymbl_show_mobile_menu() {
		tymbl_header_mobile_width = tymbl_header_mobile_width_original;
		$('#tymbl_header_mobile').show();
		$('#tymbl_header_mobile_bg').show();
		$('body').css({
			'position': 'fixed',
			'height': '100vh',
			'top': '80px'
		});
		$('#tymbl_header').css({
			'position': 'fixed'
		});
		$('#tymbl_header_hamburger_img').attr('src', 'img/header_menu_close.svg');
		$('#tymbl_header_mobile_bg').animate({opacity:0},0);
		$('#tymbl_header_mobile_bg').animate({opacity:0.6},150);
		$('#tymbl_header_mobile').animate({width:0},0);
		$('#tymbl_header_mobile_btn').animate({width:0},0);
		$('#tymbl_header_mobile').animate({width:tymbl_header_mobile_width},150);
		$('#tymbl_header_mobile_btn').animate({width:tymbl_header_mobile_width},150);
	}

	$( "#tymbl_header_mobile_link_products" ).click(function() {
		if($('#tymbl_header_mobile_products_drop_list:visible').length) {
			tymbl_hide_mobile_menu_products();
		} else {
			tymbl_show_mobile_menu_products();
		}
	});
	function tymbl_hide_mobile_menu_products() {
		tymbl_header_mobile_products_drop_list_height = $('#tymbl_header_mobile_products_drop_list').css('height');
		$('#tymbl_header_mobile_products_drop_list').animate({height:tymbl_header_mobile_products_drop_list_height},0);
		$('#tymbl_header_mobile_products_drop_list').animate({height:0},150);
		setTimeout(
			function() 
			{
				$('#tymbl_header_mobile_products_drop_list').hide();
				$('#tymbl_header_mobile_link_products_arrow').css({
					'transform': 'rotate(0deg)'
				});
				$('#tymbl_header_mobile_products_drop_list').animate({height:tymbl_header_mobile_products_drop_list_height},0);
			}, 150);	
	}
	function tymbl_show_mobile_menu_products() {
		tymbl_header_mobile_products_drop_list_height = $('#tymbl_header_mobile_products_drop_list').css('height');
		$('#tymbl_header_mobile_products_drop_list').show();
		$('#tymbl_header_mobile_link_products_arrow').css({
			'transform': 'rotate(180deg)'
		});
		$('#tymbl_header_mobile_products_drop_list').animate({height:0},0);
		$('#tymbl_header_mobile_products_drop_list').animate({height:tymbl_header_mobile_products_drop_list_height},150);
	}

/*CHECK & SET HEIGHT FOR iOS*/
	tymbl_header_mobile_set_height();
	tymbl_descriptions_set_height('lqs');
	tymbl_descriptions_set_height('sd');
	tymbl_descriptions_set_height('rm');
	
	$(window).resize(function() {
		tymbl_header_mobile_set_height();
		tymbl_descriptions_set_height('lqs');
		tymbl_descriptions_set_height('sd');
		tymbl_descriptions_set_height('rm');
	});

	function tymbl_descriptions_set_height(tymbl_descriptions_set_height_product) {
		$('#tymbl_home_descriptions_block_' + tymbl_descriptions_set_height_product).css('height', 'auto');
		tymbl_home_descriptions_block_height = $('#tymbl_home_descriptions_block_' + tymbl_descriptions_set_height_product).height();
		$('#tymbl_home_descriptions_block_' + tymbl_descriptions_set_height_product).height(tymbl_home_descriptions_block_height);
	}
	function tymbl_header_mobile_set_height() {
		/* window.innerHeight || $(window).height();	*/
		$('#tymbl_header_mobile').height($.windowHeight() - 80);
	}