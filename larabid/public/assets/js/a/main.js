var error = '';
var regEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
//$('.actions').css('bottom', '-12%');
$('#mls').hide();
var current_step = '0';


$(function(){

  $(document).on('keypress',function(e) {
    cstep = current_step;
      if(e.which == 13) {
          if($("input,textarea").is(":focus")){
            $('.actions ul li').each(function () {
              $(this).find('a').click();
              if(cstep == '0'){
                $('#broker-error').text('');
              }
            });
          }else{
            if(cstep == '1'){
              $("input,textarea").focus();
              $(this).find('a').click();
            }
          }
      }
  });

  $("#wizard").steps({
    onInit: function (event, currentIndex, newIndex){
      if (currentIndex == '0'){
        $("#imageUpload").change(function() {
          if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
              $('#imagePreview').css('background-image', 'url('+e.target.result +')');
              $('#imagePreview').hide();
              $('#imagePreview').fadeIn(650);
            }
            reader.readAsDataURL(this.files[0]);
          }

          var formData = new FormData();
          var image = $(this)[0].files[0];
          formData.append('photo', image);

          $.ajax({
              url: '/upload-image',
              type: 'POST',
              data: formData,
              cache: false,
              contentType: false,
              processData: false,
              success: function (data) {
                  if(data != ''){
                    $('#userphoto').val(data);
                  }
              },
              cache: false,
              processData: false
          });
            return false;
        });
      }
    },
    headerTag: "h2",
    bodyTag: "section",
    transitionEffect: "fade",
    enableAllSteps: true,
    transitionEffectSpeed: 0,
    labels: {
      finish: "Submit",
      next: "Continue ",
      previous: " Back"
    },
    onStepChanged: function (event, currentIndex, priorIndex) {

      current_step = currentIndex;
    },
    onStepChanging: function (event, currentIndex, newIndex)
    {
      if (currentIndex > newIndex){
        return true;
      }

      if(newIndex == '1'){
        var isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;
          if (isMobile) {
            $('.actions').css('bottom', '-3%');
          }

      }

      $('#error').html('');
      $('[id="idstate"]').click(function(){
        //alert($(this).html());
        $.ajax({
          url: '/get-city',
          type: 'POST',
          data: { state: $(this).html(), _token: '{{csrf_token()}}' },
          success: function(data) {
            //console.log(data);
            $('#city').html('');
            $('#mls').hide();
            $.each(data, function( index, value ) {
              //console.log( index + ": " + value.city_name );
              $('#city').append($("<li>").text(value.city_name).attr('rel', value.id).attr('id', "cityval"));
            });
          }
        });
      });

      if(currentIndex == 0){

        if($('#email').val() != ''){
          if(!regEmail.test($('#email').val())){
            $('#email-error').html('* email is invalid');
            $("#wizard").steps('previous');
          }else{
            $.ajax({
              url: '/check-email',
              type: 'POST',
              data: { email: $('#email').val() , _token: '{{csrf_token()}}' },
              success: function(data) {
                if(data == '1'){
                  $('#email-error').html('* email is already used');
                  current_step = '0';
                  $("#wizard").steps('previous');
                }else if(data == '2'){
                  $('#email-error').html('* email is blocked');
                  current_step = '0';
                  $("#wizard").steps('previous');
                }else{
                  $('#email-error').html('');
                }
              }
            });
          }
        }
      }
        $('#fname-error').html('');
        $('#email-error').html('');
        $('#lname-error').html('');
        $('#password-error').html('');
        $('#zip-error').html('');
        $('#pc-error').html('');
        $('#error').html('');
        var validnum = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/;

        if($('#first_name').val() == ''){
          $('#fname-error').html('*first name is required');
          errors = '1';
          return false;
        }if($('#last_name').val() == ''){
          $('#lname-error').html('*last name is required');
          errors = '1';
          return false;
        }else if($('#email').val() == ''){
          $('#email-error').html('* email is required');
          errors = '1';
          return false;
        }else if(!regEmail.test($('#email').val())){
          $('#email-error').html('*email is invalid');
          errors = '1';
          return false;
        }else if($('#password').val() == ''){
          $('#password-error').html('*password is required');
          errors = '1';
          return false;
        }else if($('#phone').val() == ''){
          $('#phone-error').html('*phone is required');
          errors = '1';
          return false;
        }else if(!$('#phone').val().match(validnum)){
          $('#phone-error').html('*enter valid mobile number ex.9999999999');
          errors = '1';
          return false;
        }else if(!$('[name="authorized-to-sign"]').is(":checked")){
          alert('Sorry, you may not register, unless you obtain an authorization to sign referral agreements from your broker');
          errors = '1';
          return false;
        }else{
          errors = '';
          return true;
        }
    },
  });

  $('.wizard > .steps li a').click(function(){
    if(errors == ''){
      $(this).parent().addClass('checked');
      $(this).parent().prevAll().addClass('checked');
      $(this).parent().nextAll().removeClass('checked');
    }

  });
  // Custome Jquery Step Button
  $('.forward').click(function(){
    $("#wizard").steps('next');
  })
  $('.backward').click(function(){
    $("#wizard").steps('previous');
  })
  // Select Dropdown
  $('html').click(function() {
    $('[class="select"] .dropdown').hide();

  });
  $('[class="select"]').click(function(event){
    event.stopPropagation();
  });
  $('[class="select"] .select-control').click(function(){
    $(this).parent().next().toggle();
  })
  $('[class="select"] .dropdown li').click(function(){
    $(this).parent().toggle();
    //alert($(this).text());
    var text = $(this).text();
    $(this).parent().prev().find('div').text(text);
    $('#state_name').val(text);
    var selected_state = text;
    $.each(mls, function (key, i) {
      if(key == selected_state){
        //console.log(key);
        $('#mls li').remove();
        $.each(i, function (key2, val) {
          //console.log(val.id);
          $('#mls').append('<li id="mlsid" rel="'+val.id+'">'+val.name+'</li>');
        });
        $('#mls').append('<li id="mlsid" rel="other">Other MLS</li>');
      }
    });

  })

  $('ul').on('click', '#mlsid', function(){
    $('.select .dropdown').hide();
    var text = $(this).text();
    $(this).parent().prev().find('div').text(text);
    $('#mls_value').val($(this).attr('rel'));
    mls_id = $(this).attr('rel');
    if(text == 'Other MLS'){
      $('#other_mls').show();
      $('#other_mls').focus();
    }
    $(this).hide();
  });

  $('.select').on("click","#cityval",function(){
    $(this).parent().toggle();
    $('#city-value').html($(this).text());
    $('#city-select').val($(this).attr('rel'));
  });

  $(document).on('input change', '#slider', function() {
    $('#slider_value').html( $(this).val() + " MILES");
    $('.custom-pill').remove();
    pills_ar = [];
  });

  var pills_ar = new Array();
  var pills_ar2 = new Array();
  $(document).on('click', '.zip-item', function(){

    var zip_val = $(this).text();
    var num_pill = $('div.custom-pill').length;

    $('#zip_input').val('');
    $('#ul-zips').hide();

    if(num_pill < 10){
      if(pills_ar.indexOf(zip_val) === -1){
        pills_ar.push(zip_val);
        //console.log(zip_val + ' added');
        $('#pill').append('<div class="custom-pill">'+zip_val+' <span title="remove item" id="remove">x</span></div>');
      }else{
        alert("Zip code already added");
      }
    }else{
      alert('Up to 10 zip codes only')
    }
  });

  $(document).on('click', '.zip_item', function(){
    var zip_val = $(this).text();
    $('#ul-zip').hide();
    $('#zip').val(zip_val);
  });

  $(document).on('click', '#remove', function(){
    var item_remove = $(this).parent().text().replace(' x', '');
    if (confirm('Remove ' + item_remove + '?')) {
      $(this).parent().remove();
      pills_ar.splice(pills_ar.indexOf(item_remove),1);
    }
  });

  $('#lead_notify').click(function(){

    var isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;

      if (isMobile) {
        $('.actions').css('bottom', '-43%');
        $('.or-mobile').show();
        $('.or-ds').hide();
      }else{
        $('.actions').css({'bottom':'-6%'});
        $('body').css({'height':'100vh'});
        $('.or-mobile').hide();
        $('.or-ds').show();
      }

    if ($(this).is(':checked')) {
      $('.range-slider').show();
      $('.ui-widget-zip').show();

    }else{
      $('.range-slider').hide();
      $('.ui-widget-zip').hide();

    }
  });

  $('#zip_input').on('keyup', function() {
    $('#slider').val('0');
    $('#slider_value').text('AREA: 0 MILES');
    $('#ul-zips').show();
    var country = $('#ctr').attr('rel');
    var textsearch = $(this).val();
    $('#ul-zips').html('');
    $.ajax({
      url: '/searchzip',
      type:"GET",
      data:{'zip':textsearch, 'country':country},
      success:function (data) {
        $('#ul-zips').html(data);
      }
    })
  });

  $('#zip').on('keyup', function() {
    $('#ul-zip').show();

    var textsearch = $(this).val();
    var state = $('#state_name').val();
    var country = $('#ctr').attr('rel');
    var city = $('#city-value').text();
    $('#ul-zip').html('');
    $.ajax({
      url: '/searchzip_single',
      type:"GET",
      data:{'zip':textsearch, 'state':state, 'country': country, 'city':city},
      success:function (data) {
        $('#ul-zip').html(data);
      }
    })
  });

});
