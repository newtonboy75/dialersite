<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationSearch extends Model
{
    protected $table = 'notification_search';
    protected $fillable = ['user_id', 'terms'];
}
