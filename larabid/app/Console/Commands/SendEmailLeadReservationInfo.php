<?php

namespace App\Console\Commands;

use App\Ad;
use App\User;
use Carbon\Carbon;
use App\ListingContracts;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class SendEmailLeadReservationInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendMail:LeadReservationInfo';
    protected $subject = '';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Mail Reserved Lead Information';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reserved_leads = ListingContracts::with('buyer', 'prospect_contact_status')
            ->where([
                ['list_status', '=', ListingContracts::LS_ACTIVE],
                ['follow_up', '=', 1]
            ])
            ->whereHas('prospect_contact_status', function ($query) {
                $query->whereDate('created_at', '=', Carbon::yesterday()->toDateString())
                    ->orWhereDate('created_at', '=', Carbon::now()->subDays(7)->toDateString());
            })
            ->get();

        //Log::info('this'.Carbon::now()->toDateString().'==='.Carbon::yesterday()->toDateString());
        //Log::info($reserved_leads);
        //Log::info('...');

        if ($reserved_leads) {
            foreach ($reserved_leads as $reserved_lead) {
                if (!isset($reserved_lead->prospect_contact_status->current_prospect_contact_status)) {
                    $this->processContactInfo($reserved_lead);
                }
            }
        }
    }

    //check new signed contract and send buyer email
    public function processContactInfo($reserved_lead)
    {
        $ad = Ad::find($reserved_lead->listing_id);
        $days_date_reserved = Carbon::parse(Carbon::now())->diffInDays($reserved_lead->created_at);

        if ($days_date_reserved === '2') {
            $this->subject = 'Referral ' . $reserved_lead->listing_id . ' First Contact';
            $email_template = 'emails.lead_info_after_signing_agreement';

        } else {
            $this->subject = 'Referral ' . $reserved_lead->listing_id . ' needs a status update';
            $email_template = 'emails.lead_info_after_update_status';
        }

        Mail::send($email_template, ['oAd' => $reserved_lead, 'ad' => $ad], function ($m) use ($reserved_lead) {
            $m->from('info@tymbl.com', 'Tymbl Team');
            $m->to($reserved_lead->buyer->email)->subject($this->subject);
        });
    }
}
