<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanContract extends Model
{
    protected $table = 'plan_contract';
    protected $fillable = ['user_id', 'contract_id', 'status', 'file_name', 'plan_type'];
}
