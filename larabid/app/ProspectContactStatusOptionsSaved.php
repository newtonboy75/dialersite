<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProspectContactStatusOptionsSaved extends Model
{
    protected $table = 'prospect_contact_status_options_saved';

    protected $fillable = ['prospect_contact_status_options_id', 'option_value', 'prospect_contact_status_id'];

    public $timestamps = false;
}
