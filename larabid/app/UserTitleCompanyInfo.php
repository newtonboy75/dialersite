<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTitleCompanyInfo extends Model
{
  protected $fillable = ['user_id', 'company_name', 'representative_name', 'representative_email'];

  public function user()
   {
       return $this->belongsTo(App\User);
   }
}
