<?php

namespace App\Services\Payments;

use App\Contract;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

/**
 * Class Docusign
 *
 * @package \App\Services\Payments
 */
class DocusignPayment implements PaymentInterface
{
    
    /**
     * @param $buyer
     * @param $seller
     * @return \Illuminate\Http\RedirectResponse
     */
    public function send($buyer, $seller)
    {
        //envelopes?from_date=3%2F1%2F2014&status=sent,delivered,completed
        //accounts/:accountId/envelopes?status=delivered,signed,completed,declined,processing&envelope_ids=request_body
        //GET /accounts/{accountId}/envelopes/status?envelope_ids=abc,def
        $email = config('larabid.email'); //"86717902-0f70-4bfe-9893-8a78a9407483"; //26950388
        $password = config('larabid.password');
        $integratorKey = config('larabid.integratorKey');
        $templateId    = config('larabid.templateId');//"c408c013-0178-48fd-96c1-9a53a201b0c2";//"c408c013-0178-48fd-96c1-9a53a201b0c2";//"ed2ceb75-78df-44ae-b185-8e7efdf61e9d";
        $firstRoleName = config('larabid.firstTemplateRole');
        $secondRoleName = config('larabid.secondTemplateRole');
    
        //        $email         = "muhamed.didovic@gmail.com";
        //        $password      = "sanica000";
        //        $integratorKey = "c5462989-febe-44f7-b6b7-c8fcdbcb0baa";
        //        $templateId    = "32db87a4-ca82-4156-8474-ca3812cb6068";//"8d06fd1d-c86b-4d1b-bd29-addb06bf2823";//"46e00d68-4ebf-4961-80c4-b93dafb74cfc";
    
        //$templateRoleName = config('larabid.firstTemplateRole');
        $clientUserId     = $buyer->id;
        $recipientName    = $buyer->name;//"Timour as buyer";
        $recipientEmail   = $buyer->email;//"timourkh@gmail.com";
    
        // construct the authentication header:
        $header = "<DocuSignCredentials><Username>" . $email . "</Username><Password>" . $password . "</Password><IntegratorKey>" . $integratorKey . "</IntegratorKey></DocuSignCredentials>";
    
        /////////////////////////////////////////////////////////////////////////////////////////////////
        // STEP 1 - Login (retrieves baseUrl and accountId)
        /////////////////////////////////////////////////////////////////////////////////////////////////
        $url  = config('larabid.url');
        //$url  = "https://docusign.com/restapi/v2/login_information/";
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("X-DocuSign-Authentication: $header"));
    
        $json_response = curl_exec($curl);
        $status        = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    
        if ($status != 200) {
            //echo "1error calling webservice, status is:" . $status;
            echo "1. error calling webservice, status is:" . $status . "\nerror text is --> ";
            print_r($json_response);
            echo "\n";
            exit(-1);
        }
    
        $response  = json_decode($json_response, true);
        $accountId = $response["loginAccounts"][0]["accountId"];
        $baseUrl   = $response["loginAccounts"][0]["baseUrl"];
        curl_close($curl);
    
        //--- display results
        echo "accountId = $accountId \n baseUrl $baseUrl \n";
        
        /////////////////////////////////////////////////////////////////////////////////////////////////
        // STEP 1.1 - Getting a Template
        /////////////////////////////////////////////////////////////////////////////////////////////////
        //        curl -i -H 'X-DocuSign-Authentication:
        //              { "Username": "developer@example.com",
        //                "Password": "example",
        //                "IntegratorKey":"00000000-aaaa-bbbb-cccc-0123456789b" }'\
        //        https://demo.docusign.net/restapi/v2/accounts/9999999/templates/00000000-aaaa-bbbb-cccc-0123456789c
    
    
        //        $curl        = curl_init($baseUrl . "/templates/" . $templateId);
        //        curl_setopt($curl, CURLOPT_HEADER, false);
        //        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //        curl_setopt($curl, CURLOPT_HTTPHEADER, array("X-DocuSign-Authentication: $header"));
        //        $json_response = curl_exec($curl);
        //        $status        = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        //        if ($status != 200) {
        //            echo "1.1error calling webservice, status is:" . $status . "\nerror text is --> ";
        //            print_r($json_response);
        //            echo "\n";
        //            exit(-1);
        //        }
        //
        //        $response  = json_decode($json_response, true);
    
        /////////////////////////////////////////////////////////////////////////////////////////////////
        // STEP 2 - Create an envelope with an Embedded recipient (uses the clientUserId property)
        /////////////////////////////////////////////////////////////////////////////////////////////////
        $data = [
            "status"       => 'sent',
            "accountId"    => $accountId,
            "emailSubject" => config('larabid.emailSubject'),
            "templateId"   => $templateId,
        
            "templateRoles" => [
                [
                    "roleName"     => $firstRoleName,
                    "email"        => $buyer->email,
                    "name"         => $buyer->name,
                    "clientUserId" => $clientUserId,
                
                    "signers" => [
                        [
                            "name"         => $buyer->name,
                            "email"        => $buyer->email,
                            "recipientId"  => $clientUserId,
                            "clientUserId" => $clientUserId,
                            "routingOrder" => "1",
                            "roleName"     => $firstRoleName,
                            "tabs"         => [
                                "textTabs" => [
                                    //                                    [
                                    //                                        "xPosition" => "77",
                                    //                                        "yPosition" => "402",
                                    //                                        "name"      => "First Name",
                                    //                                        "tabLabel"  => "first",
                                    //                                        "value"     => "Cchirag",
                                    //                                    ],
                                    //                                    [
                                    //                                        "xPosition" => "156",
                                    //                                        "yPosition" => "402",
                                    //                                        "name"      => "Last Name",
                                    //                                        "tabLabel"  => "last",
                                    //                                        "value"     => "Netmaxims",
                                    //                                    ],
                                    //                                    [
                                    //                                        "xPosition" => "235",
                                    //                                        "yPosition" => "402",
                                    //                                        "name"      => "Title",
                                    //                                        "tabLabel"  => "title",
                                    //                                        "value"     => "Default title value",
                                    //                                    ],
                                
                                    //                                    [
                                    //                                        //                                        "xPosition" => "235",
                                    //                                        //                                        "yPosition" => "402",
                                    //                                        "name"     => "text1",
                                    //                                        "tabLabel" => "text1",
                                    //                                        "value"    => "text1",
                                    //                                    ],
                            
                                ],
                            ],
                        ],
                
                    ],
                ],
                [
                    "roleName" => $secondRoleName,
                    "name"     => $seller->name,
                    "email"    => $seller->email,
                    //"clientUserId" => $seller->id,
                    "signers" => [
                        [
                            "name"  => $seller->name,//Timour Khamnayev <timourkh@gmail.com>
                            "email" => $seller->email,
                            "recipientId"  => $seller->id,
                            "clientUserId" => $seller->id,//rand(1, 1000),
                            "routingOrder" => "2",
                            "roleName"     => $secondRoleName,
                            //                            "tabs"         => [
                            //                                "textTabs" => [
                            //                                    [
                            //                                        "xPosition" => "77",
                            //                                        "yPosition" => "402",
                            //                                        "name"      => "First Name",
                            //                                        "tabLabel"  => "first",
                            //                                        "value"     => "Cchirag",
                            //                                    ],
                            //                                    [
                            //                                        "xPosition" => "156",
                            //                                        "yPosition" => "402",
                            //                                        "name"      => "Last Name",
                            //                                        "tabLabel"  => "last",
                            //                                        "value"     => "Netmaxims",
                            //                                    ],
                            //                                    [
                            //                                        "xPosition" => "235",
                            //                                        "yPosition" => "402",
                            //                                        "name"      => "Title",
                            //                                        "tabLabel"  => "title",
                            //                                        "value"     => "Default title value",
                            //                                    ],
                            //
                            //                                    [
                            //                                        //                                        "xPosition" => "235",
                            //                                        //                                        "yPosition" => "402",
                            //                                        "name"     => "text1",
                            //                                        "tabLabel" => "text1",
                            //                                        "value"    => "text1",
                            //                                    ],
                            //
                            //                                ],
                            //                            ],
                        ],
                
                    ],
                ],
            ],
        ];
    
        $data_string = json_encode($data);
        $curl        = curl_init($baseUrl . "/envelopes");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt(
            $curl,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string),
                "X-DocuSign-Authentication: $header",
            )
        );
    
        $json_response = curl_exec($curl);
        $status        = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($status != 201) {
            echo "2. error calling webservice, status is:" . $status . "\nerror text is --> ";
            print_r($json_response);
            echo "\n";
            exit(-1);
        }
    
        $response   = json_decode($json_response, true);
        $envelopeId = $response["envelopeId"];
        curl_close($curl);
        
        //Save contract
        Contract::create([
            'buyer_id' => $buyer->id,
            'seller_id' => $seller->id,
            'envelope_id' => $envelopeId,
            'status' => 2
        ]);
    
        //--- display results
        echo "Envelope created! Envelope ID: " . $envelopeId . "\n";

        /////////////////////////////////////////////////////////////////////////////////////////////////
        // STEP 3 - Get the Embedded Singing View
        /////////////////////////////////////////////////////////////////////////////////////////////////
        $data = array(
            "returnUrl"            => config('larabid.returnUrl'),
            "authenticationMethod" => "None",
            "email"                => $recipientEmail,
            "userName"             => $recipientName,
            //"role"                 => "buyers",
            "clientUserId"         => $clientUserId,
        );
    
        $data_string = json_encode($data);
        $curl        = curl_init($baseUrl . "/envelopes/$envelopeId/views/recipient");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt(
            $curl,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string),
                "X-DocuSign-Authentication: $header",
            )
        );
    
        $json_response = curl_exec($curl);
        $status        = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($status != 201) {
            echo "3. error calling webservice, status is:" . $status . "\nerror text is --> ";
            print_r($json_response);
            echo "\n";
            exit(-1);
        }
    
        $response = json_decode($json_response, true);
        $url      = $response["url"];
    
        //--- display results
        echo "Embedded URL is: \n\n" . $url . "\n\nNavigate to this URL to start the embedded signing view of the envelope\n";
        //header('Location: ' . $url);
        
        DB::commit();
        //redirect to Docusign
        return redirect()->to($url)->send();
    }
}