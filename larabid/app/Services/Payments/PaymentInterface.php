<?php

namespace App\Services\Payments;

interface PaymentInterface
{
    /**
     * @param $buyer
     * @param $seller
     * @return mixed
     */
    public function send($buyer, $seller);
}