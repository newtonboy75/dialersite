<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProspectContactStatusOptions extends Model
{
    const OT_TEXT       = 'text';               //  Option input type <input type='text'/>
    const OT_DATA       = 'data';               //  Option input type <input type='date'/>
    const OT_SELECT     = 'select';             //  Option input type <select><option></option></select>
    const OT_CHECKBOX   = 'checkbox';           //  Option input type <input type='checkbox'/>
    const OT_RADIO      = 'radio';              //  Option input type <input type='radio'/>

    //Option Type List
    const OT_LIST = [
        self::OT_TEXT,
        self::OT_SELECT,
        self::OT_DATA,
        self::OT_CHECKBOX,
        self::OT_RADIO
    ];

    //Description of option types
    const OT_DESCRIPTION = [
        self::OT_TEXT       => 'Text input type',
        self::OT_SELECT     => 'Select input type',
        self::OT_DATA       => 'Date input type',
        self::OT_CHECKBOX   => 'Checkbox input type',
        self::OT_RADIO      => 'Radio input type'
    ];

    protected $table = 'prospect_contact_status_options';

    protected $fillable = ['option_name', 'option_type', 'prospect_contact_status_name'];
}
