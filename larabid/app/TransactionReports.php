<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionReports extends Model
{
    protected $table = 'transaction_reports';
    protected $fillable = ['ad_id', 'status', 'created_at', 'updated_at'];
}
