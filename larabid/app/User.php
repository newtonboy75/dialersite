<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function uploadedLeads()
    {
        //return $this->hasMany(UploadedLead::class);
        return  $this->hasOne('App\PlanContract', 'user_id');
    }

    /**
     * @param int $s
     * @param string $d
     * @param string $r
     * @param bool $img
     * @param array $atts
     * @return string
     */
    public function get_gravatar($s = 40, $d = 'mm', $r = 'g', $img = false, $atts = [])
    {
        $parse_url = parse_url($this->photo);
        //$url = '';
        $email = $this->email;

        if ( !empty($parse_url['scheme'])) {
            $url = $this->photo;
        } else {
            $url = '//www.gravatar.com/avatar/';
            $url .= md5(strtolower(trim($email)));
            $url .= "?s=$s&d=$d&r=$r";

            if (!empty($this->photo)) {
                $url = avatar_img_url($this->photo, $this->photo_storage);
            }

            if ($img) {
                $url = '<img src="' . $url . '"';
                foreach ($atts as $key => $val)
                    $url .= ' ' . $key . '="' . $val . '"';
                $url .= ' />';
            }
        }
        return $url;
    }

    public function get_address()
    {
        $address = '';
        if ($this->country) {
            $address .= $this->country->country_name.', ';
        }
        if (!empty($this->address)) {
            $address .= $this->address;
        }
        return $address;
    }

    public function ads()
    {
        return $this->hasMany(Ad::class);
    }

    public function favourite_ads()
    {
        return $this->belongsToMany(Ad::class, 'favorites');
    }

    public function contracts()
    {
        return $this->hasMany(Contract::class);
    }

    public function signed_up_datetime()
    {
        $created_date_time = false;
        if ($this->created_at) {
            $created_date_time = $this->created_at->timezone(get_option('default_timezone'))
                ->format(get_option('date_format_custom') . ' ' . get_option('time_format_custom'));
        }
        return $created_date_time;
    }

    public function status_context()
    {
        $status = $this->active_status;
        switch ($status) {
            case '0':
                $context = 'Pending';
                break;
            case '1':
                $context = 'Active';
                break;
            case '2':
                $context = 'Block';
                break;
            default:
                $context = '';
                break;
        }
        return $context;
    }

    public function is_admin()
    {
        return $this->user_type == 'admin';
    }

    public function user_title_company_infos()
    {
        return  $this->hasOne('App\UserTitleCompanyInfo', 'user_id');
    }

    public function broker()
    {
        return $this->hasOne(Broker::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function mls()
    {
        return $this->belongsTo(Mls::class);
    }

    public function adsStatusOne()
    {
        return $this->hasMany(Ad::class);
    }
}
