<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadedLeadSms extends Model
{
    const S_QUEUED = 'queued';
    const S_FAILED = 'failed';
    const S_SENT = 'sent';
    const S_DELIVERED = 'delivered';
    const S_UNDELIVERED = 'undelivered';
    /**
     * The table associated with the model.
     * @var string */
    protected $table = 'uploaded_lead_sms';

    /**
     * Attributes that can be mass distributed.
     * @var array */
    protected $fillable = ['fullname', 'body', 'from', 'to', 'sid', 'status', 'uploaded_lead_id'];

    /**
     * Attributes omitted in model serialization.
     * @var array */
    protected $hidden = ['id'];
}
