<?php

namespace App\Http\Controllers;

use App\Ad;
use App\LeadNote;
use Auth;
use Illuminate\Http\Request;
use App\Jobs\SendNotesToAdmins;

class NoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notes = LeadNote::all();

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function store(Request $request)
    {

        $user = Auth::user();
        $ad = Ad::find($request->ad_id);
        $data = [
            'ad_id' => $ad->id,
            'author' => $user->id,
            'lead_note_body' => $request->lead_note_body,
        ];

        $note = LeadNote::create($data);

        // $request->session()->flash('note_add_message', 'Noted Added');

        SendNotesToAdmins::dispatch($request->lead_note_body, $ad->title, $ad->id)->delay(1);

        return redirect()->back()->with('success', 'Note Added');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        LeadNote::findOrfail($request->id)->delete();
        return redirect()->back()->with('success', 'Note Deleted');
    }
}
