<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use LaravelEsignatureWrapper;
use App\Ad;
use App\User;
use App\Broker;
use Illuminate\Support\Facades\Auth;
use App\ReferralContactInfo;
use App\ListingContracts;

class ReferralAgreementController extends Controller
{
  //to be used in regular lead where status = 1
  public function createContractRegular($id){

    $request_data = Ad::where('id', $id)->first();
    $seller = User::where('id', '=', $request_data->user_id)->first();
    $broker = Broker::where('user_id', '=',  $request_data->user_id)->first();

    $broker_name = '';
    if($broker){
      $broker_name = $broker->name;
    }

    if(!Auth::check()){
      return redirect('login')->with('message', 'Please login to continue.');
    }

    $buyer = Auth::user();
    $broker = Broker::where('user_id', '=', $buyer->id)->first();
    $seller_name = $request_data['seller_name'];
    $seller_email = $request_data['seller_email'];
    $seller_address = $request_data['address'];
    $seller_phone = $request_data['seller_phone'];
    $seller_fax = $request_data['seller_phone'];
    $ref_fee_percent = (float)($request_data['referral_fee']*100)-10;
    $listing_type = $request_data['category_type']  == 'selling' ? 'Seller' : 'Buyer';
    $referral = ReferralContactInfo::where('ad_id', $request_data->user_id)->first();
    $chbox1 = $request_data['category_type'] == 'selling' ? 'x' : '☐';
    $chbox2 = $request_data['category_type'] == 'buying' ? 'x' : '☐';

    $whitelist = array("127.0.0.1", "::1", "34.216.20.30", "localhost");
    $locs = new LocationController();
    $client_ip = $locs->get_client_ip();

    $template_id_use = config('app.esig')['template_id'];

    $data = ["template_id" => $template_id_use,
    "signers" => array (
      //pavel ***change email address
      ["name" => "Pavel Stepanov", "email" =>  "info@tymbl.com", "auto_sign"=>  "yes", "skip_signature_request_email"=>  "yes"],
      //buyer
      ["name" => $buyer->name, "email" =>   $buyer->email, "auto_sign"=>  "no", "skip_signature_request_email"=>  "yes",
      //"redirect_url" => url('referral-thankyou')
      ],
      //seller
      ["name" => $seller_name, "email" =>  $seller->email, "auto_sign"=>  "yes", "skip_signature_request_email"=>  "yes",],
    ),
    "custom_fields" => array(
      ["api_key" => "date", "value" => date("Y-m-d")],
      ["api_key" => "ref_fee_percentage", "value" => $ref_fee_percent],
      ["api_key" => "txt_referring_firm", "value" => $seller->name],
      ["api_key" => "txt_destination_firm", "value" => $buyer->name],
      ["api_key" => "referral_fee", "value" => $listing_type],
      ["api_key" => "seller_name", "value" => $broker_name],
      ["api_key" => "seller_email","value" => $seller->email,],
      ["api_key" => "seller_address", "value" => $seller->address],
      ["api_key" => "seller_phone", "value" => $seller->phone],
      ["api_key" => "seller_fax", "value" => $request_data['seller_phone']],
      ["api_key" => "buyer_name", "value" => $broker->name],
      ["api_key" => "buyer_address", "value" => $buyer->address],
      ["api_key" => "buyer_phone", "value" => $buyer->phone],
      ["api_key" => "buyer_fax", "value" => $buyer->phone],
      ["api_key" => "buyer_email", "value" => $buyer->email],
    ),

    "locale" => "en",
    "embedded" => "yes",
    "test" => "XXXxxxXXXxxx",
    "status" => 'queued'
  ];

  $contract = LaravelEsignatureWrapper::createContract($data);

  $code = $this->contactCode();

  if($contract){
    $created_contract['contract_id'] = $contract['data']['contract_id'];
    $created_contract['listing_id'] = $request_data['id'];
    $created_contract['user_contract_id'] = $contract['data']['contract']['signers'][1]['id'];
    $listing = ListingContracts::firstOrNew(array('listing_id' => $id));
    $listing->contract_id = $contract['data']['contract_id'];
    $listing->user_contract_id = $contract['data']['contract']['signers'][1]['id'];
    $listing->code_id = $code;
    $listing->list_status = '0';
    $listing->buyer_id = Auth::user()->id;
    $listing->save();
  }
}

public function contactCode(){
  $code = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"), 0, 6);
  $contracts = ListingContracts::where('code_id', $code)->first();
  if($contracts){
    $this->contactCode();
  }else{
    return $code;
  }
}

//free vip where status = 7
public function createContractFreeVip($id){

  $request_data = Ad::where('id', $id)->first();
  $ref_fee_percentage = (float)$request_data['referral_fee'];
  $user = Auth::user();
  $broker_name = '';
  $buyer_name = $user->name;

  $broker = Broker::where('user_id', '=', $user->id)->first();


  if($broker){
    $broker_name = $broker->name;
  }

  if(!Auth::check()){
    return redirect('login')->with('message', 'Please login to continue.');
  }

  $template_id_use = config('app.esig')['vip_template_id'];

  $data = ["template_id" => $template_id_use,
  "signers" => array (
    //pavel ***change email address
    ["name" => "Pavel Stepanov", "email" =>  "info@tymbl.com", "auto_sign"=>  "yes", "skip_signature_request_email"=>  "yes"],
    //buyer
    ["name" => $user->name, "email" =>   $user->email, "auto_sign"=>  "no", "skip_signature_request_email"=>  "yes",
    //"redirect_url" => url('referral-thankyou')
    ],

  ),
  "custom_fields" => array(
    ["api_key" => "date", "value" => date("m-d-Y")],
    ["api_key" => "txt_destination_firm  ", "value" => $broker_name],
    ["api_key" => "buyer_name", "value" => $buyer_name],
    ["api_key" => "buyer_address", "value" => $user->address],
    ["api_key" => "buyer_phone", "value" => $user->phone],
    ["api_key" => "buyer_fax", "value" => $user->fax],
    ["api_key" => "buyer_email", "value" => $user->email],
    ["api_key" => "ref_fee_percentage", "value" =>  (float)($ref_fee_percentage*100)],
  ),

  "locale" => "en",
  "embedded" => "yes",
  "test" => "XXXxxxXXXxxx",
  "status" => 'queued'
];

$contract = LaravelEsignatureWrapper::createContract($data);

$code = $this->contactCode();

if($contract){
  $created_contract['contract_id'] = $contract['data']['contract_id'];
  $created_contract['listing_id'] = $request_data['id'];
  $created_contract['user_contract_id'] = $contract['data']['contract']['signers'][1]['id'];
  $listing = ListingContracts::firstOrNew(array('listing_id' => $id));
  $listing->contract_id = $contract['data']['contract_id'];
  $listing->user_contract_id = $contract['data']['contract']['signers'][1]['id'];
  $listing->code_id = $code;
  $listing->list_status = '0';
  $listing->buyer_id = Auth::user()->id;
  $listing->save();
}

}

//sign referral FREE UPLOAD
public function signContractFreeUpload(){
  $title = 'Sign Referral';

  if(!Auth::check()){
    return redirect('login')->with('error', 'Please login to continue.');
  }

  $date = date('m-d-Y');
  $user = User::whereId(Auth::user()->id)->first();
  $user_name = $user->first_name.' '.$user->last_name;
  $user_address = $user->address;
  $broker = Broker::where('user_id', '=', Auth::user()->id)->first();

  if(!$broker){
    return redirect()->route('profile_edit')->with('error', 'Broker is required to sign the Engagement Agreement');
    exit;
  }elseif($broker->name == ''){
    return redirect()->route('profile_edit')->with('error', 'Broker is required to sign the Engagement Agreement');
    exit;
  }elseif($user->last_name == '' || $user->first_name == ''){
    return redirect()->route('profile_edit')->with('error', 'First name and Last name are required to sign the Engagement Agreement');
    exit;
  }elseif($user_address == ''){
    return redirect()->route('profile_edit')->with('error', 'Address is required to sign the Engagement Agreement');
    exit;
  }

  $broker_name = $broker->name;
  $broker_address = $broker->address;
  $plan_1 = request()->option == '1' ? '☑' : '☐';
  $plan_2 = request()->option == '2' ? '☑' : '☐';
  $plan_3 = request()->option == '3' ? '☑' : '☐';
  $option_type = '';

  switch (request()->option) {
    case "1":
    $option_type = 'Option 1';
    break;
    case "2":
    $option_type = 'Option 2';
    break;
    case "3":
    $option_type = 'Option 3';
    break;
    default:
    $option_type = 'Option 1';
  }



  $template_id_use = config('app.esig')['plan_template_id'];

  $data = ["template_id" => $template_id_use,
  "signers" => array (
    //pavel ***change email address
    ["name" => "Pavel Stepanov", "email" =>  "info@tymbl.com", "auto_sign"=>  "yes", "skip_signature_request_email"=>  "yes"],
    //buyer
    ["name" => $user_name, "email" => $user->email, "auto_sign"=>  "no", "skip_signature_request_email"=>  "yes",
    //"redirect_url" => url('referral-thankyou')
    ],
  ),
  "custom_fields" => array(
    ["api_key" => "date", "value" => date("Y-m-d")],
    ["api_key" => "buyer_name", "value" => $user_name],
    ["api_key" => "txt_destination_firm", "value" => $broker_name],
    ["api_key" => "buyer_address", "value" => $user_address],
    ["api_key" => "plan_1", "value" => $plan_1],
    ["api_key" => "plan_2", "value" => $plan_2],
    ["api_key" => "plan_3", "value" => $plan_3],
    ["api_key" => "option_type", "value" => $option_type],
  ),

  "locale" => "en",
  "embedded" => "yes",
  "test" => "XXXxxxXXXxxx",
  "status" => 'queued'
];

$contract = LaravelEsignatureWrapper::createContract($data);
$contract_id = $contract['data']['contract_id'];
$url = LaravelEsignatureWrapper::getContract($contract_id);
$contract_url = $url['data']['signers'][1]['embedded_url'];
return view('tymbl.esignature_plan', compact('title', 'contract_url', 'contract_id', 'user'));
}


//end here
}
