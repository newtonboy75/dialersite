<?php

namespace App\Http\Controllers;

use App\Ad;
use App\ContactQuery;
use App\Payment;
use App\ProspectContactStatus;
use App\ProspectContactStatusOptions;
use App\ProspectContactStatusOptionsSaved;
use App\Report_ad;
use App\UploadedLeadSms;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\City;
use App\Comment;
use App\Country;
use App\State;
use App\Notification;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Log;
use Twilio\Jwt\ClientToken;
use Twilio\Rest\Client;
use Twilio\Twiml;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use ZipArchive;
use App\UserTitleCompanyInfo;
use App\Media;
use App\ReferralContactInfo;
use App\Category;
use Illuminate\Filesystem\Filesystem;
use Session;
use App\NotificationsUsers;
use App\ListingContracts;
use App\TransactionReports;
use App\TitleCompanyInfoLTB;
use LaravelEsignatureWrapper;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\Broker;
use App\Jobs\SendContractToBroker;
use AWS;
use App\PlanContract;
use App\Jobs\SendVipSuccessEmailJob;
use App\UploadedLead;
use App\PopupInquiry;

class DashboardController extends Controller
{
  public function dashboard(){
    $user = Auth::user();
    $user_id = $user->id;
    $user_notification_optin = $user->notification_optin;

    $total_users = 0;
    $total_reports = 0;
    $total_payments = 0;
    $ten_contact_messages = 0;
    $reports = 0;
    $total_payments_amount = 0;
    $comments = 0;
    $notif = $this->getUserNotifications($user->id, '10');
    $notifications_users = $notif[0];
    $user_notification_count = $notif[1];
    $comment_counts = '0';
    $num_reserved_leads = '0';
    $num_listed_leads = '0';
    $total_users_all = '';

    if ($user->is_admin()){
      $approved_ads = Ad::whereStatus('1')->count();
      //$pending_ads = Ad::whereStatus('0')->count();
      $blocked_ads = Ad::whereStatus('3')->count();

      $total_users = User::where('active_status', '=', '1')->count();
      $total_users_all = User::count();
      $total_reports = Report_ad::count();
      $total_payments = Payment::whereStatus('success')->count();
      $total_payments_amount = Payment::whereStatus('success')->sum('amount');
      $ten_contact_messages = ContactQuery::take(10)->orderBy('id', 'desc')->get();
      $reports = Report_ad::orderBy('id', 'desc')->with('ad')->take(10)->get();

    }else{
      $approved_ads = Ad::whereStatus('1')->whereUserId($user_id)->count();
      $pending_ads = Ad::whereStatus('0')->whereUserId($user_id)->count();
      $blocked_ads = Ad::whereStatus('3')->whereUserId($user_id)->count();
      $comments = Comment::whereStatus('0')->whereUserId($user_id)->take(5)->orderBy('id', 'desc')->get();
      $comment_counts = Comment::whereStatus('0')->whereUserId($user_id)->count();
      //$notifications_users = NotificationsUsers::where('recipient', '=', $user_id)->limit(10)->get();

      //$user_notification_count = NotificationsUsers::where('recipient', '=', $user_id)->count();

      $num_reserved_leads = DB::table('ads')->join('listing_contracts', 'ads.id', '=', 'listing_contracts.listing_id')->select('ads.*', 'listing_contracts.*')->where('listing_contracts.buyer_id', $user_id)->where('listing_contracts.list_status', '!=', '3')->orderBy('ads.id', 'desc')->get();

      $num_listed_leads = DB::table('ads')->join('listing_contracts', 'ads.id', '=', 'listing_contracts.listing_id')->select('ads.*', 'listing_contracts.*')->where('ads.user_id', $user_id)->where('listing_contracts.list_status', '!=', '3')->orderBy('ads.id', 'desc')->get();

      //dd($notifications_users);
    }

    //start new registration remind me pop up

    $countries  = Country::all();

    if(old('country') == ''){
      $previous_states = State::where('country_id', '231')->get();
      $previous_cities = City::where('state_id', old('state'))->get();
    }else{
      $previous_states = State::where('country_id', old('country'))->get();
      $previous_cities = City::where('state_id', old('state'))->get();
    }

    if($user->user_type == 'admin') {
      $dashboard_template = 'tymbl.dashboard.index';
    }else{
      $dashboard_template = 'tymbl.dashboard.index_user';
    }

    //end new registraton popup
    return view($dashboard_template, compact('approved_ads', 'blocked_ads', 'total_users', 'total_reports', 'total_payments', 'total_payments_amount', 'ten_contact_messages', 'reports', 'countries', 'previous_states', 'previous_cities', 'user_notification_optin', 'comments', 'comment_counts', 'num_reserved_leads', 'num_listed_leads', 'total_users_all', 'notifications_users', 'user_notification_count'));
  }

  public function logout(){
    if (Auth::check()){
      Auth::logout();
    }
    return redirect(route('login'));
  }

  public function allLeads(){
    $title = 'Leads';
    return view('tymbl.dashboard.all_leads', compact('title'));
  }

  public function leadsData(){
    $ads = Ad::select('id', 'title', 'description', 'status', 'seller_name', 'address', 'country_id', 'state_id', 'city_id', 'zipcode', 'user_id', 'escrow_amount', 'created_at', 'slug')->where('status', '=', '1')->orWhere('status', '=', '4')->orWhere('status', '=', '5')->get();

    return  Datatables::of($ads)

    ->editColumn('title', function($ads){
      $html = '<a href="/listing/'.$ads->id.'/'.$ads->slug.'">'.safe_output($ads->title).'</a>';
      return $html;
    })

    ->editColumn('seller_name', function($ads){
      $user = User::whereId($ads->user_id)->first();
      $html = '<a href="'.route('user_info', $user->id).'">'.safe_output($user->name).'</a>';
      return $html;
    })

    ->editColumn('country_id', function($ads){
      $country = Country::whereId($ads->country_id)->first();
      return $country->country_name;
    })

    ->editColumn('status', function($ads){
      $status = '';
      $status_color = '';
      switch ($ads->status) {
        case '1':
        $status = 'Active';
        $status_color = 'active-status';
        break;
        case '2':
        $status = 'Paused';
        $status_color = 'pending-status';
        break;
        case '3':
        $status = 'Block';
        $status_color = 'block-status';
        break;
        case '4':
        $status = 'Pending for Approval';
        $status_color = 'sale-pending';
        break;
        case '5':
        $status = 'Reserved';
        $status_color = 'sale-approved';
        break;
      }
      $html = '<span class="m-list-search__result-item-icon"><i class="fas fa-circle '.$status_color.'"></i></span>
      <span class="m-list-search__result-item-text">'.$status.'</span>';
      return $html;
    })
    ->removeColumn('user_id')
    ->removeColumn('slug')
    ->addColumn('', function($ads){
      $html = '';
      if($ads->status == '5'){
        $html = '<a href="#" title="view transaction"><i class="fas fa-file-invoice"></i></a>';
      }
      return $html;
    })
    ->make();
  }


  public function leadsBlockedData(){
    $ads = Ad::select('id', 'title', 'description', 'status', 'seller_name', 'address', 'country_id', 'state_id', 'city_id', 'zipcode', 'user_id', 'escrow_amount', 'created_at', 'list-status')->where('status', '=', '3')->orderBy('updated_at', 'desc')->get();

    return  Datatables::of($ads)

    ->editColumn('title', function($ads){
      $html = '<a href="/listing/'.$ads->id.'/'.$ads->slug.'">'.safe_output($ads->title).'</a>';
      return $html;
    })

    ->editColumn('seller_name', function($ads){
      $user = User::whereId($ads->user_id)->first();
      $html = '<a href="'.route('user_info', $user->id).'">'.safe_output($user->name).'</a>';
      return $html;
    })

    ->editColumn('country_id', function($ads){
      $country = Country::whereId($ads->country_id)->first();
      return $country->country_name;
    })

    ->editColumn('status', function($ads){
      $status = '';
      $status_color = '';
      switch ($ads->status) {
        case '1':
        $status = 'Active';
        $status_color = 'active-status';
        break;
        case '2':
        $status = 'Paused';
        $status_color = 'pending-status';
        break;
        case '3':
        $status = 'Block';
        $status_color = 'block-status';
        break;
        case '4':
        $status = 'Pending for Approval';
        $status_color = 'sale-pending';
        break;
        case '5':
        $status = 'Reserved';
        $status_color = 'sale-approved';
        break;
      }
      $html = '<span class="m-list-search__result-item-icon"><i class="fas fa-circle '.$status_color.'"></i></span>
      <span class="m-list-search__result-item-text">'.$status.'</span>';
      return $html;
    })
    ->removeColumn('user_id')
    ->addColumn('', function($ads){
      $html = '';
      if($ads->status == '5'){
        $html = '<a href="#" title="view transaction"><i class="fas fa-file-invoice"></i></a>';
      }
      return $html;
    })
    ->make();
  }

  public function importLeads(){
    $title = 'Import Leads';

    $records = [];
    $file_name = request()->lead_file_name;

    $path = storage_path('csv_uploads');

    //foreach (glob($path.'/*.csv') as $file) {
      //$file = request()->lead_file_name;
      //$file->seek(PHP_INT_MAX);
      //$records[] = $file->value();
      //if($file){
        //$file->setFlags(\SplFileObject::READ_CSV);
        //foreach ($file as $num=>$row){
          //if($num >= 1){
          //  $records[] = $row;
          //}

        //}
    //  }
    //}

    $file = fopen($path.'/'.$file_name, "r");
    $data = fgetcsv($file, 1000, ",");

    while (($data = fgetcsv($file, 1000, ",")) !== FALSE)
    {
      $records[] = $data;
    }

    fclose($file);

    return view('tymbl.dashboard.import_leads', compact('title', 'records'));
  }

  public function importCsv(){

    request()->validate([
      'csv_file' => 'required|mimes:csv,txt'
    ]);

    //get file from upload
    $path = request()->file('csv_file')->getRealPath();

    $file = file($path);

    //remove header
    $data = array_slice($file, 1);

    $parts = (array_chunk($data, 100));
    $i = 1;
    foreach($parts as $line) {
      $filename = storage_path('csv_uploads/'.date('y-m-d-H-i-s').$i.'.csv');
      file_put_contents($filename, $line);
      $i++;
    }

    return redirect(route('import-leads'))->with('success', 'File upload success. Parsing files...');
  }

  public function importImages(){
    request()->validate([
      'zip_file' => 'required|mimes:zip,rar'
    ]);

    $path = request()->file('zip_file')->getRealPath();
    $file = Storage::putFile('temp_uploads', request()->file('zip_file'));

    $zip = new ZipArchive();
    //$zip->open($path);
    //$zip->extractTo(public_path('uploads').'/'.'images');

    if ($zip->open($path) === true) {
      for($i = 0; $i < $zip->numFiles; $i++) {
        $zip->extractTo(public_path('uploads').'/images', array($zip->getNameIndex($i)));

        try {
          $image = $zip->getNameIndex($i);
          $imageFileName  = public_path('uploads').'/images/'.$image;
          $imageThumbName = public_path('uploads').'/images/thumbs/'.$image;

          $resized = Image::make($imageFileName)->resize(
            640,
            null,
            function ($constraint) {
              $constraint->aspectRatio();
            }
            )->save($imageFileName);

            $resized_thumb  = Image::make($imageFileName)->resize(320, 213)->save($imageThumbName);

          } catch (\Exception $e) {
            return redirect(route('import-leads'))->with('error', $e->getMessage());
          }

          //$zip->extractTo(public_path('uploads').'/'.'images');
        }
        $zip->close();
      }

      return redirect(route('import-leads'))->with('success', 'Images uploaded successfully...');

    }



    public function allUserMessages(){
      $title = 'Messages';
      $user = Auth::user();
      $notif = $this->getUserNotifications($user->id, '10');
      $notifications_users = $notif[0];
      $user_notification_count = $notif[1];
      return view('tymbl.dashboard.all_messages', compact('title', 'notifications_users', 'user_notification_count'));
    }

    public function getUserMessagesData(){
      $user = Auth::user();

      $messages = NotificationsUsers::select('id', 'subject', 'created_at', 'user_read')->where('recipient', '=', $user->id)->orderBy('id', 'desc')->get();


      return  Datatables::of($messages)

      ->editColumn('id', function($messages){
        if($messages->user_read == '0'){
          $html = '<i class="far fa-envelope"></i>';
        }else{
          $html = '<i class="far fa-envelope-open"></i>';
        }

        return $html;
      })

      ->editColumn('subject', function($messages){
        $url = '/dashboard/message/'.safe_output($messages->id);

        if($messages->user_read == '0'){
          $html = '<div style="overflow: hidden"><a class="messages-unread" rel="'.$messages->id.'" href="'.$url.'">'.safe_output($messages->subject).'</a></div>';
        }else{
          $html = '<div style="overflow: hidden"><a rel="'.$messages->id.'" href="'.$url.'">'.safe_output($messages->subject).'</a></div>';
        }

        return $html;
      })

      ->make();
    }

    public function getUserNotifications($user, $limit){
      $user_notification = NotificationsUsers::where('recipient', '=', $user)->limit($limit)->orderBy('id', 'desc')->get();
      $notification_count = count($user_notification);
      $data = [$user_notification, $notification_count];
      return $data;
    }

    public function getUserMessage($id){
      $user = Auth::user();
      $title = "Message";
      $message = NotificationsUsers::where('recipient', '=', $user->id)->where('id', '=', $id)->first();

      if($message->user_read == '0'){
        $message->user_read = '1';
        $message->save();
      }

      return view('tymbl.dashboard.user_message_single', compact('title', 'message'));
    }

    public function isFeaturedAd(Request $request){
      $ad = Ad::whereId($request->id)->first();
      $ad->is_featured = $request->checked;
      $ad->save();
      return $ad;
    }

    public function pausedAds(){
      $title = 'Paused Leads';

      $term = request()->get('q');
      $page = request()->get('p');

      if(!isset($term) || $term == ''){
        $ads   = Ad::with('city', 'country', 'state')->whereStatus('2')->orderBy('updated_at', 'desc')->paginate(10);
      }else{
        $ads = $this->searchAds($term, $page, '0');
      }
      $adtype = '2';

      $users = User::where('user_type', '=', 'user')->where('active_status', '=', '1')->get();
      return view('tymbl.dashboard.inactive_ads', compact('title', 'ads', 'users', 'adtype'));
    }

    public function featuredAds(){

      $term = request()->get('q');
      $page = request()->get('p');

      $title = 'Featured Leads';

      if(!isset($term) || $term == ''){
        $ads   = Ad::with('city', 'country', 'state')->whereStatus('1')->where('is_featured', '=', '1')->orderBy('id', 'desc')->paginate(10);
      }else{
        $ads = $this->searchAds($term, $page, '1');
      }

      $adtype = '1';

      $users = User::where('user_type', '=', 'user')->where('active_status', '=', '1')->get();
      return view('tymbl.dashboard.all_ads', compact('title', 'ads', 'users', 'adtype'));
    }

    public function successfulLeads(){
      if(Auth::user()->user_type != 'admin'){
        return redirect('dashboard')->with('error', 'You cannot access this area.');
      }
      $title = 'Reserved Leads';
      return view('tymbl.dashboard.successful_leads', compact('title'));
    }

    public function successLeadsData()
    {
        $ads = Ad::with(
            'lead_notes',
            'listingContracts',
            'media',
            'referral_info',
            'listingContracts.buyer',
            'listingContracts.prospect_contact_status',
            'listingContracts.prospect_contact_status.prospectContactStatusOptionsSaved',
            'payment'
        )->select('id', 'title', 'seller_name', 'slug')
            ->where('status', '=', '5')
            ->orWhere('status', '=', '8')
            ->orderBy('updated_at', 'desc')
            ->get();

        return  Datatables::of($ads)
            ->editColumn('title', function($ads) {
                return view('tymbl.dashboard.successful_leads_title', compact('ads'));
            })
            ->editColumn('id', function($ads) {
                return view('tymbl.dashboard.successful_leads_img', compact('ads'));
            })
            ->removeColumn('seller_name', 'slug', 'lead_notes', 'listingContracts', 'media', 'referral_info', 'payment')
            ->make();
    }



      public function searchAds($term, $page, $featured)
      {
          $ads = Ad::with('city', 'country', 'state')
              ->whereStatus($page)
              ->where('title', 'LIKE', "%$term%")
              ->where('is_featured', '=', $featured)
              ->orderBy('updated_at', 'desc')
              ->paginate(20);
          return $ads;
      }

      public function transactionsSettingsBuyingCheck(){
        $reserved_contract = ListingContracts::where('listing_id', request()->id)->first();
        $contract = LaravelEsignatureWrapper::getContract($reserved_contract->contract_id);
        $status = $contract['data']['status'];

        if($status == 'signed'){
          //$ad = Ad::where('id', request()->id)->update(['status' => '5']);

          $ad = Ad::where('id', request()->id)->first();
          $ad_title = $ad->title;
          $ad_id = $ad->id;

          if($ad->get_ad_status() == 'lead-free-active'){
            $ad->set_ad_status('lead-free-reserved');
          }elseif($ad->get_ad_status() == 'lead-vip-free-active'){
            $ad->set_ad_status('lead-vip-free-reserved');
          }

          $reserved = ListingContracts::where('listing_id', request()->id)->update(['list_status' => '1']);

          if($ad){
            $referral_transaction = Payment::where('ad_id', '=', $reserved_contract->listing_id)->first();

            if($referral_transaction){
              //$this->sendReferralNotification(request()->id);

              $broker = Broker::where('user_id', '=', $reserved_contract->buyer_id)->first();
              $referral_info = ReferralContactInfo::where('ad_id', '=', request()->id)->first();

              $contract = ListingContracts::where('listing_id', request()->id)->first();
              $contract_id = $contract->contract_id;

              $url = LaravelEsignatureWrapper::getContract($contract_id);
              $user = User::where('id', '=', $reserved_contract->buyer_id)->first();
              $transaction = Payment::where('ad_id', '=', request()->id)->first();
              $transaction_id = $transaction->local_transaction_id;
              //$contract_url = $url['data']['signers'][1]['embedded_url'];

              //$this->dispatch(new SendContractToBroker($broker, $referral_info, $user, $contract_id, $transaction_id));
              SendContractToBroker::dispatch($broker, $referral_info, $user, $contract_id, $transaction_id)->delay(1);

              if($ad->get_ad_status() == 'lead-free-reserved'){
                return redirect()->route('reserved-leads-success', ['id' => request()->id]);
              }elseif($ad->get_ad_status() == 'lead-vip-free-reserved'){
                SendVipSuccessEmailJob::dispatch($contract_id, $transaction_id, $ad_title, $ad_id);
                return redirect()->route('reserved-leads', ['id' => request()->id]);
              }

            }
          }

          //return redirect()->route('reserved-leads');

        }else{
          return redirect()->route('dashboard')->with('error', 'You decline the contract');
        }
      }


      public function reservedLeadSuccess(){
        $title = 'Reserved Lead Success';
        return view('tymbl.lead_reserved_sucess', compact('title'));
      }



      public function transactionsSettingsBuying()
      {
          $user = Auth::user();
          $lists = Ad::whereHas('listingContracts', function(Builder $query) use ($user) {
              $query->where('buyer_id', '=', $user->id)
                  ->where('list_status', '!=', ListingContracts::LS_RESERVED)
                  ->where('list_status', '!=', ListingContracts::LS_DEFAULT);
          })->orderBy('updated_at', 'desc')
              ->with(
                  'listingContracts',
                  'listingContracts.titleCompanyInfoLTB',
                  'listingContracts.referralContactInfo',
                  'listingContracts.transactionReports',
                  'listingContracts.leadNote',
                  'listingContracts.leadNote.user',
                  'listingContracts.prospect_contact_status',
                  'listingContracts.prospect_contact_status.prospectContactStatusOptionsSaved',
                  'img'
              )
              ->paginate(10);

          $title = trans('app.transactions');
          $header = "Reserved Leads";
          $selltab = '0';

          return view('tymbl.dashboard.transaction', compact('title', 'lists', 'header', 'selltab'));
      }

    public function transactionSettingsBuying($id)
    {
        $user = Auth::user();
        $list = Ad::whereHas('listingContracts', function(Builder $query) use ($user) {
            $query->where('buyer_id', '=', $user->id)
                ->where('list_status', '!=', ListingContracts::LS_RESERVED)
                ->where('list_status', '!=', ListingContracts::LS_DEFAULT);
        })->with(
                'listingContracts',
                'listingContracts.titleCompanyInfoLTB',
                'listingContracts.referralContactInfo',
                'listingContracts.transactionReports',
                'listingContracts.leadNote',
                'listingContracts.leadNote.user',
                'img'
            )->find($id);

        $title = trans('app.transactions');
        $header = $list->title;
        $selltab = '0';

        return view('tymbl.dashboard.transaction', compact('title','list', 'header', 'selltab'));
    }

    public function transactionsSettingsBuyingAddStatus(Request $oRequest)
    {
        $oUser = Auth::user();
        $aProspectContactStatusOptions = ProspectContactStatusOptions::where(
            'prospect_contact_status_name',
            "{$oRequest->input('contact_status')}"
        )->get();

        $aProspectContactStatus = ProspectContactStatus::where([
            ['reserved_lead_id', '=', $oRequest->input('lead_id')],
            ['user_id', '=', $oUser->id],
            ['lead_type', '=', $oRequest->input('lead_type')],
            ['current_prospect_contact_status', '=', $oRequest->input('contact_status')]
        ])->with('prospectContactStatusOptionsSaved')->first();

        $aSavedOptions = [];
        if ($aProspectContactStatus && count($aProspectContactStatus->prospectContactStatusOptionsSaved)) {
            foreach ($aProspectContactStatus->prospectContactStatusOptionsSaved as $oSavedOption) {
                $aSavedOptions[$oSavedOption->prospect_contact_status_options_id] = $oSavedOption;
            }
        }

        $aOptions = [];
        if (count($aProspectContactStatusOptions)) {
            foreach ($aProspectContactStatusOptions as $oProspectContactStatusOptions) {
                $aOptions[$oProspectContactStatusOptions->option_type][] = $oProspectContactStatusOptions;
            }
        }

        return view('tymbl.dashboard.prospect_contact_status_add', [
            'lead_type'       => $oRequest->input('lead_type'),
            'lead_id'         => $oRequest->input('lead_id'),
            'contact_status'  => $oRequest->input('contact_status'),
            'options'         => $aOptions,
            'savedOptions'    => $aSavedOptions,
            'modal_title'     => ProspectContactStatus::CS_DESCRIPTION[$oRequest->input('contact_status')]
        ]);
    }

    public function transactionsSettingsBuyingUpdateStatus(Request $oRequest)
    {
        $validator = \Validator::make($oRequest->all(), [
            'lead_type'                         => ['required', 'string'],
            'reserved_lead_id'                  => ['required', 'integer', 'exists:listing_contracts,id'],
            'current_prospect_contact_status'   => [
                'required',
                'string',
                function ($attribute, $value, $fail) {
                    if (!in_array($value, ProspectContactStatus::CS_LIST)) {
                        $fail($attribute . ' is invalid.');
                    }
                }
            ],
            'date'                              => [
                'required',
                'date',
                function ($attribute, $value, $fail) {
                $date = explode('/', $value, 3);
                    if (Carbon::now()->setDate($date[2], $date[0], $date[1]) > Carbon::now()) {
                        $fail('Event date should not be later than today.');
                    }
                }
            ],
            'options.*.value'                   => 'nullable|string',
            'options.*.id'                      => 'integer|exists:prospect_contact_status_options,id'
        ]);

        if ($validator->fails()) {
            return json_encode(['status' => 'error', 'message' => implode("\n", $validator->messages()->all())]);
        }
        $iOptionsCount = 0;
        foreach (array_column($oRequest->input('options'), 'value') as $key => $val) {
            if (!empty($val)) {
                ++$iOptionsCount;
            }
        }

        if ($iOptionsCount == 0) {
            return json_encode(['status' => 'error', 'message' => 'Choose at least one of the properties!']);
        }

        if ($iOptionsCount <= 1 && $oRequest->input('current_prospect_contact_status') == ProspectContactStatus::CS_CONTACTED_CUSTOMER) {
            return json_encode([
                'status' => 'error',
                'message' => 'For this type of status, you must select at least two properties!'
            ]);
        }
        $data = [
            'reserved_lead_id'                  => $oRequest->input('reserved_lead_id'),
            'lead_type'                         => $oRequest->input('lead_type'),
            'current_prospect_contact_status'   => $oRequest->input('current_prospect_contact_status'),
            'user_id'                           => Auth::id()
        ];
        $whereData = [];
        foreach ($data as $key => $val) {
            $whereData[] = [$key, '=', $val];
        }
        $date = explode('/', $oRequest->input('date'), 3);
        $oProspectContactStatus = ProspectContactStatus::where($whereData)->first();
        if (!($oProspectContactStatus instanceof ProspectContactStatus)) {
            $oProspectContactStatus = new ProspectContactStatus($data);
            $oProspectContactStatus->setUpdatedAt(Carbon::now()->setDate($date[2], $date[0], $date[1]));
        } else {
            $oProspectContactStatus->updated_at = Carbon::now()->setDate($date[2], $date[0], $date[1]);
        }

        if (!$oProspectContactStatus->save()) {
            return json_encode(['status' => 'error', 'message' => 'Saving errors!']);
        }

        $oProspectContactStatus->prospectContactStatusOptionsSaved()->delete();

        if ($oRequest->has('options')) {
            foreach ($oRequest->input('options') as $key => $val) {
                if (!empty($val['value'])) {
                    $val['prospect_contact_status_id'] = $oProspectContactStatus->id;
                    $aProspectContactStatusOptionsSaved[] = ProspectContactStatusOptionsSaved::create([
                        'prospect_contact_status_options_id' => $val['id'],
                        'option_value' => $val['value'],
                        'prospect_contact_status_id' => $oProspectContactStatus->id
                    ]);
                }
            }
        }
        return json_encode(['status' => 'ok', 'message' => 'Status Added success!']);
    }

      public function transactionsSettingsSelling(){
        $user = Auth::user()->id;
        $referral = '';
        $transaction_reports = '';

        $lists = DB::table('ads')
            ->join('listing_contracts', 'ads.id', '=', 'listing_contracts.listing_id')
            ->join('media', 'media.ad_id', '=', 'ads.id')
            ->select('ads.*', 'listing_contracts.*', 'media.*')
            ->where('ads.user_id', $user)
            ->where('ads.status', '!=', '0')
            ->orderBy('ads.id', 'desc')
            ->paginate('10');

        foreach($lists as $li){
          $listid[] = $li->listing_id;
          $userid[] = $li->user_id;
        }

        if(isset($listid)){
          $referral = ReferralContactInfo::whereIn('ad_id', $listid)->orderBy('id', 'desc')->get();
          $transaction_reports = TransactionReports::whereIn('ad_id', $listid)->orderBy('id', 'desc')->get();
        }

        $no_button = true;
        $title = trans('app.transactions');
        $header = "Lead Status";
        $selltab = '1';

        //$notifications_users = NotificationsUsers::where('recipient', '=', $user)->where('user_read', '=', '0')->get();
        //$user_notification_count = count($notifications_users);

        return view('tymbl.dashboard.transaction', compact('title', 'lists', 'header', 'referral', 'no_button', 'transaction_reports', 'user', 'selltab'));
      }

      public function relistAd(Request $request)
      {
          $ad = Ad::find($request->id);

          if (Auth::user()->user_type != 'admin') {
              return '0';
          }

          if ($ad) {
              $ad->set_ad_status('lead-free-active');
              $listing_contract = ListingContracts::where('listing_id', '=', $request->id)->first();
              $dt = Carbon::parse($listing_contract->updated_at);
              $date_diff = $dt->diffInDays(Carbon::now());

              if ($listing_contract) {
                  $listing_contract->set_contract_status(ListingContracts::LS_RESERVED);
                  $this->sendRelistedEmail($ad->id, $date_diff);
              }
          }
          return '1';
      }

      public function sendReferralNotification(Request $request){
        $id = $request->id;
        $contract = ListingContracts::where('listing_id', '=', $id)->first();
        $user = User::whereId($contract->buyer_id)->first();
        $ad = Ad::whereId($id)->first();
        $transaction = Payment::where('ad_id', '=', $id)->first();
        $email = array($user->email);
        $amount = $transaction->currency.$transaction->total;

        $email_template = $transaction->payment_method == 'free' ? 'emails.referral_notice' : 'emails.referral_notice_reservation_fee';

        Mail::send($email_template, ['name' => $user->name, 'contract_id' => $contract->contract_id, 'transaction_id' => $transaction->local_transaction_id, 'id' => $ad->id, 'title' => $ad->title, 'slug' => $ad->slug, 'amount' => $amount, 'date_reserved' => $contract->updated_at], function ($message) use ($email, $ad)
        {
          $message->from('info@tymbl.com','Tymbl Team');
          $message->to($email);
          $message->subject('Referral Agreement for Ad#'.$ad->id.' has been signed!');
        });
      }

      public function sendRelistedEmail($id, $date_diff){

        $contract = ListingContracts::where('listing_id', '=', $id)->first();
        $ad = Ad::where('id', '=', $contract->listing_id)->first();

        $user = User::whereId($contract->buyer_id)->first();

        $reason = $date_diff >= 2 ? 'Lack of contract' : 'Inactivity. No contact with the prospect was made within 24 hours since the lead was reserved.';

        $email = array($user->email);
        Mail::send('emails.relist_email', ['name' => $user->name, 'date_reserved' => $contract->updated_at, 'contract_id' => $contract->contract_id, 'id' => $ad->id, 'title' => $ad->title, 'slug' => $ad->slug, 'reason' => $reason], function ($message) use ($email, $contract)
        {
          $message->from('info@tymbl.com','Tymbl Team');
          $message->to($email);
          $message->cc('support@tymbl.com');
          $message->subject('Referral Agreement #'.$contract->contract_id.' Termination notice');
        });
      }

      public function uploadedLeads(){
        $title = 'Uploaded Files';
        return view('tymbl.dashboard.uploaded_files', compact('title'));
      }

      public function uploadedLeadsStatus($id)
      {
          $title = 'Leads Status';
          $file_uploaded = PlanContract::find($id);
          $file_status = UploadedLead::where('file_id', '=', $file_uploaded->id)->first();
          $date_added = $file_uploaded->created_at;
          $file_id = $file_uploaded->id;

          return view('tymbl.dashboard.uploaded_files_status', compact('title', 'file_status', 'date_added', 'file_id'));
      }

      public function uploadedLeadsStatusAction(Request $request)
      {
          $seller_stat = $request->buyer_seller == 'seller' ? 'yes' : 'no';
          $buyer_stat = $request->buyer_seller == 'buyer' ? 'yes' : 'no';
          $file_upload_update = UploadedLead::updateOrCreate([
                  'id' => $request->cid
              ], [
                  'date_added' => $request->date_added,
                  'status' => $request->status,
                  'first_name' => $request->first_name,
                  'last_name' => $request->last_name,
                  'seller' => $seller_stat,
                  'buyer' => $buyer_stat,
                  'lead_source' => $request->lead_source,
                  'email' => $request->email,
                  'phone' => $request->phone,
                  'address' => $request->address,
                  'notes_tags' => $request->notes_tags,
                  'prioritize_scrub' => $request->prioritize_scrub,
                  'first_dial_date' => $request->first_dial_date,
                  'first_dial_outcome' => $request->first_dial_outcome,
                  'first_dial_notes' => $request->first_dial_notes,
                  'second_dial_date' => $request->second_dial_date,
                  'second_dial_outcome' => $request->second_dial_outcome,
                  'second_dial_notes' => $request->second_dial_notes,
                  'sms_date' => $request->sms_date,
                  'sms_outcome' => $request->sms_outcome,
                  'sms_notes' => $request->sms_notes,
                  'email_date' => $request->email_date,
                  'email_outcome' => $request->email_outcome,
                  'email_notes' =>$request->email_notes
              ]
          );
          return redirect()->back()->withInput()->with('success', 'Status updated');
      }

      public function deleteLeadEntry(Request $request){
        $id = $request->id;
        $file_id = 0;
        $lead = UploadedLead::whereId($id)->first();
        if($lead){
          $file_id = $lead->file_id;
          UploadedLead::whereId($id)->delete();
        }

        return $file_id;
      }

      public function uploadedLeadsStatusEdit($id){
        $title = 'Edit';
        $file_id = '';

        $file_status = UploadedLead::where('id', '=', $id)->first();
        $file_uploaded = PlanContract::where('id', '=', $file_status->file_id)->first();
        $date_added = $file_uploaded->created_at;
        $file_id = $file_uploaded->id;

        return view('tymbl.dashboard.uploaded_files_status_edit', compact('title', 'file_status', 'date_added', 'file_id'));
      }

      public function uploadLeadsData()
      {
          $fileid = request()->fid;
          $user = Auth::user();

          $uploaded_leads_data = UploadedLead::where('file_id', '=', $fileid)
              ->select('id as fid',
                  'date_added',
                  'status',
                  'first_name',
                  'last_name',
                  'seller',
                  'buyer',
                  'email',
                  'phone',
                  'address'/*,
                  'notes_tags',
                  'prioritize_scrub',
                  'first_dial_date',
                  'first_dial_outcome',
                  'first_dial_notes',
                  'second_dial_date',
                  'second_dial_outcome',
                  'second_dial_notes',
                  'sms_date',
                  'sms_outcome',
                  'sms_notes',
                  'email_date',
                  'email_outcome',
                  'email_notes'*/
              )->orderBy('id', 'desc');
          if ($user->user_type == 'admin') {
              return  Datatables::of($uploaded_leads_data)
                  ->editColumn('fid', function($contracts) {
                      $html = '<div class="text-center">'.$contracts->fid;
                      $html .= '<br><a href="/dashboard/uploaded_leads_status_edit/'.$contracts->fid.'"><i class="far fa-edit"></i></a></div>';
                      return $html;
                  })
                  ->addColumn('sms', function ($contracts) {
                      return sprintf(
                          '<a href="%s" title="Send SMS" class="sms-button"><i>SMS</i></a>',
                          route('twilio.sms.create', ['phone_number' => $contracts->phone])
                      );
                  })
                  ->addColumn('dialer', function ($contracts) {
                      return sprintf(
                          '<a data-phone="%s" data-is-single-call="true" title="Make a single call" class="circle start-call dialer dialer-%d"><i class="fa fa-phone" style="transform: scaleX(-1);"></i></a>',
                          $contracts->phone,
                          $this->getPosition()
                      );
                  })
                  ->make();
          } else {
              return  Datatables::of($uploaded_leads_data)
                  ->editColumn('fid', function($contracts) {
                      return "<div class=\"text-center\">{$contracts->fid}</div>";
                  })
                  ->editColumn('first_name', function($contracts) {
                      return "<div class=\"text-center\">{$contracts->first_name} {$contracts->last_name}</div>";
                  })
                  ->addColumn('sms', function ($contracts) {
                      return sprintf(
                          '<a href="%s" title="Send SMS" class="sms-button"><i>SMS</i></a>',
                          route('twilio.sms.create', ['phone_number' => $contracts->phone])
                      );
                  })
                  ->addColumn('dialer', function ($contracts) {
                      return sprintf(
                          '<a data-phone="%s" data-is-single-call="true" title="Make a single call" class="circle start-call dialer dialer-%d"><i class="fa fa-phone" style="transform: scaleX(-1);"></i></a>',
                          $contracts->phone,
                          $this->getPosition()
                      );
                  })
                  ->removeColumn('last_name', 'seller', 'buyer', 'email', 'phone', 'address', 'notes_tags', 'prioritize_scrub')
                  ->make();
          }
      }

      private $pos = 0;

      private function getPosition()
      {
          return $this->pos++;
      }


      public function userLeadsUploadsData(){

        $user = Auth::user();

        if($user->user_type == 'admin') {
          return $this->loadAllUploadedLeads();
        } else {
          return $this->loadUserploadedLeads($user);
        }
      }

      public function loadUserploadedLeads($user)
      {
          $contracts = PlanContract::where('user_id', '=', $user->id)
              ->where('status', '=', 'signed')
              ->where('file_name', '!=', '')
              ->select('id', 'contract_id', 'updated_at', 'status')
              ->orderBy('id', 'desc');

          return  Datatables::of($contracts)
              ->editColumn('status', function($contracts) {
                  return '<a class="circle" href=' . route('uploaded_leads_status', $contracts->id) .' title="View Leads" style="background-color: #E8E2DF"><i class="fa fa-eye" style="color: black"></i></a>';
              })->make();
      }

      public function loadAllUploadedLeads()
      {
          $contracts = PlanContract::where('status', '=', 'signed')
              ->where('file_name', '!=', '')
              ->select('id', 'contract_id', 'user_id', 'file_name', 'plan_type', 'updated_at', 'status')
              ->orderBy('id', 'desc');
          return  Datatables::of($contracts)
              ->editColumn('user_id', function($contracts) {
                  $user = User::whereId($contracts->user_id)->first();
                  return $user->name;
              })
              ->editColumn('file_name', function($contracts) {
                  $s3 = AWS::createClient('s3');
                  $cmd = $s3->getCommand('GetObject', ['Bucket' => 'csv-user-uploads','Key' => 'csv_files/'.$contracts->file_name]);
                  $request = $s3->createPresignedRequest($cmd, '+20 minutes');
                  $presignedUrl = (string)$request->getUri();
                  $url = $s3->getObjectUrl('csv-user-uploads', 'csv_files/' . $contracts->file_name);
                  return "<a href='$presignedUrl' target='_blank'>".$contracts->file_name."</a>";
              })
              ->editColumn('status', function($contracts) {
                  return sprintf(
                      '<a class="btn btn-primary" href="%s" title="View Leads"><i class="fa fa-eye"></i></a>',
                      route('uploaded_leads_status', $contracts->id)
                  );
              })
              ->editColumn('plan_type', function($contracts) {
                  switch($contracts->plan_type) {
                      case '1':
                          return 'free';
                          break;
                      case '2':
                          return '2';
                          break;
                      case '3';
                          return '3';
                          break;
                      default:
                          return '0';
                          break;
                  }
              })
              ->make();
      }

      public function followUp(Request $request)
      {
          $validation = \Validator::make($request->all(), [
              'id' => 'required|integer|exists:listing_contracts,id'
          ]);
          if ($validation->fails()) {
              return json_encode(['status' => 'error', 'message' => 'Validation Error!']);
          }
          $oListingContracts = ListingContracts::find($request->input('id'));
          $oListingContracts->follow_up = !$oListingContracts->follow_up;
          if ($oListingContracts->save()) {
              return json_encode([
                  'status' => 'ok',
                  'message' => 'Status Updated Successfully',
                  'value' => ($oListingContracts->follow_up == 1 ? 'No follow up' : 'Follow up')
              ]);
          } else {
              return json_encode(['status' => 'error', 'message' => 'Error saving status']);
          }
      }

      public function sendMailReservationInfo(Request $request)
      {
          $validation = \Validator::make($request->all(), [
              'id' => 'required|integer|exists:listing_contracts,id'
          ]);
          if ($validation->fails()) {
              return json_encode(['status' => 'error', 'message' => 'Validation Error!']);
          }
          $oListingContracts = ListingContracts::with('buyer', 'ad')->find($request->input('id'));
          try {
              Mail::send('emails.lead_info_after_update_status', [
                  'oAd' => $oListingContracts,
                  'ad' => $oListingContracts->ad
              ], function ($m) use ($oListingContracts) {
                  $m->from('info@tymbl.com', 'Tymbl Team');
                  $m->to($oListingContracts->buyer->email);
                  $m->subject('Referral ' . $oListingContracts->listing_id . ' needs a status update');
              });
          } catch (\Exception $e) {
              return json_encode(['error' => 'ok', 'message' => 'Errors sending message']);
          }
          return json_encode(['status' => 'ok', 'message' => 'Email sent successfully']);
      }

      public function twilioSmsCreate(Request $request)
      {
          $phone_number = $request->input('phone_number');
          $user = Auth::user();
          $message = view('emails.uploaded_lead_sms', compact('user'));
          $partner_realtors = 'Partner Realtors';
          return view('tymbl.dashboard.uploaded_lead_sms_modal', compact('phone_number', 'message', 'partner_realtors'));
      }

      public function twilioSmsSend(Request $request)
      {
          $user = Auth::user();
          $validator = \Validator::make($request->all(), [
              'message'         => 'nullable|string',
              'phone_number'    => 'required|regex:/^\+[1-9]\d{1,14}$/'
          ]);
          if ($validator->fails()) {
              return ['status'  => 'error', 'message' => 'Validation Error!'];
          }
          $sid = config('app.twilio')['account_sid'];
          $token = config('app.twilio')['app_token'];
          $from = config('app.twilio')['from_num'];
          $to = $request->input('phone_number');

          $body = $request->input('message');
          if ($request->has('use_default_template')) {
              $partner_realtors = 'Partner Realtors';
              if ($request->has('use_default_partner_realtors')) {
                  $partner_realtors = $request->input('partner_realtors', $partner_realtors);
              }
              $body = view('emails.uploaded_lead_sms', compact('user', 'partner_realtors'));
          }

          try {
              $client = new Client($sid, $token);
              $message = $client->messages->create($to, [
                  "body" => $body,
                  "from" => $from,
                  "statusCallback"  => route('twilio.sms.status')
              ]);

          } catch (\Exception $e){
              if ($e->getCode() == 21211) {
                  return ['status'  => 'error', 'message' => $e->getMessage()];
              }
          }
          return ['status'  => 'ok', 'message' => 'Sms successful send'];
      }

      public function twilioToken(Request $request)
      {
          $forPage = $request->input('forPage');
          $sid = config('app.twilio')['account_sid'];
          $token = config('app.twilio')['app_token'];
          $applicationSid = config('app.twilio')['app_sid'];
          $clientToken = new ClientToken($sid, $token);

//          $twilio = new Client($sid, $token);
//          $application = $twilio->applications->create('Phone Me', [
//              'voiceMethod' => 'POST',
//              'voiceUrl'    => route('twilio.call'),
//          ]);

          $clientToken->allowClientOutgoing($applicationSid);

          if ($forPage === route('uploaded_leads_status', ['id' => $request->input('id')], false)) {
              $clientToken->allowClientIncoming('support_agent');
          } else {
              $clientToken->allowClientIncoming('customer');
          }

          $token = $clientToken->generateToken();
          return response()->json(['token' => $token]);
      }

    public function twilioCall(Request $request)
    {
        $response = new Twiml();
        $callerIdNumber = config('app.twilio')['from_num'];
        $dial = $response->dial(['callerId' => $callerIdNumber]);
        $phoneNumberToDial = $request->input('phoneNumber');

        if (isset($phoneNumberToDial)) {
            $dial->number($phoneNumberToDial);
        } else {
            $dial->client('support_agent');
        }
        return $response;
    }

    public function tempInquirySubmit(Request $request){

      $data = [
        'name' => $request->name,
        'company' => $request->company,
        'email' => $request->email
      ];

      $inquiry = PopupInquiry::create($data);

      if($request->name == ''){
        return 'Name is required';
        exit;
      }elseif($request->email == ''){
        return 'Email is required';
        exit;
      }

      $to = 'newtonboy@gmail.com';
      $html = '<p><strong>NAME</strong>: '.$request->name.'</p>';
      $html .= '<p><strong>COMPANY</strong>: '.$request->company.'</p>';
      $html .= '<p><strong>EMAIL</strong>: '.$request->email.'</p>';
      $html .= '<p><strong>Date</strong>: '.date('M d, Y h:i A').'</p>';


      Mail::send([], [], function ($message) use ($to, $html) {
      $message->to($to)
        ->subject('Smart Dialer Inquiry')
        ->setBody('<h5>Smart Dialer Inquiry Submission</h5>'.$html, 'text/html'); // for HTML rich messages
    });

    return '1';

    }
}
