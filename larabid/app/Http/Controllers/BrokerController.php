<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request ;
use App\Http\Requests ;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Ad;
use App\User;
use App\Broker;
use App\ListingContracts;
use Illuminate\Support\Facades\Mail;

class BrokerController extends Controller
{

  public function verifyAgent(Request $request, $id){
    $title = 'Verify Agent';
    $broker = Broker::where('id', '=', $id)->first();
    $user = User::where('id', '=', $broker->user_id)->first();
    return view('tymbl.verify_agent', compact('title', 'broker', 'user'));
  }

  public function verifiedAgent(Request $request){
    $success_status = 'Thank you. Agent has been verified';
    $broker = Broker::whereId($request->broker_id)->first();
    if($broker){
      $broker->broker_verified = $request->verify_val;
      if($request->verify_val == '1'){
        $broker->verified_on = date('Y-m-d h:i:s');
      }
      $broker->save();

      if($request->verify_val == '0'){
        $user = User::whereId($broker->user_id)->first();
        $user->active_status = '3';
        $user->save();

        $listing_contract = ListingContracts::where('buyer_id', '=', $broker->user_id)->update(['buyer_id' => '0', 'list_status' => '0']);
        $ad_by_user = Ad::where('user_id', '=', $broker->user_id)->update(['status' => '1', 'listing_status' => '1']);

        $this->brokerUserNotify($user, $broker);

        $success_status = 'Thank you. Agent has not been verified';
      }
    }
    return back()->with('success', $success_status);
  }

  public function brokerUserNotify($user, $broker){

  $mail = Mail::send('emails.broker_user_notify', ['user' => $user, 'broker' => $broker], function ($m) use ($user, $broker) {
      $m->from('info@tymbl.com', 'Tymbl Team');
      $m->to($user->email, $user->name)->subject($user->name.'\'s account confirmation on Tymbl required');
  });

  }

}
