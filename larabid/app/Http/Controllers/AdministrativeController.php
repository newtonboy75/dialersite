<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Ad;
use App\User;
use App\Country;
use App\State;
use App\City;
use App\Category;
use App\Notification;
use App\NotificationTask;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Mail;
use Illuminate\Filesystem\Filesystem as FS;
use App\ReferralContactInfo;
use App\UserTitleCompanyInfo;
use App\Media;
use App\ListingContracts;
use Session;
use App\PlanContract;
use Illuminate\Support\Facades\Auth;
use AWS;
use Illuminate\Support\Facades\Input;
use App\UploadedLead;

class AdministrativeController extends Controller
{

  private $admin = array('newtonboy', 'timour', 'andy', 'pavel', 'min');

  public function checkServerStatus(){
    $host = request()->getHost();
    $port = 80;
    $timeout = 10;

    try{
      $status = fsockopen($host, $port, $errno, $errstr, $timeout);
      return 'OK';
    }catch(\Exception $ex){
      return 'FAIL';
    }

  }

  public function checkDbServerStatus(){
    try {
      $status = DB::connection()->getDatabaseName();
      return 'DB OK';
    } catch (\Exception $e) {
      die("Could not connect to the database.  Please check your configuration. error:" . $e );
    }
  }

  public function generalMailer(){
    Mail::raw('Server is down', function($message) {
      $message->subject('Please check tymbl.com');
      $message->from('info@tymbl.com', 'Server status down');
      $message->to('newtonboy@gmail.com')->cc('info@tymbl.com');
    });
  }

  public function validateEmail($email)
  {

    $emailIsValid = FALSE;

    if (!empty($email))
    {
      $domain = ltrim(stristr($email, '@'), '@') . '.';
      $user   = stristr($email, '@', TRUE);

      if(!empty($user) && !empty($domain) && checkdnsrr($domain)){
        $emailIsValid = TRUE;
      }
    }

    return $emailIsValid;
  }

  public function testPost(Request $request){


    if(!$this->checkPostHeader($request)){
      echo 'username or password error\r\n';
      exit;
    }

    $time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];

    $user_exitsts = User::where('email', $request->email)->first();
    if($user_exitsts){
      return "User already existst ".$time."\r\n";
      exit;
    }

    $country = Country::where('country_name', $request->country)->first();
    $state = State::where('country_id', $country->id)->where('state_name', $request->state)->first();
    $city = City::where('state_id', $state->id)->where('city_name', $request->city)->first();

    $user_add = new User;
    $user_add->name = $request->name;
    $user_add->email = $request->email;
    $user_add->sms_activation_code = mt_rand(100000,999999);
    $user_add->zip_code = $request->postal_code;
    $user_add->save();

    if($user_add){
      echo "User ".$user_add->name." was added to the  db successfully.\n";

      $add_notification = new Notification;
      $add_notification->user_id = $user_add->id;
      $add_notification->email = $request->email;
      $add_notification->country_id = $country->id;
      $add_notification->state_id = $state->id;
      $add_notification->city_id = $city->id;
      $add_notification->range = $request->distance;
      $add_notification->interests = $request->interest;
      $add_notification->save();

      if($add_notification){
        echo "Notification for user ".$user_add->name." has been created.\n execution time (".$time.")\n";
      }else{
        echo "An error occured during data processing";
      }

    }else{
      echo "An error occured duting data processing";
    }

  }

  public function checkAllEntries(Request $request){

    if(!$this->checkPostHeader($request)){
      echo 'username or password error \r\n';
      exit;
    }

    $users = User::orderBy('id', 'desc')->take(5)->get();
    foreach($users as $user){
      echo $user->name." | ".$user->email."\n";
    }

  }

  public function checkPostHeader($request){

    if(isset($request)){

      preg_match("'xxx12345(.*?)54321xxx'si", $request->headers->get('php-auth-pw'), $match);

      if(in_array($request->headers->get('php-auth-user'), $this->admin)){
        if($match[1] == $request->headers->get('php-auth-user')){
          return true;
        }
      }
    }

    return false;
  }

  public function deleteFavoriteList(){
    echo 'test';
  }

  public function getPostById(Request $request){
    $ad = Ad::whereId($request->id)->first();
    return $ad;
  }

  public function testPostLocal(Request $request){

    $title = filter_var($request->listing_title, FILTER_SANITIZE_STRING);
    $slug  = unique_slug($title);
    $video_url = $request->video_url ? $request->video_url : '';
    $feature1 = serialize($request->feature);
    $feature2 = serialize($request->feature2);

    $category_type_status = $request->cat_type_q == 'yes' ? 'qualified' : 'unqualified';
    $referral_fee = (($request->referral_fee+10)/100);

    $sub_category   = null;
    $ads_price_plan = get_option('ads_price_plan');

    //if($request->category) {
    $sub_category = Category::findOrFail($request->category);
    //}

    if($request->cat_type == 'ltb'){
      $ctype = 'buying';
    }elseif($request->cat_type == 'lts'){
      $ctype = 'selling';
    }else{
      $ctype = 'other';
    }

    $data = [
      'title'           => strip_tags($request->listing_title),
      'slug'            => $slug,
      'description'     => filter_var($request->listing_description, FILTER_SANITIZE_STRING),
      'category_id'     => $sub_category->category_id,
      'sub_category_id' => $request->category,
      'type'            => $request->type,
      //'ad_condition'    => $request->condition,
      'price'           => $request->price,
      'seller_name'    => filter_var($request->seller_name, FILTER_SANITIZE_STRING),
      'seller_email'   => filter_var($request->seller_email, FILTER_VALIDATE_EMAIL),
      'seller_phone'   => preg_replace('/\D/', '', $request->seller_phone),
      'country_id'     => $request->country,
      'state_id'       => $request->state,
      'city_id'        => $request->city,
      'zipcode'        => $request->zipcode,
      //'address'        => filter_var($request->referral_contact_address, FILTER_SANITIZE_STRING),
      'video_url'      => $video_url,
      'category_type'  => $ctype,
      'price_plan'     => 'regular',
      'listing_status' => '3',
      'price_range'    => $request->price_range,
      'referral_fee'   => $referral_fee,
      //'mark_ad_urgent' => $mark_ad_urgent,
      'status'         => '5',
      'user_id'        => $request->user_id,
      'latitude'       => $request->latitude,
      'longitude'      => $request->longitude,
      'escrow_amount'  => $request->escrow_amount,
      'feature_1'      => $feature1,
      'feature_2'      => $feature2,
      'cat_type_status'=> $category_type_status,
      'date_contacted_qualified' =>  date("Y-m-d", strtotime($request->date_contacted_qualified))
    ];

    $created_ad = Ad::create($data);

    $remove_ad = Ad::find($created_ad->id);
    $remove_ad->delete();

    return $created_ad;
  }

  public function csvUploader(){
    $records = [];
    $path = storage_path('csv_uploads');

    foreach (glob($path.'/*.csv') as $file) {
      $file = new \SplFileObject($file, 'r');
      //$file->seek(PHP_INT_MAX);
      //$records[] = $file->value();
      if($file){
        $file->setFlags(\SplFileObject::READ_CSV);
        foreach ($file as $num=>$row){
          $records[] = $row;
        }
      }
    }


    return $records;
  }


  public function importCsv(){

    ini_set('MAX_EXECUTION_TIME', 36000);

    $timestamp = date('y-m-d-H-i-s');
    $file_name_no_extension = pathinfo(Input::file('csv_file')->getClientOriginalName(), PATHINFO_FILENAME);
    $file_extension = Input::file('csv_file')->getClientOriginalExtension();
    $leads_file_name = $file_name_no_extension.'_'.$timestamp.'.'.$file_extension;
    $file = Input::file('csv_file')->getClientOriginalName();
    $storage_path = storage_path('csv_uploads');
    Input::file('csv_file')->move($storage_path,$leads_file_name);

    if(!isset(request()->plan_upload)){

      //$this->processCsvFile($contract_id);
      //echo 'test';
      //exit;
      //$mail = new Mail;
      //$mail::send([], [], function ($m) use ($leads_file_name) {
      //$m->from('info@tymbl.com', 'Tymbl Team');
      //  $m->to(['support@gmail.com'])->subject('New lead file uploaded');
      //  $m->cc('info@tymbl.com', 'Tymbl Team');
      //$m->setBody("Filename: ".$leads_file_name);
      //});

      return redirect(url()->previous().'&lead_file_name='.$leads_file_name)->with('success', 'File was uploaded successfully');
    }else{
      //uploader from homepage
      //return $csv_file_name;
      $contract = PlanContract::where('contract_id', '=', request()->contract_id)->first();
      $contract->file_name = $leads_file_name;
      $contract->save();

      $mail = new Mail;
      $mail::send([], [], function ($m) use ($leads_file_name) {
        $m->from('info@tymbl.com', 'Tymbl Team');
        $m->to(['support@tymbl.com'])->subject('New Leads Uploaded');
        $m->cc('info@tymbl.com', 'Tymbl Team');
        $m->setBody(Auth::user()->first_name.' '.Auth::user()->last_name.' uploaded new leads - Contract ID: '.request()->contract_id);
      });
    }

    //$dir = storage_path('csv_uploads/'.$csv_file_name);
    $uploaded = true;

    try{
      $s3 = AWS::createClient('s3');
      $s3->putObject(array(
        'Bucket'     => 'csv-user-uploads',
        'Key'        => 'csv_files/'.$leads_file_name,
        'SourceFile' => storage_path('csv_uploads/'.$leads_file_name)
      ));
    }catch (\Exception $e) {
      $uploaded = false;
      Log::info('uploaded'.$e->getMessage());
      return $e->getMessage();
    }


    if($uploaded == true){
      unlink(storage_path('csv_uploads/'.$leads_file_name));
    }

    return redirect(url()->previous())->with('success', 'File was uploaded successfully');
  }

  public function importImages(){
    request()->validate([
      'zip_file' => 'required|mimes:zip,rar'
    ]);

    $path = request()->file('zip_file')->getRealPath();
    $file = Storage::putFile('temp_uploads', request()->file('zip_file'));

    $zip = new ZipArchive();
    //$zip->open($path);
    //$zip->extractTo(public_path('uploads').'/'.'images');

    if ($zip->open($path) === true) {
      for($i = 0; $i < $zip->numFiles; $i++) {
        $zip->extractTo(public_path('uploads').'/images', array($zip->getNameIndex($i)));

        try {
          $image = $zip->getNameIndex($i);
          $imageFileName  = public_path('uploads').'/images/'.$image;
          $imageThumbName = public_path('uploads').'/images/thumbs/'.$image;

          $resized = Image::make($imageFileName)->resize(
            640,
            null,
            function ($constraint) {
              $constraint->aspectRatio();
            }
            )->save($imageFileName);

            $resized_thumb  = Image::make($imageFileName)->resize(320, 213)->save($imageThumbName);

          } catch (\Exception $e) {
            return redirect(route('import-leads'))->with('error', $e->getMessage());
          }

          //$zip->extractTo(public_path('uploads').'/'.'images');
        }
        $zip->close();
      }

      return redirect(route('import-leads'))->with('success', 'Images uploaded successfully...');

    }

    public function importSaveAll(Request $request){

      $file_id = $request->file_id;
      $filename = $request->filename;
      $path = storage_path('csv_uploads');

      $file = fopen($path.'/'.$filename, "r");
      $data = fgetcsv($file, 1000, ",");

      $errors = 0;

      while (($data = fgetcsv($file, 1000, ",")) !== FALSE)
      {

        $date_added = $data[0];
        $status = $data[1]  == '' ? 'New' : $data[1];
        $first_name = $data[2];
        $last_name = $data[3];
        $seller = $data[4];
        $buyer = $data[5];
        $lead_source = $data[6];
        $email = $data[7];
        $phone = $data[8];
        $address = $data[9];
        $notes_tags = $data[10];
        $prioritize_scrub = $data[11];
        $first_dial_date = $data[12];
        $first_dial_outcome = $data[13] == '' ? 'na' : $data[13];
        $first_dial_notes = $data[14];
        $second_dial_date = $data[15];
        $second_dial_outcome = $data[16]  == '' ? 'na' : $data[16];
        $second_dial_notes = $data[17];
        $sms_date = $data[18];
        $sms_outcome = $data[19]  == '' ? 'na' : $data[19];
        $sms_notes = $data[20];
        $email_date = $data[21];
        $email_outcome = $data[22]  == '' ? 'na' : $data[22];
        $email_notes = $data[23];

        if($date_added == ''){
          $errors += 1;
          return 'Date Added cannot be empty';
          exit;
        }elseif($first_name == ''){
          return 'First Name cannot be empty';
          exit;
        }elseif($last_name == ''){
          $errors += 1;
          return 'Last Name cannot be empty';
          exit;
        }elseif($phone == ''){
          $errors += 1;
          return 'Phone cannot be empty';
          exit;
        }

      }

      fclose($file);

      if($errors >= 1){
        return '0';
        exit;
      }else{

        $file = fopen($path.'/'.$filename, "r");
        $data = fgetcsv($file, 1000, ",");

        while (($data = fgetcsv($file, 1000, ",")) !== FALSE)
        {
          $date_added = $data[0];
          $status = $data[1]  == '' ? 'New' : $data[1];
          $first_name = $data[2];
          $last_name = $data[3];
          $seller = $data[4];
          $buyer = $data[5];
          $lead_source = $data[6];
          $email = $data[7];
          $phone = $data[8];
          $address = $data[9];
          $notes_tags = $data[10];
          $prioritize_scrub = $data[11];
          $first_dial_date = $data[12];
          $first_dial_outcome = $data[13] == '' ? 'na' : $data[13];
          $first_dial_notes = $data[14];
          $second_dial_date = $data[15];
          $second_dial_outcome = $data[16]  == '' ? 'na' : $data[16];
          $second_dial_notes = $data[17];
          $sms_date = $data[18];
          $sms_outcome = $data[19]  == '' ? 'na' : $data[19];
          $sms_notes = $data[20];
          $email_date = $data[21];
          $email_outcome = $data[22]  == '' ? 'na' : $data[22];
          $email_notes = $data[23];

          $uploaded_leads_data = UploadedLead::updateOrCreate(['file_id' => $request->file_id, 'address' => $address],
          [
            'date_added' => date("Y-m-d h:i:s", strtotime($date_added)),
            'status' => $status,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'seller' => $seller,
            'buyer' => $buyer,
            'lead_source' => $lead_source,
            'email' => $email,
            'phone' => $phone,
            'address' => $request->address,
            'notes_tags' => $notes_tags,
            'prioritize_scrub' => $prioritize_scrub,
            'first_dial_date' => $first_dial_date,
            'first_dial_outcome' => $first_dial_outcome,
            'first_dial_notes' => $first_dial_notes,
            'second_dial_date' => $second_dial_date,
            'second_dial_outcome' => $second_dial_outcome,
            'second_dial_notes' => $second_dial_notes,
            'sms_date' => $sms_date,
            'sms_outcome' => $sms_outcome,
            'sms_notes' => $sms_notes,
            'email_date' => $email_date,
            'email_outcome' => $email_outcome,
            'email_notes' =>$email_notes
          ]
        );
      }

      fclose($file);

    }

    $this->removeFiles($filename);

    return '1';

  }

  public function importSaveAllOld(Request $request){


    $path = storage_path('csv_uploads');
    $files = glob("$path/*.csv");

    foreach($files as $file) {
      $ads = array();
      if (($handle = fopen($file, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 4096, ",")) !== FALSE) {

          $post_title = $data[0];
          $description = $data[1];
          $category_id = $data[18];
          $name = $data[2];
          $email = $data[3];
          $phone = $data[4];
          $country = $data[5];
          $state_name = $data[6];
          $city_name = $data[7];
          $zipcode = $data[8];
          $address = $data[9];
          $vid_url = $data[11];
          $feature1 = $data[14];
          $feature2 = $data[15];
          $escrow_amount = $data[12];
          $referral_fee = $data[13];
          $price_range = $data[17];
          $referral_name_info = $data[22];
          $referral_email = $data[23];
          $referral_phone = $data[24];
          $referral_fax = $data[25];
          $referral_address = $data[26];
          $image_name = $data[10];
          $lead_status = $data[16];
          $category_type = $data[19];
          $cat_type_status = $data[20];
          $contract_signed = $data[21];
          $feature1_data = [];
          $feature1_data = array();
          $active = 0;

          $feature1_array = explode(',', $feature1);

          foreach($feature1_array as $n=>$v){
            $d = explode(':', $v);
            $feature1_data[$n] = $d[1];
          }

          $feature2_array = explode(',', $feature2);
          foreach($feature2_array as $k=>$fs2){
            $d2 = explode(':', $fs2);
            if($d2[1] != '0'){
              $feature2_data[] = $d2[0];
            }
          }

          if($country == 'Canada'){
            $ctr = '38';
          }else{
            $ctr = '231';
          }

          $state = State::where('state_name', '=', trim($state_name))->where('country_id', '=', $ctr)->first();
          if(!$state){
            return 'State is not found '.$state_name;
            exit;
          }
          $stid = $state->id;
          $city = City::where('city_name', 'LIKE', trim($city_name))->where('state_id', '=', $stid)->first();
          if(!$city){
            return 'City is not found: '.trim($city_name).', '.trim($state_name);
            exit;
          }

          $user = User::where('email', '=', $email)->first();
          $mycity = ($city) ? $city->id : '';

          if($user){
            $user_id = $user->id;
            $active = '1';
          }else{

            $new_user = [
              'name' => $name,
              //'first_name' => $user_name_exp[0],
              //'last_name'  => $user_name_exp[1],
              'email' => $email,
              'phone' => $phone,
              'sms_activation_code' => ''
            ];

            $new_usr = User::create($new_user);
            $user_id = $new_usr->id;
            $active = '0';
          }

          $category = Category::whereId($category_id)->first();

          $ads = [
            'title' => $post_title,
            'slug' => unique_slug($post_title),
            'description' => $description,
            'sub_category_id' => $category_id,
            'seller_name' => $name,
            'seller_email' => $email,
            'seller_phone' => $phone,
            'country_id'  => $ctr,
            'state_id'  => $stid,
            'city_id' => $mycity,
            'zipcode' => $zipcode,
            'address' => $address,
            'video_url' => $vid_url,
            'escrow_amount' => $escrow_amount,
            'referral_fee' => $referral_fee,
            'feature_1' =>  serialize($feature1_data),
            'feature_2' => serialize($feature2_data),
            'price_range' => $price_range,
            'status' => $lead_status,
            'user_id' => $user_id,
            'category_type' => $category_type,
            'cat_type_status' => $cat_type_status
          ];

          $ad = Ad::create($ads);

          if($ad){
            $refs = [
              'ad_id' => $ad->id,
              'referral_name' => $referral_name_info,
              'referral_contact_email' => $referral_email,
              'referral_contact_phone' => $referral_phone,
              'referral_contact_fax' => $referral_fax,
              'referral_contact_address' => $referral_address,
            ];

            $title_company = ReferralContactInfo::create($refs);
            $image = [
              'user_id' => $user_id,
              'ad_id' => $ad->id,
              'media_name' => $image_name,
              'type'  => 'image',
              'storage' => 'public',
              'ref' => 'ad'
            ];
            $media = Media::create($image);
          }
        }

        fclose($handle);
      } else {
        echo "Could not open file: " . $file;
      }
    }

    //end for
    $this->removeFiles();
    Session::flash('success', 'New leads have been saved successfully');
    return 1;
  }

  public function importReset(){
    $filename = request()->filename;
    $file_id = request()->fileid;
    $this->removeFiles($filename);
    return redirect()->route('import-leads', ['file_id' => $file_id]);
  }

  public function removeFiles($filename){
    $path = storage_path('csv_uploads');
    if (!unlink($path.'/'.$filename)) {
      echo ("Error deleting $filename");
    }
  }


  public function getAllUsers(Request $request){
    $ad = Ad::where('id', '=', $request->id)->first();

    $users = User::where('active_status', '=', '1')->get();

    $data[0] = $ad->seller_name;
    $data[1] = $users;

    return $data;
  }

  public function saveNewAgent(Request $request){
    $user = User::whereId($request->user)->first();
    $ad = Ad::whereId($request->id)->first();

    if($ad->user_id == $request->user){
      return '3';
    }

    $ad->user_id = $user->id;
    $ad->seller_name = $user->first_name.' '.$user->last_name;
    $ad->seller_email = $user->email;
    $ad->save();

    if($ad){
      if($request->test_postman == '1'){
        $ad = Ad::where('id', '=', $request->id)->first();
        return $ad;
        exit;
      }
      Session::flash('success','Agent/poster for this lead has been changed. You may proceed to make changes to the lead.');
      return '1';
    }

    return '0';
  }

  public function testReassignUser(Request $request){
    $new_user = array (
      'first_name' => 'Test User',
      'last_name' => 'Tester',
      'email' => 'testuser123@test.com',
      'password' => 'testpass',
      'password_confirmation' => 'testpass',
      'address' => 'Valenzuela City',
      'phone' => '995-064-9875',
      'state' => 'California',
      'company_name' => 'RealtoNeato',
      'representative_name' => 'Reynaldo Tugadi',
      'representative_email' => 'reynaldo.tugadi@gmail.com',
      'brokerage_name' => 'BrokersHaven',
      'mls' => '16',
      'brokerage_addr' => 'Broeker Street',
      're_license_number' => 'REN134',
      'from' => 'a',
      'active_status' => '1',
      'zip_code' => '90210',
      'range' => '10',
      'sms_notify' => '0',
      'brokerage_contact_person' => 'Modioboy Tugadi',
      'brokerage_email' => 'modiboy@gmail.com',
      'brokerage_phone' => '9999999999',
      'test_postman' => '1',
      'state_name' => 'California',
      'city_name' => 'Beverly Hills',
      'tc_checked' => '1',
      'city' => '42847',
      'country' => '231',
      'send_account_info' => '0'
    );

    //create a user
    $req_user = Request::create('/register', 'POST', $new_user);
    $res_user = app()->handle($req_user);
    $user_resp = $res_user->getContent();
    $resp_user = json_decode($user_resp);
    $new_user_id = $resp_user->id;
    $new_user_email = $resp_user->email;
    $new_user_name = $resp_user->name;

    $sample_lead = array (
      '_token' => '7J38AILRyTIvS4GhDDUAOWRLB44sPSP3Wya36l7Y',
      'cat_type' => 'other',
      'cat_type_q' => 'no',
      'date_contacted_qualified' => NULL,
      'referral_fee' => '10%',
      'price' => '0.00',
      'category' => '11',
      'listing_title' => 'This is Newton\'s test edit',
      'feature' =>
      array (
        0 => '3',
        1 => '1',
        2 => '1188',
        3 => '1',
        4 => '1',
        5 => '0',
      ),
      'listing_description' => '<p>sfsfdsf df dffdf</p>',
      'feature2' =>
      array (
        0 => 'security',
        1 => 'laundry',
        2 => 'garden',
        3 => 'swimming',
      ),
      'price_range' => '< $50K',
      'video_url' => NULL,
      'referral_first_name' => 'Renato',
      'referral_last_name' => 'Tugadi',
      'referral_contact_email' => 'modioboy@gmail.com',
      'referral_contact_phone' => '7777777777',
      'referral_contact_address' => '123 ABC Street, Test',
      'country_name' => 'United States',
      'state_name' => 'California',
      'city_name' => 'Beverly Hills',
      'zipcode' => '90210',
      'country' => '231',
      'state' => '3924',
      'city' => '42847',
      'referral_contact_fax' => NULL,
      'seller_name' => $new_user_name,
      'seller_email' => $new_user_email,
      'test_postman' => '1',
      'user_id' => $new_user_id
    );

    //crete a test lead
    $req_ad = Request::create('/post-new', 'POST', $sample_lead);
    $res_ad = app()->handle($req_ad);
    $ad_create_response = $res_ad->getContent();

    //new lead is returned
    $new_ad_id = ['id' => $ad_create_response];

    $data = [
      'id' => $new_ad_id,
      'user' => $request->id,
      'test_postman' => '1',
    ];

    //reassign a user in the lead
    $reassign_ad_user = Request::create('/save-new-agent', 'POST', $data);
    $res_ad = app()->handle($reassign_ad_user);
    $ad_create_response = $res_ad->getContent();
    $ad_updated = $ad_create_response;

    $this->deleteUserById($new_user_id);

    return $ad_updated;
  }

}
