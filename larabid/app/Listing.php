<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Listing extends Model
{
  protected $fillable = [
      'user_id', 'title', 'description', 'category_id', 'slug', 'seller_name', 'seller_email', 'seller_phone', 'address', 'country_id', 'state_id', 'city_id', 'video_url', 'referral_contact_info', 'escrow_amount'];
}
