<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TcApprovalSenderTaskModel extends Model
{
    protected $fillable = ['contract_id', 'email_sent_to_buyer', 'email_sent_to_seller', 'reason'];
}
