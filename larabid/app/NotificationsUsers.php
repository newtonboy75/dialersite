<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationsUsers extends Model
{
    protected $table = 'notifications_users';
    protected $fillable = ['type', 'recipient', 'sender', 'contents', 'user_read', 'subject'];
}
