<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdsSaveByUser extends Model
{
    protected $table = 'leads_saved';
    protected $fillable = ['ad_id', 'user_id', 'created_at', 'updated_ad'];
}
