<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationNewRegistration extends Model
{
    protected $fillable = ['user_id', 'notified'];
}
