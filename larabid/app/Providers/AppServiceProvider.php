<?php

namespace App\Providers;

use App\Country;
use App\Post;
use App\Services\Payments\DocusignPayment;
use App\Services\Payments\PayPalPayment;
use App\Services\Payments\StripePayment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //register Blade directives
        $this->bladeDirectives();
        
        //Check if app is demo?
        if (env('APP_DEMO') == true) {
            //Set default country to US if demo
            if (!session('country')) {
                $country = Country::whereCountryCode('US')->first();
                if ($country) {
                    //Setting default country
                    //session(['country' => $country->toArray()]);
                }
            }
        }
        
        //ensure that table options exists
        if (!Schema::hasTable('options')) {
            return;
        }
        
        /**
         * Set dynamic configuration for third party services
         */
        $amazonS3Config = [
            'filesystems.disks.s3' =>
                [
                    'driver' => 's3',
                    'key'    => get_option('amazon_key'),
                    'secret' => get_option('amazon_secret'),
                    'region' => get_option('amazon_region'),
                    'bucket' => get_option('bucket'),
                ],
        ];
        $facebookConfig = [
            'services.facebook' =>
                [
                    'client_id'     => get_option('fb_app_id'),
                    'client_secret' => get_option('fb_app_secret'),
                    'redirect'      => url('login/facebook-callback'),
                ],
        ];
        $googleConfig   = [
            'services.google' =>
                [
                    'client_id'     => get_option('google_client_id'),
                    'client_secret' => get_option('google_client_secret'),
                    'redirect'      => url('login/google-callback'),
                ],
        ];
        $twitterConfig  = [
            'services.twitter' =>
                [
                    'client_id'     => get_option('twitter_consumer_key'),
                    'client_secret' => get_option('twitter_consumer_secret'),
                    'redirect'      => url('login/twitter-callback'),
                ],
        ];
        config($amazonS3Config);
        config($facebookConfig);
        config($googleConfig);
        config($twitterConfig);
        
        view()->composer(
            '*',
            function ($view) {
                $header_menu_pages   = Post::whereStatus('1')->where('show_in_header_menu', 1)->get();
                $show_in_footer_menu = Post::whereStatus('1')->where('show_in_footer_menu', 1)->get();
                
                $enable_monetize = get_option('enable_monetize');
                $loggedUser      = null;
                if (Auth::check()) {
                    $loggedUser = Auth::user();
                }
                
                $current_lang = current_language();
                
                $view->with(
                    [
                        'lUser'               => $loggedUser,
                        'enable_monetize'     => $enable_monetize,
                        'header_menu_pages'   => $header_menu_pages,
                        'show_in_footer_menu' => $show_in_footer_menu,
                        'current_lang'        => $current_lang,
                    ]
                );
            }
        );
    }
    
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app
            ->when(AdsController::class)
            ->needs(PaymentInterface::class)
            ->give(
                function () {
                    //check the route in order to give right instance instead PaymentInterface when binding
//                    if (request('payment_method') === 'paypal') {
//                        return new PayPalPayment();
//                    }
//
//                    if (request('payment_method') === 'stripe') {
//                        return new StripePayment();
//                    }
//
//                    if (request('docusign')) {
//                        return new DocusignPayment();
//                    }
    
                    switch(request('payment_method')) {
                        case 'paypal':
                            return new PayPalPayment();
                            break;
                        case 'stripe':
                            return new StripePayment();
                            break;
                        case 'docusign':
                            return new DocusignPayment();
                            break;
                    }
                }
            );
    }
    
    /**
     * Register the blade directives
     *
     * @return void
     */
    private function bladeDirectives()
    {
        /**
         * Creating (declaring) PHP variables
         *
         * Usage:
         * @set($variable, value)
         */
        Blade::directive(
            'set',
            function ($expression) {
                list($variable, $value) = explode(', ', str_replace(['(', ')'], '', $expression));
                
                return "<?php {$variable} = {$value}; ?>";
            }
        );
        
        /**
         * PHP eplode() function
         *
         * Usage:
         * @explode($delimiter, $string)
         */
        Blade::directive(
            'explode',
            function ($expression) {
                list($delimiter, $string) = explode(', ', str_replace(['(', ')'], '', $expression));
                
                return "<?php echo explode({$delimiter}, {$string}); ?>";
            }
        );
        
        /**
         * PHP implode() functions
         *
         * Usage:
         * @implode($delimiter, $array)
         */
        Blade::directive(
            'implode',
            function ($expression) {
                list($delimiter, $array) = explode(', ', str_replace(['(', ')'], '', $expression));
                
                return "<?php echo implode({$delimiter}, {$array}); ?>";
            }
        );
        
        /**
         * PHP var_dump() function
         *
         * Usage:
         * @var_dump($variable)
         */
        Blade::directive(
            'var_dump',
            function ($expression) {
                return "<?php var_dump({$expression}); ?>";
            }
        );
        
        /**
         * Laravel dd() function
         *
         * Usage:
         * @dd($variable)
         */
        Blade::directive(
            'dd',
            function ($expression) {
                return "<?php dd({$expression}); ?>";
            }
        );
    }
}
