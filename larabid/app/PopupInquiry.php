<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PopupInquiry extends Model
{
    protected $table = 'popup_inquiry';
    protected $fillable = ['name', 'company', 'email'];
    public $timestamps = false;

}
