<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactQuery extends Model
{
  protected $table = 'contact_queries';
  protected $fillable = ['name', 'email', 'message', 'created_at'];
}
