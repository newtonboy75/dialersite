<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class ProcessSendEmailByTc implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
     public $seller;
     public $referral_name;
     public $email;
     public $reason;
     public $url;
     public $contract_id;
     public $listing_name;
     public $repost;
     public $tries = 5;

    public function __construct($seller, $referral_name, $email, $reason, $url, $contract_id, $listing_name, $repost)
    {
        $this->seller = $seller;
        $this->referral_name = $referral_name;
        $this->email = $email;
        $this->reason = $reason;
        $this->url = $url;
        $this->contract_id = $contract_id;
        $this->listing_name = $listing_name;
        $this->repost = $repost;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $email = array($this->email);

      Mail::send('emails.approve_by_tc', ['seller' => $this->seller, 'referral_name' => $this->referral_name, 'reason' => $this->reason, 'url' => $this->url, 'contract_id' => $this->contract_id, 'listing_name' => $this->listing_name, 'repost' => $this->repost], function ($message) use ($email)
      {
        $message->from('info@tymbl.com','Tymbl Team');
        $message->to($email);
        $message->subject("Listing Contract");
       });
    }
}
