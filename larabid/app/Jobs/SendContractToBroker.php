<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Broker;
use App\ReferralContactInfo;
use App\User;
use Illuminate\Support\Facades\Log;
use LaravelEsignatureWrapper;

class SendContractToBroker implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $broker_name;
    protected $referral_info;
    protected $user;
    protected $contract_id;
    protected $file;
    protected $transaction_id;
    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Broker $broker_name, ReferralContactInfo $referral_info, User $user, $contract_id, $transaction_id)
    {
      $this->broker_name = $broker_name;
      $this->$referral_info = $referral_info;
      $this->user = $user;
      $this->contract_id = $contract_id;
      $this->transaction_id = $transaction_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mail $mail)
    {
      //Log::info('helo there '.$this->broker_name->name.' - contractid is '.$this->contract_id);
      $email = $this->broker_name->broker_email;

      if (file_exists('downloads/'.$this->contract_id.'.pdf')) {
        $file = public_path('downloads/'.$this->contract_id.'.pdf');
      }else{
        $url = LaravelEsignatureWrapper::getContract($this->contract_id);
        $pdf_url = $url['data']['contract_pdf_url'];

        if($pdf_url != ''){
          $pdf_file = file_get_contents($pdf_url);
          file_put_contents(public_path('downloads/'.$this->contract_id.'.pdf'), $pdf_file);
        }

        $this->file = public_path('downloads/'.$this->contract_id.'.pdf');
      }

      $mail::send('emails.send_contract_to_broker', ['broker' => $this->broker_name, 'referral_info' => $this->referral_info, 'user' => $this->user, 'transaction_id' => $this->transaction_id], function ($m) use ($email) {
        $m->from('info@tymbl.com', 'Tymbl Team');
        $m->to($email)->subject('Referral Agreement');
        $m->attach($this->file, ['mime' => 'application/pdf']);
      });
    }
}
