<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class ProcessSendContract implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $email;
    public $id;
    public $buyer;
    public $seller;
    public $listing_title;
    public $payout_sender_id;
    public $payout;
    public $broker;
    public $subject;
    public $tries = 5;

    public function __construct($email, $id, $buyer, $seller, $listing_title, $payout_sender_id, $payout, $broker, $subject)
    {
        $this->email = $email;
        $this->id = $id;
        $this->buyer = $buyer;
        $this->seller = $seller;
        $this->listing_title = $listing_title;
        $this->payout_sender_id = $payout_sender_id;
        $this->payout = $payout;
        $this->broker = $broker;
        $this->subject = $subject;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $email = array($this->email);
      Mail::send('emails.contract_signed', ['id' => $this->id, 'buyer' => $this->buyer, 'seller' => $this->seller, 'listing_title' => $this->listing_title, 'payout_id' => $this->payout_sender_id, 'payout' => $this->payout, 'broker' => $this->broker], function ($message) use ($email)
      {
        $message->from('tymblapp@gmail.com','Tymbl Team');
        $message->to($email);
        $message->subject($this->subject);
       });
    }
}
