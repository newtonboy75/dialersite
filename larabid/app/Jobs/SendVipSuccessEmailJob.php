<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Ad;
use App\ReferralContactInfo;
use App\User;
use Illuminate\Support\Facades\Log;
use LaravelEsignatureWrapper;

class SendVipSuccessEmailJob implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
  protected $ad_title;
  protected $ad_id;
  protected $contract_id;
  protected $file;
  protected $transaction_id;
  public $tries = 5;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct($contract_id, $transaction_id, $ad_title, $ad_id)
  {
    $this->contract_id = $contract_id;
    $this->transaction_id = $transaction_id;
    $this->ad_title = $ad_title;
    $this->ad_id = $ad_id;
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle(Mail $mail)
  {
    $email = 'support@tymbl.com';

    if (file_exists('downloads/'.$this->contract_id.'.pdf')) {
      $file = public_path('downloads/'.$this->contract_id.'.pdf');
    }else{
      $url = LaravelEsignatureWrapper::getContract($this->contract_id);
      $pdf_url = $url['data']['contract_pdf_url'];

      if($pdf_url != ''){
        $pdf_file = file_get_contents($pdf_url);
        file_put_contents(public_path('downloads/'.$this->contract_id.'.pdf'), $pdf_file);
      }

      $this->file = public_path('downloads/'.$this->contract_id.'.pdf');
    }

    $mail::send('emails.notify_support_vip_lead_success', ['ad_title' => $this->ad_title, 'ad_id' => $this->ad_id, 'transaction_id' => $this->transaction_id], function ($m) use ($email) {
      $m->from('info@tymbl.com', 'Tymbl Team');
      $m->to($email)->subject('New VIP Referral Agreement');
      $m->attach($this->file, ['mime' => 'application/pdf']);
    });
  }
}
