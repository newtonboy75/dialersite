<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class ProcessSendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $email;
    public $url;
    public $code_id;
    public $title_company_name;
    public $buyer_name;
    public $subject;
    public $tries = 5;

    public function __construct($email, $url, $code_id, $title_company_name, $buyer_name, $subject)
    {
        $this->email = $email;
        $this->url = $url;
        $this->code_id = $code_id;
        $this->title_company_name = $title_company_name;
        $this->buyer_name = $buyer_name;
        $this->subject = $subject;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $email = array($this->email);
      Mail::send('emails.contract', ['url' => $this->url, 'code' => $this->code_id, 'buyer_name' => $this->buyer_name, 'title_company_name' => $this->title_company_name], function ($message) use ($email)
      {
        $message->from('info@tymbl.com.com','Tymbl Team');
        $message->to($email);
        $message->subject($this->subject);
       });
    }
}
