<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\User;
use Illuminate\Support\Facades\Mail;

class NewRegistrationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $tries = 5;
    public $user;
    protected $user_email;
    protected $user_name;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->user_email = $this->user->email;
        $this->user_name = $this->user->name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $email = array($this->user_email);

      $mail = Mail::send('emails.welcome', ['user' => $this->user], function ($m) use ($email) {
        $m->from('info@tymbl.com', 'Tymbl Team test');
        $m->to($email, $this->user_name )->subject('Please activate your Tymbl account');
      });
    }
}
