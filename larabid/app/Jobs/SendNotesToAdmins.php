<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\User;
use Illuminate\Support\Facades\Mail;

class SendNotesToAdmins implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $lead_note_body;
    protected $ad_title;
    protected $ad_id;
    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($lead_note_body, $ad_title, $ad_id)
    {
      $this->lead_note_body = $lead_note_body;
      $this->ad_title = $ad_title;
      $this->ad_id = $ad_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mail $mail)
    {
        $admins = User::where('user_type', '=', 'admin')->where('active_status', '=', '1')->get();
        $ad_title = $this->ad_title;

        foreach($admins as $admin){
          $email = $admin->email;

          $mail::send('emails.send_note_to_admin', ['email' => $email, 'notes' => $this->lead_note_body, 'ad_title' => $ad_title, 'ad_id' => $this->ad_id], function ($m) use ($email, $ad_title) {
            $m->from('info@tymbl.com', 'Tymbl Team');
            $m->to($email)->subject('New note for '.$ad_title);
          });



        }
    }
}
