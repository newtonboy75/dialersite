<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendNewRegistrationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $user;
    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

      Mail::send('emails.new_user', ['name' => $this->user->name, 'email' => $this->user->email, 'interest' => $this->user->interest, 'zip_code' => $this->user->zip_code], function ($message) {
        $message->from('newtonboy@gmail.com', 'Tymbl');
        $message->to('info@tymbl.com, "New Message from Contact Us Page");
        $message->subject("New Registration");
      });
    }
}
