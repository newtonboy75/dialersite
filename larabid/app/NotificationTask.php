<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationTask extends Model
{
    protected $fillable = ['listing_id', 'user_notified'];
}
