<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payouts extends Model
{
    protected $fillable = ['list_id', 'sender_id', 'amount', 'type'];
}
