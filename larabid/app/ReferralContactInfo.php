<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferralContactInfo extends Model
{
    protected $fillable = [
        'ad_id',
        'referral_name',
        'referral_first_name',
        'referral_last_name',
        'referral_contact_email',
        'referral_contact_phone',
        'referral_contact_fax',
        'referral_contact_address'
    ];

    public function referral_info()
    {
        return $this->belongsTo(Ad::class);
    }
}
