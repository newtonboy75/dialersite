<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TitleCompanyInfoLTB extends Model
{
    protected $table = 'title_company_info_ltb';

    protected $fillable = ['ad_id', 'user_id', 'company_name', 'representative_name', 'representative_email'];
}
