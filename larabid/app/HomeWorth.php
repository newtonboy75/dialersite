<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeWorth extends Model
{
    protected $table = 'homeworth';
    protected $fillable = ['first_name', 'last_name', 'email', 'phone', 'address'];
}
