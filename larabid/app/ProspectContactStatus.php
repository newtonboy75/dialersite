<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ProspectContactStatus extends Model
{
    const CS_CONTACTED_CUSTOMER     = 'contacted_customer';
    const CS_MET_CUSTOMER           = 'met_customer';
    const CS_IN_ESCROM              = 'in_escrow';
    const CS_CLOSING                = 'closing';
    const CS_LONGTERM_NUTURE        = 'longterm_nuture';

    const CS_LIST = [
        self::CS_CONTACTED_CUSTOMER,
        self::CS_MET_CUSTOMER,
        self::CS_IN_ESCROM,
        self::CS_CLOSING,
        self::CS_LONGTERM_NUTURE
    ];

    const CS_DESCRIPTION = [
        self::CS_CONTACTED_CUSTOMER => 'I contacted customer',
        self::CS_MET_CUSTOMER => 'I met with customer',
        self::CS_IN_ESCROM => 'We are working on the offer',
        self::CS_CLOSING => 'We are in closing!',
        self::CS_LONGTERM_NUTURE => 'Moved lead to long-term nurture'
    ];

    protected $table = 'prospect_contact_status';

    protected $fillable = ['reserved_lead_id', 'lead_type', 'current_prospect_contact_status', 'user_id'];

    public function setUpdatedAt($value)
    {
        $this->{static::UPDATED_AT} = $value;
    }

    public function prospectContactStatusOptionsSaved()
    {
        return $this->hasMany(
            ProspectContactStatusOptionsSaved::class,
            'prospect_contact_status_id',
            'id')->orderBy('id', 'asc');
    }
}
