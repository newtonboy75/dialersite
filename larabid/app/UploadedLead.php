<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadedLead extends Model
{
    /**
     * Attributes that can be mass distributed.
     * @var array
     */
    protected $fillable = [
        'file_id',
        'date_added',
        'status',
        'first_name',
        'last_name',
        'seller',
        'buyer',
        'lead_source',
        'email',
        'phone',
        'address',
        'notes_tags',
        'prioritize_scrub',
        'first_dial_date',
        'first_dial_outcome',
        'first_dial_notes',
        'second_dial_date',
        'second_dial_outcome',
        'second_dial_notes',
        'sms_date',
        'sms_outcome',
        'sms_notes',
        'email_date',
        'email_outcome',
        'email_notes',
        'user_id',
    ];

    /**
     * The table associated with the model.
     * @var string */
    protected $table = 'uploaded_files_status';

    /**
     * Determines the need for time stamps for the model.
     * @var bool */
    public $timestamps = false;

    /**
     * Attributes omitted in model serialization.
     * @var array */
    protected $hidden = ['id'];

    public function uploaded_lead_sms()
    {
        return $this->hasOne(UploadedLeadSms::class, 'uploaded_lead_id', 'id');
    }
}
