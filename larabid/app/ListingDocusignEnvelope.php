<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListingDocusignEnvelope extends Model
{
    protected $fillable = ['docusign_id', 'listing_id'];
}
