<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListingContracts extends Model
{
    const LS_DEFAULT    = '0';          //  List status "DEFAULT"
    const LS_ACTIVE     = '1';          //  List status "ACTIVE"
    const LS_PENDING    = '2';          //  List status "PENDING"
    const LS_RESERVED   = '3';          //  List status "RESERVED"

    const LS_STATUSES = [
        self::LS_DEFAULT,
        self::LS_ACTIVE,
        self::LS_PENDING,
        self::LS_RESERVED,
    ];

    protected $fillable = [
        'contract_id',
        'listing_id',
        'buyer_id',
        'user_contract_id',
        'code_id',
        'list_status',
        'follow_up'
    ];

    public function ad()
    {
        return $this->hasOne(Ad::class, 'id', 'listing_id');
    }

    public function set_contract_status($status)
    {
        $this->list_status = $status;
        $this->save();
    }

    public function titleCompanyInfoLTB()
    {
        return $this->hasMany(TitleCompanyInfoLTB::class, 'ad_id', 'listing_id');
    }

    public function referralContactInfo()
    {
        return $this->hasOne(ReferralContactInfo::class, 'ad_id', 'listing_id');
    }

    public function referralContactInfos()
    {
        return $this->hasMany(ReferralContactInfo::class, 'ad_id', 'listing_id');
    }

    public function transactionReports()
    {
        return $this->hasMany(TransactionReports::class, 'ad_id', 'listing_id');
    }

    public function leadNote()
    {
        return $this->hasMany(LeadNote::class, 'ad_id', 'listing_id');
    }

    public function prospect_contact_status()
    {
        return $this->hasOne(ProspectContactStatus::class, 'reserved_lead_id', 'id')
            ->orderBy('updated_at', 'desc');
    }

    public function buyer()
    {
        return $this->hasOne(User::class, 'id', 'buyer_id');
    }
}
