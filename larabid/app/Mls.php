<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mls extends Model
{
    protected $fillable = ['name', 'state'];
}
