<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBrokerNotification extends Model
{
    protected $table = 'user_broker_notification';
    protected $fillable = ['user_id', 'check_again'];
}
