<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = ['user_id', 'email', 'phone', 'country_id', 'state_id', 'city_id', 'zip_id', 'loc_range'];
}
