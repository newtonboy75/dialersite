<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Broker extends Model
{
    protected $fillable = ['user_id', 'name', 'address', 'broker_contact_person', 'broker_email', 'broker_phone', 'broker_verified', 'verified_on'];
}
