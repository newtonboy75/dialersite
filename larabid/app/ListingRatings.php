<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListingRatings extends Model
{
  protected $fillable = ['listing_id', 'rating', 'user_id', 'low_rating_reason', 'other'];
}
