<?php

return [
    'email'         => 'pavel.stepanov@gmail.com',
    'password'      => 'tymbl18',
    'integratorKey' => 'ff6cbf0b-3bff-4f72-95a0-3cc5edaa0b81',
    'templateId'    => '195fc338-7395-416f-b20a-c25fe4d10074',
    
    'firstTemplateRole'  => 'buyers',
    'secondTemplateRole' => 'sellers',
    
    'url'       => 'https://demo.docusign.net/restapi/v2/login_information',
    'returnUrl' => 'http://larabid.test/docusign-callback',
    
    'emailSubject' => 'Please sign the contract',
    
    'statuses' => [
        'draft'                              => 1,
        'sent'                               => 2,
        'delivered'                          => 3,
        'waiting for Others'                 => 4,
        'needs To Sign'                      => 5,
        'needs to View'                      => 6,
        'correcting'                         => 7,
        'voided'                             => 8,
        'declined'                           => 9,
        'completed'                          => 10,
        'expired'                            => 11,
        'delivery failure'                   => 12,
        'authentication failed'              => 13,
        'completed or voided - purging soon' => 14,
        'completed or voided - purged'       => 15,
    ],
];