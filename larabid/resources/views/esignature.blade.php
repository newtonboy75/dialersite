@extends('layouts.app')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('content')
    <p>&nbsp;</p>
    @if($check_id_error == '')
    <div id="framewrapper" rel="{{$id}}">
      <iframe src="{{$contract_url}}" name="esignatures-io-iframe" id="esignature-iframe" frameborder="0" width="100%" height="1800"></iframe>
    </div>
    @else
      <div class="text-center invalid-id">{{$check_id_error}}
        <p>&nbsp;</p><p><a href="#" onclick="javascript: window.history.back()">Back</a></p>
      </div>
    @endif
@endsection
