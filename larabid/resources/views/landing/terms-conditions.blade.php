@extends('landing.layout.main')
@section('content')
	<header>
		<div class="container">
			<a class="logo" href="/">
				<img src="{{url('assets/img/landing/logo.png')}}" alt="Tymbl" class="mx-auto d-block">
			</a>
			<div class="row header-content justify-content-md-center">
				<div class="col-md-12 page-header">
					<h2>Terms And Conditions</h2>
				</div>
			</div>
		</div>
	</header>
	<section class="about">
		<div class="container">
			<div class="row">
				<div class="col-md-2">&nbsp;</div>
				<div class="col-md-8 center-block">
					<p>Please read these Terms of Use ("Terms") carefully before using the http:// www.tymbl.com website and the Tymbl services operated by Tymbl Technologies, Inc ("we", “our”).</p>
					<p>Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.</p>
					<p>By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.</p>
					<h3>Purchases</h3>
					<p>If you wish to purchase any product or service made available through the Service ("Purchase"), you may be asked to supply certain information relevant to your Purchase including and not limited to the following: name, address, e-mail address, and telephone number); information about transactions carried out over this website (including credit card payments); information that you provide for the purpose of subscribing to the Tymbl Technologies, Inc services; and any other information that you send to Tymbl Technologies, Inc ("Personal Information").</p>
					<h3>Termination</h3>
					<p>We may terminate or suspend access to our Service immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.</p>
					<p>All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.</p>
					<h3>Links to Other Web Sites</h3>
					<p>Our Service may contain links to third-party web sites or services that are not owned or controlled by Tymbl Technologies, Inc.</p>
					<p>Tymbl Technologies, Inc has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party web sites or services. You further acknowledge and agree that Tymbl Technologies, Inc shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such web sites or services.</p>
					<h3>Changes</h3>
					<p>We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 30 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.</p>
				</div>
				<div class="col-md-2">&nbsp;</div>
			</div>
		</div>
	</section>
  @endsection
