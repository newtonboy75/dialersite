@extends('landing.layout.main')
@section('content')
<header>
  <div class="container">
    <a class="logo" href="/">
      <img src="{{url('assets/img/landing/logo.png')}}" alt="Tymbl" class="mx-auto d-block">
    </a>
    <div class="coming-soon">
      <h1>Coming Soon</h1>
    </div>
    <div class="row header-content justify-content-md-center">
      <div class="col-md-12 tagline">
        <h2>the first real estate</h2>
        <h1>Leads and Referrals exchange</h1>
      </div>
      <div class="col-md-9 register-form">
        @include('admin.flash_msg')
        <h3>Register to be notified</h3>
        <h4>When referrals and leads are avaible in your area</h4>
        <form method="POST" action="preregister" class="form-inline form-area">
          {!! csrf_field() !!}
          <div class="form-group col-md-6 col-sm-12 col-xs-12">
            <label class="sr-only" for="name">Name</label>
            <input type="text" class="form-control mb-2" id="name" aria-describedby="nameHelp" placeholder="Name" name="name" value="{{ old('name') }}" onclick="actionClick()">
            {!! $errors->has('name')? '<p class="help-block">'.$errors->first('name').'</p>':'' !!}
          </div>

          <div class="form-group col-md-6 col-sm-12 col-xs-12">
            <select class="form-control mb-2" id="state state_select" name="state" required="">
              <option disabled="" selected="">Select State </option>
              @foreach($all_states as $state_i)
                <option value="{{$state_i->id}}" {{ old('state') == $state_i->state_name ? 'selected' : '' }}>{{$state_i->state_name}}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group col-md-6 col-sm-12 col-xs-12">
            <label class="sr-only" for="email">Name</label>
            <input type="email" name="email" value="{{ old('email') }}" class="form-control mb-2" id="email" aria-describedby="emailHelp" placeholder="Email Address" onclick="actionClick()">
            {!! $errors->has('email')? '<p class="help-block">'.$errors->first('email').'</p>':'' !!}
          </div>

          <div class="form-group col-md-6 col-sm-12 col-xs-12">

            <select class="form-control mb-2" id="city_select" name="city" required="">
              <option selected="" disabled="">Select City</option>
              @foreach($all_cities as $city_i)
                <option value="{{$city_i->id}}" {{ old('city') == $city_i->city_name ? 'selected' : '' }}>{{$city_i->city_name}}</option>
              @endforeach
              </select>
          </div>

          <div class="form-group col-md-6 col-sm-12 col-xs-12">
            <label class="sr-only" for="interested">Name</label>
            <select class="form-control mb-2" id="interested" name="interest" onclick="actionClick()">
              <option value="1">Interested in referrals in my area</option>
              <option value="2">Interested in posting a referral</option>
              <option value="3">Both</option>
            </select>
          </div>

          <div class="form-group col-md-6 col-sm-12 col-xs-12">
            <select class="form-control mb-2" id="zipcode_select" name="zipcode">
              <option selected="" disabled="">Select Zipcode</option>
              @foreach($all_zips as $zip_i)
                <option value="{{$zip_i->zip}}" {{ old('zipcode') == $zip_i->zip ? 'selected' : '' }}>{{$zip_i->zip}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group col-md-12 col-sm-12 col-xs-122">
            <label class="" for="formControlRange">Select the range of your area</label>
            <div style="position: absolute; right: 20px; margin-top: -20px;" id="slider_value"></div>
            <input type="range" min="0" max="30" value="0" id="slider" name="distance" class="form-control-range mb-md-4 mr-md-2" onclick="actionClick()">
          </div>
          <div class="spacer" style="padding-top: 25px;"></div>
          <input type="hidden" name="country" value="{{$country}}">
          <input type="hidden" name="state" id="st" value="{{$state}}">
          <input type="hidden" name="city" id="ct" value="{{$city}}">
          @if($country == "Canada" || $country == "United States")
                <input type="submit" class="btn btn-primary btn-block" name="submit" value="NOTIFY ME" onclick="actionClick()">
          @else
                <div>Service not yet available in your area.</div>
          @endif
        </form>
      </div>
    </div>
  </div>
</header>
<section class="features">
  <div class="container">
    <div class="row">
      <div class="col-md-3 feature-item">
        <img src="{{url('assets/img/landing/electronic.png')}}" alt="Excellent Value" class="center-block">
        <h4>Electronic Referral Agreement</h4>
      </div>
      <div class="col-md-3 feature-item">
        <img src="{{url('assets/img/landing/escrow.png')}}" alt="Ease of Use" class="center-block">
        <h4>Escrow deposits to establish trust</h4>
      </div>
      <div class="col-md-3 feature-item">
        <img src="{{url('assets/img/landing/notification.png')}}" alt="Huge Varities" class="center-block">
        <h4>Title company rep notifications</h4>
      </div>
      <div class="col-md-3 feature-item">
        <img src="{{url('assets/img/landing/blockchain.png')}}" alt="Human Support" class="center-block">
        <h4>Blockchain payment options</h4>
      </div>
    </div>
  </div>
</section>
<section class="about">
  <div class="container">
    <div class="row">
      <div class="col-md-2">&nbsp;</div>
      <div class="col-md-8 center-block">
        <h1 class="blue-title">WHAT IS TYMBL?</h1>
        <p style="text-align:center;">Tymbl is a marketplace platform for trading real estate leads and referrals that leverages technology for establishing trust between all parties involved. Tymbl provides a hassle-fee way for trading leads, signing referral agreements electronically and working with escrow/title company reps.</p>
      </div>
      <div class="col-md-2">&nbsp;</div>
    </div>
  </div>
</section>
<section class="empty-bg">
  &nbsp;
</section>
<section class="seller">
  <div class="container">
    <h2 class="blue-title">LOOKING FOR REFERRALS?</h2>
    <div class="container">
      <div class="steps">
        <div class="row">
          <div class="col-md-4 arrow">
            <img src="{{url('assets/img/landing/home-search.png')}}" alt="" class="mx-auto d-block">
            <h4>Find a referral in your area</h4>
          </div>
          <div class="col-md-4 arrow">
            <img src="{{url('assets/img/landing/agreement.png')}}" alt="" class="mx-auto d-block">
            <h4>Sign a referral agreement</h4>
          </div>
          <div class="col-md-4">
            <img src="{{url('assets/img/landing/home-percent.png')}}" alt="" class="mx-auto d-block">
            <h4>Sell or help buy a property and share a percentage of the proceeds</h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="buyer">
  <div class="container">
    <h2 class="blue-title">GOT A CLIENT TO REFER?</h2>
    <div class="container">
      <div class="steps">
        <div class="row">
          <div class="col-md-3 arrow">
            <img src="{{url('assets/img/landing/post.png')}}" alt="" class="mx-auto d-block">
            <h4>Post a referral on Tymbl</h4>
          </div>
          <div class="col-md-3 arrow">
            <img src="{{url('assets/img/landing/two-agents.png')}}" alt="" class="mx-auto d-block">
            <h4>Another agent claims your referral and sign a referral agreement with you</h4>
          </div>
          <div class="col-md-3 arrow">
            <img src="{{url('assets/img/landing/notified.png')}}" alt="" class="mx-auto d-block">
            <h4>Get notified by escrow/title company automatically when the deal is closed</h4>
          </div>
          <div class="col-md-3">
            <img src="{{url('assets/img/landing/get-paid.png')}}" alt="" class="mx-auto d-block">
            <h4>Get Paid</h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
