@extends('landing.layout.main')
@section('content')
  <header>
		<div class="container">
			<a class="logo" href="/">
				<img src="{{url('assets/img/landing/logo.png')}}" alt="Tymbl" class="mx-auto d-block">
			</a>
			<div class="row header-content justify-content-md-center">
				<div class="col-md-12 page-header">
					<h2>Frequently Asked Questions</h2>
				</div>
			</div>
		</div>
	</header>
	<section class="about">
		<div class="container">
			<div class="row">
				<div class="col-md-2">&nbsp;</div>
				<div class="col-md-8 center-block">
          <div class="single-faq">
                        <h4>I want to sell a referral, how do I go about it?</h4>
                        <p>You will post a listing on Tymbl, which should only take a few minutes, without disclosing the contact name of the your client. Tymbl will notify real estate agents in the area, that there is a referal available. An agent will "buy" you referral by signing a legally-binding referral agreement with you on Tymbl and we'll notify this agent's title company and send them a copy of the contract.</p>
                    </div>

                    <div class="single-faq">
                        <h4>How do I buy a referral?</h4>
                        <p>Register and select a state or states and cities you are interested in. When there are referrals available in your area, we'll notify you. Then you login and sign a referral agreement and the seller of this referral will send you the contact info of the prospect.</p>
                    </div>

                    <div class="single-faq">
                        <h4>How do I make sure the other agent follows through on the terms of the referral agreement and I actually get paid when the property is sold?</h4>
                        <p>Tymbl establishes trust between parties involved in the transaction by</p>
                        <ul>
                            <li>Ensuring that the electronic referral agreement signed by you and the other party on Tymbl is a legally binding contract.</li>
                            <li>Enabling you to set an escrow fee the other party will have to pay when signing the referral agreement.</li>
                            <li>Holding the fee in the Tymbl escrow until the title company rep of the other party lets Tymbl know whether the referral resulted in a succesful sale or a purchase or not to release or refund the fee accordingly.</li>
                            <li>Collecting ratings and displaying an average score for every user who completed a transaction on Tymbl.</li>
                        </ul>
                    </div>

                    <div class="single-faq">
                        <h4>Is Tymbl for independent brokerages or franchises?</h4>
                        <p>Tymbl is completely brokerage agnostic</p>
                    </div>

                    <div class="single-faq">
                        <h4>How does Tymbl use blockchain?</h4>
                        <p>Tymbl is working on its own utility token leveraging blockchain technology. Agents will be able to buy Tymbl tokens and use it for Tymbl escrow transactions to cut down on fees involved in transacting with PayPal.</p>
                    </div>                 
				</div>
				<div class="col-md-2">&nbsp;</div>
			</div>
		</div>
	</section>
@endsection
