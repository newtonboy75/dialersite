<!DOCTYPE html>
<html>
<head>
    <title>Contract Approval</title>
    <style type="text/css">
      div{
        margin-left: 40px;
        margin-right:40px;
        font-size:12px;
      }
      .footer-text{
        font-size: 14px;
      }
    </style>
</head>

<body style="font-size: 15px;">
  <p><strong>Hello {{$name}}</strong></p>
  <p>Thank you for registering. We will inform you when there are new referrals or leads in your area.</p>
</body>
</html>
