@extends('landing.layout.main')
@section('content')
	<header>
		<div class="container">
			<a class="logo" href="/">
				<img src="{{url('assets/img/landing/logo.png')}}" alt="Tymbl" class="mx-auto d-block">
			</a>
			<div class="row header-content justify-content-md-center">
				<div class="col-md-12 page-header">
					<h2>{{$title}}</h2>
				</div>
			</div>
		</div>
	</header>
	<section class="about">
		<div class="container">
			<div class="row">
				<div class="col-md-2">&nbsp;</div>
				<div class="col-md-8 center-block">
					<form method="POST" action="contact-send" class="form-inline form-area" style="margin-top:-100px !important;">
            <div class="col-md-12 center-block">@include('admin.flash_msg')</div>
            {!! csrf_field() !!}
            <div class="form-group col-md-6 col-sm-12 col-xs-12">
							<label class="sr-only" for="name">Name</label>
							<input type="text" class="form-control mb-2" id="name" aria-describedby="nameHelp" placeholder="Name" name="name" value="{{ old('name') }}" onclick="actionClick()">
              {!! $errors->has('name')? '<p class="help-block">'.$errors->first('name').'</p>':'' !!}
						</div>
						<div class="form-group col-md-6 col-sm-12 col-xs-12">
							<label class="sr-only" for="email">Name</label>
							<input type="email" name="email" value="{{ old('email') }}" class="form-control mb-2" id="email" aria-describedby="emailHelp" placeholder="Email Address" onclick="actionClick()">
              {!! $errors->has('email')? '<p class="help-block">'.$errors->first('email').'</p>':'' !!}
						</div>
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
							<label class="sr-only" for="message">Message</label>
							<textarea class="form-control" name="message" placeholder="Message" style="width:100%;margin-top:10px;"></textarea>
              {!! $errors->has('message')? '<p class="help-block">'.$errors->first('message').'</p>':'' !!}
						</div>
            <input type="hidden" name="to" value="tymblapp@gmail.com">
            <div class="form-group col-md-12 col-sm-12 col-xs-12" style="width:100%;margin-top:20px;">
							<button type="submit" class="btn btn-primary btn-block">SUBMIT</button>
						</div>
					</form>
				</div>
				<div class="col-md-2">&nbsp;</div>
			</div>
		</div>
	</section>
@endsection
