<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Open Graph data -->
  <meta property="og:description" content="<?php echo trans('app.meta_description') ?>" />
  <meta property="og:title" content="<?php echo trans('app.meta_title') ?>" />
  <meta property="og:url" content="{{ url()->current() }}" />
  <meta property="og:type" content="website" />
  <meta property="og:locale" content="en_US" />
  <meta property="og:locale:alternate" content="en-us" />
  <meta property="og:site_name" content="{{get_option('site_name')}}" />
  <meta property="og:image:url" content="{{URL::asset('assets/img/tymbl/tymbl-post-leads.jpg')}}" />
  <meta property="og:image:size" content="300" />
  <!-- Twitter Card -->
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@tymbl1">
  <meta name="twitter:title" content="<?php echo trans('app.meta_title') ?>">
  <meta name="twitter:description" content="<?php echo trans('app.meta_description') ?>">
  <meta name="twitter:creator" content="@author_handle">
  <meta name="twitter:image:src" content="{{URL::asset('assets/img/tymbl/tymbl-post-leads.jpg')}}">
  <!--Google+ -->
  <meta itemprop="name" content="<?php echo trans('app.meta_title') ?>">
  <meta itemprop="description" content="<?php echo trans('app.meta_description') ?>">
  <meta itemprop="image" content="{{URL::asset('assets/img/tymbl/tymbl-post-leads.jpg')}}">
	<title>Tymbl - The First Real Estate Lead and Referrals Exchange</title>
	<!-- favicon -->
  <link rel="shortcut icon" href="{{url('assets/img/landing/fav.png')}}">
    <!-- bootstrap -->
	<link rel="stylesheet" href="{{ asset('css/landing/bootstrap-reboot.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/landing/bootstrap-grid.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/landing/bootstrap.min.css') }}">
	<!-- fontawesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <!-- custom style -->
  <link href="{{ asset('css/landing/style.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('css/landing/responsive.css') }}" rel="stylesheet" type="text/css">
  <meta name="google-site-verification" content="fa25hLOssKHKzyrF0ilVxhIvfzqZfgtPq2J0aEe0Neo" />
</head>
<body>
@yield('content')
@if(request()->route()->getName() != 'contact')
<section class="contact">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h1>Have questions?</h1>
        <a href="contact" type="button" class="btn btn-success btn-lg" style="margin-bottom:20px;">CONTACT US</a>&nbsp;&nbsp;
        <a href="faq" type="button" class="btn btn-success btn-lg" style="margin-bottom:20px;"">FAQ</a>
      </div>
    </div>
  </div>
</section>
@endif
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-4 footer-logo">
        <a href="/" style="width: 100%;"><img src="{{url('assets/img/landing/logo.png')}}" alt="" width="40%" height="auto" class="align-middle"></a>
      </div>
      <div class="col-md-4 footer-links">
        <ul class="list-inline">
          <li><a href="privacy-policy">Privacy Policy</a></li>
          <li><a href="terms-and-conditions">Terms & Conditions</a></li>
          <li><a href="faq">Frequently Asked Questions</a></li>
          <li><a href="contact">Contact Us</a></li>
        </ul>
      </div>
      <div class="col-md-4 social-icons">
        <ul class="list-inline">
          <li class="list-inline-item"><a href="https://www.facebook.com/Tymbl-1093758424133121"><i class="fab fa-facebook-f"></i></a></li>
          <li class="list-inline-item"><a href="https://twitter.com/tymbl1"><i class="fab fa-twitter"></i></a></li>
          <li class="list-inline-item"><a href="https://www.instagram.com/tymbltech/"><i class="fab fa-instagram"></i></a></li>
        </ul>
      </div>

    </div>
    <div class="copyright">Copyright 2018, &copy; Tymbl. All rights reserved</div>
  </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{ asset('js/retina.min.js') }}"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127180364-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-127180364-1');
</script>

<script>
function actionClick(){
gtag('event', 'RegistrationBegin', {
'event_category': 'Registration',
'event_label': 'PreLaunch',
});
}
</script>

@if (Session::has('success'))
<script>
gtag('event', 'RegistrationSuccess', {
'event_category': 'Registration',
'event_label': 'PreLaunch',
});
</script>

<script>
$.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
$.ajax({
  url: '/landing-mail',
  type: 'POST',
  dataType: 'JSON',
  data: { name: '{{ Session::get('f_name') }}', email: '{{ Session::get('f_email') }}', _token: '{{csrf_token()}}' },
});
</script>
@endif

<script>
$(document).on('input change', '#slider', function() {
  $('#slider_value').html( $(this).val() + " miles");
});
</script>

<script src="{{ asset('js/retina.min.js') }}"></script>

<script>

  $('[name="state"]').on('change', function() {
    var state_id = $(this).val();
    $('[name="zipcode"]').html('<option value="0" selected>Select Zip Code</option>');
    $.ajax({
      type: 'POST',
      url: '{{ route('get_city_by_state') }}',
      data: {state_id: state_id, _token: '{{ csrf_token() }}'},
      success: function (data) {
        $('#st').val($('[name="state"] option:selected').text());
        generate_option_from_json(data, 'state_to_city');

      }
    });
  });

  $('[name="city"]').change(function () {
    var state_id = $('[name="state"] option:selected').text();
    var city_id = $(this).find('option:selected').text();
    var country_name = $('[name="country"]').val();
    $('#ct').val($('[name="city"] option:selected').text());
    var country = '';
    if(country_name == 'United States'){
      country = '231';
    }else{
      country = '38';
    }

    //alert(state_id + city_id);

    $('[name="zipcode"]').html('<option disabled selected>Select Zip Code</option>');
    $.ajax({
      type: 'POST',
      url: '{{ route('get_zip_by_city') }}',
      data: {country_id: country, state_id: state_id, city_id: city_id, _token: '{{ csrf_token() }}'},
      success: function (data) {

        //console.log(data);

        var option = '';
        if (data.length > 0) {
          option += '<option value="0" selected>Select Zip Code</option>';
          for (i in data) {
            option += '<option value="' + data[i].zip + '"> ' + data[i].zip + ' </option>';
          }
          $('[name="zipcode"]').html(option);
          //$('#zipcode_select').select2();
        } else {
          $('[name="zipcode"]').html('<option value="" selected>(Zip Code not found)</option>');
          //$('#zipcode_select').select2();
        }

      }
    });
  });



function generate_option_from_json(jsonData, fromLoad) {
  //Load Category Json Data To Brand Select

  if (fromLoad === 'country_to_state') {
    var option = '';
    if (jsonData.length > 0) {
      option += '<option value="0" selected> @lang('app.select_state') </option>';
      for (i in jsonData) {
        option += '<option value="' + jsonData[i].id + '"> ' + jsonData[i].state_name + ' </option>';
      }
      $('#state_select').html(option);
      $('#state_select').select2();
    } else {
      $('#state_select').html('');
      $('#state_select').select2();
    }
    $('#state_loader').hide('slow');

  } else if (fromLoad === 'state_to_city') {
    var option = '';
    if (jsonData.length > 0) {
      option += '<option value="0" selected> @lang('app.select_city') </option>';
      for (i in jsonData) {
        option += '<option value="' + jsonData[i].id + '"> ' + jsonData[i].city_name + ' </option>';
      }
      $('#city_select').html(option);

      $('#city_select').select2();
    } else {
      $('#city_select').html('');
      $('#city_select').select2();
    }
    $('#city_loader').hide('slow');
  } else if(fromLoad === 'state_with_cities') {
    //console.log(jsonData.length);
    var option = '';
    if (jsonData.length > 0) {
      //console.log(jsonData);
      option += '<option value="0" selected> @lang('app.select_city') </option>';

      for (var i=0; i<jsonData.length; i++) {
        let statename = jsonData[i][0];
        let cityarray = jsonData[i][1];
        option += '<option disabled="disabled" value="' + statename + '"> <div class="background:#333 !important; font-size:10px !important;">' + statename + '</div></option>';

        for(var j=0; j<cityarray.length; j++){
          option += '<option value="' + cityarray[j]['id'] + '">&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp' + cityarray[j]['city_name'] + ' </option>';
        }
      }

      $('#city_select').html(option);
      $('#city_select').select2();
    } else {
      $('#city_select').html('');
      $('#city_select').select2();
    }
  }
}
</script>
</body>
</html>
