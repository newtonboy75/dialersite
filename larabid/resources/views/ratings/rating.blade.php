
<ul class="inline-rating rating">
    <li id="rating" rel="5"><i class="fa fa-star" title="Rate 5"></i></li>
    <li id="rating" rel="4"><i class="fa fa-star" title="Rate 4"></i></li>
    <li id="rating" rel="3"><i class="fa fa-star" title="Rate 3"></i></li>
    <li id="rating" rel="2"><i class="fa fa-star" title="Rate 2"></i></li>
    <li id="rating" rel="1"><i class="fa fa-star" title="Rate 1"></i></li>
</ul>
