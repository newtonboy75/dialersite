@extends('layouts.app')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('content')

    <div class="container">
        <div id="wrapper">


            <div id="page-wrapper">
                @if( ! empty($title))
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"> {{ $title }}  </h1>
                        </div> <!-- /.col-lg-12 -->
                    </div> <!-- /.row -->
                @endif

                @include('admin.flash_msg')

                <div class="row" style="h-100 justify-content-center align-items-center">
                  <div class="row h-100 justify-content-center align-items-center">
                    <div class="col-xs-6">
                        <div class="panel panel-default center-block">
                            <div class="panel-heading">
                                <h4 class="panel-title">@lang('app.review_your_order') </h4>
                            </div>
                            <div class="panel-body text-center">
                                <div style="font-size: 2.5em; font-weight:400;">{{$amount}}</div>
                                <div style="font-size: 13px;">PayPal will charge a 2.9% + $0.30 for this transaction</div>
                                <br>
                                <form action="/payment/paid" method="Post">
                                  {{ csrf_field() }}
                                  <input name="amount" value="{{$amount}}" type="hidden">
                                  <input name="transaction_id" value="{{$transaction_id}}" type="hidden">
                                  <p>
                                    <button class="btn btn-info">Pay with Paypal</button>
                                  </p>
                                </form>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>

            </div>   <!-- /#page-wrapper -->

        </div>   <!-- /#wrapper -->

    </div> <!-- /#container -->
@endsection
