@extends('layouts.app')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('content')

    <div class="container">
        <div id="wrapper">


            <div id="page-wrapper">
                @if( ! empty($title))
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"> {{ $title }}  </h1>
                        </div> <!-- /.col-lg-12 -->
                    </div> <!-- /.row -->
                @endif

                @include('admin.flash_msg')

                <div class="row" style="h-100 justify-content-center align-items-center">
                  <div class="row h-100 justify-content-center align-items-center">
                    <h4>Payment Success</h4>
                    you will now be redirected to Referral Agreement page<br/>
                    (click <a href="/referral-document/{{$ad_id}}">here</a> if you are not automatically redirected)
                  </div>
                </div>

            </div>   <!-- /#page-wrapper -->

        </div>   <!-- /#wrapper -->

    </div> <!-- /#container -->
@endsection
