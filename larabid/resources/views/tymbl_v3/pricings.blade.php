<div class="cols-4">
  <div class="box">
    <div class="price_plan"><span class="price">Smart Dialer</span></div>
    <p class="text-center">Coming Soon</p><p>&nbsp;</p><p>&nbsp;</p><br><br>
    <div class="btn-upload mx-auto text-center"><a href="#smvid" rel="modal:open" class="tymbl_btn tymbl_btn_32 tymbl_btn_fill tymbl_btn_round">INQUIRE</a></div>
  </div>
</div>

<div class="cols-4">
  <div class="box">
    <div class="price_plan"><span class="price">$29.99</span><br><small>per qualified lead</small></div>
    <ul>
      <li>Automated Scrubbing to prioritize dials</li>
      <li>At least 5 touches (Text, Email, Dials) each lead</li>
      <li>5% Referral fee at closing.</li>
    </ul>
    <p>&nbsp;</p>
    <div class="btn-upload mx-auto text-center"><a href="http://127.0.0.1:8802/sign-contract/plan?option=2" role="button" class="tymbl_btn tymbl_btn_32 tymbl_btn_fill tymbl_btn_round">IMPORT CSV</a></div>
  </div>
</div>
