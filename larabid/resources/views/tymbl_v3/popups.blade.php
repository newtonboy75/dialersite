<div id="lqvid" class="modal">
  <video id="provid" width="100%" preload="auto"  poster="{{ asset('assets/img/tymbl_v3/preload.png') }}">
  <source src="https://tymbl-assets.s3-us-west-2.amazonaws.com/tymbl_present_onboard.mp4" type="video/mp4">
  <source src="movie.ogg" type="video/ogg">
  Your browser does not support the video tag.
  </video>
</div>


<div id="smvid" class="modal">

<div id="inquiry-success">
<p style="font-weight: bold; font-size: 20px;" class="text-center">Thank you for signing up for our Beta program. Please stay tuned, we’ll be in touch shortly!</p>
</div>

<div id="inquiry-form">
<div><h2 class="text-center">Smart Dialer<br>Coming Soon!</h2>
Sign up to participate in our Beta program and help us improve our Smart Dialer. In return for your feedback, you will get a substantial discount when we are ready for prime time! </div>

<div>
  <br>
    <input placeholder="Name" type="text" name="name" id="temp-name" required>
    <br>
    <input placeholder="Company" type="text" name="Company" id="temp-company">
    <br>
    <input placeholder="Email" type="text" name="email" id="temp-email" required>
    <br><br>
    <button id="btn-submit" class="tymbl_btn tymbl_btn_32 tymbl_btn_fill tymbl_btn_round" style="outline: none;">Submit</button>
  <br>
</div>
</div>

</div>
