@extends('tymbl_v3.layouts.app')
@section('content')
<div id="tymbl_home_head">
  <div class="column">
    <div id="tymbl_home_head_top">
      @include('tymbl_v3.pricings')
    </div>
  </div>
</div>
@include('tymbl_v3.popups')
@endsection
