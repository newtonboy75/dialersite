<div id="tymbl_home_head">
  <div class="column">
    <div id="tymbl_home_head_top">
      <h1>Convert 5× more leads <span>With our Human and AI-powered solutions</span></h1>
      <div id="tymbl_home_head_products">
        <div class="tymbl_home_head_products_block" id="tymbl_home_head_products_block_sd">
          <div class="tymbl_home_head_products_block_top">
            <img src="{{ asset('assets/img/tymbl_v3/product_sd_logo.svg') }}" alt="Smart Dialer Logo" />
            <p class="tymbl_home_meta_h2">Smart Dialer</p>
          </div>
          <div class="tymbl_home_head_products_block_bottom">
            <a id="load-sm-vid" href="#smvid" rel="modal:open"><img src="{{ asset('assets/img/tymbl_v3/arrow_watch.svg') }}" alt="Play" /> Watch video</a>
          </div>
        </div>
        <div class="tymbl_home_head_products_block" id="tymbl_home_head_products_block_lqs">
          <div class="tymbl_home_head_products_block_top">
            <img src="{{ asset('assets/img/tymbl_v3/product_lqs_logo.svg') }}" alt="Lead Qualification Services Logo" />
            <p class="tymbl_home_meta_h2">Lead Qualification Services</p>
          </div>
          <div class="tymbl_home_head_products_block_bottom">
            <a href="#lqvid" rel="modal:open"><img src="{{ asset('assets/img/tymbl_v3/arrow_watch.svg') }}" alt="Play" /> Watch video</a>
          </div>
        </div>
      </div>
    </div>
    <div id="tymbl_home_head_bottom">
      <img src="{{ asset('assets/img/tymbl_v3/arrow_more.svg') }}" alt="More" />
    </div>
  </div>
</div>

<div id="tymbl_home">
  <div id="tymbl_home_products">
    <div class="column">
      <div class="tymbl_home_products_block"  id="tymbl_home_products_block_sd">
        <div>
          <img src="{{ asset('assets/img/tymbl_v3/product_sd_logo.svg') }}" alt="Smart Dialer Logo" />
          <p class="tymbl_home_meta_h2">Smart Dialer</p>
          <ul class="tymbl_home_products_block_features">
            <li class="tymbl_home_products_block_features_block">
              <img src="{{ asset('assets/img/tymbl_v3/product_sd_feature_lines.svg') }}" alt="Call via 10 Lines" />
              <p>Automatically contact hundreds of&nbsp;prospects per day making call via up&nbsp;to&nbsp;10&nbsp;lines simultaneously.</p>
            </li>
            <li class="tymbl_home_products_block_features_block">
              <img src="{{ asset('assets/img/tymbl_v3/product_sd_feature_import.svg') }}" alt="CRM Integration" />
              <p>Load your leads from your CRM.</p>
            </li>
            <li class="tymbl_home_products_block_features_block">
              <img src="{{ asset('assets/img/tymbl_v3/product_sd_feature_sort.svg') }}" alt="Priority Sort" />
              <p>With the&nbsp;help of&nbsp;machine learning Tymbl will scrub and&nbsp;sort your real estate leads in&nbsp;a&nbsp;priority order, so&nbsp;that you&nbsp;know what leads you&nbsp;should call first.</p>
            </li>

          </ul>
        </div>
        <div>
          <a href="#dialer" class="tymbl_btn tymbl_btn_56 tymbl_btn_stroke tymbl_btn_rectangle">Learn more&nbsp;&nbsp;→</a>
        </div>
      </div>
      <div class="tymbl_home_products_block" id="tymbl_home_products_block_lqs">
        <div>
          <img src="{{ asset('assets/img/tymbl_v3/product_lqs_logo.svg') }}" alt="Lead Qualification Services Logo" />
          <p class="tymbl_home_meta_h2">Lead Qualification Services</p>
          <ul class="tymbl_home_products_block_features">
            <li class="tymbl_home_products_block_features_block">
              <img src="{{ asset('assets/img/tymbl_v3/product_lqs_feature_handshake.svg') }}" alt="FLAT FEE" />
              <h3>FLAT FEE</h3>
              <p>Let Tymbl qualify your leads, live transfer to your phone or set appointments directly on your calendar in exchange for a $29.99 fee and 5% of your commission.</p>
            </li>
            <li class="tymbl_home_products_block_features_block">
              <img src="{{ asset('assets/img/tymbl_v3/product_lqs_feature_hourglass.svg') }}" alt="Or Pay Hourly" />
              <h3><em>OR</em> PAY HOURLY</h3>
              <p>Hire highly trained Tymbl Inside Sales Associated on hourly basis.	</p>
              <p>They will use Tymbl systems to qualify and return your leads back to you.</p>
            </li>
          </ul>
        </div>
        <div>
          <a href="#qualification" class="tymbl_btn tymbl_btn_56 tymbl_btn_stroke tymbl_btn_rectangle">Learn more&nbsp;&nbsp;→</a>
        </div>
      </div>

    </div>
  </div>
  <div id="tymbl_home_descriptions">

    <div class="tymbl_home_descriptions_block" id="tymbl_home_descriptions_block_sd">
      <div class="column">
        <a name="dialer" class="tymbl_anchor"></a>
        <div class="tymbl_home_descriptions_block_title">
          <img src="{{ asset('assets/img/tymbl_v3/product_sd_logo.svg') }}" alt="Smart Dialer Logo" />
          <h2>Smart Dialer with AI&nbsp;data scrubber</h2>
        </div>
        <div class="tymbl_home_descriptions_block_text">
          <p>Tymbl Auto-dialer will increase productivity and&nbsp;profit by&nbsp;making 5×&nbsp;more calls per day. Simply load your prospecting lists or&nbsp;connect directly to&nbsp;your CRM and&nbsp;start dialing immediately.</p>
          <p>Have thousands of&nbsp;leads to&nbsp;call? No&nbsp;problem, let Tymbl data scrubber prioritize your lists with the help of&nbsp;Machine Learning, so&nbsp;that you know what leads you call first.</p>
        </div>
        <div class="tymbl_home_descriptions_block_icons">
          <div class="tymbl_home_descriptions_block_icons_block show-on-scroll">
            <img src="{{ asset('assets/img/tymbl_v3/product_sd_feature02_5x.svg') }}" alt="More Calls" />
            <p>5×&nbsp;more calls per day</p>
          </div>
          <div class="tymbl_home_descriptions_block_icons_block show-on-scroll">
            <img src="{{ asset('assets/img/tymbl_v3/product_sd_feature02_neuro.svg') }}" alt="ML Sorting" />
            <p>Machine Learning prioritizing</p>
          </div>
        </div>
      </div>
      <div class="tymbl_home_descriptions_block_bg">
        <div class="tymbl_home_descriptions_block_bg_color">&nbsp;</div>
        <div class="tymbl_home_descriptions_block_bg_img show-on-scroll">&nbsp;</div>
      </div>
    </div>
    <div class="tymbl_home_descriptions_block" id="tymbl_home_descriptions_block_lqs">
      <div class="column">
        <a name="qualification" class="tymbl_anchor"></a>
        <div class="tymbl_home_descriptions_block_title">
          <img src="{{ asset('assets/img/tymbl_v3/product_lqs_logo.svg') }}" alt="Lead Qualification Services Logo" />
          <h2>Lead Qualification Services</h2>
        </div>
        <div class="tymbl_home_descriptions_block_text">
          <h3 class="tymbl_home_subtitle">FLAT FEE</h3>
          <p>You have leads that you are not servicing or simply don’t have the time to service. Every agent does. Let Tymbl do the legwork to pre-qualify them so that you can focus on turning them into deals. Tymbl charges $29.99 fee for each qualified lead and 5% of your commision. Each qualified leads can be live-transferred directly to you or members of your team via a phone or as an appointment directly on your calendar. </p>
          <h3 class="tymbl_home_subtitle">HOURLY</h3>
          <p>Hire our highly trained and certified inside sales associates (ISA) who specialize in&nbsp;real estate on&nbsp;affordable hourly basis. Each ISA average 300–400 dials per day. Hiring our ISAs will come with access to&nbsp;progress monitoring/analytics tools, Tymbl proprietary scripts and&nbsp;Tymbl Smart dialer. Once your hire our ISAs, we&nbsp;will take them with a&nbsp;customized onboarding process to&nbsp;fit your unique needs and&nbsp;your area market specifics. </p>
        </div>
        <div class="tymbl_home_descriptions_block_icons">
          <div class="tymbl_home_descriptions_block_icons_block show-on-scroll">
            <img src="{{ asset('assets/img/tymbl_v3/product_lqs_feature02_funnel.svg') }}" alt="Qualify Leads" />
            <p>Pre-qualify leads</p>
          </div>
          <div class="tymbl_home_descriptions_block_icons_block show-on-scroll">
            <img src="{{ asset('assets/img/tymbl_v3/product_lqs_feature02_3x.svg') }}" alt="More Sales" />
            <p>Turn 3×&nbsp;more leads into&nbsp;sales</p>
          </div>
          <div class="tymbl_home_descriptions_block_icons_block show-on-scroll">
            <img src="{{ asset('assets/img/tymbl_v3/product_lqs_feature02_call.svg') }}" alt="ISA Dials" />
            <p>Each ISA&nbsp;average 300–400&nbsp;dials per&nbsp;day</p>
          </div>
          <div class="tymbl_home_descriptions_block_icons_block show-on-scroll">
            <img src="{{ asset('assets/img/tymbl_v3/product_lqs_feature02_board.svg') }}" alt="ISA’s onboarding" />
            <p>ISA’s customized onboarding</p>
          </div>
        </div>
      </div>
      <div class="tymbl_home_descriptions_block_bg">
        <div class="tymbl_home_descriptions_block_bg_color">&nbsp;</div>
        <div class="tymbl_home_descriptions_block_bg_img show-on-scroll">&nbsp;</div>
      </div>
    </div><a name="prvid"></a>
    <br>
  </div>
  <div id="tymbl_home_video">
    <div class="column">
      <video id="provid2" width="100%" controls poster="{{ asset('assets/img/tymbl_v3/preload.png') }}">
      <source src="https://tymbl-assets.s3-us-west-2.amazonaws.com/tymbl_present_onboard.mp4" type="video/mp4">
      <source src="movie.ogg" type="video/ogg">
      Your browser does not support the video tag.
      </video>
    </div>
  </div>
  <div id="tymbl_home_cta">
    <div class="column">
      <p>Try Tymbl Today</p>
      <a href="https://dialer.tymbl.com/register" class="tymbl_btn tymbl_btn_56 tymbl_btn_fill tymbl_btn_rectangle">Register for Free</a>
    </div>
  </div>
</div>
@include('tymbl_v3.popups')
