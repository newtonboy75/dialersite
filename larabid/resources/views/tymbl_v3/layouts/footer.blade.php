<div id="tymbl_footer">
  <div id="tymbl_footer_pre">
    <div class="column">
      <div id="tymbl_footer_pre_navigation">
        <div class="tymbl_footer_pre_navigation_block">
          <h4>PRODUCTS</h4>
          <ul>
            <li><a href="#smvid2" id="smvid2">Smart Dialer</a></li>
            <li><a href="#lqvid2" id="lqvid2">Lead Qualification</a></li>
          </ul>
        </div>
        <div class="tymbl_footer_pre_navigation_block">
          <h4>RESOURCES</h4>
          <ul>
            <li><a href="">Help System</a></li>
            <li><a href="/page/frequently-asked-questions">FAQ</a></li>
          </ul>
        </div>
        <div class="tymbl_footer_pre_navigation_block">
          <h4>COMPANY</h4>
          <ul>
            <li><a href="">What is Tymbl</a></li>
            <li><a href="/page/about-us">Team</a></li>
          </ul>
        </div>
        <div class="tymbl_footer_pre_navigation_block">
          <h4>LEGAL</h4>
          <ul>
            <li><a href="/page/privacy-policy">Privacy Policy</a></li>
            <li><a href="/page/terms-and-conditions-1">Terms & Conditions</a></li>
          </ul>
        </div>
      </div>
      <a href="/contact-us" class="tymbl_btn tymbl_btn_40 tymbl_btn_stroke tymbl_btn_round">Contact Us</a>
    </div>
  </div>
  <div id="tymbl_footer_post">
    <div class="column">
      <div id="tymbl_footer_post_logo">
        <img src="{{ asset('assets/img/tymbl_v3/tymbl_logo_white_on_blue.svg') }}" alt="Tymbl Logo" />
        <p>Tymbl</p>
      </div>
      <p id="tymbl_footer_post_legal">© 2019&nbsp;&nbsp;<span>•</span>&nbsp;&nbsp;Tymbl&nbsp;&nbsp;<span>•</span>&nbsp;&nbsp;All rights reserved</p>
    </div>
</div>
