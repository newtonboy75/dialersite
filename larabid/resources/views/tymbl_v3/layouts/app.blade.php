<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="{{ asset('assets/img/tymbl_v3/fav.png') }}">
  <link rel="apple-touch-icon" href="icon.png">
  <link href="https://fonts.googleapis.com/css?family=Poppins:600,700|Source+Sans+Pro:400,400i,600,700,700i&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl_v3/normalize.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl_v3/main.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl_v3/style.css') }}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
  <meta name="theme-color" content="#fafafa">
</head>

<body>
  <!--[if IE]>
  <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->
  <div>
    <!-- start header -->
    @include('tymbl_v3.layouts.header')
    <!-- end header -->
    <!-- -->
    <!-- start index contents -->
    @yield('content')
    <!-- end index contents -->
    <!-- start footer -->
    @include('tymbl_v3.layouts.footer')
    <!-- footer -->
  </div>

  <script src="{{ asset('assets/js/tymbl_v3/vendor/modernizr-tymbl.js') }}"></script>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
  <script src="{{ asset('assets/js/tymbl_v3/plugins.js') }}"></script>
  <script src="{{ asset('assets/js/tymbl_v3/main.js') }}"></script>
  <script src=" {{ asset('assets/js/tymbl_v3/vendor/jquery.documentsize.min.js') }}"></script>

  <script src="{{ asset('assets/js/tymbl_v3/tymbl_scripts.js') }}"></script>
  <script src="{{ asset('assets/js/tymbl_v3/scroll_trigger.js') }}"></script>
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl_v3/afterload.css') }}">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>

  <script>
  $('#lqvid').on($.modal.BEFORE_CLOSE, function(event, modal) {
    $('#provid').trigger('pause');
  });

  $('#lqvid').on($.modal.BLOCK, function(event, modal) {
    setTimeout(
      function()
      {
        $('#provid').trigger('play');
      }, 1000);

    });

    $('#lqvid2').click(function(){
      $("html, body").animate({ scrollTop: 0 }, "fast");
      setTimeout(
        function()
        {
          $('#lqvid').modal();
        }, 1000);

      });


    $('#smvid2').click(function(){
      $("html, body").animate({ scrollTop: 0 }, "fast");
      setTimeout(
        function()
        {
          $('#smvid').modal();
        }, 1000);
      });

      $('#smvid').on($.modal.BLOCK, function(event, modal) {

        $('#inquiry-form').show();
        $('#inquiry-success').hide();
      });

      $('#smvid').on($.modal.CLOSE, function(event, modal) {

        $('#temp-name').val('');
        $('#temp-company').val('');
        $('#temp-email').val('');

      });

      $('#btn-submit').click(function(){
        $('#btn-submit').text('Submitting... please wait...');
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: 'submit-inquiry-sm',
            type: 'post',
            data: { name: $('#temp-name').val(), company: $('#temp-company').val(),  email: $('#temp-email').val() },
            success: function(data) {
              if(data == '1'){
                //console.log(data);
                $('#inquiry-form').hide();
                $('#inquiry-success').show();
                $('#btn-submit').text('Submit');
              }else{
                alert(data);
                $('#btn-submit').text('Submit');
              }

            }
          });
        });
        </script>

      </body>

      </html>
