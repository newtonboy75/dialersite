<div id="tymbl_header">
  <div class="column">
    <div id="tymbl_header_left">
      <div id="tymbl_header_logo">
        <img src="{{ asset('assets/img/tymbl_v3/tymbl_logo_color.svg') }}" alt="Tymbl Logo" />
        <p><a class="url_home" href="/">Tymbl</a></p>
      </div>
      <ul id="tymbl_header_products">
        <li><a href="#" class="tymbl_header_link" id="tymbl_header_link_products">Products</a></li>
      </ul>
    </div>
    <div id="tymbl_header_products_drop">
      <div id="tymbl_header_products_drop_list">
        <a href="#qualification" class="tymbl_header_products_drop_block">
          <img src="{{ asset('assets/img/tymbl_v3/product_lqs_logo.svg') }}" alt="Lead Qualification Services Logo" />
          <p class="tymbl_header_products_drop_block_title">Lead Qualification Services</p>
          <p class="tymbl_header_products_drop_block_description">We&nbsp;qualify your unused leads. No&nbsp;money upfront.</p>
        </a>
        <a href="#dialer" class="tymbl_header_products_drop_block">
          <img src="{{ asset('assets/img/tymbl_v3/product_sd_logo.svg') }}" alt="Smart Dialer Logo" />
          <p class="tymbl_header_products_drop_block_title">Smart Dialer with AI&nbsp;data scrubber</p>
          <p class="tymbl_header_products_drop_block_description">5×&nbsp;more dials with AI&nbsp;scrubbing your leads. Track your progress.</p>
        </a>
      </div>
    </div>
    <div id="tymbl_header_right">
      <ul id="tymbl_header_nav">
        <li><a href="/page/about-us" class="tymbl_header_link">Team</a></li>
        <li><a href="/page/how-it-works" class="tymbl_header_link">Help</a></li>
        <li><a href="/pricing" class="tymbl_header_link">Pricing</a></li>
      </ul>
      <ul id="tymbl_header_btn">
        <li><a href="https://dialer.tymbl.com/login" class="tymbl_btn tymbl_btn_32 tymbl_btn_stroke tymbl_btn_round">Sign In</a></li>
        <li><a href="https://dialer.tymbl.com/register" class="tymbl_btn tymbl_btn_32 tymbl_btn_fill tymbl_btn_round">Register</a></li>
      </ul>
    </div>
    <a id="tymbl_header_hamburger" href="#"><img id="tymbl_header_hamburger_img" src="{{ asset('assets/img/tymbl_v3/header_menu_open.svg') }}" /></a>
  </div>
  <div id="tymbl_header_mobile">
    <ul id="tymbl_header_mobile_products">
      <li><a href="#" class="tymbl_header_mobile_link" id="tymbl_header_mobile_link_products">
        <span>Products</span>
        <img src="{{ asset('assets/img/tymbl_v3/arrow_products_down.svg') }}" id="tymbl_header_mobile_link_products_arrow" />
      </a></li>
    </ul>
    <div id="tymbl_header_mobile_products_drop_list">
      <a href="#qualification" class="tymbl_header_mobile_products_drop_block">
        <img src="{{ asset('assets/img/tymbl_v3/product_lqs_logo.svg') }}" alt="Lead Qualification Services Logo" />
        <p class="tymbl_header_mobile_products_drop_block_title">Lead Qualification Services</p>
        <p class="tymbl_header_mobile_products_drop_block_description">We&nbsp;qualify your unused leads. No&nbsp;money upfront.</p>
      </a>
      <a href="#dialer" class="tymbl_header_mobile_products_drop_block">
        <img src="{{ asset('assets/img/tymbl_v3/product_sd_logo.svg') }}" alt="Smart Dialer Logo" />
        <p class="tymbl_header_mobile_products_drop_block_title">Smart Dialer with AI&nbsp;data scrubber</p>
        <p class="tymbl_header_mobile_products_drop_block_description">5×&nbsp;more dials with AI&nbsp;scrubbing your leads. Track your progress.</p>
      </a>
      <a href="#referrals" class="tymbl_header_mobile_products_drop_block">
        <img src="{{ asset('assets/img/tymbl_v3/product_rm_logo.svg') }}" alt="Referral Management Logo" />
        <p class="tymbl_header_mobile_products_drop_block_title">Referral management and&nbsp;exchange</p>
        <p class="tymbl_header_mobile_products_drop_block_description">Manage referrals and find agents to&nbsp;work with them. Automate folow-up.</p>
      </a>
    </div>
    <ul id="tymbl_header_mobile_nav">
      <li><a href="https://www.tymbl.com/page/about-us" class="tymbl_header_mobile_link">Team</a></li>
      <li><a href="https://www.tymbl.com/page/how-it-works" class="tymbl_header_mobile_link">Help</a></li>
      <li><a href="/pricing" class="tymbl_header_mobile_link">Pricing</a></li>
    </ul>
    <ul id="tymbl_header_mobile_btn">
      <li id="tymbl_header_mobile_btn_signin"><a href="https://dialer.tymbl.com/login" class="tymbl_header_mobile_link">Sign In</a></li>
      <li id="tymbl_header_mobile_btn_register"><a href="https://dialer.tymbl.com/register" class="tymbl_header_mobile_link">Register</a></li>
    </ul>
  </div>
  <div id="tymbl_header_mobile_bg">&nbsp;</div>
</div>
