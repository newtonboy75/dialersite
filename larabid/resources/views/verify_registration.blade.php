@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default register-form">
                <div class="panel-heading">@lang('app.verify')</div>
                <div class="panel-body">

                    {{-- @include('admin.flash_msg') --}}

                    @if (Session::has('success'))
                    	@if(Session::get('success')=='2')
                        <div class="text-center verify-text"><p class="font-weight-bold">Thank you.</p>Please check your email to activate your account.</div><br><br>
                      @else
                        <div class="text-center verify-text"><p class="font-weight-bold">Thank you.</p>An activation code is sent to your mobile number. To confirm your registration, enter the 6-digit number inside the box and click Confirm.<br>This may take a few minute</div><br><br>

                        <form class="form-horizontal" role="form" method="POST" action="{{ route('verify-complete') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                          <div class="col-md-6 col-md-offset-3">
                            <input type="number" class="form-control" name="sms_activation_code" required>
                          </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-5">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-user-plus"></i> @lang('app.confirm_registration')
                                </button>

                                &nbsp;&nbsp;<a class="pull-right" href='{{ route('dashboard') }}'>Finish Later</a>
                            </div>
                        </div>
                    </form>
                      @endif
                    @else
                      @include('admin.flash_msg')
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@if(get_option('enable_recaptcha_registration') == 1)
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endif
