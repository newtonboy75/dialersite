<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Open Graph data -->
  <meta property="og:description" content="<?php echo trans('app.meta_description') ?>" />
  <meta property="og:title" content="<?php echo trans('app.meta_title') ?>" />
  <meta property="og:url" content="{{ url()->current() }}" />
  <meta property="og:type" content="website" />
  <meta property="og:locale" content="en-us" />
  <meta property="og:locale:alternate" content="en-us" />
  <meta property="og:site_name" content="{{get_option('site_name')}}" />
  <meta property="og:image:url" content="{{URL::asset('assets/img/tymbl-fb-image.jpg')}}" />
  <meta property="og:image:size" content="300" />

  <!-- favicon -->
  <link rel="shortcut icon" href="{{url('assets/img/landing/fav.png')}}">

  <!-- Twitter Card -->
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@tymbl1">
  <meta name="twitter:title" content="<?php echo trans('app.meta_title') ?>">
  <meta name="twitter:description" content="<?php echo trans('app.meta_description') ?>">
  <meta name="twitter:creator" content="@author_handle">
  <meta name="twitter:image:src" content="{{URL::asset('assets/img/tymbl-fb-image.jpg')}}">

  <!--Google+ -->
  <meta itemprop="name" content="<?php echo trans('app.meta_title') ?>">
  <meta itemprop="description" content="<?php echo trans('app.meta_description') ?>">
  <meta itemprop="image" content="{{URL::asset('assets/img/tymbl-fb-image.jpg')}}">

  <title>@section('title') {{ get_option('site_title') }} @show</title>

  <!-- bootstrap css -->
  <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-theme.min.css') }}">
  <!-- Font awesome 4.4.0 -->
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>

  <!-- load page specific css -->

  <!-- main select2.css -->
  <link href="{{ asset('assets/select2-4.0.3/css/select2.css') }}" rel="stylesheet" />
  <link rel="stylesheet" href="{{ asset('assets/plugins/toastr/toastr.min.css') }}">

  <!-- Conditional page load script -->
  @if(request()->segment(1) === 'dashboard')
  <link rel="stylesheet" href="{{ asset('assets/css/admin.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/plugins/metisMenu/dist/metisMenu.min.css') }}">
  @endif

  @if((request()->segment(1) === 'referral-document' && request()->segment(3)) == 'success' || request()->segment(1) === 'dashboard')
  <link type="text/css" rel="stylesheet" href="{{ asset('assets/plugins/waitMe/waitMe.css') }}">
  @endif

  @if((request()->segment(1) === 'title-company' && request()->segment(2)) == 'approve')
  <link type="text/css" rel="stylesheet" href="{{ asset('assets/plugins/waitMe/waitMe.css') }}">
  @endif

  <!-- main style.css -->
  <link rel="stylesheet" href="{{ asset("assets/css/style.css") }}">
  <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" >

  @if(is_rtl())
  <link rel="stylesheet" href="{{ asset("assets/css/rtl.css") }}">
  @endif

  @yield('page-css')

  @if(get_option('additional_css'))
  <style type="text/css">
  {{ get_option('additional_css') }}
  </style>
  @endif



  <script src="{{ asset('assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
  <script type="text/javascript">
  window.jsonData = {!! frontendLocalisedJson() !!};
  </script>

  <meta name="google-site-verification" content="fa25hLOssKHKzyrF0ilVxhIvfzqZfgtPq2J0aEe0Neo" />

</head>
<body class="@if(is_rtl()) rtl @endif">
  <div id="app">

    @if(env('APP_DEMO') == true)
    @include('demobar')
    @endif
    <div id="sub-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <div class="social-icons">
              @php
              $facebook_url = get_option('facebook_url');
              $twitter_url = get_option('twitter_url');
              $linked_in_url = get_option('linked_in_url');
              $dribble_url = get_option('dribble_url');
              $google_plus_url = get_option('google_plus_url');
              $youtube_url = get_option('youtube_url');
              @endphp
              <ul>
                @if($facebook_url)
                <li><a href="{{$facebook_url}}"><i class="fa fa-facebook"></i> </a> </li>
                @endif
                @if($twitter_url)
                <li><a href="{{$twitter_url}}"><i class="fa fa-twitter"></i> </a> </li>
                @endif
                @if($google_plus_url)
                <li><a href="{{$google_plus_url}}"><i class="fa fa-google-plus"></i> </a> </li>
                @endif
                @if($youtube_url)
                <li><a href="{{$youtube_url}}"><i class="fa fa-youtube"></i> </a> </li>
                @endif
                @if($linked_in_url)
                <li><a href="{{$linked_in_url}}"><i class="fa fa-linkedin"></i> </a> </li>
                @endif
                @if($dribble_url)
                <li><a href="{{$dribble_url}}"><i class="fa fa-dribbble"></i> </a> </li>
                @endif
              </ul>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="right-info">
              <ul>
                @if(get_option('enable_language_switcher') == 1)

                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> @if($current_lang) {{$current_lang->language_name}} @else @lang('app.language') @endif <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="{{ route('switch_language', 'en') }}">English</a></li>
                    @foreach(get_languages() as $lang)
                    <li><a href="{{ route('switch_language', $lang->language_code) }}">{{ $lang->language_name }}</a></li>
                    @endforeach
                  </ul>
                </li>
                @endif


                @if (Auth::guest())
                <li><a href="{{ route('login') }}">@lang('app.login')</a></li>
                <li><a href="{{ route('register') }}">@lang('app.register')</a></li>
                @else
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    {{ auth()->user()->name }} <span class="headerAvatar"> <img src="{{auth()->user()->get_gravatar()}}" /> </span> <span class="caret"></span>
                  </a>

                  <ul class="dropdown-menu" role="menu">
                    <li><a href="{{route('dashboard')}}"> @lang('app.dashboard') </a> </li>
                    <li>
                      <a href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                      @lang('app.logout')
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                  </li>
                </ul>
              </li>
              @endif



            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>


  <nav class="navbar navbar-default navbar-static-top">
    <div class="container">
      <div class="navbar-header">

        <!-- Collapsed Hamburger -->
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
          <span class="sr-only">Toggle Navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>

        <!-- Branding Image -->
        <a class="navbar-brand" href="{{ route('home') }}">
          <img src="{{ logo_url() }}" title="{{get_option('site_name')}}" alt="{{get_option('site_name')}}" />
        </a>
      </div>

      <div class="collapse navbar-collapse" id="app-navbar-collapse">
        <!-- Left Side Of Navbar -->
        <ul class="nav navbar-nav">
          &nbsp;<li><a href="{{route('home')}}">@lang('app.home')</a> </li>
          @if($header_menu_pages->count() > 0)
          @foreach($header_menu_pages as $page)
          <li><a href="{{ route('single_page', $page->slug) }}">{{ $page->title }} </a></li>
          @endforeach
          @endif


          &nbsp;<li><a href="{{route('create_ad')}}">@lang('app.post_an_ad')</a> </li>

        </ul>

        <!-- Right Side Of Navbar -->
        <ul class="nav navbar-nav navbar-right nav-sarchbar">
          <!-- Authentication Links -->


          <li>
            <p><a href="#" id="example-show" class="showLink" onclick="showHide('example');return false;" style="display: inline;"><i class="fa fa-search"></i></a></p>
            <div id="example" class="more" style="display: none;">
              {!! Form::open(['route' => 'search_redirect','method' => 'get', 'class' => 'form-inline']) !!}
              <input type="text" class="form-control" id="searchKeyword" name="q" placeholder="@lang('app.what_are_u_looking')">

              {!! Form::close() !!}
              <p><a href="#" id="example-hide" class="hideLink" onclick="showHide('example');return false;"><i class="fa fa-close"></i></a></p>
            </div>
          </li>

        </ul>
      </div>
    </div>
  </nav>

  @yield('content')

  <div id="footer">
    <div class="container">
      <div class="row">
        <div class="col-md-12">

          <ul class="footer-menu">
            <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> @lang('app.home')</a></li>

            @if($show_in_footer_menu->count() > 0)
            @foreach($show_in_footer_menu as $page)
            <li><a href="{{ route('single_page', $page->slug) }}">{{ $page->title }} </a></li>
            @endforeach
            @endif
            <li><a href="{{ route('contact_us_page') }}">@lang('app.contact_us')</a></li>
          </ul>

          <div class="footer-heading">
            <h3>{{get_option('site_name')}}</h3>
          </div>

          <div class="footer-copyright">
            <p>{{get_text_tpl(get_option('footer_copyright_text'))}}</p>
          </div>

          <div class="footer-social-links">
            @php
            $facebook_url = get_option('facebook_url');
            $twitter_url = get_option('twitter_url');
            $linked_in_url = get_option('linked_in_url');
            $dribble_url = get_option('dribble_url');
            $google_plus_url = get_option('google_plus_url');
            $youtube_url = get_option('youtube_url');
            @endphp
            <ul>
              @if($facebook_url)
              <li><a href="{{$facebook_url}}"><i class="fa fa-facebook"></i> </a> </li>
              @endif
              @if($twitter_url)
              <li><a href="{{$twitter_url}}"><i class="fa fa-twitter"></i> </a> </li>
              @endif
              @if($google_plus_url)
              <li><a href="{{$google_plus_url}}"><i class="fa fa-google-plus"></i> </a> </li>
              @endif
              @if($youtube_url)
              <li><a href="{{$youtube_url}}"><i class="fa fa-youtube"></i> </a> </li>
              @endif
              @if($linked_in_url)
              <li><a href="{{$linked_in_url}}"><i class="fa fa-linkedin"></i> </a> </li>
              @endif
              @if($dribble_url)
              <li><a href="{{$dribble_url}}"><i class="fa fa-dribbble"></i> </a> </li>
              @endif
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@if(request()->segment(1) === 'referral-document' && request()->segment(3) === 'success')
<div id="rating_popup" class="rating_popup">
  <a href="#" id="popup_close" class="alert-link alert alert-secondary" style="display: none; float:right;padding:0 0.4em;font-size:26px;">×</a>
  <div>
    <div class="text-center" id="rating-referral">


      <h3>Thank you for signing the REFERRAL AGREEMENT!</h3>
      <p class="misc-text-popup">This contract has now been returned, and a copy of the document will be sent to you when all parties have signed.</p>

      <h4 class="center-btn popup-txt">Please rate how your transaction went</h4>
      @include('ratings.rating')
    </div>
    <div id="option">
      <h4  class="center-btn">Let us know why you gave this transaction 1 star</h4>
      <div class="input-group">
        @if($type == 'selling')
        <div class="input-group-text">
          <input name="reason" id="reason" type="radio" value="Prospect contact info was incorrect"> <div>Prospect contact info was incorrect</div>
        </div>
        <div class="input-group-text">
          <input name="reason" id="reason"  type="radio" value="Seller didn't provide the prospect contact information"> <div>Seller didn't provide the prospect contact information</div>
        </div>
        <div class="input-group-text">
          <input name="reason" id="reason"  type="radio" value="Prospect didn't expect to be contacted"> <div>Prospect didn't expect to be contacted</div>
        </div>
        <div class="input-group-text">
          <input name="reason" id="reason"  type="radio" value="Seller was slow to respond"> <div>Seller was slow to respond</div>
        </div>
        <div class="input-group-text">
          <input name="reason" id="reason" type="radio" value="Other"> <div>Other</div>
        </div>
        @else
        <div class="input-group-text">
          <input name="reason" id="reason"  type="radio" value="Buyer was slow to respond"> <div>Buyer was slow to respond</div>
        </div>
        <div class="input-group-text">
          <input name="reason" id="reason" type="radio" value="Buyer was slow to sign the referral agreement"> <div>Buyer was slow to sign the referral agreement</div>
        </div>
        <div class="input-group-text">
          <input name="reason" id="reason" type="radio" value="Other"> <div>Other</div>
        </div>
        @endif
        <div id="reason-details">Please provide details<br>
          <input type="text" class="form-control" id="other-details"><br><br>
          <div class="center-btn"><a id="submit-feedback" class="btn btn-primary active text-center" role="button" href="#">Submit</a></div>
        </div>
      </div>
    </div>
    <div class="text-center text-success" id="thank-you-feedback">Thank you for your feedback.<br><br>
      <button type="button" id="popup_close" class="btn" href="#">Close</button><br><br>
    </div>

    <div class="popup-footer text-center">Signatures are collected and stored by https://esignatures.io</div>&nbsp;
  </div>
</div>
@endif

<script src="{{ asset('assets/js/vendor/jquery-1.11.2.min.js') }}"></script>
<script src="{{ asset('assets/js/vendor/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/plugins/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('assets/select2-4.0.3/js/select2.min.js') }}"></script>

@if((request()->segment(1) === 'referral-document' && request()->segment(3)) == 'success' || request()->segment(1) === 'dashboard')
<script src="{{ asset('assets/plugins/jquery-popup-overlay-gh-pages/jquery.popupoverlay.js') }}"></script>
<script src="{{ asset('assets/plugins/waitMe/waitMe.js') }}"></script>
@endif

@if((request()->segment(1) === 'title-company' && request()->segment(2)) == 'approve')
<script src="{{ asset('assets/plugins/waitMe/waitMe.js') }}"></script>
@endif

@if(request()->segment(1) === 'referral-document' && request()->segment(3) == 'success')
<!-- edit title company popup -->
<script>
$('#rating_popup').popup('hide');

$('[id="edit-title-company"]').click(function() {
  window.location = '/dashboard/u/posts/profile/edit#rep';
});

$('[id="ok-title-company"]').click(function() {

  $('#loader').waitMe({
    effect : 'bounce',
    text : '',
    color : '#000',
  });


  $.ajax({
    url: '/send-contract-to-tc',
    method: 'post',
    data: { id: '{{ request()->segment(2) }}', code: '{{ $code }}', title_company_name: '{{$title_company_name}}', url: '{{ $url }}', email: '{{ $email }}', _token: '{{csrf_token()}}' },
    success: function(data) {
      // console.log(data);
      if(data == '1'){
        window.location = '/dashboard/payments';
      }
    }
  });
});
</script>

<!-- rating popup -->
<script>
$('#rating_popup').popup('show');
$('[id="popup_close"]').click(function() {
  $('#rating_popup').popup('hide');
});

$('[id="rating"]').click(function() {
  var item_rating = $(this).attr('rel');
  var currentListID = {{request()->segment(2)}};
  $(this).addClass('check');
  $(this).nextAll().addClass('check');
  $(this).prevAll().removeClass('check');

  if(item_rating == 1){
    $('#option').fadeIn('slow');
    $('[id="reason"]').on('click', function() {
      var reason = $(this).val();
      if( reason == 'Other'){
        $('#reason-details').fadeIn();
        $('#thank-you-feedback').fadeOut();
        $('#other-details').val('');
        $('#submit-feedback').click(function() {
          if($('#other-details').val() == ''){
            alert('Detail should not be empty.');
          }else{
            submit_rating(item_rating, currentListID, reason, $('#other-details').val());
            $('#other-details').val('');
            $(this).fadeOut();
          }
        });
      }else{
        $('#reason-details').fadeOut('slow');
        submit_rating(item_rating, currentListID, reason, '');
        $('#thank-you-feedback').fadeIn('slow');
      }
    });
  }else{
    submit_rating(item_rating, currentListID, '', '');
    $('#option').fadeOut('slow');
  }
});

function submit_rating(item_rating, list_id, reason, other){
  $.ajax({
    url: '/post-rating/',
    method: 'post',
    data: { id: item_rating, list_id: list_id, reason: reason, other: other, _token: '{{csrf_token()}}' },
    success: function(data) {
      $('#thank-you-feedback').fadeIn();
      //console.log(data);
    }
  });
}
</script>
@endif

@if(request()->segment(6) === '#')
<script>
alert('this');
</script>
@endif



<!-- Conditional page load script -->
@if(request()->segment(1) == 'dashboard')
<script src="{{ asset('assets/plugins/metisMenu/dist/metisMenu.min.js') }}"></script>
<!--
<script>
$(function() {
  $('#side-menu').metisMenu();
  getElementsByTagName('option')[1].selected;
});
</script>
-->
<script>
$('#remind_me_popup').popup('hide');
$('#remind_me_popup').popup({
  transition: 'all 0.3s',
  opacity: 0.1,
  setzindex: true,
  autofocus: true,
  onclose: function() {
    $.ajax({
      type: 'POST',
      url: '{{ route('save-notification') }}',
      data: { optin: '2', _token: '{{csrf_token()}}' },
      success: function(data) {
        $('#remind_me_popup').popup('hide');
      }
    });
  }
});
$('#remind_me_popup').popup('show');
$('[name="country"]').change(function () {
  var country_id = $(this).val();
  $('#state_loader').show();
  $.ajax({
    type: 'POST',
    url: '{{ route('get_state_by_country') }}',
    data: {country_id: country_id, _token: '{{ csrf_token() }}'},
    success: function (data) {
      generate_option_from_json(data, 'country_to_state');
    }
  });
});

$('[name="state"]').change(function () {
  var state_id = $(this).val();
  $('#city_loader').show();
  $.ajax({
    type: 'POST',
    url: '{{ route('get_city_by_state') }}',
    data: {state_id: state_id, _token: '{{ csrf_token() }}'},
    success: function (data) {
      generate_option_from_json(data, 'state_to_city');
    }
  });
});

$('#poppup-remind-sms').click(function() {
  $('.popup-text-email').fadeOut();
  $('#poppup-remind-email').prop('checked', false);
  $('#poppup-remind-both').prop('checked', false);
  $('.popup-text-phone').fadeIn();
});

$('#poppup-remind-email').click(function() {
  $('.popup-text-phone').fadeOut();
  $('#poppup-remind-sms').prop('checked', false);
  $('#poppup-remind-both').prop('checked', false);
  $('.popup-text-email').fadeIn();
});
$('#poppup-remind-both').click(function() {
  $('#poppup-remind-sms').prop('checked', false);
  $('#poppup-remind-email').prop('checked', false);
  $('.popup-text-phone').fadeIn();
  $('.popup-text-email').fadeIn();
});

$('#popup_close').click(function() {
  $('#remind_me_popup').popup('hide');
});

$('#popup-submit').click(function() {
  $('#popup-error').hide();
  if($('[name="country"]').val()==''){
    $('#popup-error').text('Please choose a country');
    $('#popup-error').fadeIn();
  }else if($('[name="state"]').val()==''){
    $('#popup-error').text('Please choose a state');
    $('#popup-error').fadeIn();
  }else if(!$('#remind-email').is(':checked') && !$('#remind-phone').is(':checked')){
    $('#popup-error').text('Either mobile number or email address is required to send alerts');
    $('#popup-error').fadeIn();
  }else if($('#remind-phone').is(':checked') && $('#popup-text-phone').val() ==''){
    $('#popup-error').text('Mobile number is required');
    $('#popup-error').fadeIn();
  }else if($('#remind-email').is(':checked') && $('#popup-text-email').val()==''){
    $('#popup-error').text('Email is required');
  }else{
    $.ajax({
      type: 'POST',
      url: '{{ route('save-notification') }}',
      data: { country_id: $('[name="country"]').val(), state_id: $('[name="state"]').val(), city_id: $('[name="city"]').val(), phone: $('#popup-text-phone').val(), email: $('#popup-text-email').val(), _token: '{{csrf_token()}}' },
      success: function(data) {
        $('#popup-error').html('<span style="color: #333 !important;">Notification preferences saved.</span>');
        $('#popup-error').fadeIn();
        setTimeout(function () {$('#remind_me_popup').popup('hide');}, 3000);
        //console.log(data);
      }
    });
  }
});
</script>
<script>
function generate_option_from_json(jsonData, fromLoad) {
  //Load Category Json Data To Brand Select
  if (fromLoad === 'country_to_state') {
    var option = '';
    if (jsonData.length > 0) {
      option += '<option value="0" selected> @lang('app.select_state') </option>';
      for (i in jsonData) {
        option += '<option value="' + jsonData[i].id + '"> ' + jsonData[i].state_name + ' </option>';
      }
      $('#state_select').html(option);
      $('#state_select').select2();
    } else {
      $('#state_select').html('');
      $('#state_select').select2();
    }
    $('#state_loader').hide('slow');

  } else if (fromLoad === 'state_to_city') {
    var option = '';
    if (jsonData.length > 0) {
      option += '<option value="0" selected> @lang('app.select_city') </option>';
      for (i in jsonData) {
        option += '<option value="' + jsonData[i].id + '"> ' + jsonData[i].city_name + ' </option>';
      }
      $('#city_select').html(option);
      $('#city_select').select2();
    } else {
      $('#city_select').html('');
      $('#city_select').select2();
    }
    $('#city_loader').hide('slow');
  } else if(fromLoad === 'state_with_cities') {
    //console.log(jsonData.length);
    var option = '';
    if (jsonData.length > 0) {
      //console.log(jsonData);
      option += '<option value="0" selected> @lang('app.select_city') </option>';

      for (var i=0; i<jsonData.length; i++) {
        let statename = jsonData[i][0];
        let cityarray = jsonData[i][1];
        option += '<option disabled="disabled" value="' + statename + '"> <div class="background:#333 !important; font-size:10px !important;">' + statename + '</div></option>';

        for(var j=0; j<cityarray.length; j++){
          option += '<option value="' + cityarray[j]['id'] + '">&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp' + cityarray[j]['city_name'] + ' </option>';
        }
      }

      $('#city_select').html(option);
      $('#city_select').select2();
    } else {
      $('#city_select').html('');
      $('#city_select').select2();
    }
  }
}
</script>
@endif

<script src="{{ asset('assets/js/main.js') }}"></script>
<script>
var toastr_options = {closeButton : true};
$('#flash-success').delay(5000).fadeOut(400);

var load_num = 0;

$('#esignature-iframe').load(function()
{
  var listid = {!! request()->segment(2); !!}

  load_num += 1;
  if(load_num == 2){
    $.ajax({
      url: '/referral-signed/',
      method: 'post',
      data: { id: listid, _token: '{{csrf_token()}}' },
      success: function(data) {
        window.location = listid+'/success';
      }
    });
  }else if(load_num > 2){
    window.location = '/dashboard';
  }
});
</script>

@if(request()->segment(1) === 'title-company')
<script src="{{ asset('assets/plugins/Bootstrap-Confirmation-2/bootstrap-confirmation.js') }}"></script>

<script>
if (window.performance && window.performance.navigation.type == window.performance.navigation.TYPE_BACK_FORWARD) {
  location.reload();
}
</script>
@endif

@if(request()->segment(1) === 'title-company' && request()->segment(2) == 'approve')
<script>
$('[data-toggle=sold]').confirmation({
  rootSelector: '[data-toggle=confirmation]',
  onConfirm: function(value) {
    $.ajax({
      url: '/title-approve/',
      method: 'post',
      data: { id: '{{ $valid_contract->contract_id }}', status: "1", _token: '{{csrf_token()}}' },
      beforeSend: function() {
        $('#loader').waitMe({
          effect : 'bounce',
          text : '',
          color : '#000',
        });
      },
      success: function(data) {
        //console.log(data);
        if(data == '1'){
          window.location = '/title-approve/1';
          //alert(data);
        }else{
          alert('An error occured while proccessing your request. Please try again later')
        }
      }
    });
  }
});

$('[data-toggle=not-sold').confirmation({
  rootSelector: '[data-toggle=confirmation]',
  onConfirm: function(value) {
    $.ajax({
      url: '/title-approve/',
      method: 'post',
      data: { id: '{{ $valid_contract->contract_id }}', status: "2", _token: '{{csrf_token()}}' },
      beforeSend: function() {
        $('#loader').waitMe({
          effect : 'bounce',
          text : '',
          color : '#000',
        });
      },
      success: function(data) {
        //console.log(data);
        if(data == '1'){
          window.location = '/title-approve/2';
        }else{
          alert('An error occured while proccessing your request. Please try again later')
        }
      }
    });
  }
});
</script>
@endif

@if(request()->segment(1) === 'payment' && request()->segment(2) == 'success')
<script>
setTimeout(function(){ window.location = '/referral-document/{{$ad_id}}' }, 3000);
</script>
@endif

@if (session()->has('success'))
  @if(session('success') == 'Listing Saved Successfully')
  <script>
  alert('New listing created successfully');

    gtag('event', 'PostAdSuccess', {
      'event_category': 'Post Page',
      'event_label': session('message'),
    });

  </script>
  @endif

@endif
@if(get_option('additional_js'))
{!! get_option('additional_js') !!}
@endif

@yield('page-js')
</body>
</html>
