@extends('layouts.app')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('content')


<div id="post-new-ad">
    <div class="container">
        <div class="row">
          <div class="thankyou-text">
            When the seller signs the referral agreement, a copy of the contract will be forwarded to <strong>{{$title_company_name}}</strong> representative.
            <p>&nbsp;</p>
            <div class="referral-success-buttons">
              <button id="ok-title-company" type="button" class="btn btn-primary">Ok</button>
              <button id="edit-title-company" type="button" class="btn btn-secondary">Change</button>
            </div>
            <div id="loader" style="margin-top:20px;">&nbsp;</div>
          </div>
        </div>
      </div>
  </div>
</div>

@endsection
