@extends('layouts.app')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('content')

    <div class="container">
        <div id="wrapper">

            @include('admin.sidebar_menu')

            <div id="page-wrapper">
                @if( ! empty($title))
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"> {{ $title }}  </h1>
                        </div> <!-- /.col-lg-12 -->
                    </div> <!-- /.row -->
                @endif

                @include('admin.flash_msg')

                <div class="row">
                    <div class="col-xs-12">

                        {!! Form::open(['class'=>'form-horizontal', 'files'=>'true']) !!}
                        <div class="form-group {{ $errors->has('name')? 'has-error':'' }}">
                            <label for="name" class="col-sm-4 control-label">@lang('app.name')</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="name" value="{{ old('name')? old('name') : $user->name }}" name="name" placeholder="@lang('app.name')">
                                {!! $errors->has('name')? '<p class="help-block">'.$errors->first('name').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('email')? 'has-error':'' }}">
                            <label for="email" class="col-sm-4 control-label">@lang('app.email')</label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control" id="email" value="{{ old('email')? old('email') : $user->email }}" name="email" placeholder="@lang('app.email')">
                                {!! $errors->has('email')? '<p class="help-block">'.$errors->first('email').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('gender')? 'has-error':'' }}">
                            <label for="gender" class="col-sm-4 control-label">@lang('app.gender')</label>
                            <div class="col-sm-8">
                                <select id="gender" name="gender" class="form-control select2">
                                    <option value="">Select Gender</option>
                                    <option value="male" {{ $user->gender == 'male'?'selected':'' }}>Male</option>
                                    <option value="female" {{ $user->gender == 'female'?'selected':'' }}>Fe-Male</option>
                                    <option value="third_gender" {{ $user->gender == 'third_gender'?'selected':'' }}>Third Gender</option>
                                </select>

                                {!! $errors->has('gender')? '<p class="help-block">'.$errors->first('gender').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('mobile')? 'has-error':'' }}">
                            <label for="mobile" class="col-sm-4 control-label">@lang('app.mobile')</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="mobile" value="{{ old('mobile')? old('mobile') : $user->mobile }}" name="mobile" placeholder="@lang('app.mobile')">
                                {!! $errors->has('mobile')? '<p class="help-block">'.$errors->first('mobile').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('phone')? 'has-error':'' }}">
                            <label for="phone" class="col-sm-4 control-label">@lang('app.phone')</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="phone" value="{{ old('phone')? old('phone') : $user->phone }}" name="phone" placeholder="@lang('app.phone')">
                                {!! $errors->has('phone')? '<p class="help-block">'.$errors->first('phone').'</p>':'' !!}
                            </div>
                        </div>


                        <div class="form-group {{ $errors->has('country_id')? 'has-error':'' }}">
                            <label for="phone" class="col-sm-4 control-label">@lang('app.country')</label>
                            <div class="col-sm-8">
                                <select id="country_id" name="country_id" class="form-control select2">
                                    <option value="">@lang('app.select_a_country')</option>
                                    @foreach($countries as $country)
                                        <option value="{{ $country->id }}" {{ $user->country_id == $country->id ? 'selected' :'' }}>{{ $country->country_name }}</option>
                                    @endforeach
                                </select>
                                {!! $errors->has('country_id')? '<p class="help-block">'.$errors->first('country_id').'</p>':'' !!}
                            </div>
                        </div>


                        <div class="form-group {{ $errors->has('address')? 'has-error':'' }}">
                            <label for="address" class="col-sm-4 control-label">@lang('app.address')</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="address" value="{{ old('address')? old('address') : $user->address }}" name="address" placeholder="@lang('app.address')">
                                {!! $errors->has('address')? '<p class="help-block">'.$errors->first('address').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('website')? 'has-error':'' }}">
                            <label for="website" class="col-sm-4 control-label">@lang('app.website')</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="website" value="{{ old('website')? old('website') : $user->website }}" name="website" placeholder="@lang('app.website')">
                                {!! $errors->has('website')? '<p class="help-block">'.$errors->first('website').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="form-group  {{ $errors->has('photo')? 'has-error':'' }}">
                            <label class="col-sm-4 control-label">@lang('app.change_avatar')</label>
                            <div class="col-sm-8">
                                <input type="file" id="photo" name="photo" class="filestyle" >
                                {!! $errors->has('photo')? '<p class="help-block">'.$errors->first('photo').'</p>':'' !!}
                            </div>
                        </div>

                        <hr />

                        <div id="rep" class="form-group {{ $errors->has('company_name')? 'has-error':'' }}">
                            <label for="company_name" class="col-sm-4 control-label">@lang('app.company_name')</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="company_name" value="{{ old('company_name')? old('company_name') : (isset($user->user_title_company_infos->company_name) ? $user->user_title_company_infos->company_name : '') }}" name="company_name" placeholder="@lang('app.company_name')">
                                {!! $errors->has('company_name')? '<p class="help-block">'.$errors->first('company_name').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('representative_name')? 'has-error':'' }}">
                            <label for="representative_name" class="col-sm-4 control-label">@lang('app.representative_name')</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="representative_name" value="{{ old('representative_name')? old('representative_name') : (isset($user->user_title_company_infos->representative_name) ? $user->user_title_company_infos->representative_name : '')  }}" name="representative_name" placeholder="@lang('app.representative_name')">
                                {!! $errors->has('representative_name')? '<p class="help-block">'.$errors->first('representative_name').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('representative_email')? 'has-error':'' }}">
                            <label for="representative_email" class="col-sm-4 control-label"></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="representative_email" value="{{ old('representative_email')? old('representative_email') : (isset($user->user_title_company_infos->representative_email) ? $user->user_title_company_infos->representative_email : '')  }}" name="representative_email" placeholder="@lang('app.representative_email')">
                                {!! $errors->has('representative_email')? '<p class="help-block">'.$errors->first('representative_email').'</p>':'' !!}
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-4">
                                <button type="submit" class="btn btn-primary">@lang('app.save')</button>
                            </div>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>

            </div>   <!-- /#page-wrapper -->

        </div>   <!-- /#wrapper -->


    </div> <!-- /#container -->
@endsection

@section('page-js')
    <script src="{{ asset('assets/js/bootstrap-filestyle.min.js') }}"></script>
    <script>
        $(":file").filestyle({buttonName: "btn-primary", buttonBefore: true});
    </script>
@endsection
