@extends('layouts.app')

@section('content')

    <div class="container">

    <div id="wrapper">

        @include('admin.sidebar_menu')

        <div id="page-wrapper">

            @if(session('error'))
                <div class="row">
                    <div class="col-lg-12">
                        <br />
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@lang('app.dashboard')</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="huge">{{ $approved_ads }}</div>
                                    <div>@lang('app.approved_ads')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="huge">{{ $pending_ads }}</div>
                                    <div>@lang('app.pending_ads')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="huge">{{ $blocked_ads }}</div>
                                    <div>@lang('app.blocked_ads')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @if($ten_contact_messages)
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="huge">{{ $total_users }}</div>
                                    <div>@lang('app.users')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="huge">{{ $total_reports }}</div>
                                    <div>@lang('app.reports')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="huge">{{ $total_payments }}</div>
                                    <div>@lang('app.success_payments')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="huge">  {{ $total_payments_amount }} <sup>{{ get_option('currency_sign') }}</sup></div>
                                    <div>@lang('app.total_payment')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

            </div>
            <!-- /.row -->

                @if($lUser->is_admin())
            <div class="row">
                @if($ten_contact_messages)
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            @lang('app.latest_ten_contact_messages')
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <tr>
                                    <th>@lang('app.sender')</th>
                                    <th>@lang('app.message')</th>
                                </tr>

                                @foreach($ten_contact_messages as $message)
                                    <tr>
                                        <td>
                                            <i class="fa fa-user"></i> {{ $message->name }} <br />
                                            <i class="fa fa-envelope-o"></i> {{ $message->email }} <br />
                                            <i class="fa fa-clock-o"></i> {{ $message->created_at->diffForHumans() }}
                                        </td>
                                        <td>{{ $message->message }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
                @endif

                @if($reports)
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            @lang('app.latest_ten_ads_report')
                        </div>
                        <div class="panel-body">

                            @if($reports->count() > 0)
                                <table class="table table-bordered table-striped table-responsive">
                                    <tr>
                                        <th>@lang('app.reason')</th>
                                        <th>@lang('app.email')</th>
                                        <th>@lang('app.message')</th>
                                        <th>@lang('app.ad_info')</th>
                                    </tr>

                                    @foreach($reports as $report)
                                        <tr>
                                            <td>{{ $report->reason }}</td>
                                            <td> {{ $report->email }}  </td>
                                            <td>
                                                {{ $report->message }}
                                                <hr />
                                                <p class="text-muted"> <i>@lang('app.date_time'): {{ $report->posting_datetime() }}</i></p>
                                            </td>
                                            <td>
                                                @if($report->ad)
                                                    <a href="{{ route('single_ad', [$report->ad->id, $report->ad->slug]) }}" target="_blank">@lang('app.view_ad')</a>
                                                    <i class="clearfix"></i>
                                                    <a href="{{ route('reports_by_ads', $report->ad->slug) }}">
                                                        <i class="fa fa-exclamation-triangle"></i> @lang('app.reports') : {{ $report->ad->reports->count() }}
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
                @endif
            </div>
            @endif
        </div>

        @if($user_notification_optin=='0')
        <div id="remind_me_popup">
          <div class="wrapper_close_button"><a href="#" id="popup_close" class="alert-link alert alert-secondary" style="float:right;padding:0 0.4em;font-size:26px;">×</a></div>
          <div>
            <div class="notify_cbx">
              <h4>Notify me when there are new referrals available in my area</h4>
              <div id="popup-dropdown" class="form-group  {{ $errors->has('country')? 'has-error':'' }}">
                  <label class="col-sm-4 control-label">@lang('app.country')</label>
                  <div class="col-sm-8">
                      <select class="form-control select2" name="country">
                          <option value="" >@lang('app.select_a_country')</option>
  //show only Canada and USA
                          @foreach($countries as $country)
                              @if($country->country_code_number=='124' || $country->country_code_number=='840')
                                <option value="{{ $country->id }}" {{ old('country') == $country->id ? 'selected' : '' }} {{ old('country') == '' && $country->country_code_number=='840' ? 'selected' : '' }}>{{ $country->country_name }}</option>
                              @endif
                          @endforeach
                      </select>
                      {!! $errors->has('country')? '<p class="help-block">'.$errors->first('country').'</p>':'' !!}
                  </div>
              </div>

              <div id="popup-dropdown" class="form-group  {{ $errors->has('state')? 'has-error':'' }}">
                  <label for="state_select" class="col-sm-4 control-label">@lang('app.state')</label><div class="text-info left-loader1">
                      <span id="state_loader" style="display: none;"><i class="fa fa-spin fa-spinner"></i> </span>
                  </div>
                  <div class="col-sm-8">
                      <select class="form-control select2" id="state_select" name="state">
                      <option value="">@lang('app.select_a_state')</option>
                          @if($previous_states->count() > 0)
                              @foreach($previous_states as $state)
                                  <option value="{{ $state->id }}" {{ old('state') == $state->id ? 'selected' : '' }}>{{ $state->state_name }}</option>
                              @endforeach
                          @endif
                      </select>
                  </div>
              </div>

              <div id="popup-dropdown" class="form-group  {{ $errors->has('city')? 'has-error':'' }}">
                  <label for="city_select" class="col-sm-4 control-label">@lang('app.city')</label><div class="text-info left-loader2">
                      <span id="city_loader" style="display: none;"><i class="fa fa-spin fa-spinner"></i> </span>
                  </div>
                  <div class="col-sm-8">
                      <select class="form-control select2" id="city_select" name="city">
                      <option value="">@lang('app.select_a_state')</option>
                          @if($previous_cities->count() > 0)
                              @foreach($previous_cities as $city)
                                  <option value="{{ $city->id }}" {{ old('city') == $city->id ? 'selected':'' }}>{{ $city->city_name }}</option>
                              @endforeach
                          @endif
                      </select>
                  </div>
                  <div class="popup-input">
                    <div><input type="checkbox" id="remind-phone"></div><div class="form-group popup-text-phone"><input id="popup-text-phone" class="form-control" type="text" name="mobile" value="" placeholder="Enter your mobile number"></div>
                    <div><input type="checkbox" id="remind-email"></div><div class="form-group popup-text-email"><input id="popup-text-email" class="form-control" type="text" name="email" value="" placeholder="Enter your email address"></div>
                </div>
                    <div id="popup-error">test</div>
                    <div class="text-center"><a class="btn btn-primary popup-submit" id="popup-submit" href="#" role="button">Save</a></div>
              </div>
            </div>
          </div>
        </div>
        @endif
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    </div> <!-- /#container -->
@endsection
