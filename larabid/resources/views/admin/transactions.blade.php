@extends('layouts.app')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection


@section('content')

    <div class="container">

        <div id="wrapper">

            @include('admin.sidebar_menu')

            <div id="page-wrapper">
                @if( ! empty($title))
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"> {{ $title }}  </h1>
                        </div> <!-- /.col-lg-12 -->
                    </div> <!-- /.row -->
                @endif

                @include('admin.flash_msg')

                <div class="row">
                    <div class="col-xs-12">
                      <div><h3>{{$header}}</h3></div>
                      <table class="table table-bordered table-striped">
                        <tr>
                            <th class="text-center">@lang('app.title')</th>
                            <th class="text-center">Contract Details</th>
                            <th class="text-center">Status</th>
                        </tr>
                          <?php $i = 0; ?>
                          @foreach($lists as $list)
                          <tr>
                              <td class="text-center">
                                @if(count($images) >= 1)
                                  @if($images[$i]->ad_id == $list->listing_id)
                                    <a href="/listing/{{$list->listing_id}}/{{$list->slug}}"><img width="100" src="{{url('/uploads/images/thumbs/'.$images[$i]->media_name)}}" /></a>
                                  @endif
                                @endif
                              </td>
                              <td>
                                <div class="contract_details">
                                  List Title: <a href="/listing/{{$list->listing_id}}/{{$list->slug}}">{{$list->title}}</a><br>
                                  Contract ID: {{$list->contract_id}}<br>
                                  Date: {{$list->created_at}}<br>
                                  <a href="/download-contract/{{$list->contract_id}}" rel="nofollow">Contract Pdf file</a>
                                </div>
                              </td>
                              <td class="text-center">
                                @if($list->list_status == '0')
                                  <div style="font-size:14px;"><i class="fa fa-clock-o" aria-hidden="true">&nbsp;&nbsp;Pending</i>
                                    <br><br>
                                    <form action="{{url('status-action')}}" method="POST" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                      <input type="hidden" value="0" name="id" />
                                      <input type="hidden" name="contract_id" value="{{$list->contract_id}}" />
                                      <input type="submit" value="Resend" style="border-radius: 4px; padding:3px 12px; border: 1px solid gray;" />
                                    </form>
                                  </div>
                                @elseif($list->list_status == '1')
                                  <div style="color: #333; font-size: 14px;"><i class="fa fa-check">&nbsp;&nbsp;Approved</i></div>
                                @else
                                  <div style="color: #333; font-size: 14px;"><i class="fa fa-ban">&nbsp;&nbsp;Rejected</i>
                                    <br><br>
                                    <form action="{{url('status-action')}}" method="POST" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                      <input type="hidden" value="2" name="id" />
                                      <input type="hidden" name="contract_id" value="{{$list->contract_id}}" />

                                      @if(request()->segment(2) === 'selling')
                                        <input type="submit" value="Repost" style="border-radius: 4px; padding:3px 12px; border: 1px solid gray;" />
                                      @endif

                                  </form>
                                  </div>
                                @endif
                              </td>
                          </tr>
                          @endforeach
                      </table>
                    </div>
                </div>





            </div>   <!-- /#page-wrapper -->




        </div>   <!-- /#wrapper -->


    </div> <!-- /#container -->
@endsection

@section('page-js')

@endsection
