@extends('layouts.app')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection


@section('content')

    <div class="container">

        <div id="wrapper">

            @include('admin.sidebar_menu')

            <div id="page-wrapper">
                @if( ! empty($title))
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"> {{ $title }}  </h1>
                        </div> <!-- /.col-lg-12 -->
                    </div> <!-- /.row -->
                @endif

                @include('admin.flash_msg')
                <div class="row">
                    <div class="col-xs-12">
                      <table class="table table-bordered table-striped">
                        <tr>
                            <th class="fixed-left">@lang('app.email')</th>
                            <td id="email" rel="{{$email}}"><input class="form-control editable" type="email" id="notification-email" value="{{$email}}" disabled="disabled"><a id="edit_email" class="add_btn" href="#">Edit</a><a href="#" id="btn-save-mail">Save</a></td>
                        </tr>
                        <tr>
                            <th class="fixed-left">@lang('app.phone')</th>
                            <td id="phone" rel="{{$phone}}"><input class="form-control editable" type="text" id="notification-phone" value="{{$phone}}" disabled="disabled"><a id="edit_phone" class="add_btn" href="#">Edit</a><a href="#" id="btn-save-phone">Save</a>

                            </td>
                        </tr>
                        <tr>
                            <th class="fixed-left">@lang('app.country')</th>
                            <td id="country_tag">
                              @if($countries=='')
                                nothing
                              @else
                              <select id="country_ti" style="display:none;" multiple data-role="tagsinput">
                                @foreach($countries as $country)

                                  @if($country == '231')
                                    <option rel="321" value="United States">United States</option>
                                  @else
                                    <option rel="38" value="Canada">Canada</option>
                                  @endif
                                @endforeach
                              </select>
                              <a id="add_country" class="add_btn" href="#">Change country</a>
                              <div id="selector-wrapper-country"><select class="form-control select2" id="country_select" name="country">
                                <option rel="" value="" selected disabled="disabled">Select Country</option>
                                <option value="38">Canada</option>
                                <option value="231">United States</option>
                              </select>
                              </div>
                              @endif
                              </td>
                        </tr>
                        <tr>
                            <th class="fixed-left">@lang('app.state')</th>
                            <td>
                              @if($states != '')
                              <select id="states_ti" style="display:none;" multiple data-role="tagsinput">
                              @foreach($states as $state => $st)
                                      @for($i=0; $i < count($st); $i++)
                                        <option rel="{{$st[$i]->id}}" value="{{$st[$i]->state_name}}">{{$st[$i]->state_name}}</option>
                                      @endfor
                              @endforeach
                            </select>
                            @endif

                              <a id="add_state" class="add_btn" href="#">Add state</a>
                              <div id="selector-wrapper-state"><select class="form-control select2" id="state_select" name="state"></select>
                              </div>

                            </td>
                        </tr>

                        <tr>
                            <th class="fixed-left">@lang('app.city')</th>
                            <td>
                              @if($cities != '')
                              <select id="city_ti" style="display:none;"  multiple data-role="tagsinput">
                                @foreach($cities as $city=> $ct)
                                        @for($i=0; $i < count($ct); $i++)
                                          @if($ct[$i]->city_name != '0')
                                            <option rel="{{$ct[$i]->id}}" value="{{$ct[$i]->city_name}}">{{$ct[$i]->city_name}}</option>
                                          @endif
                                        @endfor
                                @endforeach
                            </select>
                            @endif

                            <a id="add_city" class="add_btn" href="#">Add city</a>
                            <div id="selector-wrapper-city"><select class="form-control select2" id="city_select" name="city"></select>

                            </td>
                        </tr>

                      </table>
                    </div>

                </div>





            </div>   <!-- /#page-wrapper -->




        </div>   <!-- /#wrapper -->


    </div> <!-- /#container -->
@endsection

@section('page-js')

@endsection
