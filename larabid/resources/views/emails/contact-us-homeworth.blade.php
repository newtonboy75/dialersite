<!DOCTYPE html>
<html>
<head>
    <title>Homeworth Submission</title>
    <style type="text/css">
      div{
        margin-left: 40px;
        margin-right:40px;
        font-size:12px;
      }
      .footer-text{
        font-size: 14px;
      }
    </style>
</head>
<body style="font-size: 15px;">
  <h4>New Homeworth Submission</h4>
  <p><strong>From: </strong> {{$name}}</p>
  <p><strong>Email: </strong> {{$email}}</p>
  <p><strong>Date: </strong> {{$date}} </p>
  <p><strong>Message: </strong><br> {{$bodymessage}}</p>
</body>
</html>
