<!DOCTYPE html>
<html>
<head>
    <title>Contract Buyer</title>
  <style>
  	body {font-family: arial; padding: 10px 10px; font-size: 16px; color: #212121;}
  	.footer-text{font-size: 15px; margin-top:70px; text-align:center;  width: 90%; margin-bottom: 10px;}

    .listing-link{font-size: 17px !important; display: block; padding:8px; margin-left:auto; margin-right: auto; text-align: center;     color: rgb(255, 255, 255);
    background-color: rgb(0, 123, 255); border-color: rgb(0, 123, 255); font-weight: 600; text-decoration:none; width: 280px; margin-bottom: 20px; margin-top: 5px;border-radius: 2px!important;}
    .list_img{width:100%;margin-top:10px;margin-bottom:10px;}
    table{ border:1px solid #f0f0f0; background:#eee; padding: 8px; font-size: 14px; color: #212121; margin: 20px 0px;}
    .tl{padding-right: 30px; font-weight: 600; text-align: top;}
    td{padding: 5px;}
    .logo{width: 120px;}
  </style>
</head>
<body>
<div><img class="logo" src="https://tymbl.com/assets/img/tymbl/logo.png"></div><p>&nbsp;</p>
    <div>
      <p>Hello {{ ucfirst($user->first_name) }} {{ ucfirst($user->last_name) }}!</p>
      <p>As we mentioned in our previous communication, we are legally required to include your Designated/Principal  Broker contact information in the referral agreements you will be signing in order to reserve these leads. Since you haven't provided these details we are required to suspend your Tymbl account. Please note that any referral agreements you have signed on Tymbl are now terminated. If you feel that this change in your account status has been committed in error, please contact us at <a href="mailto:support@tymbl.com">support@tymbl.com</a> immediately.</p>
      <br>
      Thank you
      <p><strong>The Tymbl Team</strong></p>
  </div>


<div class="footer-text"><a href="tymbl.com/">Browse New Listings</a> | <a href="mailto:admin@tymbl.com">Contact us<a/></div></div>
</body>
