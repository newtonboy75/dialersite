<!DOCTYPE html>
<html>
<head>
    <title>Contract Seller</title>
  <style>
  	body {font-family: arial; padding: 10px 10px; font-size: 16px; color: #212121;}
  	.footer-text{font-size: 15px; margin-top:70px; text-align:center;  width: 90%; margin-bottom: 10px;}

    .listing-link{font-size: 17px !important; display: block; padding:8px; margin-left:auto; margin-right: auto; text-align: center;     color: rgb(255, 255, 255);
    background-color: rgb(0, 123, 255); border-color: rgb(0, 123, 255); font-weight: 600; text-decoration:none; width: 280px; margin-bottom: 20px; margin-top: 5px;border-radius: 2px!important;}
    .list_img{width:100%;margin-top:10px;margin-bottom:10px;}
    table{ border:1px solid #f0f0f0; background:#eee; padding: 8px; font-size: 14px; color: #212121; margin: 20px 0px;}
    .tl{padding-right: 30px; font-weight: 600; text-align: top;}
    td{padding: 5px;}
    .logo{width: 120px;}
  </style>
</head>
<body>
<div><img class="logo" src="https://tymbl.com/assets/img/tymbl/logo.png"></div><p>&nbsp;</p>

  @if($reason == '1')
    <div><br>
      <p>Congratulations! We have been notified by the title company representative of the Destination Firm that your referral agreement - {{$listing_name}} {{$id }} signed {{$date_signed}} has resulted in a successful transaction! Your share of the commissions will be sent to you directly by the title company. Also, the funds in the amount of {{$escrow}} - the escrow amount you set to reserve your listing, have been successfully sent to your PayPal account. Please note that there was a 1% deducted from the total to cover PayPal fees.
      <br>
      <table>
      	<tr>
        <td class="tl">Status</td><td>Transfer successful</td>
        </tr>
        <tr>
        <td class="tl">Initiated on</td><td>{{date("F d, Y h:i:s", strtotime($payout_date))}}</td>
        </tr>
        <tr>
        <td class="tl">Transfer Amount&nbsp;&nbsp;</td><td>{{$escrow}}</td>
        </tr>
        <tr>
        <td class="tl">Escrow Transfer Fee</td><td>1%</td>
        </tr>
        <tr>
          @php
          $formatted_amount = trim(preg_replace("/[^0-9.]/", "", trim($escrow)));
          $currency = substr($escrow, 0, 3);
          $escrow_fee = ($formatted_amount / 100) * 1;
          $final_amount = ($formatted_amount -$escrow_fee);
          @endphp
        <td class="tl">What you'll get</td><td>{{$currency}} ${{$final_amount}}</td>
        </tr>
        <tr>
        <td class="tl">Transaction ID</td><td>{{$payout_sender_id}}</td>
        </tr>
      </table>
      <br>
      Thank you
      <p><strong>The Tymbl Team</strong></p>
    </div>
  @else
    <div>
      <p>Hi {{$poster}}!</p><p>We have been notified by the title company representative of the Destination Firm that your referral agreement - {{$listing_name}}  {{$id}}  signed {{$date_signed}} has not resulted in a successful transaction. The funds in the amount of {{$escrow}} - the escrow amount you set to reserve your listing, will be will be returned to the Destination Firm agent's PayPal account within 30 days.</p>
      <br>
      Thank you
      <p><strong>The Tymbl Team</strong></p>
  </div>
  @endif

<div class="footer-text"><a href="tymbl.com/">Browse New Listings</a> | <a href="mailto:admin@tymbl.com">Contact us<a/></div></div>
</body>
