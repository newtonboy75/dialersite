<!DOCTYPE html>
<html>
<head>
  <title>Email Verification</title>
  <style>
  body {font-family: arial; padding: 10px 10px; font-size: 16px;}
  .footer-text{font-size: 15px; margin-top:40px; text-align:center;  width: 90%; margin-bottom: 30px;}

  .listing-link{font-size: 17px !important; display: block; padding:8px; margin-left:auto; margin-right: auto; text-align: center;     color: rgb(255, 255, 255);
    background-color: rgb(0, 123, 255); border-color: rgb(0, 123, 255); font-weight: 600; text-decoration:none; width: 280px; margin-bottom: 20px; margin-top: 5px;border-radius: 2px!important;}
    .list_img{width:100%;margin-top:10px;margin-bottom:10px;}
    .code{padding:8px; background: #eeeeee; font-size: 20px; text-align:center; width: 300px; margin-left:auto; margin-right: auto; margin-bottom: 40px;}
    .logo{width: 120px;}
    </style>
  </head>
  <body>
    <div><img class="logo" src="https://tymbl.com/assets/img/tymbl/logo.png"></div>

    @if($seller == '0')

    <p>Congratulations, {{$buyer}}!</p>
    <p>You have successfully reserved {{$id}} + {{$listing_title}}, Transaction ID - {{$payout_id}}.  The funds in the amount of {{$payout}} have been collected and placed into the Tymbl escrow account. A copy of the signed agreement has been sent to the title company you indicated.</p>

    <h4>NOW WHAT?</h4>
    <ul>
      <li>If the referral results in a successful transaction, please make sure your title company representative logs in Tymbl with the the code we provided to them to mark the transaction complete.</li>
      <li>If the referral does not turn into a successful purchase or a sale, within the next 90 days from today or a Title Company rep notifies us that transaction is not successful, Tymbl will refund the escrow amount you paid to reserve the listing.</li>
    </ul>
    <p>&nbsp;</p>
    <p>Thank you,</p>
    <p><strong>The Tymbl team</strong></p>
    @else
    <p>Congratulations, {{$seller}}!</p>
    <p>{{$buyer}} of {{$broker}} has just signed the referral agreement for your listing {{$listing_title}} - {{$id}}. Transaction ID - {{$payout_id}}.  The funds in the amount of {{$payout}} you set to reserve your listing have been collected from {{$buyer}} and placed into the Tymbl escrow account. A copy of the signed agreement has been sent to the title company selected by {{$buyer}}.</p>
    <p><strong>IMPORTANT:</strong> Tymbl support team will be following up with the prospect in the next 24 hours. If no contact has been made with the prospect, the referral agreement [CONTRACT ID] will be terminated and the lead will become available for other agents to reserve.</p>
    <h4>NOW WHAT?</h4>
    <ul>
      <li>The Prospect Contact Information is now available in under Transactions - Reserved Leads section of the Dashboard. Make the "Transactions - Reserved" a link - <a href="{{ route('reserved-leads') }}">{{ route('reserved-leads') }}</a></li>
      <li>If the referral results in a successful transaction within the next 90 days, the title company will send your share of the commission set by the terms of the referral agreement.</li>
      <li>Tymbl will transfer the amount of the escrow you set to your PayPal account.</li>
      <li>If the referral does not turn into a successful purchase or a sale, within the next 90 days from today or a Title Company rep notifies us that transaction is not successful, Tymbl will refund the escrow amount you set to reserve your listing back to {{$buyer}} and your ad will be re-listed.</li>
    </ul>
    <p>&nbsp;</p>
    <p>Thank you,</p>
    <p><strong>The Tymbl team</strong></p>
    @endif
    <div class="footer-text"><a href="tymbl.com/">Browse New Listings</a> | <a href="mailto:admin@tymbl.com">Contact us<a/></div>
    </body>
    </html>
