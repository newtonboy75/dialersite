<!DOCTYPE html>
<html>
<head>
    <title>Contract Approval</title>
  <style>
  	body {font-family: arial; padding: 20px 20px; color: #212121;}
  	.footer-text{font-size: 20px; margin-top:10px;}
    .logo{width: 120px;}
  </style>
</head>

<body>
<div><img class="logo" src="https://tymbl.com/assets/img/tymbl/logo.png"></div><p>&nbsp;</p>
  @if($repost == '0')
    <div>
      <p>Congratulations! Your title company representative {{$referral_name}} has just let us know that the referral agreement for <strong>{{$listing_name}}</strong> you signed resulted in a successful sale! The funds from the escrow have been successfully transferred to the seller. We appreciate your business. Please come back and also share with your fellow real estate professionals!
      <br>
      <p class="footer-text">The Tymbl Team</p>
    </div>
  @else
    <div>
      <p>Buyer's title company representative has just let us know that the referral agreement for {{$listing_name}} didn't result in a successful sale. The funds from the escrow in the amount of [amount] have been successfully transferred back to the buyer. We appreciate your business! Please come back and also share with your fellow real estate professionals!</p>
      <br>
      <p class="footer-text">The Tymbl Team</p>
  </div>
  @endif

</body>
</html>
