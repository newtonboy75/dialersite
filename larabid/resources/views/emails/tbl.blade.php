<!DOCTYPE html>
<html>
<head>
  <title>Reservation Successful</title>
  <style>
  body {font-family: arial; padding: 10px 10px; font-size: 16px;}
  .footer-text{font-size: 15px; margin-top:40px; text-align:center;  width: 90%; margin-bottom: 30px;}
  .listing-link{font-size: 17px !important; display: block; padding:8px; margin-left:auto; margin-right: auto; text-align: center;     color: rgb(255, 255, 255);
    background-color: rgb(0, 123, 255); border-color: rgb(0, 123, 255); font-weight: 600; text-decoration:none; width: 280px; margin-bottom: 20px; margin-top: 5px;border-radius: 2px!important;}
    .list_img{width:100%;margin-top:10px;margin-bottom:10px;}
    .code{padding:8px; background: #eeeeee; font-size: 20px; text-align:center; width: 300px; margin-left:auto; margin-right: auto; margin-bottom: 40px;}
    .logo{width: 120px;}
    </style>
  </head>
  <body>
    <div><img class="logo" src="https://tymbl.com/assets/img/tymbl/logo.png"></div>
    <p>Hello {{$buyer_name}},</p>
    <p>Thank you for you successfully reserving the following lead on Tymbl:</p>
    <p>{{$transaction_id}}</p>
    {{$ad_title}}
    <p>
      Please find the full prospect details below:<br><br>
      <strong>Name: {{$referral_name}}</strong><br>
      Email: {{$referral_contact_email}}<br>
      Phone: {{$referral_phone}}<br>
      Address: {{$referral_address}}<br>
      <br><br>
      You can also find this information by logging in to your Tymbl account and navigating to your Dashboard.
      The Prospect Contact Information is now available in under Transactions - Reserved Leads section of the Dashboard/Transactions/Leads I reserved or following this link:<br><br>
      <a href="{{ route('reserved-leads') }}">https://tymbl.com/dashboard/reserved-leads</a>
      <br>
    </p>
    <p>Thank you,</p>
    <p><strong>The Tymbl team</strong></p>
    <div class="footer-text"><a href="tymbl.com/">Browse New Leads</a> | <a href="mailto:admin@tymbl.com">Contact us<a/></div>
    </body>
    </html>
