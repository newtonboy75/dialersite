<!DOCTYPE html>
<html>
<head>
    <title>Contract Seller</title>
  <style>
  	body {font-family: arial; padding: 10px 10px; font-size: 16px; color: #212121;}
  	.footer-text{font-size: 15px; margin-top:70px; text-align:center;  width: 90%; margin-bottom: 10px;}

    .listing-link{font-size: 17px !important; display: block; padding:8px; margin-left:auto; margin-right: auto; text-align: center;     color: rgb(255, 255, 255);
    background-color: rgb(0, 123, 255); border-color: rgb(0, 123, 255); font-weight: 600; text-decoration:none; width: 280px; margin-bottom: 20px; margin-top: 5px;border-radius: 2px!important;}
    .list_img{width:100%;margin-top:10px;margin-bottom:10px;}
    table{ border:1px solid #f0f0f0; background:#eee; padding: 8px; font-size: 14px; color: #212121; margin: 20px 0px;}
    .tl{padding-right: 30px; font-weight: 600; text-align: top;}
    td{padding: 5px;}
    .logo{width: 120px;}
  </style>
</head>
<body>
<div><img class="logo" src="https://tymbl.com/assets/img/tymbl/logo.png"></div><p>&nbsp;</p>

    <div>
      <p>Hi {{$poster}}!</p><p>It has been 90 days since the the referral agreement - {{$listing_name}} {{$contract_id}} you signed on {{$date_signed}}. It appears that the referral agreement has not resulted in a successful transaction. The funds in the amount of {{$escrow}} - the escrow amount you set to reserve your listing, will be will be returned to the Destination Firm agent's PayPal account within 30 days and the ad will become be re-listed. Please <a href="mailto:tymblapp@gmail.com">contact us</a> if you believe that this is incorrect and the referral has actually resulted in a successful transaction or you think it still has a strong chance to be successful, but there just is more time required.</p>
      <br>
      Thank you
      <p><strong>The Tymbl Team</strong></p>
  </div>


<div class="footer-text"><a href="tymbl.com/">Browse New Listings</a> | <a href="mailto:admin@tymbl.com">Contact us<a/></div></div>
</body>
