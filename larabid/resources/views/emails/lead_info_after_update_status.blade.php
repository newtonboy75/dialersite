<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Referral First Contact</title>
    <style>
        body {
            font-family: arial;
            padding: 10px 10px;
            font-size: 16px;
        }
        .footer-text {
            font-size: 15px;
            margin-top:70px;
            width: 90%;
            margin-bottom: 30px;
            text-align: center;
        }
    </style>
</head>
<body>
<div>
    <img src="https://tymbl.com/assets/img/tymbl/logo.png">
</div>

<div>
    <p>Hi {{ $oAd->buyer->first_name }} {{ $oAd->buyer->last_name }}!</p>
    <p>
        Hope you are doing well! We would like to hear from you on how things are with {{ $ad->title }}.
        Please click on the link below to provide an update.<br>
        <a href="{{url('/dashboard/reserved-leads')}}">STATUS UPDATE</a><br>
        Please note if no update has been provided within 48 hours from the receipt of this message,
        Tymbl reserves the right to terminate the referral agreement {{ $oAd->contract_id }} and
        let other agents reserve the lead.
    </p>
    <br>
    Thank you
    <p>
        <strong>The Tymbl Team</strong>
    </p>
</div>

<div class="footer-text">
    <a href="{{ route('home') }}">Browse New Listings</a> | <a href="mailto:admin@tymbl.com">Contact us</a>
</div>
</body>
</html>
