<!DOCTYPE html>
<html>
<head>
    <title>Contract Buyer</title>
  <style>
  	body {font-family: arial; padding: 10px 10px; font-size: 16px; color: #212121;}
  	.footer-text{font-size: 15px; margin-top:70px; text-align:center;  width: 90%; margin-bottom: 10px;}

    .listing-link{font-size: 17px !important; display: block; padding:8px; margin-left:auto; margin-right: auto; text-align: center;     color: rgb(255, 255, 255);
    background-color: rgb(0, 123, 255); border-color: rgb(0, 123, 255); font-weight: 600; text-decoration:none; width: 280px; margin-bottom: 20px; margin-top: 5px;border-radius: 2px!important;}
    .list_img{width:100%;margin-top:10px;margin-bottom:10px;}
    table{ border:1px solid #f0f0f0; background:#eee; padding: 8px; font-size: 14px; color: #212121; margin: 20px 0px;}
    .tl{padding-right: 30px; font-weight: 600; text-align: top;}
    td{padding: 5px;}
    .logo{width: 120px;}
  </style>
</head>
<body>
<div><img class="logo" src="https://tymbl.com/assets/img/tymbl/logo.png"></div><p>&nbsp;</p>

  @if($reason == '1')
    <div>
      <p>Congratulations! We have been notified by the title company representative of the Destination Firm that your referral agreement - [AD TITLE] [Transaction ID] signed [DAY/MONTH/YEAR] has resulted in a successful transaction! Your share of the commissions will be sent to you directly by the title company. Also, the funds in the amount of USD$[AMOUNT OF THE FEE SET IN THE AD - THE PAYPAL FEES] - the escrow amount you set to reserve your listing, have been successfully sent your PayPal account.
      <br>
      <table>
      	<tr>
        <td class="tl">Status</td><td>Transfer successful</td>
        </tr>
        <tr>
        <td class="tl">Initiated on</td><td>{{date("F d, Y h:i:s", strtotime($payout_date))}}</td>
        </tr>
        <tr>
        <td class="tl">Transfer Amount&nbsp;&nbsp;</td><td>{{$escrow}}</td>
        </tr>
        <tr>
        <td class="tl">PayPal Fee</td><td>$0.25</td>
        </tr>
        <tr>
        <td class="tl">What you'll get</td><td>{{$escrow}}</td>
        </tr>
        <tr>
        <td class="tl">Transaction ID</td><td>{{$payout_sender_id}}</td>
        </tr>
      </table>
      <br>
      Thank you
      <p><strong>The Tymbl Team</strong></p>
    </div>
  @else
    <div>
      <p>Hi {{$buyer}}!</p><p>We have been notified by your title company representative {{$title_company_name}} that the referral agreement - {{$listing_name}} {{$id}} you signed on {{$date_signed}} has not resulted in a successful transaction.  The funds in the amount of {{$escrow}} - the escrow amount you paid to reserve the listing, will be returned to your PayPal account within 30 days.</p>
      <br>
      Thank you
      <p><strong>The Tymbl Team</strong></p>
  </div>
  @endif

<div class="footer-text"><a href="tymbl.com/">Browse New Listings</a> | <a href="mailto:admin@tymbl.com">Contact us<a/></div></div>
</body>
