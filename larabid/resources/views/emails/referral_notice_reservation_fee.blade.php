<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Tymbl Team</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="Tymbl Team">
  <style>
  	body {font-family: arial; padding: 10px 10px; font-size: 16px;}
  	.footer-text{font-size: 15px; margin-top:70px; width: 90%; margin-bottom: 30px; text-align: center;}
    .listing-link{font-size: 17px !important; display: block; padding:8px; margin-left:auto; margin-right: auto; text-align: center; color: #ffffff; background-color: rgb(0, 123, 255); border-color: rgb(0, 123, 255); font-weight: 600; text-decoration:none; width: 280px; margin-bottom: 20px; margin-top: 5px;border-radius: 2px!important;}
    .list_img{width:100%;margin-top:10px;margin-bottom:10px;}
    .logo{width: 120px;}
  </style>
</head>

<body>
<div><img class="logo" src="https://tymbl.com/assets/img/tymbl/logo.png"></div>
<div style="text-align: left !important;"><br>

  <p>Congratulations, {{$name}}!</p>

  <p>You have successfully reserved {{$id}} <a href="{{route('home')}}/listing/{{$id}}/{{$slug}}">{{$title}}</a>, Transaction ID - {{$transaction_id}}. The funds in the amount of {{$amount}} have been collected and placed into the Tymbl escrow account. A copy of the signed agreement has been sent to the title company you indicated.</p>

  <p>NOW WHAT?</p>
  <ul>
  <li>If the referral results in a successful transaction, please make sure your title company representative logs in Tymbl with the the code we provided to them to mark the transaction complete.</li>
  <li>If the referral does not turn into a successful purchase or a sale, within the next 90 days from today or a Title Company rep notifies us that transaction is not successful, Tymbl will refund the escrow amount you paid to reserve the listing.</li>
  <li>Tymbl support team will be following up with the prospect in the next 24 hours. If no contact has been made with the prospect, the referral agreement {{$contract_id}} will be terminated and the lead will become available for other agents to reserve.</li>
  </ul>
<p>Thank you,</p>
<p>The Tymbl Team</p>
<br>
<div class="footer-text"><a href="{{ route('home') }}">Browse New Leads</a> | <a href="{{ route('contact_us_page') }}">Contact us<a/></div></div>
</body>
</html>
