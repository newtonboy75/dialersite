<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Tymbl Team</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="Tymbl Team">
  <style>
  	body {font-family: arial; padding: 10px 10px; font-size: 16px;}
  	.footer-text{font-size: 15px; margin-top:50px; text-align:center;}

    .listing-link{font-size: 17px !important; display: block; padding:8px; margin-left:auto; margin-right: auto; text-align: center;     color: rgb(255, 255, 255);
    background-color: rgb(0, 123, 255); border-color: rgb(0, 123, 255); font-weight: 600; text-decoration:none; width: 280px; margin-bottom: 20px; margin-top: 5px;border-radius: 2px!important;}
    .list_img{width:100%;margin-top:10px;margin-bottom:10px;}
    .logo{width: 120px;}
  </style>
</head>

<body>
<div><img class="logo" src="https://tymbl.com/assets/img/tymbl/logo.png"></div><br>
<div>
  <p>Hello {{$name}}!</p><br><br>
  <p>There is a new referral available in your area! Act now to be the first one to reserve this lead.</p>
  <br>
  <h3>{{$title}}</h3>
  <p><a href="{{$link}}"><img src="{{ url('/uploads/images/') }}{{ $image }}"></a></p>
  <p><a href="{{$link}}">{{$title}}<a></p>

  <p>&nbsp;</p>
  Sincerely,<br>
  The Tymbl team

<div class="footer-text"><a href="tymbl.com/">Browse New Leads</a> | <a href="mailto:admin@tymbl.com">Contact us<a/></div></div>
</body>
</html>
