<!DOCTYPE html>
<html>
    <head>
        <title>Referral First Contact</title>
    </head>
    <body>
        <div>
            <img src="https://tymbl.com/assets/img/tymbl/logo.png">
        </div>

        <div>
            <p>Hi [name]!</p>
            <p>
                Hope you are doing well! Hope your fist contact with [prospect's name] went well!
                Please click on the link below to provide an update.<br>
                [Link] FIRST CONTACT UPDATE [link to the reserved leads screen]<br>
                Please note if no update has been provided within 48 hours from the receipt of this message,
                Tymbl reserves the right to terminate the referral agreement [ID] and let other agents reserve the lead.
            </p>
            <br>
            Thank you
            <p>
                <strong>The Tymbl Team</strong>
            </p>
        </div>

        <div class="footer-text">
            <a href="{{ route('home') }}">Browse New Listings</a> | <a href="mailto:admin@tymbl.com">Contact us</a>
        </div>
    </body>
</html>