<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Tymbl Team</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="Tymbl Team">
  <style>
  	body {font-family: arial; padding: 10px 10px; font-size: 16px;}
  	.footer-text{font-size: 15px; margin-top:70px; width: 90%; margin-bottom: 30px; text-align: center;}
    .listing-link{font-size: 17px !important; display: block; padding:8px; margin-left:auto; margin-right: auto; text-align: center; color: #ffffff; background-color: rgb(0, 123, 255); border-color: rgb(0, 123, 255); font-weight: 600; text-decoration:none; width: 280px; margin-bottom: 20px; margin-top: 5px;border-radius: 2px!important;}
    .list_img{width:100%;margin-top:10px;margin-bottom:10px;}
    .logo{width: 120px;}
  </style>
</head>

<body>
<div><img class="logo" src="https://tymbl.com/assets/img/tymbl/logo.png"></div>
<div style="text-align: left !important;"><br>

  <p>Hi {{ ucfirst($broker->broker_contact_person) }}!</p>
  <p>{{ ucfirst($user->first_name) }} {{ ucfirst($user->last_name) }} signed a referral agreement <strong>{{$transaction_id}}</strong> on Tymbl. Please review a copy of the contract attached.</p>
  Please contact us if you believe this change in the lead status was made in error.</p><br>
  <p>Thank you,</p>
  <p>The Tymbl Team</p>
<br>

<div class="footer-text"><a href="{{ route('home') }}">Browse New Leads</a> | <a href="{{ route('contact_us_page') }}">Contact us<a/></div></div>
</body>
</html>
