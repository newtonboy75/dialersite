<!DOCTYPE html>
<html>
<head>
  <title>Email Verification</title>
  <style>
  body {font-family: arial; padding: 10px 10px; font-size: 16px;}
  .footer-text{font-size: 15px; margin-top:40px; text-align:center;  width: 90%; margin-bottom: 30px;}

  .listing-link{font-size: 17px !important; display: block; padding:8px; margin-left:auto; margin-right: auto; text-align: center;     color: rgb(255, 255, 255);
    background-color: rgb(0, 123, 255); border-color: rgb(0, 123, 255); font-weight: 600; text-decoration:none; width: 280px; margin-bottom: 20px; margin-top: 5px;border-radius: 2px!important;}
    .list_img{width:100%;margin-top:10px;margin-bottom:10px;}
    .code{padding:8px; background: #eeeeee; font-size: 20px; text-align:center; width: 300px; margin-left:auto; margin-right: auto; margin-bottom: 40px;}
    .logo{width: 120px;}
    </style>
  </head>
  <body>
    <div><img class="logo" src="https://tymbl.com/assets/img/tymbl/logo.png"></div>
    <p>Hello {{$title_company_name}},</p><br>
    <p>{{$buyer_name}} has signed a new referral agreement on Tymbl.com. To view the signed contract, please visit <a href="{{$url}}">{{$url}}</a> and use the following code to sign in.</p><br>

    <p class="code">{{$code}}</p>

    <h4>NOW WHAT?</h4>
    <ul>
        <li>If the referral results in a successful transaction, please approve the transaction on Tymbl on the day of signing.</li>
        <li>If the referral does not turn into a successful purchase or a sale, within the next 60 days from today, we or {{$buyer_name}} will remind you to log in to mark the transaction as "Property unsold". It will only take a minute and it'll release the amount {{$buyer_name}} paid to reserve the referral.</li>
      </ul>
      <p>&nbsp;</p>
      <p>Thank you,</p>
      <p><strong>The Tymbl team</strong></p>

      <div class="footer-text"><a href="tymbl.com/">Browse New Leads</a> | <a href="mailto:admin@tymbl.com">Contact us<a/></div>
      </body>
      </html>
