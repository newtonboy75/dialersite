<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Tymbl Team</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="Tymbl Team">
  <style>
  body {font-family: arial; padding: 10px 10px; font-size: 16px;}
  .footer-text{font-size: 15px; margin-top:70px; text-align:center;  width: 90%; margin-bottom: 30px;}

  .listing-link{font-size: 17px !important; display: block; padding:8px; margin-left:auto; margin-right: auto; text-align: center;     color: rgb(255, 255, 255);
    background-color: rgb(0, 123, 255); border-color: rgb(0, 123, 255); font-weight: 600; text-decoration:none; width: 280px; margin-bottom: 20px; margin-top: 5px;border-radius: 2px!important;}
    .list_img{width:100%;margin-top:10px;margin-bottom:10px;}
    .logo{width: 120px;}
    </style>
  </head>
  <body>
    <div><img class="logo" src="https://tymbl.com/assets/img/tymbl/logo.png"></div>
    <div><br>
      <p>Hi {{ucfirst($user->first_name)}} {{ucfirst($user->last_name)}}!</p>

      <p>Please note that {{ucfirst($broker->contact_person)}} has notified our support team that you are not part of {{$broker->name}}. This is to notify you that your account has been suspended or any referral agreements you signed on Tymbl are officially terminated.</p>

      <p>Please contact us if you believe this change in the lead status was made in error.<p><br>
        <p>Thank you,</p>
        <p><strong>The Tymbl Team</strong></p>
        <br>

        <div class="footer-text"><a href="{{url('/')}}">Browse New Leads</a> | <a href="{{ route('contact_us_page') }}">Contact us<a/></div></div>
        </body>
        </html>
