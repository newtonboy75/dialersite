<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Tymbl Team</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="Tymbl Team">
  <style>
  	body {font-family: arial; padding: 10px 10px; font-size: 16px;}
  	.footer-text{font-size: 15px; margin-top:70px; text-align:center;  width: 90%; margin-bottom: 30px;}

    .listing-link{font-size: 17px !important; display: block; padding:8px; margin-left:auto; margin-right: auto; text-align: center;     color: rgb(255, 255, 255);
    background-color: rgb(0, 123, 255); border-color: rgb(0, 123, 255); font-weight: 600; text-decoration:none; width: 280px; margin-bottom: 20px; margin-top: 5px;border-radius: 2px!important;}
    .list_img{width:100%;margin-top:10px;margin-bottom:10px;}
    .logo{width: 120px;}
  </style>
</head>
<body>
<div><img class="logo" src="https://tymbl.com/assets/img/tymbl/logo.png"></div>
<div><br>
  <p>Hello {{ucfirst($broker->broker_contact_person)}}!<p>
  <p>Hope this message finds you well! {{ucfirst($user->first_name)}} {{ucfirst($user->last_name)}} registered on Tymbl in order to reserve referrals.{{ucfirst($user->first_name)}} listed you as a broker they are working with. In order for {{ucfirst($user->first_name)}} to sign referral agreements on Tymbl, we would like to verify this information.</p>

  <p>Does {{ucfirst($user->first_name)}} {{ucfirst($user->last_name)}} work for {{$broker->name}}?</p>
  <p>Click on the link below to confirm or simply reply "yes" to this email.</p>

  <a href="{{url('verifyagent', $broker->id)}}">Verify Agent</a>
  <p><strong>The Tymbl Team</strong></p>
    <br>

<div class="footer-text"><a href="{{url('/')}}">Browse New Leads</a> | <a href="{{ route('contact_us_page') }}">Contact us<a/></div></div>
</body>
</html>
