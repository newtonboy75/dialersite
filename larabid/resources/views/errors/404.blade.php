@extends('tymbl.layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col text-center" style="min-height:60vh; padding-top:70px;">
                <div class="error-404-wrap">
                    <h1><i class="fa fa-frown-o"></i> Oops!</h1>
                    <h2> 404 Not Found</h2>
                    <div class="error-details">
                        Sorry, an error has occured, Requested page not found!
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
