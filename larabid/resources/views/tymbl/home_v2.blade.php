@extends('tymbl.layouts.app')
@section('content')

<section class="single-section non-auth-hero">
  <div class="container">
    <div class="row justify-content-md-center">
      <div class="col-md-8">
        <h1>Tymbl</h1>
        <h2>Enables You To</h2>
      </div>
    </div>
    <div class="row" style="padding-top: 20px;">
      <div class="col" data-aos="fade-left" data-aos-duration="1500">
        <a href="/post-new">
          <img src="{{ asset('assets/img/tymbl/circle1.png') }}" alt="" class="img-fluid float-right">
        </a>
      </div>
      <div class="col" data-aos="fade-right" data-aos-duration="1500">
        <a href="/leads">
          <img src="{{ asset('assets/img/tymbl/circle2.png') }}" alt="" class="img-fluid float-left">
        </a>
      </div>
    </div>
  </div>
</section>

<main class="main-content-2">
  <!-- Video-->
  <section class="video">
    <div>
      <a href="javascript:;" class="video-btn" data-toggle="modal" data-src="https://tymbl-assets.s3-us-west-2.amazonaws.com/tymbl_present_onboard.mp4" data-target="#myModal"><img src="{{ asset('assets/img/tymbl/play-button.svg') }}" alt="" class="mx-auto d-block"></a>
    </div>
  </section>

  <section>
    <div class="container">
      <h1 style="text-align: center;">Choose what describes you the best</h1>

      <div class="row who-are-you justify-content-md-center">
        <div class="col-md-4">
          <a href="#lead-qualification">
            <div class="card">
              <div class="card-body">
                <h3>Agent looking for qualified leads</h3>
              </div>
            </div>
          </a>
        </div>
        <div class="col-md-4">
          <a href="#referral-sourcing">
            <div class="card">
              <div class="card-body">
                <h3>Agent that has leads to qualify</h3>
              </div>
            </div>
          </a>
        </div>
        <div class="col-md-4">
          <a href="#referral-matchmaking">
            <div class="card">
              <div class="card-body">
                <h3>Agent looking to refer a client</h3>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>

  <section class="role-details">
    <div class="container">
      <div class="row justify-content-md-center">
        <div class="col-md-10">
          <div id="lead-qualification" class="role-person-detail">
            <h2>Lead Qualification</h2>
            <p>
              You have leads that you are not servicing or simply don’t have the time to service.
              Every agent does.
              Let Tymbl do the legwork to pre-qualify them so that you can focus on turning them into deals.
              Tymbl does not charge per lead, time or any set up fees.
              You pay a referral fee (a percentage of your commission) at closing. We only get paid when you do.
            </p>
          </div>
          <div id="referral-sourcing" class="role-person-detail">
            <h2>Referral Sourcing</h2>
            <p>
              Like every smart agent out there you are looking for a qualified lead/referral/appointment,
              basically anything that’s warmer than a cold lead in your area. Register,
              select your service area radius or list our zip codes you want to work and sign-up for notifications.
              We’ll let you know when there are referrals available in your area. Sign a referral agreement.
              Turn it into a deal. Pay a referral fee (a percentage of your commission) at closing.
              We only get paid when you do.
            </p>
          </div>
          <div id="referral-matchmaking" class="role-person-detail">
            <h2>Referral Matchmaking</h2>
            <p>
              You are looking for a trustworthy agent to take care of a qualified referral e.g.
              you got a client relocating to another state or
              you got a Tymbl-qualified lead you can’t attend, etc.
              Post this referral on Tymbl and we will notify local agents who are vetted and
              verified who will turn this referral into a deal. You get paid at closing.
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <!-- 16:9 aspect ratio -->
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always" allow="autoplay"></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</main> <!--/.main-content-->

@endsection
