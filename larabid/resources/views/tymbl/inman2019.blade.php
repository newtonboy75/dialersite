@extends('tymbl.layouts.app')
@section('content')
<section class="page-header">
  <h1>{{ $title }}</h1>
</section>
<div class="container">
  <div class="hh-div">

    <div>
      <h2>Tymbl announces new Lead Qualification Service for Agents</h2>

      <p><strong>Kirkland, WA— July 8, 2019</strong> — Today, Tymbl, an online real estate referrals platform, announced that they launched а new service for Real Estate Agents – a Lead Qualification Service. Realtors can now turn their leads to Tymbl, where Tymbl’s in-house ISA team will contact the leads to find out if potential buyers and sellers are ready to work with an agent and schedule time to speak. Then these qualified leads are given back to the agents and agents agree to pay Tymbl a referral fee at closing. No credit card bill, no pay per lead, just a referral fee which is usually ranges from 10-20% of the total commission amount.</p>
      <p>“By actually working with agents on referral fee basis and not upfront charges, Tymbl has skin in the game and is vested that the quality of the leads is top notch”,  said Timour Khamnayev, CPO of Tymbl.</p>

      <h5>Agents are loving it!</h5><p>
        Many customers have already benefited from deploying Tymbl’s  new Lead Qualification Service as this is a chance to revive the old and hopeless leads. Matt Fagiolo, an EXP Team leader and founder of Xplode Conferences recently provided Tymbl some of his leads that were generated from Facebook, and Tymbl has qualified several of them already and gave Matt’s team willing, able and ready home buyers.
        “We love the idea of not paying for an ISA or have another monthly vendor bill on our credit card” said Matt. “The leads were qualified, given back to us and my team is currently showing homes to these ready-to-buy home buyers”.</p>

        <h5>Tymbl History</h5>
        <p>Tymbl was founded in 2017 in an attempt to provide more transparency to agents and establish seamless process of referral agreement signing and exchange of leads and referrals in real time.
          The names of actual companies and products mentioned herein may be the trademarks of their respective owners.</p>
        </div>

        <div class="mx-auto"><p>For more information, press only:</p>
          Pavel Stepanov<br>
          206-683-4386<br>
          info@tymbl.com<br>
          For more information on Product:<br>
          <a href="//tymbl.com">www.tymbl.com</a>
        </div>
        <p>&nbsp;</p>
      </div>

      <div>

        <h5 style="color:red">Tymbl</h5>
        <p>Tymbl is a AI and human-powered technology for referrals qualification , that provides real estate professionals with a convenient way to source and post qualified referrals and leads. Tymbl launched а new service for Real Estate Agents – a Lead Qualification Service. Tymbl can take agent’s old and new leads, qualify them and give them back to the agent on referral basis, without monthly contracts or fees. Seamless platform with cutting edge functionalities allow posting and reserving a lead and signing of a referral agreement with only 2 clicks.</p><p>&nbsp;</p>
      </div>

    </div>
    @endsection
