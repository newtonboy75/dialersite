@extends('tymbl.layouts.app')
@section('content')
<section class="page-header">
  <h1>HELP dfsdfdsfds</h1>
</section>
<div class="container">
    <div class="hh-div">

            <div><h2>Looking for a qualified referral?</h2>
                <img class="mx-auto d-block d-image" src="/assets/img/tymbl/looking-for-a-qualified-referral.jpg" alt="looking-for-a-qualified-referral" class="img-fluid">
            </div>

            <div><h2>Looking to refer a potential client?</h2>
                <img class="mx-auto d-block d-image" src="/assets/img/tymbl/looking-to-refer-a-potential-client.jpg" alt="looking-to-refer-a-potential-client" class="img-fluid">
            </div>

            <div>
              @if(request()->segment(1) === 'page' && request()->segment(2) != 'how-it-works')
              yes
                <div class="col-md-8 col-md-offset-2" >
                    <div class="page_wrapper page-{{ $page->id }}" style="padding-top:40px; padding-left: 10px;padding-bottom:40px; text-align: justify;">
                        {!! safe_output($page->post_content) !!}
                    </div>
                </div>
              @endif
            </div>

    </div>
</div>
@endsection
