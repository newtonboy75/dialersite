@extends('tymbl.layouts.app')
@section('content')
<div class="container">
<div class="col text-center" style="height: 70vh; padding: 50px;">

            @include('admin.flash_msg')
              <div class="col h-100 text-center">
                <h4>Payment Success</h4><p>&nbsp;</p>
                <div style="font-size: 15px;">you will now be redirected to Referral Agreement page<br/>
                (click <a style="text-decoration:underline" href="/referral-document/{{$ad_id}}">here</a> if you are not automatically redirected)</div>

              </div>

</div>
</div> <!-- /#container -->
@endsection
