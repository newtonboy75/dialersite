@extends('tymbl.layouts.app')
@section('content')
<div class="container">
  <div id="post-new-ads">
    <div class="container">

        <div class="lbt-wrapper" style="height:70vh">
          @if($validList->status == '5')
            <h3 class="text-center">Success!</h3><p>&nbsp;</p>
            <p>When you find a dream home for <strong>{{$validList->title}}</strong>, please make sure to update the title company info you get from the Seller agent here</p>
            <p>&nbsp;</p>
            <div class="text-center">
            <a href="/referral-document/{{$validList->id}}/title_company" class="btn btn-primary btn-lg">OK</a>
          </div>
          @else
          <div style="text-align:center; width: 100%" >
            <h3 class="text-center">Invalid Lead</h3><p>&nbsp;</p>
            <a href="/">Back to Home</a>
          </div>
          @endif
        </div>

    </div>
  </div>
</div>
@endsection
