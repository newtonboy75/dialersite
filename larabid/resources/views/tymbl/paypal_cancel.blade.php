@extends('tymbl.layouts.app')
@section('content')
<div class="container">
<div class="col text-center" style="height: 70vh; padding: 50px;">

            @include('admin.flash_msg')

            <div class="col h-100 text-center">
              <h1>Payment Cancelled</h1><p>&nbsp;</p><p>&nbsp;</p>
              <h5>Payment was not successful</h5><p>&nbsp;</p><p>&nbsp;</p>
              <a href="/"> Go Home</a>
            </div>

</div>
</div> <!-- /#container -->
@endsection
