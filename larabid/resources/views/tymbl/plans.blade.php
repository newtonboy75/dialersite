@extends('tymbl.layouts.app')
@section('content')
<div class="plans-container">
  <div class="container">
    <div class="select-option"><h5 style="font-size: 1.4em;">Select your plan</h5></div>
    <div class="row">
      <div class="col-sm-4">
        <div class="box mx-auto">
          <div class="price_plan"><span class="price">FREE</span><br><small>&nbsp;</small></div>
          <ul>
            <li>Automated Scrubbing</li>
            <li>3 touches (Text, Email, Dials) for scrubbed leads</li>
            <li>30% Referral fee at closing</li>

          </ul>
          <div class="txt-just-mobile" style="width: 80%; margin-left: auto; margin-right: auto; font-size: 12px; padding-botom: 40px !important;"><span class="text-danger">*</span> With this plan, Tymbl reserves the right to re-assign the lead to other agents and reduce your share of the commission to 25% if you do not get in touch with the qualified lead.</div>
          <br>
          <div class="btn-upload mx-auto text-center"><a href="{{ URL::route('plan', array('option' => '1')) }}" role="button" class="btn btn-primary">IMPORT CSV</a></div>
        </div>
        <div class="txt-important">
          <span class="text-danger">*</span> With this plan, Tymbl reserves the right to re-assign the lead to other agents and reduce your share of the commission to 25% if you do not get in touch with the qualified lead.
        </div>
      </div>

      <div class="col-sm-4">
        <div class="box mx-auto">
          <div class="price_plan"><span class="price">$1.00</span><br><small>per contact</small></div>
          <ul>
            <li>Automated Scrubbing to prioritize dials</li>
            <li>At least 3 touches (Text, Email, Dials) each lead</li>
            <li>5% Referral fee at closing.</li>
          </ul>

          <div class="btn-upload mx-auto text-center"><a href="{{ URL::route('plan', array('option' => '2')) }}" role="button" class="btn btn-primary">IMPORT CSV</a></div>
        </div>
      </div>

      <div class="col-sm-4">
        <div class="box mx-auto">
          <div class="price_plan"><span class="price">$20.00</span><br><small>per qualified lead</small></div>
          <ul>
            <li>Automated Scrubbing to prioritize dials</li>
            <li>At least 3 touches (Text, Email, Dials) each lead</li>
            <li>10% Referral fee at closing.</li>
          </ul>

          <div class="btn-upload mx-auto text-center"><a href="{{ URL::route('plan', array('option' => '3')) }}" role="button" class="btn btn-primary">IMPORT CSV</a></div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
