@extends('tymbl.layouts.app')
@section('content')
<section class="single-section-search hero-2">
  <div class="container" id="search-br">
    @include('tymbl.layouts.search_form')
  </div>
</section>
<div class="itemViewFilterWrap">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="listingTopFilterBar">
          <div class="result_num float-left">
            <span>Total of {{$ads->count()}} leads found</span>
          </div>
          <div  class="float-right">
            <ul class="listingViewIcon">
              <li class="dropdown shortByListingLi">
                <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">@lang('app.short_by') <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="{{ request()->fullUrlWithQuery(['shortBy'=>'price_high_to_low']) }}">@lang('app.price_high_to_low')</a></li>
                  <li><a href="{{ request()->fullUrlWithQuery(['shortBy'=>'price_low_to_high']) }}">@lang('app.price_low_to_high')</a></li>
                  <li><a href="{{ request()->fullUrlWithQuery(['shortBy'=>'latest']) }}">@lang('app.latest')</a></li>
                </ul>
              </li>
              <li><a href="javascript:;" class="itemListView"><i class="fa fa-bars"></i> </a></li>
              <li><a href="javascript:;" class="itemImageListView"><i class="fa fa-th-list"></i> </a> </li>
              <li><a href="javascript:;" class="itemGridView"><i class="fa fa-th-large"></i> </a></li>
            </ul>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

@if($ads->count())
<div id="regular-ads-container">
  <div class="container">

    @foreach($ads as $ad)
    @php
    $images = \App\Media::where('ad_id', $ad->id)->orderBy('created_at', 'desc')->where('type', '=', 'image')->first();

    $ad_features = unserialize($ad->feature_1);

    $filled_features['bedroom'] = $ad_features[0];
    $filled_features['bathroom'] = $ad_features[1];
    $filled_features['sq ft'] = $ad_features[2];
    $filled_features['garage'] = $ad_features[3];
    $filled_features['kitchen'] = $ad_features[4];
    $filled_features['balconies'] = $ad_features[5];

    $features_recent = array_filter($filled_features, function($value) { return $value !== ''; });
    $featured_text = '';

    foreach(array_slice($features_recent, 0, 3) as $k=>$v){
      if($v != ''){
        $featured_text .= $v.' '.$k.' &#183; ';
      }
    }

    @endphp

    <div class="mx-auto row section-single" id="single-section" rel="{{$ad->id}}" data-slug="{{$ad->slug}}">

      <div class="col-sm-12 showonmobile">
        <div  style="display: inline; width: 100px;">@if($ad->category_type == 'buying')
          <img src="{{ asset('assets/img/tymbl/buyer-referral.png') }}" style="width: 50px; vertical-align: top">
          @php $marker_color = '#ffa242'; @endphp
          @elseif($ad->category_type == 'selling')
          <img src="{{ asset('assets/img/tymbl/seller-referral.png') }}" style="width: 50px; vertical-align: top">
          @php $marker_color = '#4285f4'; @endphp
          @else
            @if($ad->cat_type_status == 'qualified')
              <img src="{{ asset('assets/img/tymbl/icon_qualified.png') }}" style="width: 50px; vertical-align: top">
              @php $marker_color = '#ffa242'; @endphp
            @else
              <img src="{{ asset('assets/img/tymbl/icon_unqualified.png') }}" style="width: 50px; vertical-align: top">
              @php $marker_color = '#4285f4'; @endphp
            @endif
          @endif

          @if(!$ad->is_my_favorite())
          <div class="category-like" data-toggle="tooltip" data-placement="top" rel="{{$ad->id}}"
            data-slug="{{$ad->slug}}" id="save-as-favorite" data-original-title="Add to favorites">
            <i class="fas fa-heart"></i>
          </div>
          @else
          <div class="category-like recent-favorite-active" data-toggle="tooltip" data-placement="top" rel="{{$ad->id}}"
            data-slug="{{$ad->slug}}" id="save-as-favorite" data-original-title="Remove from favorite">
            <i class="fas fa-heart"></i>
          </div>
          @endif

        </div>
        <div style="display: inline-block; width: 200px; padding-left: 10px;">
          <div class="address-section" style="font-size: 1.0rem"><i class="fas fa-map-marker-alt" style="color: {{ $marker_color }}"></i>&nbsp;&nbsp;{{ isset($ad->city->city_name) ? $ad->city->city_name .',' : '' }} {{ isset($ad->state->state_name) ? $ad->state->state_name  : '' }}</div>
          <div>Referral Fee:
            @if((float)$ad->referral_fee > 0.00)
            @if($ad->get_ad_status() == 'lead-vip-free-active')
              {{ (float)($ad->referral_fee*100)-10 }}%
            @else
              {{ (float)($ad->referral_fee*100) }}%
            @endif
             <span class="to-reserve">referral fee</span>
            @else
            @if($ad->country_id=='231')
            @php
            setlocale(LC_MONETARY, 'en_US');
            echo '$'.number_format($ad->escrow_amount,2);
            @endphp
            @else
            @php
            echo 'CA$'.number_format($ad->escrow_amount,2);
            @endphp
            @endif
            <span class="to-reserve">reservation fee</span>
            @endif
          </div>
          <div class="desc">{{ safe_output(str_limit($ad->title, 50)) }}</div>
          <div>{{ html_entity_decode($featured_text) }}</div>
        </div>
      </div>

      <div class="col-sm-2 hideonmobile">
        @if($ad->category_type == 'buying')
        <img src="{{ asset('assets/img/tymbl/buyer-referral.png') }}" class="img-lead-type">
        @php $marker_color = '#ffa242'; @endphp
        @else
        <img src="{{ asset('assets/img/tymbl/seller-referral.png') }}" class="img-lead-type">
        @php $marker_color = '#4285f4'; @endphp
        @endif
      </div>
      <div class="col-sm-10 contents-right hideonmobile">
        @if(!$ad->is_my_favorite())
        <div class="category-like " data-toggle="tooltip" data-placement="top" rel="{{$ad->id}}"
          data-slug="{{$ad->slug}}" id="save-as-favorite" data-original-title="Add to favorites">
          <i class="fas fa-heart"></i>
        </div>
        @else
        <div class="category-like recent-favorite-active" data-toggle="tooltip" data-placement="top" rel="{{$ad->id}}"
          data-slug="{{$ad->slug}}" id="save-as-favorite" data-original-title="Remove from favorite">
          <i class="fas fa-heart"></i>
        </div>
        @endif
        <div class="address-section"><i class="fas fa-map-marker-alt" style="color: {{ $marker_color }}"></i>&nbsp;&nbsp;{{ isset($ad->city->city_name) ? $ad->city->city_name .',' : '' }} {{ isset($ad->state->state_name) ? $ad->state->state_name  : '' }}</div>
        <div>Referral Fee:
          @if((float)$ad->referral_fee > 0.00)
            @if($ad->get_ad_status() == 'lead-vip-free-active')
              {{ (float)($ad->referral_fee*100)-10 }}%
            @else
              {{ (float)($ad->referral_fee*100) }}%
            @endif
           <span class="to-reserve">referral fee</span>
          @else
          @if($ad->country_id=='231')
          @php
          setlocale(LC_MONETARY, 'en_US');
          echo '$'.number_format($ad->escrow_amount,2);
          @endphp
          @else
          @php
          echo 'CA$'.number_format($ad->escrow_amount,2);
          @endphp
          @endif
          <span class="to-reserve">reservation fee</span>
          @endif
        </div>
        <div class="desc">{{ safe_output(str_limit($ad->title, 50)) }}</div>
        <div>{{ html_entity_decode($featured_text) }}</div>


      </div>
    </div>

    <!-- /.recent-signle -->
    @endforeach
    <div class="clearfix"></div>
</div>
</div>

@else
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="no-content-wrap">
        <h2> <i class="fa fa-info-circle"></i>There are ads with these criteria found</h2>
      </div>
      <div>
        @if (Auth::check())
        @include ('tymbl.layouts.remind_search_loggedin')
        @else
        @include ('tymbl.layouts.remind_search')
        @endif
      </div>
    </div>
  </div>
</div>
@endif
<p>&nbsp;</p><p>&nbsp;</p>
<div class="container mx-auto" style="width: 65%;">
  <div class="row">
    <div class="col-md-12 text-center">
      @php
      $sort_by = isset($_GET['shortBy']) ? $_GET['shortBy'] : '';
      @endphp

      {{ $ads->appends(['shortBy' => $sort_by])->links() }}
    </div>
  </div>
</div><p>&nbsp;</p><p>&nbsp;</p>

@endsection

@section('page-js')
<script type="text/javascript">
$(".select2LoadCity").select2({
  ajax: {
    url: "{{route('searchCityJson')}}",
    dataType: 'json',
    delay: 250,
    data: function (params) {
      return {
        q: params.term, // search term
        page: params.page
      };
    },
    processResults: function (data, params) {
      params.page = params.page || 1;
      return {
        results: data.items,
        pagination: {
          more: (params.page * 30) < data.total_count
        }
      };
    },
    cache: true
  },
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 3,
  templateResult: formatRepo, // omitted for brevity, see the source of this page
  templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
});
function formatRepo (repo) {
  if (repo.loading) return repo.city_name;

  var markup = "<div class='clearfix'>"+
  "<div class='select2-result-repository__title'> <i class='fa fa-map-marker'></i> " + repo.city_name + "</div></div>" +
  "</div></div>";

  return markup;
}
function formatRepoSelection (repo) {
  return repo.city_name || repo.text;
}




</script>
@endsection
