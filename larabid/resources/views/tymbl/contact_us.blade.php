@extends('tymbl.layouts.app')
@section('content')
<section class="page-header">
  <h1>Contact Us</h1>
</section>
<p>&nbsp;</p><p>&nbsp;</p>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <form>
                <h1><span class="glyphicon glyphicon-globe"></span> @lang('app.our_office')</h1>

                <address>
                    <strong>Tymbl Technologies Inc</strong>
                    @if(get_option('footer_address'))
                        <br />
                        <i class="fa fa-map-marker-alt" style="font-size: 18px; padding-right: 10px;"></i>
                        {!! get_option('footer_address') !!}
                    @endif

                </address>

            </form>

            <div class="well well-sm" style="margin-bottom: 50px; padding-top:30px;">
                <h2>Looking for help?</h2>
                <h6>Please fill out the form below.</h6>
                <div class="spacer" style="height: 30px;"></div>
                {!! Form::open(array('url' => 'contact-send')) !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('name')? 'has-error':'' }}">
                                <label for="name">@lang('app.name')</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="@lang('app.enter_name')" value="{{ old('name') }}" required="required" />
                                {!! $errors->has('name')? '<p class="help-block">'.$errors->first('name').'</p>':'' !!}
                            </div>
                            <div class="form-group {{ $errors->has('email')? 'has-error':'' }}">
                                <label for="email">@lang('app.email_address')</label>
                                <div class="input-group">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-envelope"></span>
                            </span>
                                    <input type="email" class="form-control" id="email" placeholder="@lang('app.enter_email_address')" name="email" value="{{ old('email') }}" required="required" />
                                </div>
                                {!! $errors->has('email')? '<p class="help-block">'.$errors->first('email').'</p>':'' !!}

                            </div>

                            <div class="form-group {{ $errors->has('message')? 'has-error':'' }}">
                                <label for="name">@lang('app.message')</label>
                                <textarea name="message" id="message" class="form-control" required="required" placeholder="@lang('app.message')">{{ old('message') }}</textarea>
                                {!! $errors->has('message')? '<p class="help-block">'.$errors->first('message').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary pull-right" id="btnContactUs"> @lang('app.send_message')</button>
                        </div>
                    </div>
                {{!Form::close()}}
            </div>
        </div>

        <div class="col-md-6">
            {!! get_option('google_map_embedded_code') !!}
        </div>
    </div>
</div>
<p>&nbsp;</p><p>&nbsp;</p>
@endsection
@section('page-js')
@endsection
