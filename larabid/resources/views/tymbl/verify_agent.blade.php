@extends('tymbl.layouts.app')
@section('content')
<div class="container">


  <div class="row">
    <div class="col-sm-12" style="height:70vh; text-align:center; margin-top: 40px;">
      <h5>Verify Agent</h5><br>
      @if($broker->broker_verified == '1')
      <p>Thank you! Agent has been verified</p><p>&nbsp;</p>
      @else
      <p>Does <span style="font-weight: 600">{{$user->first_name}} {{$user->last_name}}</span> work for <span style="font-weight: 600">{{ $broker->name }}</span>?</p>
      <div style="max-width: 400px;" class="mx-auto">
        <form method="POST" action="{{URL::route('verified-agent')}}" id="adsPostForm" class="form-vertical">

          <div class="form-check form-check-inline margin20" style="width: 360px; margin: 10px; ">
            <input class="form-check-input" name="verify_val" type="radio" value="1" required checked>
            <label class="form-check-label">&nbsp;</label><div style="width: 320px; display: inline; text-align: left !important;">Yes, {{$user->first_name}} {{$user->last_name}} works for {{ $broker->name }}</div>
          </div>

          <div class="form-check form-check-inline margin20" style="width: 360px; margin: 0px; ">
            <input class="form-check-input" name="verify_val" type="radio" value="0" required>
            <label class="form-check-label">&nbsp;</label><div style="width: 320px; display: inline; text-align: left !important;">No, {{$user->first_name}} {{$user->last_name}} does not work for {{ $broker->name }}</div>
          </div>
          <input type="hidden" name="broker_id" value="{{$broker->id}}">
          <input type="submit" value="SUBMIT" class="btn btn-lg btn-primary" style="margin-top: 50px; width: 80%;">

        </form>

        @if(session()->has('success'))
            <div class="alert alert-info" style="margin-top: 30px;">
                {{ session()->get('success') }}
            </div>
        @endif


      </section>
      @endif
    </div>
  </div>
</div>


</div>
@endsection
