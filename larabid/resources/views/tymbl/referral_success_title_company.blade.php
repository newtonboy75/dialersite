@extends('tymbl.layouts.app')
@section('content')
<div class="container">
  <div id="post-new-ads">
    <div class="container">



        <div class="lbt-wrapper">

            @if (strpos(URL::previous(), "reserved-leads") === false)
            <h5>Thank you.<br>Please indicate the Title Company Details</h5><br>
            @else
            <h5>Complete the Title Company Details</h5><br>
            @endif

          {{ Form::open(array('url' => 'title_company_lbt', 'method' => 'POST')) }}
          <div class="form-check form-check-inline text-bold">
            <label for="company_name">Title Company Name*</label><input type="text" name="company_name" id="company_name" placeholder="Title Company Name*" class="form-control" id="titleCompany" required data-toggle="tooltip">
          </div>

          <div class="form-check form-check-inline text-bold">
            <label for="representative_name">Representative Name*</label><input type="text" name="representative_name" id="representative_name" placeholder="Representative Name*" class="form-control" id="repName" required>
          </div>

          <div class="form-check form-check-inline text-bold">
            <label for="representative_email">Representative Email*</label><input type="email" name="representative_email" id="representative_email" placeholder="Representative Email*" class="form-control" id="repEmail" required>
          </div>
          <p><input type="hidden" name="ad_id" value="{{request()->segment(2)}}">
              <input type="hidden" name="page" value="ltb">
            &nbsp;</p>
          <div class="form-check form-check-inline text-bold text-center">
            <button class="btn btn-primary btn-lg">Save</button>
          </div>
          {{ Form::close() }}<p>&nbsp;</p>

          <div  class="text-center" ><a href="{{ route('reserved-leads') }}">

            @if (strpos(URL::previous(), "reserved-leads") !== false)
              back to Reserved Leads
            @else
              finish later
            @endif
          </a></div>
        </div>


    </div>
  </div>
</div>
@endsection
