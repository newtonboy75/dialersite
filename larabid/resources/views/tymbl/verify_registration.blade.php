@extends('tymbl.layouts.app')
@section('content')
<section class="page-header">
  <h1>Activate Account</h1>
</section>
<div class="container" style="min-height: 60vh;padding-top:60px;">
    <div class="row">
        <div class="col text-center">
            <div>
                <div class="pbody">

                    @if (Session::has('success'))
                    	@if(Session::get('success')=='2')
                        <div class="text-center verify-text"><h4>Thank you.</h4>Please check your email to activate your account.</div><br><br>
                      @else
                        <div class="text-center verify-text"><h4 class="font-weight-bold">Thank you.</h4>An activation code is sent to your mobile number. To confirm your registration, enter the 6-digit number inside the box and click Confirm</div><br><br>

                        <form class="form-horizontal" role="form" method="POST" action="{{ route('verify-complete') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                          <div class="col">
                            <input type="number" class="form-control" name="sms_activation_code" required>
                          </div>
                        </div>

                        <div class="form-group">
                            <div class="cold">
                                <button type="submit" class="btn btn-primary btn-lg">
                                    <i class="fa fa-user-plus"></i> @lang('app.confirm_registration')
                                </button>

                                &nbsp;&nbsp;<a class="pull-right" href='{{ route('dashboard') }}'>Finish Later</a>
                            </div>
                        </div>
                    </form>
                      @endif
                    @else
                      @include('admin.flash_msg')
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
