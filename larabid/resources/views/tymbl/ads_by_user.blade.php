@extends('tymbl.layouts.app')
@section('content')
<section class="single-section recent">
  <div class="container">


    <div class="user-ads-section mx-auto">
      <div class="row">

        <div class="col-sm-2"><img class="mr-3 id-size" src="{{ $user->get_gravatar() ? $user->get_gravatar() : '/uploads/avatar/gravatar-default.png' }}" alt="Generic placeholder image"></div>
        <div class="col-sm-10">
          <h5>{{ substr($user->name, 0, 2) }}<sup class="conceal">
              <i class="fas fa-asterisk"></i><i class="fas fa-asterisk"></i><i class="fas fa-asterisk"></i><i class="fas fa-asterisk"></i><i class="fas fa-asterisk"></i><i class="fas fa-asterisk"></i><i class="fas fa-asterisk"></i><i class="fas fa-asterisk"></i><i class="fas fa-asterisk"></i><i class="fas fa-asterisk"></i><i class="fas fa-asterisk"></i><i class="fas fa-asterisk"></i><i class="fas fa-asterisk"></i></sup>{{ substr($user->name, -3, 4) }}</h5>
              <div class="user_rating" title="{{$final_user_rating}} user rating">
                <?php
                $wholenum1 =  array(1, 2);
                $halfnum = array(3, 4, 5, 6);
                $wholenum2 =  array(7, 8, 9);
                ?>

                @if((int)$final_user_rating >= 1)
                <span class="fas fa-star full"></span>
                @else
                @if(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum1) && (int)$final_user_rating == 0)
                <i class="fas fa-star"></i>
                @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum2) && (int)$final_user_rating == 0)
                <span class="fas fa-star full"></span>
                @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $halfnum) && (int)$final_user_rating == 0)
                <span class="fas fa-star half"></span>
                @else
                <span class="fas fa-star"></span>
                @endif
                @endif

                @if((int)$final_user_rating >= 2)
                <span class="fas fa-star full"></span>
                @else
                @if(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum1) && (int)$final_user_rating == 1)
                <span class="fas fa-star"></span>
                @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum2) && (int)$final_user_rating == 1)
                <span class="fas fa-star full"></span>
                @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $halfnum) && (int)$final_user_rating == 1)
                <span class="fas fa-star half"></span>
                @else
                <span class="fas fa-star"></span>
                @endif
                @endif

                @if((int)$final_user_rating >= 3)
                <span class="fas fa-star full"></span>
                @else
                @if(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum1) && (int)$final_user_rating == 2)
                <span class="fas fa-star"></span>
                @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum2) && (int)$final_user_rating == 2)
                <span class="fas fa-star full"></span>
                @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $halfnum) && (int)$final_user_rating == 2)
                <span class="fas fa-star half"></span>
                @else
                <span class="fas fa-star"></span>
                @endif
                @endif

                @if((int)$final_user_rating >= 4)
                <span class="fas fa-star full"></span>
                @else
                @if(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum1) && (int)$final_user_rating == 3)
                <span class="fas fa-star"></span>
                @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum2) && (int)$final_user_rating == 3)
                <span class="fas fa-star full"></span>
                @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $halfnum) && (int)$final_user_rating == 3)
                <span class="fas fa-star half"></span>
                @else
                <span class="fas fa-star"></span>
                @endif
                @endif

                @if((int)$final_user_rating >= 5)
                <span class="fas fa-star full"></span>
                @else
                @if(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum1) && (int)$final_user_rating == 4)
                <span class="fas fa-star"></span>
                @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum2) && (int)$final_user_rating == 4)
                <span class="fas fa-star full"></span>
                @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $halfnum) && (int)$final_user_rating == 4)
                <span class="fas fa-star half"></span>
                @else
                <span class="fas fa-star"></span>
                @endif
                @endif
              </div>
              <div>
                @if($loc)
                @if($loc->city != '')
                <p>{{$loc->city}},
                  @endif
                  @if($loc->state != '')
                  {{$loc->state}}
                  @endif
                  @endif
                  <br>
                  @if($user->zip_code != '' || $user->zip_code != '0')
                  {{$user->zip_code}}
                  @endif
                  <br>

                  @php
                  $date_joined  = strtotime($user->created_at);
                  $month = date('F', $date_joined);
                  $year  = date('Y', $date_joined);
                  @endphp
                  Member since <br><strong>{{$month}} {{$year}}</strong></p>

                  @if($poster_broker)
                  @if($poster_broker->broker_verified == '1')
                  @php
                  $verified_on  = strtotime($poster_broker->verified_on);
                  $month_v = date('F', $verified_on);
                  $year_v  = date('Y', $verified_on);
                  @endphp
                  <i class="text-primary fas fa-check-circle" title="Vefiried"></i> Verified on <br><strong>{{$month_v}} {{$year_v}}</strong></p>
                  @endif
                  @endif

                </div>

        </div>

      </div>

      <div>
        @foreach($ads as $recent)
        @php
        $images = \App\Media::where('ad_id', $recent->id)->orderBy('created_at', 'desc')->where('type', '=', 'image')->first();

        $recent_features = unserialize($recent->feature_1);

        $filled_features['bedroom'] = $recent_features[0];
        $filled_features['bathroom'] = $recent_features[1];
        $filled_features['sq ft'] = $recent_features[2];
        $filled_features['garage'] = $recent_features[3];
        $filled_features['kitchen'] = $recent_features[4];
        $filled_features['balconies'] = $recent_features[5];

        $features_recent = array_filter($filled_features, function($value) { return $value !== ''; });
        $featured_text = '';

        foreach(array_slice($features_recent, 0, 3) as $k=>$v){
          if($v != ''){
            $featured_text .= $v.' '.$k.' &#183; ';
          }
        }

        @endphp

        <div class="mx-auto row section-single" id="single-section" rel="{{$recent->id}}" data-slug="{{$recent->slug}}" style="width: 100% !important;">
          <div class="col-sm-2 mx-auto">

            @if($recent->category_type == 'buying')
              <img src="{{ asset('assets/img/tymbl/buyer-referral.png') }}" class="img-lead-type"></div>
              @php $marker_color = '#ffa242'; @endphp
              @elseif($recent->category_type == 'selling')
              <img src="{{ asset('assets/img/tymbl/seller-referral.png') }}" class="img-lead-type"></div>
              @php $marker_color = '#4285f4'; @endphp
              @else
                @if($recent->cat_type_status == 'qualified')
                  <img src="{{ asset('assets/img/tymbl/icon_qualified.png') }}" class="img-lead-type"></div>
                  @php $marker_color = '#ffa242'; @endphp
                @else
                  <img src="{{ asset('assets/img/tymbl/icon_unqualified.png') }}" class="img-lead-type"></div>
                  @php $marker_color = '#4285f4'; @endphp
                @endif
              @endif



            <div class="col-sm-10 contents-right">
              @if(!$recent->is_my_favorite())
              <div class="category-like " data-toggle="tooltip" data-placement="top" rel="{{$recent->id}}"
                data-slug="{{$recent->slug}}" id="save-as-favorite" data-original-title="Add to favorites">
                <i class="fas fa-heart"></i>
              </div>
              @else
              <div class="category-like recent-favorite-active" data-toggle="tooltip" data-placement="top" rel="{{$recent->id}}"
                data-slug="{{$recent->slug}}" id="save-as-favorite" data-original-title="Remove from favorite">
                <i class="fas fa-heart"></i>
              </div>
              @endif
              <div class="address-section"><i class="fas fa-map-marker-alt" style="color: {{ $marker_color }}"></i>&nbsp;&nbsp;{{ isset($recent->city->city_name) ? $recent->city->city_name .',' : '' }} {{ isset($recent->state->state_name) ? $recent->state->state_name  : '' }}</div>
              <div>Referral Fee:
                @if((float)$recent->referral_fee > 0.00)
                {{ (float)$recent->referral_fee*100 }}% <span class="to-reserve">referral fee</span>
                @else
                @if($recent->country_id=='231')
                @php
                setlocale(LC_MONETARY, 'en_US');
                echo '$'.number_format($recent->escrow_amount,2);
                @endphp
                @else
                @php
                echo 'CA$'.number_format($recent->escrow_amount,2);
                @endphp
                @endif
                <span class="to-reserve">reservation fee</span>
                @endif
              </div>
              <div class="desc">{{ safe_output(str_limit($recent->title, 50)) }}</div>
              <div>{{ html_entity_decode($featured_text) }}</div>


            </div>
          </div>

          <!-- /.recent-signle -->
          @endforeach
          <div class="clearfix"></div>
      </div>
    </div>

  </div>
</section>
@endsection
