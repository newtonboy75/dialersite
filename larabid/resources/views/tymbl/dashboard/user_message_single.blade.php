@extends('tymbl.layouts.dashboard')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">{{$title}}</h3>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->
					<div class="m-content">
            						<!--begin::Portlet-->
						<div class="m-portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<h3 class="m-portlet__head-text">
												Subject: {{safe_output($message->subject)}}
											</h3>

										</div>
									</div>
								</div>
								<div class="m-portlet__body">

									<!--begin::Section-->
									<div class="m-section">
										<div class="m-section__content">
                      <div class="msg-body">
                          <div>{!!html_entity_decode($message->contents)!!}</div>
                      </div>
										</div>

									</div>
                  <a href="{{ route('messages') }}"><i class="fas fa-arrow-left"></i>&nbsp;&nbsp;Back to message</a>
									<!--end::Section-->
								</div>

								<!--end::Form-->
							</div>

							<!--end::Portlet-->
					</div>
				</div>

@endsection
