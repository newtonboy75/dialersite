@extends('tymbl.layouts.dashboard')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">Users</h3>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->
					<div class="m-content">
            						<!--begin::Portlet-->
						<div class="m-portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<h3 class="m-portlet__head-text">
												Users
											</h3>
										</div>
									</div><span id="error_status" style="color: red;">&nbsp;</span>
                  <a href="{{ route('users') }}" class="btn btn-primary btn-create-page pull-right">Back to Users</a>
								</div>
								<div class="m-portlet__body">

									<!--begin::Section-->
									<div class="m-section">
										<div class="m-section__content">
                      <table class="table table-bordered table-striped">
                        <tr>
                          <th><h5>Info</h5></th><td></td>
                        </tr>

                        <tr>
                            <th>@lang('app.status')</th>
                            <td>
                              <select class="form-control user-ddp" title="Change user status" id="user_status">
                                <option value="0" {{ $user->active_status == '0' ? 'selected="selected"' : '' }}>Pending</option>
                                <option value="1" {{ $user->active_status == '1' ? 'selected="selected"' : '' }}>Active</option>
                                <option value="2" {{ $user->active_status == '2' ? 'selected="selected"' : '' }}>Block</option>
                              </select><br>
                              <p><a href="javascript::" id="remove_user" rel="{{$user->id}}">Delete user</a></p>

                            </td>
                        </tr>

                          <tr>
                              <th>@lang('app.name')</th>
                              <td>{{ $user->name }}</td>
                          </tr>
                          <tr>
                              <th>@lang('app.email')</th>
                              <td>{{ $user->email }}</td>
                          </tr>
                          <tr>
                              <th>Title</th>
                              <td>{{ ucfirst($user->title) }}</td>
                          </tr>
                          <tr>
                              <th>@lang('app.phone')</th>
                              <td>{{ $user->phone }}</td>
                          </tr>
                          <tr>
                              <th>@lang('app.address')</th>
                              <td>{{ $user->address }}</td>
                          </tr>
                          <tr>
                              <th>@lang('app.country')</th>
                              <td>
                                  @if($user->country)
                                      {{ $user->country->country_name }}
                                  @endif
                              </td>
                          </tr>
                          <tr>
                              <th>Zip Code</th>
                              <td>{{ $user->zip_code == '0' ? '' :  $user->zip_code }}</td>
                          </tr>
                          <tr>
                              <th>MLS</th>
                              <td>{{ isset($mls->name) ? $mls->name : '' }}</td>
                          </tr>
                          <tr>
                              <th>Broker</th>
                              <td>{{ isset($broker->name) ? $broker->name : ''}}</td>
                          </tr>
                          <tr>
                              <th>@lang('app.created_at')</th>
                              <td>{{ $user->signed_up_datetime() }}</td>
                          </tr>
                          <tr>
                            <td><h5>Title Company</h5></td><td></td>
                          </tr>
                          <tr>
                              <th>Company Name</th>
                              <td>{{ isset($title_company->company_name)? $title_company->company_name : ''  }}</td>
                          </tr>
                          <tr>
                              <th>Representative</th>
                              <td>{{ isset($title_company->representative_name )? $title_company->representative_name : '' }}</td>
                          </tr>
                          <tr>
                              <th>Representative Email</th>
                              <td>{{ isset($title_company->representative_email) ? $title_company->representative_email : '' }}</td>
                          </tr>
                      </table>


                      @if($ads->total() > 0)
                          <div class="row" style="margin-left: 6px; margin-top: 30px; width: 100%;">
                              <div class="col-xs-12">
                                  <h3>@lang('app.posted_ads')</h3>

                                  <table class="table table-bordered table-striped">

                                      @foreach($ads as $ad)
                                          <tr>
                                              <td width="100">
                                                  <img src="{{ media_url($ad->feature_img) }}" class="img-responsive" alt="">
                                              </td>
                                              <td>
                                                  <h5><a href="{{  route('single_ad', [$ad->id, $ad->slug]) }}" target="_blank">{{ $ad->title }}</a> </h5>
                                                  <p class="text-muted">
                                                      <i class="fa fa-map-marker"></i> {{ str_replace("<br />", ' ', $ad->full_address()) }} <br/>  <i class="fa fa-clock-o"></i> {{ $ad->posting_datetime()  }}

                                                      @if($ad->reports->count() > 0)
                                                      <br />
                                                      <a href="{{ route('reports_by_ads', $ad->slug) }}">
                                                      <i class="fa fa-exclamation-triangle"></i> @lang('app.reports') : {{ $ad->reports->count() }}
                                                      </a>
                                                      @endif
                                                  </p>
                                              </td>
                                          </tr>
                                      @endforeach
                                  </table>

                                  {!! $ads->links() !!}

                              </div>
                          </div>

                      @endif
										</div>
									</div>

									<!--end::Section-->
								</div>

								<!--end::Form-->
							</div>
							<!--end::Portlet-->
					</div>
				</div>

@endsection
