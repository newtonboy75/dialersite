@extends('tymbl.layouts.dashboard')
@section('content')

<div class="m-content">
            <div class="mx-auto" style="width:100%;"></div>
						<!--begin::Portlet-->
						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<span class="m-portlet__head-icon m--hide">
											<i class="la la-gear"></i>
										</span>
										<h3 class="m-portlet__head-text">
											Add User
										</h3>
									</div>

                  <div class="success-error"></div>
								</div>
							</div>
							<!--begin::Form-->
              <div class="m-portlet__body">
                <div class="m-form__section m-form__section--first">
                  {!! Form::open(['class'=>'form-horizontal', 'files'=>'true']) !!}

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">User Type</label>
                    <div class="col-lg-6">
                      <select id="user_type" name="user_type" class="form-control" required>
                          <option value="">Select User Type</option>
                          <option value="user" {{ old('user_type')  == 'user'?'selected':'' }}>User</option>
                          <option value="admin" {{ old('user_type') == 'admin'?'selected':'' }}>Admin</option>
                      </select>
                      {!! $errors->has('name')? '<p class="help-block">'.$errors->first('name').'</p>':'' !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">@lang('app.name')</label>
                    <div class="col-lg-6">
                      <input type="text" class="form-control" id="name" value="{{ old('name') }}" name="name" placeholder="@lang('app.name')" required>
                      {!! $errors->has('name')? '<p class="help-block">'.$errors->first('name').'</p>':'' !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">@lang('app.email')</label>
                    <div class="col-lg-6">
                      <input type="email" class="form-control" id="email" value="{{ old('email')  }}" name="email" placeholder="@lang('app.email')" required>
                      {!! $errors->has('email')? '<p class="help-block">'.$errors->first('email').'</p>':'' !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">@lang('app.password')</label>
                    <div class="col-lg-6">
                      <input type="password" class="form-control" id="email" value="{{ old('password')  }}" name="password" placeholder="@lang('app.password')" required>
                      {!! $errors->has('email')? '<p class="help-block">'.$errors->first('email').'</p>':'' !!}
                    </div>
                  </div>

                  <div class="user-hidden">
                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">Title</label>
                    <div class="col-lg-6">
                      <select id="title" name="title" class="form-control">
                          <option value="">Select Gender</option>
                          <option value="mr" {{ old('title')  == 'mr'?'selected':'' }}>Mr</option>
                          <option value="mrs" {{ old('title') == 'mrs'?'selected':'' }}>Mrs</option>
                          <option value="other" {{ old('title')  == 'other'?'selected':'' }}>Other</option>
                      </select>
                      {!! $errors->has('title')? '<p class="help-block">'.$errors->first('title').'</p>':'' !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">@lang('app.mobile')</label>
                    <div class="col-lg-6">
                      <input type="text" class="form-control" id="mobile" value="{{ old('phone') }}" name="mobile" placeholder="@lang('app.mobile')">
                      {!! $errors->has('phone')? '<p class="help-block">'.$errors->first('phone').'</p>':'' !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">Address</label>
                    <div class="col-lg-6">
                      <input type="text" class="form-control" id="address" value="{{ old('address') }}" name="address" placeholder="@lang('app.address')">
                      {!! $errors->has('address')? '<p class="help-block">'.$errors->first('address').'</p>':'' !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">@lang('app.country')</label>
                    <div class="col-lg-6">
                      <select id="country_id" name="country" class="form-control select2">
                          <option value="">Select a country</option>
                          <option value="38" {{ old('country_id') == '38' ? 'selected' :'' }}>Canada</option>
                          <option value="231" {{ old('country_id')  == '231' ? 'selected' :'' }}>United States</option>
                      </select>
                      {!! $errors->has('country_id')? '<p class="help-block">'.$errors->first('country_id').'</p>':'' !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">@lang('app.state')<span class="required-field">*</span></label>
                    <div class="col-lg-6">
                      <select class="form-control select2" id="state_select" name="state">
                        <option selected disabled>Select State</option>
                      </select>
                      <p class="text-info">
                          <span id="state_loader" style="display: none;"><i class="fa fa-spin fa-spinner"></i> </span>
                      </p>
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">@lang('app.city')<span class="required-field">*</span></label>
                    <div class="col-lg-6">
                      <select class="form-control select2" id="city_select" name="city">
                        <option selected disabled>Select City</option>
                      </select>
                      <p class="text-info">
                          <span id="city_loader" style="display: none;"><i class="fa fa-spin fa-spinner"></i> </span>
                      </p>
                    </div>
                  </div>

                  <div class="form-group row {{ $errors->has('zipcode')? 'has-error':'' }}">
                    <label for="city" class="col-lg-3 col-form-label">Zip Code</label>
                    <div class="col-lg-6">
                    <select class="custom-select form-control ids" id="zipcode_select" name="zipcode">
                      <option value="0" selected disabled>Select Zip Code</option>
                    </select>
                  </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">@lang('app.company_name')</label>
                    <div class="col-lg-6">
                      <input type="text" class="form-control" id="company_name" value="{{ old('company_name') }}" name="company_name" placeholder="@lang('app.company_name')">
                      {!! $errors->has('company_name')? '<p class="help-block">'.$errors->first('company_name').'</p>':'' !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">@lang('app.representative_name')</label>
                    <div class="col-lg-6">
                      <input type="text" class="form-control" id="representative_name" value="{{ old('representative_name') }}" name="representative_name" placeholder="@lang('app.representative_name')">
                      {!! $errors->has('representative_name')? '<p class="help-block">'.$errors->first('representative_name').'</p>':'' !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">@lang('app.representative_email')</label>
                    <div class="col-lg-6">
                      <input type="text" class="form-control" name="representative_email" id="representative_email" value="{{ old('representative_email')}}">
                      {!! $errors->has('representative_email')? '<p class="help-block">'.$errors->first('representative_email').'</p>':'' !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">RE License Number</label>
                    <div class="col-lg-6">
                      <input type="text" class="form-control" id="re_license_no" value="{{ old('re_license_number') }}" name="re_license_number" placeholder="Re License Number">
                      {!! $errors->has('company_name')? '<p class="help-block">'.$errors->first('company_name').'</p>':'' !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">Broker Name</label>
                    <div class="col-lg-6">
                      <input type="text" class="form-control" id="broker_name" value="{{ old('broker_name') }}" name="broker_name" placeholder="Broker Name">
                      {!! $errors->has('broker->name')? '<p class="help-block">'.$errors->first('broker->name').'</p>':'' !!}
                    </div>
                  </div>

                  <div class="form-group row {{ $errors->has('zipcode')? 'has-error':'' }}">
                    <label for="city" class="col-lg-3 col-form-label">Zip Code</label>
                    <div class="col-lg-6">
                      <select class="form-control" id="mls" name="mls">
                        <option value="">Please choose MLS</option>
                        @foreach($mls as $ml)
                          <option value="{{$ml->id}}" rel="{{$ml->state}}">{{$ml->name}}</option>
                        @endforeach
                      </select>
                  </div>
                  </div>
                </div>
                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label"></label>
                    <div class="col-lg-6">
                      <p>&nbsp;&nbsp;</p>
                      <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom" >@lang('app.save')</button>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ route('users') }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">Cancel</a>
                    </div>
                  </div>



                  {!! Form::close() !!}

                  </div>

              </div>

							<!--end::Form-->
						</div>

						<!--end::Portlet-->
					</div>




@endsection
