@extends('tymbl.layouts.dashboard')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">{{$title}}</h3>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->
					<div class="m-content">
            						<!--begin::Portlet-->
						<div class="m-portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<h3 class="m-portlet__head-text">
												{{$title}}
											</h3>

										</div>
									</div>
								</div>
								<div class="m-portlet__body">

									<!--begin::Section-->
									@if($records)
                  <div class="bg-info text-white p-2">*Data imported successfully. You may now upload images (.zip) or click save to continue.</div>
                  <br><br>
                  <h5>Uploaded Data</h5>
                  <table class="table table-bordered table-striped table-responsive">
                      <thead>
                          <tr>
                              <th>#</th>
                              <th>Title</th>
                              <th>Description</th>
                              <th>Seller Name</th>
                              <th>Email</th>
                              <th>Phone</th>
                              <th>Country</th>
                              <th>State</th>
                              <th>City</th>
                              <th>Zip</th>
                              <th>Address</th>
                              <th>Image</th>
                              <th>Video</th>
                              <th>Reservation Fee(escrow)</th>
                              <th>Referral Fee(%)</th>
                              <th>Features 1</th>
                              <th>Features 2</th>
                              <th>Status</th>
                              <th>Price Range</th>
                              <th>Cat ID</th>
                              <th>Category Type</th>
                              <th>Category Type Status (qualified/unqualified)</th>
                              <th>Date Contacted (if qualified)</th>
                              <th>Referral Name</th>
                              <th>Referral Email</th>
                              <th>Referral Phone</th>
                              <th>Referral Fax</th>
                              <th>Referral Address</th>
                          </tr>
                      </thead>

                      <tbody>
                        @for($i = 0; $i < count($records); $i++)
                          @if($records[$i][0] !== null)
                            <tr><td>{{ $i+1 }}</td>
                              @foreach($records[$i] as $rec=>$r)
                                @if($rec == '1')
                                 <td title="{{ $rec == '1' ? $r : '' }}">{{ $rec == '1' ? substr($r, 0, 30) . '...' : $r }}</td>
                                @elseif($rec == '10')
                                <td><img class="img-thumbnail" src="/uploads/images/{{$r}}"></td>
                                @else
                                <td>{{ $r }}</td>
                                @endif
                              @endforeach
                            </tr>
                          @endif
                        @endfor
                      </tbody>

                  </table>

                  <table>
                    <tr>
                      <td>
                      <div class="float-left">
                        <form style="margin-right: 40px;" method="POST" action="{{ route('import-images') }}" enctype="multipart/form-data">
                              {{ csrf_field() }}
                            <div>Upload Required Images</div>
                            <input id="zip_file" type="file" class="form-control" name="zip_file">
                            @if ($errors->has('zip_file'))
                              <span class="help-block">
                                <strong>{{ $errors->first('zip_file') }}</strong>
                              </span>
                            @endif
                            <br>
                            <button type="submit" class="btn btn-primary">
                                Upload Images
                            </button>
                        </form>
                      </div>
                    </td>
                    <td><p>&nbsp;</p><p>&nbsp;</p><a href="{{ route('import-save') }}" class="btn btn-primary" id="btn-save">Save</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ route('import-reset') }}" class="btn btn-primary" id="btn-reset">Reset</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="font-weight-bold err">&nbsp;</span></td>
                    </tr>
                  </table>
                  @else
                  <div class="m-section">
										<div class="m-section__content">
                      <form class="form-horizontal" method="POST" action="{{ route('import-csv') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                          <input id="csv_file" type="file" class="form-control" name="csv_file" required>
                          @if ($errors->has('csv_file'))
                            <span class="help-block">
                              <strong>{{ $errors->first('csv_file') }}</strong>
                            </span>
                          @endif
                          <p>&nbsp;</p>
                          <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Import CSV
                                    </button>
                                </div>
                            </div>
                      </form>

										</div>
									</div>
                  @endif
									<!--end::Section-->
								</div>
								<!--end::Form-->
							</div>
							<!--end::Portlet-->
					</div>
				</div>

@endsection
