@extends('tymbl.layouts.dashboard')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">{{$title}}</h3>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->
					<div class="m-content">
            						<!--begin::Portlet-->
						<div class="m-portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<h3 class="m-portlet__head-text">
												{{$title}}
											</h3>

										</div>
									</div>
								</div>
								<div class="m-portlet__body">

									<!--begin::Section-->
									@if($records)

                  <br><br>
                  <h5>Uploaded Data</h5>
                  <table class="table table-bordered table-striped table-responsive">
                      <thead>
                          <tr>
                              <th>#</th>
                              <th>Date Added</th>
                              <th>Status</th>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>Seller</th>
                              <th>Buyer</th>
                              <th>Lead Source</th>
                              <th>Email</th>
                              <th>Phone</th>
                              <th>Address</th>
                              <th>Original Notes/Tags</th>
                              <th>Prioritize after scrubbing?</th>
                              <th>1st Dial Date</th>
                              <th>1st Dial Outcome</th>
                              <th>1st Dial Notes</th>
                              <th>2nd Dial Date</th>
                              <th>2nd Dial Outcome</th>
                              <th>2nd Dial Notes</th>
                              <th>SMS Date</th>
                              <th>SMS Outcome</th>
                              <th>SMS Notes</th>
                              <th>Email Date</th>
                              <th>Email Outcome</th>
                              <th>Email Notes</th>
                          </tr>
                      </thead>

                      <tbody>
                        @for($i = 0; $i < count($records); $i++)
                          @if($records[$i][0] !== null)
                            <tr><td>{{ $i+1 }}</td>
                              @foreach($records[$i] as $rec=>$r)
                                @if($rec == '1')
                                 <td title="{{ $rec == '1' ? $r : '' }}">{{ $rec == '1' ? substr($r, 0, 30) . '...' : $r }}</td>
                                @else
                                <td>{{ $r }}</td>
                                @endif
                              @endforeach
                            </tr>
                          @endif
                        @endfor
                      </tbody>

                  </table>

                  <table>
                    <tr>
                      <td><p>&nbsp;</p><p>&nbsp;</p><a href="{{ route('import-save') }}" class="btn btn-primary" id="btn-save">Import Leads into ads</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ route('import-reset', ['filename' => app('request')->input('lead_file_name'), 'fileid' => app('request')->input('file_id')]) }}" class="btn btn-primary" id="btn-reset">Reset</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="font-weight-bold err">&nbsp;</span></td>
                    </tr>
                  </table>
                  @else
                  <div class="m-section">
										<div class="m-section__content">
                      <form class="form-horizontal" method="POST" action="{{ route('import-csv') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                          <input id="csv_file" type="file" class="form-control" name="csv_file" required>
                          @if ($errors->has('csv_file'))
                            <span class="help-block">
                              <strong>{{ $errors->first('csv_file') }}</strong>
                            </span>
                          @endif
                          <p>&nbsp;</p>
                          <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Import CSV
                                    </button>
                                </div>
                            </div>
                      </form>

										</div>
									</div>
                  @endif
									<!--end::Section-->
								</div>
								<!--end::Form-->
							</div>
							<!--end::Portlet-->
					</div>
				</div>

@endsection
