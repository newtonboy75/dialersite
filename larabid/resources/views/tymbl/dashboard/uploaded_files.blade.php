@extends('tymbl.layouts.dashboard')
@section('css')
  <style>
    .circle, .circle:hover {
      border-radius: 200px;
      color: white;
      height: 40px;
      width: 40px;
      font-weight: bold;
      display: table;
      margin: 20px auto;
      text-align: center;
      text-decoration: none;
    }
    .circle i {
      vertical-align: middle;
      display: table-cell;
    }
  </style>
@endsection
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

  <!-- BEGIN: Subheader -->
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title ">{{$title}}</h3>
      </div>
    </div>
  </div>

  <!-- END: Subheader -->
  <div class="m-content">
    <!--begin::Portlet-->
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
              {{$title}}
            </h3>
          </div>
        </div>

      </div>
      <div class="m-portlet__body">

        <!--begin::Section-->
        <div class="m-section">
          <div class="m-section__content">
            <div class="messages-wrapper">
              <table class="table jdata display jdata" id="jDataTable">
                <thead>
                  <tr>
                    @if(Auth::user()->user_type == 'admin')
                      <th></th>
                      <th>Contract ID</th>
                      <th>Uploaded By</th>
                      <th>File</th>
                      <th>Plan Type</th>
                      <th>Date Uploaded</th>
                      <th>View</th>
                    @else
                      <th></th>
                      <th>Contract ID</th>
                      <th>Date Uploaded</th>
                      <th>View</th>
                    @endif
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>

        <!--end::Section-->
      </div>

      <!--end::Form-->
    </div>

    <!--end::Portlet-->
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" >
    <div class="modal-content" style="width: 1000px;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row mt-md-3">
          <div class="col-sm-6">
            <label class="label-sm">Date Added</label>
            <input class="form-control" type="text" name="date_added" id="date_added">
          </div>

          <div class="col-sm-6">
            <label class="label-sm">Status</label>
            <select class="custom-select form-control">
            <option>New</option>
            <option>Needs Follow Up</option>
            <option>Scrubbed</option>
            <option>Dead/Purged</option>
          </select>
          </div>
        </div>

        <div class="row mt-md-3">
          <div class="col-sm-6">
            <label class="label-sm">First Name</label>
            <input class="form-control" type="text" name="first_name" id="first_name">
          </div>

          <div class="col-sm-6">
            <label class="label-sm">Last Name</label>
            <input class="form-control" type="text" name="last_name" id="last_name">
          </div>
        </div>

        <div class="row mt-md-3">
          <div class="col-sm-6">
            <label class="label-sm">Lead Source</label>
            <input class="form-control" type="text" name="lead_source" id="lead_source">
          </div>

          <div class="col-sm-6">
            <label class="label-sm">Email</label>
            <input class="form-control" type="text" name="email" id="email">
          </div>
        </div>

        <div class="row mt-md-3">
          <div class="col-sm-6">
            <label class="label-sm">Phone</label>
            <input class="form-control" type="text" name="phone" id="phone">
          </div>

          <div class="col-sm-6">
            <label class="label-sm">Address</label>
            <input class="form-control" type="text" name="address" id="address">
          </div>
        </div>

        <div class="row mt-md-3">
          <div class="col-sm-6">
            <label class="label-sm">Original Notes</label>
            <textarea class="form-control" type="text" name="original_notes" id="original_notes"></textarea>
          </div>

          <div class="col-sm-6">
            <label class="label-sm">Prioritize after scrubbing?</label>
            <select class="custom-select form-control">
            <option>Select option</option>
            <option>Yes</option>
            <option>No</option>
          </select>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save_lead">Save changes</button>
      </div>
    </div>
  </div>
</div>

@endsection
