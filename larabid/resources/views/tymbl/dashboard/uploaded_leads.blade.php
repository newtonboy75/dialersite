@extends('tymbl.layouts.dashboard')
@section('content')
  <div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="m-subheader__title ">Upload leads</h3>
        </div>
      </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">

      <!--begin::Portlet-->
      <div class="m-portlet">
        <div class="m-portlet__head">
          <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
              <h3 class="m-portlet__head-text">
                Upload leads
              </h3>
            </div>
          </div>
        </div>
        <div class="m-portlet__body">
        @include('admin.flash_msg')

        <!--begin::Section-->
        <div class="m-section">
          <div class="m-section__content">
            <div class="responsive-table-div">
              <table class="table table-bordered table-striped text-center table-responsive" id="jDataTable" data-page-length='25'>
                <thead>
                <tr>
                  <th class="text-center" style="width:300px !important;">ID</th>
                  <th class="text-center">Added_date</th>
                  <th class="text-center" style="width:300px !important;">Status</th>
                  <th class="text-center" style="width:300px !important;">Buyer</th>
                  <th class="text-center" style="width:300px !important;">1st dial date</th>
                  <th class="text-center" style="width:300px !important;">1st dial outcome</th>
                  <th class="text-center" style="width:300px !important;">1st dial notes</th>
                  <th class="text-center" style="width:300px !important;">2st dial date</th>
                  <th class="text-center" style="width:300px !important;">2st dial outcome</th>
                  <th class="text-center" style="width:300px !important;">2st dial notes</th>
                  <th class="text-center" style="width:300px !important;">Sms date</th>
                  <th class="text-center" style="width:300px !important;">Sms outcome</th>
                  <th class="text-center" style="width:300px !important;">Sms notes</th>
                  <th class="text-center" style="width:300px !important;">Email date</th>
                  <th class="text-center" style="width:300px !important;">Email outcome</th>
                  <th class="text-center" style="width:300px !important;">Email notes</th>
                </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>

        <!--end::Section-->
      </div>

      <!--end::Form-->
    </div>

    <!--end::Portlet-->
  </div>
</div>

@endsection
