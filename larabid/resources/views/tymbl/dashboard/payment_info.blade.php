@extends('tymbl.layouts.dashboard')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">Users</h3>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->
					<div class="m-content">
            						<!--begin::Portlet-->
						<div class="m-portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<h3 class="m-portlet__head-text">
												Users
											</h3>

										</div>
									</div>
								</div>
								<div class="m-portlet__body">

									<!--begin::Section-->
									<div class="m-section">
										<div class="m-section__content">
                      <table class="table table-bordered table-striped">

                          <tr>
                              <th>@lang('app.ad')</th>
                              <td> @if($payment->ad){{ safe_output($payment->ad->title) }} @endif</td>
                          </tr>
                          <tr>
                              <th>@lang('app.user_name')</th>
                              <td>@if($payment->user){{ safe_output($payment->user->name) }} @endif</td>
                          </tr>


                          <tr>
                              <th>@lang('app.amount')</th>
                              <td>{{ sprintf("%0.2f", floatval($payment->amount) ) }}</td>
                          </tr>
                          <tr>
                              <th>@lang('app.payment_method')</th>
                              <td>{{ $payment->payment_method }}</td>
                          </tr>
                          <tr>
                              <th>@lang('app.status')</th>
                              <td>{{ $payment->status }}</td>
                          </tr>
                          <tr>
                              <th>@lang('app.currency')</th>
                              <td>{{ $payment->currency }}</td>
                          </tr>
                          <tr>
                              <th>@lang('app.charge_id_or_token')</th>
                              <td>{{ $payment->status }}</td>
                          </tr>
                          <tr>
                              <th>@lang('app.payer_email')</th>
                              <td>{{ $payment->payer_email }}</td>
                          </tr>
                          <tr>
                              <th>@lang('app.description')</th>
                              <td>{{ $payment->description }}</td>
                          </tr>
                          <tr>
                              <th>@lang('app.transaction_id')</th>
                              <td>{{ $payment->local_transaction_id }}</td>
                          </tr>
                          <tr>
                              <th>@lang('app.payment_completed_at')</th>
                              <td>{{ $payment->updated_at }}</td>
                          </tr>



                      </table>
										</div>
									</div>

									<!--end::Section-->
								</div>

								<!--end::Form-->
							</div>

							<!--end::Portlet-->
					</div>
				</div>

@endsection
