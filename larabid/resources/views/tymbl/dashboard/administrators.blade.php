@extends('tymbl.layouts.dashboard')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">Users</h3>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->
					<div class="m-content">
            						<!--begin::Portlet-->
						<div class="m-portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<h3 class="m-portlet__head-text">
												Users
											</h3>

										</div>
									</div>
								</div>
								<div class="m-portlet__body">

									<!--begin::Section-->
									<div class="m-section">
										<div class="m-section__content">
                      <table class="table table-bordered table-striped">
                          <tr>
                              <th>{{ trans('app.name') }}</th>
                              <th>{{ trans('app.email') }}</th>
                              <th>{{ trans('app.last_login') }}</th>
                              <th>{{ trans('app.action') }}</th>
                          </tr>
                          @foreach($users as $u)
                          <tr>
                              <td>{{ $u->name }}</td>
                              <td>{{ $u->email }}</td>
                              <td>{{ $u->last_login }}</td>
                              <td>

                                  @if($u->id >2)

                                  <a href="javascript:;" class="btn btn-danger blockAdministrator" data-id="{{ $u->id }}" style="display: {{ $u->active_status ==1? '': 'none' }}"><i class="fa fa-ban"></i> </a>
                                  <a href="javascript:;" class="btn btn-success unblockAdministrator" data-id="{{ $u->id }}" style="display: {{ $u->active_status ==1? 'none': '' }}"><i class="fa fa-ban"></i> </a>
                                  @endif

                              </td>
                          </tr>
                          @endforeach
                      </table>
										</div>
									</div>

									<!--end::Section-->
								</div>

								<!--end::Form-->
							</div>

							<!--end::Portlet-->
					</div>
				</div>

@endsection
