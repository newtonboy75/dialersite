@extends('tymbl.layouts.dashboard')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">Change Password</h3>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->
					<div class="m-content">
            <div class="mx-auto" style="width:100%;">@include('admin.flash_msg')</div>
						<!--begin::Portlet-->
						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<span class="m-portlet__head-icon m--hide">
											<i class="la la-gear"></i>
										</span>
										<h3 class="m-portlet__head-text">
											Change Password
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<!--begin::Form-->
							{!! Form::open(['class' => 'm-form']) !!}
								<div class="m-portlet__body">
									<div class="m-form__section m-form__section--first">
										<div class="form-group m-form__group row">
											<label class="col-lg-3 col-form-label">Current Password</label>
											<div class="col-lg-6">
												<input name="old_password" type="password" class="form-control m-input m-input--solid" placeholder="Enter Your Current Password">
                        {!! $errors->has('old_password')? '<p class="help-block"> '.$errors->first('old_password').' </p>':'' !!}
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label class="col-lg-3 col-form-label">New Password</label>
											<div class="col-lg-6">
												<input name="new_password" type="password" class="form-control m-input m-input--solid" placeholder="Enter Your New Password">
                        {!! $errors->has('new_password')? '<p class="help-block"> '.$errors->first('new_password').' </p>':'' !!}
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label class="col-lg-3 col-form-label">Confirm Password</label>
											<div class="col-lg-6">
												<input name="new_password_confirmation" type="password" class="form-control m-input m-input--solid" placeholder="Confirm Your New Password">
                        {!! $errors->has('new_password_confirmation')? '<p class="help-block"> '.$errors->first('new_password_confirmation').' </p>':'' !!}
											</div>
										</div>

									</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-3"></div>
											<div class="col-lg-6">
												<button type="submit" class="btn btn-success">Change Password</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->

							<!--end::Form-->
						</div>

						<!--end::Portlet-->
					</div>
				</div>
  @endsection
