<?php

if (isset($ads['media']) && $ads['media']['media_name'] != 'NULL') {
    if (file_exists('uploads/images/thumbs/' . $ads['media']['media_name'])) {
        $img = '<img src="/uploads/images/thumbs/' . $ads['media']['media_name'] . '" class="img-responsive dbc-reserved" />';
    } else {
        $img = '<img src="/uploads/images/' . $ads['media']['media_name'] . '" class="img-responsive dbc-reserved" />';
    }
} else {
    if ($ads->category_type == "selling") {
        $img = '<img class="img-fluid" src="' . asset("/assets/img/tymbl/house.png") . '" class="d-block w-100 dbc-reserved" />';
    } else {
        $img = '<img class="img-fluid" src="' . asset("/assets/img/tymbl/person.png") . '" class="d-block w-100 dbc-reserved" />';
    }
} ?>
<div class="jd-success-data">
    <div class="drc">
        {!! $img !!}
    </div>
    <div class="drc">
        <a href="javascript:;" class="btn btn-primary text-light drc2 btn-block text-uppercase"
           rel="'{{ $ads->id }}" data-toggle="modal" data-slug="{{ $ads->slug }}" data-target="#myModal">
            Reassign Lead
        </a>
    </div>
    <div class="drc">
        <a id="relist-ad" rel="{{ $ads->id }}" class="btn btn-primary text-light drc2 btn-block text-uppercase">
            Repost ad&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="wait-notice"></span>
        </a>
    </div>
    @if(Auth::user()->is_admin())
    <div class="drc">
        <a class="btn btn-primary text-light drc2 btn-block text-uppercase" id="send-mail" data-id="{{ $ads['listingContracts']['id'] }}">
            Request status update
        </a>
    </div>
    <div class="drc">
        <div class="btn btn-primary text-light drc2 btn-block text-uppercase">
            <a data-id="{{ $ads['listingContracts']['id'] }}" type="checkbox" name="follow_up" id="follow_up">
                {{ $ads['listingContracts']['follow_up'] == 0 ? 'Follow up' : 'No follow up' }}
            </a>
        </div>
    </div>
    @endif
</div>