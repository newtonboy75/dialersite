@extends('tymbl.layouts.dashboard')
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title ">Leads</h3>
                </div>
                <a href="{{route('import-leads')}}">Import New Leads</a>&nbsp;&nbsp;&nbsp;
            </div>
        </div>

        <!-- END: Subheader -->
        <div class="m-content">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                {{safe_output($title)}}
                            </h3>
                        </div>
                    </div>
                    <!-- search from -->
                    <form action="{{ Request::url() }}" method="GET" class="user-search-dash">
                        <div class="input-group mb-3">
                            <input class="form-control float-left" type="text" name="q"
                                   value="{{ app('request')->input('q') }}">
                            <input type="hidden" name="p" value="{{$adtype}}">
                            <div class="input-group-append">
                                <input class="btn btn-primary float-left" type="submit" value="search">
                            </div>
                        </div>
                    </form>
                    <!-- end search from -->
                </div>
                <div class="m-portlet__body">
                    <!--begin::Section-->
                    <div class="m-section">
                        <div class="m-section__content">
                            @if($ads->total() > 0)
                                <table class="table table-bordered table-striped" id="tbl-admin-user">
                                    @foreach($ads as $ad)
                                        @php
                                            $images = \App\Media::where('ad_id', $ad->id)->orderBy('created_at', 'desc')->where('type', '=', 'image')->first();
                                        @endphp
                                        <tr>
                                            <td>
                                                @if($images['media_name'] && $images['media_name'] != 'NULL')
                                                    @if(file_exists('uploads/images/thumbs/'.$images['media_name']))
                                                        <img class="dashboard-thumbs"
                                                             src="{{ media_url($ad->feature_img) }}"
                                                             class="thumb-listing-table" alt="">
                                                    @else
                                                        <img src="uploads/images/{{$images['media_name']}}"
                                                             class="dashboard-thumbs"/>
                                                    @endif
                                                @else
                                                    @if($ad->category_type == "selling")
                                                        <img src="{{ asset('assets/img/tymbl/house.png') }}"
                                                             class="dashboard-thumbs"/>
                                                    @else
                                                        <img src="{{ asset('assets/img/tymbl/person.png') }}"
                                                             class="dashboard-thumbs"/>
                                                    @endif
                                                @endif

                                            </td>
                                            <td class="ad-desc">
                                                <h5><a href="{{  route('single_ad', [$ad->id, $ad->slug]) }}"
                                                       target="_blank">{{ safe_output($ad->title) }}</a></h5>
                                                @php
                                                    $currency = 'USD';
                                                    if($ad->country->id != '231'){
                                                      $currency = 'CAD';
                                                    }
                                                @endphp
                                                <p>Agent/Poster: {{$ad->seller_name}}</p>
                                                <p>Reservation Fee: {{$currency}} ${{$ad->escrow_amount}} <br>
                                                    Referral Fee: {{(float)($ad->referral_fee*100) - 10  }}%

                                                </p>
                                                <p class="text-muted">{!! $ad->full_address_dashboard()  !!}
                                                <p title="Date published"><i
                                                            class="fa fa-clock-o"></i>{{ $ad->posting_datetime()  }}</p>
                                                </p>
                                                <hr>
                                                <div style="max-width: 300px;padding: 10px 0px;">
                                                    <p style="font-weight: 500; font-size: 15px;">REFERRAL DETAILS</p>
                                                    @php
                                                        $referral = \App\ReferralContactInfo::where('ad_id', '=', $ad->id)->first();
                                                    @endphp
                                                    @if($referral)
                                                        <p>{{ isset($referral->referral_last_name) && isset($referral->referral_first_name) ? $referral->referral_first_name.' '.$referral->referral_last_name : $referral->referral_name }}
                                                            <br>
                                                            @if($referral->referral_contact_email)
                                                                <a href="mailto:{{$referral->referral_contact_email}}">{{$referral->referral_contact_email}}</a>
                                                                <br>
                                                            @endif

                                                            @if($referral->referral_contact_phone)
                                                                {{$referral->referral_contact_phone}}<br>
                                                            @endif

                                                            @if(isset($referral->referral_contact_address) && $referral->referral_contact_address !== 'null')
                                                                {{$referral->referral_contact_address}}
                                                            @endif
                                                        </p>
                                                    @endif
                                                </div>

                                                <!--Lead Notes-->
                                                <div class="notes">
                                                    <div class="m-accordion m-accordion--default" id="m_accordion_1"
                                                         role="tablist">
                                                        <div class="m-accordion__item" style="padding: 1rem;">
                                                            <div class="m-accordion__item-head collapsed" role="tab"
                                                                 data-toggle="collapse"
                                                                 href="#note-accordion-{{$ad->id}}"
                                                                 aria-expanded="false">
                                                                <span class="m-accordion__item-icon"><i
                                                                            class="fa flaticon-speech-bubble-1"></i></span>
                                                                <span class="m-accordion__item-title">Notes</span>
                                                                <span class="m-accordion__item-mode"></span>
                                                            </div>

                                                            <div class="m-accordion__item-body collapse"
                                                                 id="note-accordion-{{$ad->id}}" role="tabpanel"
                                                                 aria-labelledby="m_accordion_1_item_1_head"
                                                                 data-parent="#m_accordion_1">
                                                                <div class="m-accordion__item-content">

                                                                    {!! Form::open(['method'=>'POST', 'action'=>'NoteController@store', 'class'=>'m-form' ]) !!}
                                                                    <input type="hidden" name="ad_id"
                                                                           value="{{$ad->id}}">
                                                                    <div class="form-group">
                                                                        {!! Form::textarea('lead_note_body', null, ['class'=>'form-control', 'rows'=>2,]  ) !!}
                                                                    </div>
                                                                    {!! Form::submit('Add Note', ['class'=>'btn btn-primary btn-sm float-right']) !!}
                                                                    {!! Form::close() !!}

                                                                    <div class="clearfix"></div>

                                                                    <!--Previous Comments-->

                                                                    <div class="all-notes" style="padding-top: 2rem;">
                                                                        @if(isset($ad->lead_notes) && count($ad->lead_notes)>0)

                                                                            @foreach($ad->lead_notes as $note)


                                                                                <div class="single-note">
                                                                                    <div class="m-separator m-separator--space m-separator--dashed"></div>
                                                                                    <div class="note-body">
                                                                                        <p>{{$note->lead_note_body}}</p>
                                                                                    </div>

                                                                                    <div class="single-note-foot row">
                                                                                        <div class="col-sm-6">
                                                                                            <small>By:
                                                                                                <strong>{{auth()->user()->name}}</strong>&nbsp;&nbsp;
                                                                                                |&nbsp;&nbsp;  {{$note->created_at->format('d M Y')}}
                                                                                            </small>
                                                                                        </div>
                                                                                        <div class="col-sm-6">
                                                                                            {!! Form::open(['method'=>'DELETE', 'action'=>['NoteController@destroy', $note->id], 'class'=>'m-form' ]) !!}
                                                                                            <button class='m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill float-right'
                                                                                                    type='submit'
                                                                                                    value='Delete'
                                                                                                    data-toggle='m-tooltip'
                                                                                                    title='Delete Note'
                                                                                                    data-original-title='Delete Note'>
                                                                                                <i class="flaticon-delete"
                                                                                                   style="font-size: 16px;"></i>
                                                                                            </button>
                                                                                            {!! Form::close() !!}
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            @endforeach
                                                                        @else
                                                                            <p class="text-center">No note added.</p>
                                                                        @endif

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Lead Notes Ends-->
                                            </td>

                                            <td id="icon-actions">
                                                @if($ad->status != '7')
                                                    <div class="f-holder">Featured Ad&nbsp;&nbsp;<label class="switch">
                                                            <input rel="{{$ad->id}}" id="f-toggler"
                                                                   type="checkbox" {{ $ad->is_featured == '1' ? 'checked' : '' }}>
                                                            <span class="slider round"
                                                                  title="Mark as featured ad"></span>
                                                        </label></div>
                                                @endif
                                                <a title="Remove this lead" href="javascript:;"
                                                   class="btn btn-danger deleteAds" data-slug="{{ $ad->slug }}"><i
                                                            class="fa fa-trash"></i> </a>

                                                @if($ad->status == '1')
                                                    <a title="Move to VIP" href="#" class="btn btn-info vipAds"
                                                       data-slug="{{ $ad->slug }}" data-value="7" rel="{{$ad->id}}"><i
                                                                class="fas fa-eye-slash"></i> </a>
                                                @elseif($ad->status == '7')
                                                    <a title="Move to regular lead" href="#"
                                                       class="btn btn-info regularAds" data-slug="{{ $ad->slug }}"
                                                       data-value="1" rel="{{$ad->id}}"><i class="fas fa-eye"></i> </a>
                                                @endif


                                                <a title="Mark Inactive" href="#" class="btn btn-success pauseAds"
                                                   data-slug="{{ $ad->slug }}" data-value="2" rel="{{$ad->id}}"><i
                                                            class="fas fa-pause"></i> </a>


                                                @if($ad->status == '1' || $ad->status == '7')
                                                    <a title="Block lead" href="javascript:;"
                                                       class="btn btn-warning blockAds" data-slug="{{ $ad->slug }}"
                                                       data-value="3"><i class="fa fa-ban"></i></a>
                                                    <a title="Edit" href="{{ route('edit_ad', $ad->id) }}"
                                                       class="btn btn-primary"><i class="fa fa-edit"></i> </a>



                                                @else
                                                    <a href="javascript:;" title="Unblock this ad"
                                                       class="btn btn-warning approveAds" data-slug="{{ $ad->slug }}"
                                                       data-value="1"><i class="fas fa-circle"></i></a>
                                                @endif

                                                @if($ad->status == '7')
                                                    <p>&nbsp;</p><p>&nbsp;</p>
                                                    <p style="margin-top: 40px;"><a href="javascript::"
                                                                                    data-slug="{{ $ad->slug }}"
                                                                                    id="send-vip-lead"
                                                                                    class="btn btn-primary"
                                                                                    role="button">Send this lead</a></p>
                                                @endif

                                                <div style="display: block; width: 100%; float: left; margin-top: 20px;">
                                                  <a href="javascript:;" class="btn btn-primary" rel="{{$ad->id}}" data-toggle="modal" data-slug="{{$ad->slug}}" data-target="#myModal">Reassign Lead</a>
                                                </div>

                                            </td>
                                        </tr>
                                    @endforeach

                                </table>

                            @else
                                <h2>@lang('app.there_is_no_ads')</h2>
                            @endif

                            {!! $ads->links() !!}
                        </div>
                    </div>

                    <!--end::Section-->
                </div>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>

@endsection
