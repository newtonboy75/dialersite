@extends('tymbl.layouts.dashboard')
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title ">My Leads</h3>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">

            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                My Leads
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">

                    <!--begin::Section-->
                    <div class="m-section">
                        <div class="m-section__content">
                            <div class="responsive-table-div">
                                @if($ads->total() > 0)
                                    @foreach($ads as $ad)
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <td><img src="{{ media_url($ad->feature_img) }}" alt="item" class="img-fluid img-thumbnail thumb">
                                                </td>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="m-list-search">
                                                    <div class="m-list-search__results">

                                                        <a href="{{  route('single_ad', [$ad->id, $ad->slug]) }}"
                                                           class="m-list-search__result-item">
                                                            <span class="m-list-search__result-item-icon"><i
                                                                        class="flaticon-home-2 m--font-warning"></i></span>
                                                            <span class="m-list-search__result-item-text ">{{ $ad->title }}</span>
                                                        </a>
                                                        <div class="m-list-search__result-item"></a>
                                                            <span class="m-list-search__result-item-icon"><i
                                                                        class="flaticon-map-location m--font-success"></i></span>
                                                            <span class="m-list-search__result-item-text">{{ isset($ad->city->city_name) ? $ad->city->city_name : '' }} {{ isset($ad->state->state_name) ? $ad->state->state_name : '' }}</span>
                                                        </div>
                                                        <div class="m-list-search__result-item">
                                                            <span class="m-list-search__result-item-icon"><i
                                                                        class="flaticon-clock-1 m--font-info"></i></span>
                                                            <span class="m-list-search__result-item-text">{{ $ad->created_at->format('M d Y') }}</span>
                                                        </div>

                                                        @php
                                                            $relisted_ad = \App\ListingContracts::where('listing_id', '=', $ad->id)->first();
                                                        @endphp

                                                        @php
                                                            $status = '';
                                                            $status_color = '';
                                                            switch ($ad->status) {
                                                              case '1':
                                                                  $status = 'Active';
                                                                  $status_color = 'active-status';
                                                                  break;
                                                              case '2':
                                                                  $status = 'Inactive';
                                                                  $status_color = 'pending-status';
                                                                  break;
                                                              case '3':
                                                                  $status = 'Block';
                                                                  $status_color = 'block-status';
                                                                  break;
                                                              case '4':
                                                                  $status = 'Pending for Approval';
                                                                  $status_color = 'sale-pending';
                                                                  break;
                                                              case '5':
                                                                  $status = 'Reserved';
                                                                  $status_color = 'sale-approved';
                                                                  break;
                                                            }
                                                        @endphp

                                                        <div class="m-list-search__result-item">
                                                            <span class="m-list-search__result-item-icon"><i
                                                                        class="fas fa-circle {{$status_color}}"></i></span>
                                                            <span class="m-list-search__result-item-text">{{$status}}</span>

                                                            @if($relisted_ad)
                                                                @if($relisted_ad->list_status == '3')
                                                                    <i class="fas fa-check"></i> Relisted
                                                                @endif
                                                            @endif

                                                        </div>
                                                    </div>
                                                </div>

                                                <!--Note: Begin-->
                                                <div class="notes">
                                                    <div class="m-accordion m-accordion--default" id="m_accordion_1"
                                                         role="tablist">
                                                        <div class="m-accordion__item">
                                                            <div class="m-accordion__item-head collapsed" role="tab"
                                                                 data-toggle="collapse"
                                                                 href="#note-accordion-{{$ad->id}}"
                                                                 aria-expanded="false">
                <span class="m-accordion__item-icon"><i
                            class="fa flaticon-speech-bubble-1"></i></span>
                                                                <span class="m-accordion__item-title">Notes</span>
                                                                <span class="m-accordion__item-mode"></span>
                                                            </div>

                                                            <div class="m-accordion__item-body collapse"
                                                                 id="note-accordion-{{$ad->id}}" role="tabpanel"
                                                                 aria-labelledby="m_accordion_1_item_1_head"
                                                                 data-parent="#m_accordion_1">
                                                                <div class="m-accordion__item-content">

                                                                    {!! Form::open(['method'=>'POST', 'action'=>'NoteController@store', 'class'=>'m-form' ]) !!}
                                                                    <input type="hidden" name="ad_id"
                                                                           value="{{$ad->id}}">
                                                                    <div class="form-group">
                                                                        {!! Form::textarea('lead_note_body', null, ['class'=>'form-control', 'rows'=>2,]  ) !!}
                                                                    </div>
                                                                    {!! Form::submit('Add Note', ['class'=>'btn btn-primary btn-sm float-right']) !!}
                                                                    {!! Form::close() !!}

                                                                    <div class="clearfix"></div>


                                                                    <div class="all-notes" style="padding-top: 2rem;">

                                                                        @if($ad->lead_notes && count($ad->lead_notes)>=1)
                                                                            @foreach($ad->lead_notes as $note)
                                                                                <div class="single-note">
                                                                                    <div class="m-separator m-separator--space m-separator--dashed"></div>
                                                                                    <div class="note-body">
                                                                                        <p>{{$note->lead_note_body}}</p>
                                                                                    </div>

                                                                                    <div class="single-note-foot row" style="    border-bottom:none;">
                                                                                        <div class="col-sm-9">
                                                                                            <small>By:
                                                                                                <strong>{{auth()->user()->name}}</strong> &nbsp;&nbsp;
                                                                                                |&nbsp;&nbsp; {{$note->updated_at->format('d M Y')}}
                                                                                            </small>
                                                                                        </div>
                                                                                        <div class="col-sm-3">
                                                                                            {!! Form::open(['method'=>'DELETE', 'action'=>['NoteController@destroy', $note->id], 'class'=>'m-form' ]) !!}
                                                                                            <button class='m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill float-right'
                                                                                                    type='submit'
                                                                                                    value='Delete'
                                                                                                    data-toggle='m-tooltip'
                                                                                                    title='Delete Note'
                                                                                                    data-original-title='Delete Note'>
                                                                                                <i class="flaticon-delete"
                                                                                                   style="font-size: 16px;"></i>
                                                                                            </button>
                                                                                            {!! Form::close() !!}
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @endforeach

                                                                        @else
                                                                            <p>No Note Added.</p>
                                                                        @endif


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>



                                                <!--Note: End-->

                                            </div>
                                            <div class="col-sm-4">
                                                <a href="{{ route('edit_ad', $ad->id) }}"
                                                   class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill"
                                                   data-container="body" data-toggle="m-tooltip" data-placement="top"
                                                   title="Edit">
                                                    <i class="fa flaticon-edit"></i>
                                                </a>
                                                <a href="#"
                                                   class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"
                                                   data-container="body" data-toggle="m-tooltip" data-placement="top"
                                                   title="Delete" id="deleteAds" data-slug="{{ $ad->slug }}">
                                                    <i class="fa flaticon-delete"></i>
                                                </a>

                                                @if($ad->status == '1')
                                                    <a href="javascript:;"
                                                       class="pauseAds btn btn-dark m-btn m-btn--icon m-btn--icon-only m-btn--pill"
                                                       data-container="body" data-toggle="m-tooltip"
                                                       data-placement="top" title="Inactivate"
                                                       data-slug="{{ $ad->slug }}" data-value="2">
                                                        <i class="fas fa-pause"></i>
                                                    </a>
                                                @elseif($ad->status == '2')
                                                    <a href="javascript:;"
                                                       class="approveAds btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill"
                                                       data-container="body" data-toggle="m-tooltip"
                                                       data-placement="top" title="Activate" data-slug="{{ $ad->slug }}"
                                                       data-value="1">
                                                        <i class="fas fa-check"></i>
                                                    </a>
                                                @endif

                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                                <br>
                                {{ $ads->links() }}
                            </div>

                        </div>
                    </div>

                    <!--end::Section-->
                </div>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>

    <!-- end:: Body -->
@endsection
