@extends('tymbl.layouts.dashboard')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="m-subheader__title ">Edit Post</h3>
        </div>
      </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
      <div class="mx-auto" style="width:100%;"></div>
      <!--begin::Portlet-->
      <div class="m-portlet">
        <div class="m-portlet__head">
          <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
              <span class="m-portlet__head-icon m--hide">
                <i class="la la-gear"></i>
              </span>
              <h3 class="m-portlet__head-text">
                Edit Post
              </h3>
            </div>

            <div class="success-error">
              @if(session('success'))
              @if(Auth::user()->user_type == 'admin')
              <a class="btn btn-sm btn-primary"  href="{{route('approved_ads')}}">< Back to Leads</a>
              @else
              <a class="btn btn-sm btn-primary"  href="{{route('my-leads')}}">< Back</a>
              @endif
              @endif</div>
            </div>
          </div>

          <!--begin::Form-->


          <div class="m-portlet__body">
            <div class="m-form__section m-form__section--first">

              {{ Form::open(['id'=>'adsPostForm', 'class' => 'm-form', 'files' => true]) }}

              <div class="form-group m-form__group row">
                <label class="col-lg-3 col-form-label">Prospect</label>
                <div class="btn-group col-lg-6" role="group">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <!-- <button type="button" class="btn btn-primary btn-lg btn">Looking to buy</button>
                  <button type="button" class="btn btn-outline-primary btn-lg btn">Looking to sell</button> -->
                  @php
                  foreach($categories as $category){
                    if($category->category_type == 'buying'){
                      $buying[]= array('category_type' => $category->category_type, 'id' => $category->id, 'name' => $category->category_name);
                    }else{
                      $selling[]= array('category_type' => $category->category_type, 'id' => $category->id, 'name' => $category->category_name);
                    }
                  }
                  @endphp

                  <div class="form-check form-check-inline text-bold" style="margin-left: -18px;">
                    <input class="form-check-input" name="cat_type" value="buying" type="radio" id="lookingToBuy" required data-msg="Prospect is required"  {{$ad->category_type == 'buying' ? 'checked' : '' }}>
                    <label class="form-check-label" for="lookingToBuy">Looking to buy&nbsp;&nbsp;<i style="font-size: 13px;" class="fas fa-user"></i> </label>
                  </div>
                  <div class="form-check form-check-inline text-bold">&nbsp;&nbsp;
                    <input class="form-check-input" name="cat_type" value="selling" type="radio" id="lookingToSell" required {{$ad->category_type == 'selling' ? 'checked' : '' }}>
                    <label class="form-check-label" for="lookingToSell">Looking to sell&nbsp;&nbsp;<i style="font-size: 13px;" class="fas fa-home"></i> </label>
                  </div>

                  <div class="form-check form-check-inline text-bold">&nbsp;&nbsp;
                    <input class="form-check-input" name="cat_type" value="other" type="radio" id="notSure" required {{$ad->category_type == 'other' ? 'checked' : '' }}>
                    <label class="form-check-label" for="notSure">Not sure </label>
                  </div>

                </div>
              </div>

              <div class="form-group row" id="qq" >
                <label for="refEscrow" class="col-sm-2 col-form-label" >
                </label>

                <span class="q-text" style="margin-left: 100px;">Have you qualified this lead? </span>
                <div class="form-check form-check-inline text-bold lbl-sm">&nbsp;
                  <input class="form-check-input" name="cat_type_q" type="radio" value="yes" id="ns_qq" data-msg="Let us know if this lead has been qualified" {{ $ad->cat_type_status == 'qualified' ? 'checked' : ''}}>
                  <label class="form-check-label" for="notSure">Yes</label>
                </div>

                <div class="form-check form-check-inline text-bold lbl-sm">
                  <input class="form-check-input" name="cat_type_q" value="no" type="radio" id="ns_qq" {{ $ad->cat_type_status == 'unqualified' ? 'checked' : ''}}>
                  <label class="form-check-label" for="notSure">No</label>
                </div>
              </div>

              @php
              $contract_date = '';
              $style = 'display:none;';
              @endphp
              @if(isset($ad->date_contacted_qualified))
              @php $year = date("Y", strtotime($ad->date_contacted_qualified)); @endphp
                @if($year == '1970')
                  @php
                  $contract_date = '';
                  $style = 'display:none;';
                  @endphp
                  @else
                  @php $contract_date = date("m/d/Y", strtotime($ad->date_contacted_qualified)); @endphp
                @endif
              @endif

              <div class="form-group m-form__group row" id="dt_list" style="{{$style}}width: 100%;">
                <label class="col-lg-3 col-form-label">&nbsp;</label>
                <div class="col-lg-6" >
                  <div class="input-group">
                    <lable>Approx. date of last contact</label><br>
                      <input value="{{ $contract_date }}" data-msg="Let us know when this lead qualified" class="form-control" name="date_contacted_qualified" type="text" id="date_list">
                    </div>
                  </div>
                </div>


                <div class="form-group m-form__group row">
                  <label class="col-lg-3 col-form-label">Referral Fee <span class="input-group-addon">%</span><span class="required-field">*</span></label>
                  <div class="col-lg-6">
                    <div class="input-group">
                      <input required type="text" min="0" class="form-control" required data-msg="Referral fee is required"  id="refFee" value="{{ old('referral_fee') ? old('referral_fee') : (float)($ad->referral_fee*100) - 10  }}%" name="referral_fee">
                    </div>
                    {!! $errors->has('referral_fee')? '<p class="help-block">'.$errors->first('referral_fee').'</p>':'' !!}
                    <span class="text-info">Does not include third-party's 10%</span>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="type" class="col-lg-3 col-form-label">Type {{$ad->category_type}}</label>
                  <div class="col-lg-6">
                    <select class="custom-select form-control" id="sub-category" name="category">

                      @if($ad->category_type == 'buying')
                      @foreach($buying as $buy)
                      <option value="{{ $buy['id'] }}"
                      {{ $buy['id'] == $ad->sub_category_id ? 'selected' : '' }}
                      data-category-type="{{$buy['category_type']}}">{{ $buy['name'] }}
                    </option>
                    @endforeach
                    @else
                    @foreach($selling as $sell)
                    <option value="{{ $sell['id'] }}"
                    {{ $sell['id'] == $ad->sub_category_id ? 'selected' : '' }}
                    data-category-type="{{$sell['category_type']}}">{{ $sell['name'] }}
                  </option>
                  @endforeach
                  @endif
                </select>
                {!! $errors->has('category')? '<p class="help-block">'.$errors->first('category').'</p>':'' !!}
              </div>
            </div>

            <div class="form-group m-form__group row">
              <label class="col-lg-3 col-form-label">Title<span class="required-field">*</span></label>
              <div class="col-lg-6">
                <input required type="text" class="form-control" id="ad_title" value="{{ old('ad_title') ? old('ad_title') : $ad->title }}" name="ad_title" placeholder="@lang('app.ad_title')" data-msg="Title is required">
                {!! $errors->has('ad_title')? '<p class="help-block">'.$errors->first('ad_title').'</p>':'' !!}
                <span class="text-info">Ideally 70-100 characters</span>
              </div>
            </div>

            @php $features = unserialize($ad->feature_1); @endphp
            <div class="form-group row">
              <label for="" class="col-sm-3 col-form-label">Summary</label>
              <div class="col-sm-6">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="input-group">
                      <input type="number" min="0" class="form-control feature" name="feature[0]" value="{{ $features[0] }}">
                      <div class="input-group-append iga">
                        <span class="input-group-text">Beds</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="input-group">
                      <input type="number" min="0" class="form-control feature" id="feature" name="feature[1]" value="{{ $features[1] }}">
                      <div class="input-group-append iga">
                        <span class="input-group-text">Baths</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="input-group">
                      <input type="number" min="0" class="form-control feature" id="feature" name="feature[2]" value="{{ $features[2] }}">
                      <div class="input-group-append iga">
                        <span class="input-group-text">Sqft</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-4">
                    <div class="input-group">
                      <input type="number" min="0" class="form-control feature" id="feature" name="feature[3]" value="{{ $features[3] }}">
                      <div class="input-group-append iga">
                        <span class="input-group-text">Garages</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="input-group">
                      <input type="number" min="0" class="form-control feature" id="feature" name="feature[4]" value="{{ $features[4] }}">
                      <div class="input-group-append iga">
                        <span class="input-group-text">Kitchen</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="input-group">
                      <input type="number" min="0" class="form-control feature" id="feature" name="feature[5]" value="{{ $features[5] }}">
                      <div class="input-group-append iga">
                        <span class="input-group-text">Deck</span>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>

            <div class="form-group m-form__group row">
              <label class="col-lg-3 col-form-label">Description<span class="required-field">*</span></label>
              <div class="col-lg-6">
                <textarea required name="ad_description"  class="form-control" id="desc" rows="8" data-msg="Description is required">{{ old('ad_description')?  old('ad_description') : $ad->description }}</textarea>
                {!! $errors->has('ad_description')? '<p class="help-block">'.$errors->first('ad_description').'</p>':'' !!}
                <p class="text-info"> @lang('app.ad_description_info_text')</p>
              </div>
            </div>
            @php $feature2 = unserialize($ad->feature_2); @endphp

            <div class="form-group row">
              <label for="amenities" class="col-lg-3 col-form-label">General Amenities</label>
              <div class="col-lg-6">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-check form-check-inline">
                      <input name="feature2[0]"  class="form-check-input" type="checkbox" id="security" value="security" {{ isset($feature2) ? isset($feature2[0]) ? 'checked' : '' : '' }} >
                      <label class="form-check-label" for="security">24 hr security</label>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-check form-check-inline">
                      <input name="feature2[1]" class="form-check-input" type="checkbox" id="laundry" value="laundry"
                      {{ isset($feature2) ? isset($feature2[1] ) ? 'checked' : '' : '' }}>
                      <label class="form-check-label" for="laundry">Laundry Services</label>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-check form-check-inline">
                      <input name="feature2[2]" class="form-check-input" type="checkbox" id="garden" value="garden"
                      {{ isset($feature2) ? isset($feature2[2]) ? 'checked' : '' : '' }} >
                      <label class="form-check-label" for="garden">Garden</label>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-check form-check-inline">
                      <input name="feature2[3]" class="form-check-input" type="checkbox" id="swimming" value="swimming" {{ isset($feature2) ? isset($feature2[3] )? 'checked' : '' : '' }}>
                      <label class="form-check-label" for="swimming">Swimmig pool</label>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-check form-check-inline">
                      <input name="feature2[4]" class="form-check-input" type="checkbox" id="tennis" value="tennis" {{ isset($feature2) ? isset($feature2[4]) ? 'checked' : '' : '' }}>
                      <label class="form-check-label" for="tennis">Tennis Court</label>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-check form-check-inline">
                      <input name="feature2[5]" class="form-check-input" type="checkbox" id="school" value="school" {{ isset($feature2) ? isset($feature2[5]) ? 'checked' : '' : '' }}>
                      <label class="form-check-label" for="school">Close to Local Schools</label>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-check form-check-inline">
                      <input name="feature2[6]" class="form-check-input" type="checkbox" id="pet" value="pet" {{ isset($feature2) ?  isset($feature2[6]) ? 'checked' : '' : '' }}>
                      <label class="form-check-label" for="pet">Pet Friendly</label>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-check form-check-inline">
                      <input name="feature2[7]" class="form-check-input" type="checkbox" id="lift" value="lift" {{ isset($feature2) ? isset($feature2[7])  ? 'checked' : '' : '' }}>
                      <label class="form-check-label" for="lift">Lift</label>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-check form-check-inline">
                      <input name="feature2[8]" class="form-check-input" type="checkbox" id="access" value="access" {{ isset($feature2) ? isset($feature2[8] )? 'checked' : '' : '' }} >
                      <label class="form-check-label" for="access">Access Controlled</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group m-form__group row">
              <label class="col-lg-3 col-form-label">Price Range<span class="required-field">*</span></label>
              <div class="col-lg-6">
                <select class="custom-select form-control" name="price_range" id="price-range" data-msg="Choose price range" required>
                  <option selected disabled value="all">Choose Price Range</option>
                  <option value="< $50K" {{  $price_range == '< $50K' ? 'selected':'' }}>< $50K</option>
                  <option value="$50K-$99K" {{ $price_range == '$50K-$99K' ? 'selected':'' }}>$50K - $99K</option>
                  <option value="$100K-$199K" {{ $price_range == '$100K-$199K' ? 'selected':'' }}>$100K - $199K</option>
                  <option value="$200K-$299K" {{ $price_range == '$200K-$299K' ? 'selected':'' }}>$200K- $299K</option>
                  <option value="$300K-$399K" {{ $price_range == '$300K-$399K' ? 'selected':'' }}>$300K - $399K</option>
                  <option value="$400K-$499K" {{ $price_range == '$400K-$499K' ? 'selected':'' }}>$400K - $499K</option>
                  <option value="$500K-$599K" {{ $price_range == '$500K-$599K' ? 'selected':'' }}>$500K - $599K</option>
                  <option value="$600K-$699K" {{ $price_range == '$600K-$699K' ? 'selected':'' }}>$600K - $699K</option>
                  <option value="$700K-$799K" {{ $price_range == '$700K-$799K' ? 'selected':'' }}>$700K - $799K</option>
                  <option value="$800K-$899K" {{ $price_range == '$800K-$899K' ? 'selected':'' }}>$800K - $899K</option>
                  <option value="$900K-$1 million" {{ $price_range == '$900K-$1 million' ? 'selected':'' }}>$900K - $1M</option>
                  <option value="$1million-$2 million" {{ $price_range == '$1million-$2 million' ? 'selected':'' }}>$1M - $2M</option>
                  <option value="$2million-$3 million" {{ $price_range == '$2million-$3 million' ? 'selected':'' }}>$2M - $3M</option>
                  <option value="$3million-$4 million" {{ $price_range == '$3million-$4 million' ? 'selected':'' }}>$3M - $4M</option>
                  <option value="$4million-$5 million" {{ $price_range == '$4million-$5 million' ? 'selected':'' }}>$4M - $5M</option>
                  <option value="> 5 million" {{ $price_range == '> 5 million' ? 'selected':'' }}>5M+</option>
                </select>
                {!! $errors->has('price_range')? '<p class="help-block">'.$errors->first('price_range').'</p>':'' !!}
              </div>
            </div>
            <!-- price range -->

            <div class="form-group m-form__group row">
              <label class="col-lg-3 col-form-label">@lang('app.image')</label>
              <div class="col-lg-6">

                <div>
                  <div class="upload-images-input-wrap">
                    <input type="file" name="images[]" class="form-control" />
                    <input type="file" name="images[]" class="form-control" />
                  </div>

                  <div class="image-ad-more-wrap">
                    <a href="javascript:;" class="image-add-more"><i class="fa fa-plus-circle"></i> @lang('app.add_more')</a>
                  </div>
                </div>
                <p>{!! $errors->has('images')? '<p class="help-block">'.$errors->first('images').'</p>':'' !!}&nbsp;</p>

                <div>
                  <div id="uploaded-ads-image-wrap">
                    @if($ad->media_img->count() > 0)
                    @foreach($ad->media_img as $img)
                    <div class="creating-ads-img-wrap">
                      <img src="{{ media_url($img, false) }}" class="img-responsive" />
                      <div class="img-action-wrap" id="{{ $img->id }}">
                        <a href="javascript:;" class="imgDeleteBtn"><span class="m-nav__link-badge m-badge m-badge--danger">x</span></a>
                      </div>
                    </div>
                    @endforeach
                    @endif
                  </div>
                </div>

                <p>&nbsp;</p>

              </div>
            </div>

            <div class="form-group m-form__group row">
              <label class="col-lg-3 col-form-label">@lang('app.video_url')</label>
              <div class="col-lg-6">
                <input type="text" class="form-control" id="video_url" value="{{ old('video_url')? old('video_url') : $ad->video_url }}" name="video_url" placeholder="@lang('app.video_url')">
                {!! $errors->has('video_url')? '<p class="help-block">'.$errors->first('video_url').'</p>':'' !!}
                <p class="help-block">@lang('app.video_url_help')</p>
                <p class="text-info">@lang('app.video_url_help_for_modern_theme')</p>
              </div>
            </div>
            <br>
            <legend><span class="seller_text">Lead Info</legend>

              <div class="form-group m-form__group row">
                <label class="col-lg-3 col-form-label">Prospect Name<span class="required-field">*</span></label>
                <div class="col-lg-3">
                  <input required data-msg="Prospect Name is required" type="text" class="form-control" id="refFName" value="{{ old('referral_first_name')? old('referral_first_name') : isset($referral_first_name) ? $referral_first_name : ''}}" name="referral_first_name" placeholder="Prospect Name">
                  {!! $errors->has('referral_first_name')? '<p class="help-block">'.$errors->first('referral_first_name').'</p>':'' !!}
                </div>
                <div class="col-lg-3">
                  <input required id="refLName" data-msg="Last Name is required" type="text" class="form-control" id="referral_last_name" value="{{ old('referral_last_name')? old('referral_last_name') : isset($referral_last_name) ? $referral_last_name : ''}}" name="referral_last_name" placeholder="Last Name">
                  {!! $errors->has('referral_last_name')? '<p class="help-block">'.$errors->first('referral_last_name').'</p>':'' !!}
                </div>
              </div>

              <div class="form-group m-form__group row">
                <label class="col-lg-3 col-form-label">Email<span class="required-field">*</span></label>
                <div class="col-lg-6">
                  <input type="text" class="form-control" id="seller_email" value="{{ isset($referral->referral_contact_email) ? $referral->referral_contact_email : '' }}" name="referral_contact_email" placeholder="Email">
                  {!! $errors->has('referral_contact_email')? '<p class="help-block">'.$errors->first('referral_contact_email').'</p>':'' !!}
                </div>
              </div>

              <div class="form-group m-form__group row">
                <label class="col-lg-3 col-form-label">Phone<span class="required-field">*</span></label>
                <div class="col-lg-6">
                  <input required type="tel" class="form-control" id="seller_phone" value="{{ isset($referral->referral_contact_phone ) ? $referral->referral_contact_phone : '' }}" name="referral_contact_phone" placeholder="Phone" data-msg="Prospect Contact Number is required e.g. (444)444-4444">
                  {!! $errors->has('referral_contact_phone')? '<p class="help-block">'.$errors->first('referral_contact_phone').'</p>':'' !!}
                </div>
              </div>

              <div class="form-group m-form__group row">
                <label class="col-lg-3 col-form-label">Address</label>
                <div class="col-lg-6">
                  <input type="text" class="form-control" id="refAdress" value="{{ isset($referral->referral_contact_address) ? $referral->referral_contact_address : '' }}" name="referral_contact_address" placeholder="Address">
                  {!! $errors->has('referral_contact_address')? '<p class="help-block">'.$errors->first('referral_contact_address').'</p>':'' !!}
                  <p class="text-info">@lang('app.address_line_help_text')</p>
                </div>
              </div>

              <div class="form-group m-form__group row">
                <label class="col-lg-3 col-form-label">@lang('app.country')<span class="required-field">*</span> </label>
                <div class="col-lg-6">
                  <input type="text" class="form-control" name="country_name" value="{{ $ad->country_id == '231' ? 'United States' : 'Canada' }}">
                  {!! $errors->has('country')? '<p class="help-block">'.$errors->first('country').'</p>':'' !!}
                </div>
              </div>

              <div class="form-group m-form__group row">
                <label class="col-lg-3 col-form-label">@lang('app.state')<span class="required-field">*</span></label>
                <div class="col-lg-6">
                  @php $state_name = ''; @endphp
                  @if(count($previous_states) >=1)
                  @foreach($previous_states as $state)
                  @if($ad->state_id == $state->id)
                  @php $state_name = $state->state_name; @endphp
                  @endif
                  @endforeach
                  @endif
                  <input type="text" id="state" class="form-control" name="state_name" value="{{ $state_name }}">
                  <p class="text-info">
                    <span id="state_loader" style="display: none;"><i class="fa fa-spin fa-spinner"></i> </span>
                  </p>
                </div>
              </div>

              <div class="form-group m-form__group row">
                <label class="col-lg-3 col-form-label">@lang('app.city')<span class="required-field">*</span></label>
                <div class="col-lg-6">

                  @if($previous_cities->count() > 0)
                  @foreach($previous_cities as $city)
                  @if($ad->city_id == $city->id)
                  @php $city_name = $city->city_name; @endphp
                  @endif
                  @endforeach
                  @endif

                  <input type="text" class="form-control" name="city_name" value="{{ $city_name }}">

                  <p class="text-info">
                    <span id="city_loader" style="display: none;"><i class="fa fa-spin fa-spinner"></i> </span>
                  </p>
                </div>
              </div>

              <div class="form-group row {{ $errors->has('zipcode')? 'has-error':'' }}">
                <label for="city" class="col-lg-3 col-form-label">Zip Code</label>
                <div class="col-lg-6">
                  <input type="text" class="form-control"  name="zipcode" value="{{ $ad->zipcode }}">
                </div>
              </div>

              <input type="hidden" name="country" value="{{ $ad->country_id }}">
              <input type="hidden" id="state_id" name="state" value="{{ $ad->state_id }}">
              <input type="hidden" id="city_id" name="city" value="{{ $ad->city_id }}">
              <input type="hidden" name="agent_change" value="{{$agent_change}}">

              <div class="form-group m-form__group row">
                <label class="col-lg-3 col-form-label">Fax</label>
                <div class="col-lg-6">
                  <input type="text" class="form-control" id="seller_phone" value="{{ isset($referral->referral_contact_fax) ? $referral->referral_contact_fax : '' }}" name="referral_contact_fax" placeholder="Fax">
                  {!! $errors->has('referral_contact_fax')? '<p> class="help-block">'.$errors->first('referral_contact_fax').'</p>':'' !!}
                </div>
              </div>
              <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions">
                  <div class="row">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6">
                      <button id="post_submit" type="submit" class="btn btn-success">Save</button>

                      <a role="button" class="btn btn-danger" href="{{ route('my-leads') }}">Cancel</a>
                    </div>
                  </div>
                </div>
              </div>
              {{ Form::close() }}

            </div>

          </div>

          <!--end::Form-->
        </div>

        <!--end::Portlet-->
      </div>
    </div>
  </div>

  @endsection
