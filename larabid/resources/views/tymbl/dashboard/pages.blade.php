@extends('tymbl.layouts.dashboard')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">Pages</h3>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->
					<div class="m-content">
            						<!--begin::Portlet-->
						<div class="m-portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<h3 class="m-portlet__head-text">
												Pages
											</h3>
										</div>
									</div>
                  <a href="{{ route('create_new_page') }}" class="btn-create-page btn btn-info pull-right"> <i class="fa fa-floppy-o"></i> @lang('app.create_new_page')</a>
								</div>
								<div class="m-portlet__body">
									<!--begin::Section-->
									<div class="m-section">
										<div class="m-section__content">
                      <table class="table table-bordered table-striped" id="jDataTable">
                          <thead>
                          <tr>
                              <th>@lang('app.title')</th>
                              <th>@lang('app.created_at')</th>
                              <th>@lang('app.actions')</th>
                          </tr>
                          </thead>
                          <tbody>
                            @foreach ($pages as $page)
                            <tr><td>{{$page->title}}</td><td>{{$page->created_at}}</td><td>

                              <a href="{{ route('edit_page', $page->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i> </a>
                              <a href="javascript:;" class="btn btn-danger deleteAds" data-slug="{{ $page->slug }}"><i class="fa fa-trash"></i> </a>

                            </td></tr>
                            @endforeach
                          </tbody>
                      </table>
										</div>
									</div>

									<!--end::Section-->
								</div>

								<!--end::Form-->
							</div>

							<!--end::Portlet-->
					</div>
				</div>
@endsection
