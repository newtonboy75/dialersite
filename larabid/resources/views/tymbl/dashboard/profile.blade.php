@extends('tymbl.layouts.dashboard')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

  <!-- BEGIN: Subheader -->
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title ">My Profile</h3>
      </div>
    </div>
  </div>

  <!-- END: Subheader -->
  <div class="m-content">
    <div class="row">
      <div class="col-xl-3 col-lg-4">
        <div class="m-portlet m-portlet--full-height   m-portlet--unair">
          <div class="m-portlet__body">
            <div class="m-card-profile">
              <div class="m-card-profile__title m--hide">
                Your Profile
              </div>
              <div class="m-card-profile__pic">
                <div class="m-card-profile__pic-wrapper">

                  <img style="width:120px;" src="{{ auth()->user()->get_gravatar() !== null ? auth()->user()->get_gravatar() : '/uploads/avatar/gravatar-default.png' }}" />

                </div>
              </div>
              <div class="m-card-profile__details">
                <span class="m-card-profile__name">{{ safe_output($user->name) }}</span>
                <a href="" class="m-card-profile__email m-link">{{ safe_output($user->email) }}</a>
              </div>
            </div>
            <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
              <li class="m-nav__separator m-nav__separator--fit"></li>
              <li class="m-nav__section m--hide">
                <span class="m-nav__section-text">Section</span>
              </li>
              <li class="m-nav__item">
                <a href="{{ route('messages') }}" class="m-nav__link">
                  <i class="m-nav__link-icon flaticon-chat-1"></i>
                  <span class="m-nav__link-text">Messages</span>
                </a>
              </li>

              <li>
              </li>

              <li class="m-nav__item">
                <a href="{{ route('change_password') }}" class="m-nav__link">
                  <i class="m-nav__link-icon flaticon-lock"></i>
                  <span class="m-nav__link-text">Change Password</span>
                </a>
              </li>

              <li class="m-nav__item text-center">
                <p>&nbsp;</p><p>&nbsp;</p>
                @if($user->id == auth()->user()->id)
               <a class="btn btn-accent m-btn m-btn--air m-btn--custom text-center" href="{{ route('profile_edit') }}"><i class="fa fa-pencil-square-o"></i> @lang('app.edit') </a>
               @endif
               <p>&nbsp;</p>
              </li>

            </ul>
            <div class="m-portlet__body-separator"></div>
            @if(Auth::user()->isAdmin)
            <div class="m-widget1 m-widget1--paddingless">
              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col">
                    <h3 class="m-widget1__title">Member Profit</h3>
                    <span class="m-widget1__desc">Awerage Weekly Profit</span>
                  </div>
                  <div class="col m--align-right">
                    <span class="m-widget1__number m--font-brand">+$17,800</span>
                  </div>
                </div>
              </div>
              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col">
                    <h3 class="m-widget1__title">Orders</h3>
                    <span class="m-widget1__desc">Weekly Customer Orders</span>
                  </div>
                  <div class="col m--align-right">
                    <span class="m-widget1__number m--font-danger">+1,800</span>
                  </div>
                </div>
              </div>
              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col">
                    <h3 class="m-widget1__title">Issue Reports</h3>
                    <span class="m-widget1__desc">System bugs and issues</span>
                  </div>
                  <div class="col m--align-right">
                    <span class="m-widget1__number m--font-success">-27,49%</span>
                  </div>
                </div>
              </div>
            </div>
            @endif
          </div>
        </div>
      </div>
      <div class="col-xl-9 col-lg-8">
        <div class="m-portlet m-portlet--full-height m-portlet--tabs   m-portlet--unair">
          <div class="m-portlet__head">
            <div class="m-portlet__head-tools">
              <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                <li class="nav-item m-tabs__item" id="myprofile">
                  <a class="nav-link m-tabs__link" id="t1" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                    <i class="flaticon-share m--hide"></i>
                    My Profile
                  </a>
                </li>
              </ul>
            </div>

          </div>
          <div class="tab-content">
            <div class="tab-pane" id="m_user_profile_tab_1">
              <p>&nbsp;</p>
              <div class="form-group m-form__group row">
                <div class="col-10 ml-auto">
                  <h3 class="m-form__section">1. Personal Details</h3>
                </div>
              </div>

              <table class="table table-responsive tbl-profile">
                <tbody>
                  <tr>
                    <td class="col1">Full Name</td><td scope="col" class="col2">{{ safe_output($user->name) }}</td>
                  </tr>

                  <tr>
                    <td class="col1">Email</td><td scope="col" class="col2">{{ safe_output($user->email) }}</td>
                  </tr>

                  <tr>
                    <td class="col1">Mobile No.</td><td scope="col" class="col2">{{ safe_output($user->phone) }}
                    </td>
                  </tr>

                  <tr>
                    <td class="col1">Address</td><td scope="col" class="col2">{{ safe_output($user->address) }}</td>
                  </tr>

                  <tr>
                    <td class="col1">Zip Code</td><td scope="col" class="col2">{{ $user->zip_code == '0' ? '' :  $user->zip_code }}</td>
                  </tr>
                  <tr>
                    <td class="col1">Country</td><td scope="col" class="col2">
                      @if($user->country)
                      {{ $user->country->country_name }}
                      @endif</td>
                    </tr>

                    <tr>
                      <td class="col1">State</td><td scope="col" class="col2">
                        @if($user_state)
                        {{ $user_state->state_name }}
                        @endif</td>
                      </tr>

                      <tr>
                        <td class="col1">City</td><td scope="col" class="col2">
                          @if($user_city)
                          {{ $user_city->city_name }}
                          @endif</td>
                        </tr>
                        <tr><td class="col1">MLS</td><td scope="col" class="col2">{{ isset($mls) ? $mls->name : '' }}</td></tr>
                        <tr><td colspan="2"><hr></td></tr>
                        <tr>
                          <td class="col1">Brokerage Firm</td><td scope="col" class="col2">{{ isset($broker) &&  $broker != '' ? safe_output($broker->name) :  '' }}</td>
                        </tr>
                        <tr>
                          <td class="col1">Brokerage Contact Person</td><td scope="col" class="col2">{{ isset($broker) &&  $broker!= '' ? safe_output($broker->broker_contact_person) :  '' }}</td>
                        </tr>
                        <tr>
                          <td class="col1">Brokerage Email</td><td scope="col" class="col2">{{ isset($broker) &&  $broker!= '' ? safe_output($broker->broker_email) :  '' }}</td>
                        </tr>
                        <tr>
                          <td class="col1">Brokerage Phone</td><td scope="col" class="col2">{{ isset($broker) &&  $broker!= '' ? safe_output($broker->broker_phone) :  '' }}</td>
                        </tr>
                        <tr><td colspan="2"><hr></td></tr>

                        <tr><td class="col1">Notification</td>
                          <td class="col2">
                            <span style="font-weight: 400">Notify me when there are new leads in my area</span>
                            <br><br>
                            @php
                            $areacodes = '';
                            $notified_by = '';
                            $notification = '';
                            @endphp

                            @foreach($notifications as $notification)
                            @if($notification)
                              @if($notification->loc_range == '0')
                                @php
                                $areacodes .= $notification->zip_id.', ';
                                $notified_by = '1';
                                @endphp
                              @else
                                @php
                                $areacodes = $notification->loc_range;
                                $notified_by = '2';
                                @endphp
                              @endif
                            @endif

                            @endforeach

                            <div >
                              @if($notified_by == '1')

                                <div style="margin-bottom: 10px; font-weight: 500;">By zip codes: @php echo trim($areacodes, ', '); @endphp</div>
                              @elseif($notified_by == '2')

                              <div style="margin-bottom: 10px; font-weight: 500;">By location range: {{$notification->loc_range}} MILES</div>
                              &nbsp;
                              @else
                                Notification not yet set
                              @endif

                            </div>

                          </td>
                        </tr>
                      </tbody>
                    </table>

                    <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endsection
