@extends('tymbl.layouts.dashboard')
@section('content')
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
  <div class="d-flex align-items-center">
    <div class="mr-auto">
      <h3 class="m-subheader__title ">Dashboard</h3>
    </div>
  </div>
</div>

<!-- END: Subheader -->
<div class="m-content">

  <!--begin:: Widgets/Stats-->
  <div class="m-portlet  m-portlet--unair">
    <div class="m-portlet__body  m-portlet__body--no-padding">
      <div class="row m-row--no-padding m-row--col-separator-xl">
        <div class="col-md-12 col-lg-6 col-xl-3">

          <!--begin::Approved Auction-->
          <div class="m-widget24">
            <div class="m-widget24__item">
              <h4 class="m-widget24__title">
                Active Leads
              </h4><br>
              <span class="m-widget24__stats m--font-brand">
                {{$approved_ads}}
              </span>
              <span class="m-widget24__desc">
                <a href="{{ route('my-leads') }}">View all</a>
              </span>
              <div class="m--space-10"></div>
            </div>
          </div>
          <!--end::Approved Auction-->
        </div>

        <div class="col-md-12 col-lg-6 col-xl-3">

          <!--begin::Pending Auction-->
          <div class="m-widget24">
            <div class="m-widget24__item">
              <h4 class="m-widget24__title">
                Reserved Leads
              </h4><br>
              <span class="m-widget24__desc">
                <a href="{{ route('reserved-leads') }}">View all</a>
              </span>
              <span class="m-widget24__stats m--font-info">
                {{count($num_reserved_leads)}}
              </span>
              <div class="m--space-10"></div>
            </div>
          </div>
          <!--end::Pending Auction-->
        </div>

        <div class="col-md-12 col-lg-6 col-xl-3">

          <!--begin::Blocked Auction-->
          <div class="m-widget24">
            <div class="m-widget24__item">
              <h4 class="m-widget24__title">
                  Listed Leads
              </h4><br>
              <span class="m-widget24__desc">
                <a href="{{ route('lead-status') }}">View all</a>
              </span>
              <span class="m-widget24__stats m--font-danger">
                {{count($num_listed_leads)}}
              </span>
              <div class="m--space-10"></div>
            </div>
          </div>

          <!--end::Blocked Auction-->
        </div>

        <div class="col-md-12 col-lg-6 col-xl-3">


        </div>
      </div>
    </div>
  </div>

  <!--end:: Widgets/Stats-->


  <!--begin::Portlet-->
  <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
                Unread comments
            </h3>
          </div>
        </div>
      </div>
      <div class="m-portlet__body">

        <!--begin::Section-->
        <div class="m-section">
          <div class="m-section__content">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>Sender Name</th>
                  <th>Sender Email</th>
                  <th>Message</th>
                </tr>
              </thead>
              <tbody>


                @foreach($comments as $comment)
                <tr>
                  <th scope="row">{{$comment->author_name}}</th>
                  <td>{{$comment->author_email}}</td>
                  <td>{{$comment->comment}}</td>
                </tr>
                @endforeach

              </tbody>
            </table>
            &nbsp;&nbsp;<a href="{{ route('comments') }}">Read all</a>
          </div>
        </div>

        <!--end::Section-->
      </div>

      <!--end::Form-->
    </div>

    <!--end::Portlet-->
  <!--begin::Portlet-->
  <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
                Messages
            </h3>
          </div>
        </div>
      </div>
      <div class="m-portlet__body">

        <!--begin::Section-->
        <div class="m-section">
          <div class="m-section__content">
            <table class="table table-bordered table-hover">
              @foreach($notifications_users as $notif)
                <tr>
                  <!-- /dashboard/message/'.$messages->id  -->
                  @if($notif->user_read == '0')
                  <td class="messages-unread"><i class="far fa-envelope"></i>&nbsp;&nbsp;<a href="/dashboard/message/{{$notif->id}}">{{$notif->subject}}</a></td>
                  @else
                  <td><i class="far fa-envelope-open"></i>&nbsp;&nbsp;<a href="/dashboard/message/{{$notif->id}}">{{$notif->subject}}</a></td>
                  @endif
                </tr>
              @endforeach
          </table>
          </div>
        </div>

        <!--end::Section-->
      </div>

      <!--end::Form-->
    </div>

    <!--end::Portlet-->
</div>
@endsection
