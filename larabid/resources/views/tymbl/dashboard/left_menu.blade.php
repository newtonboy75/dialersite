<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

  <!-- BEGIN: Aside Menu -->
  <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500">
    <ul class="m-menu__nav ">
      <li class="m-menu__item  {{ request()->segment(2) == '' ? 'm-menu__item--active' : '' }}" aria-haspopup="true"><a href="{{ route('dashboard') }}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">Dashboard</span></a></li>

      @if(Auth::user()->user_type == 'user')
      @php
      $lead_url = ['my-leads', 'favorite-lists', 'uploaded_leads', 'uploaded_leads_status', 'reserved-leads', 'lead-status', 'payments'];

      $all_leads = \App\Ad::where('user_id', '=', Auth::user()->id)->get();

      $favorites = \DB::table('favorites')->join('ads', 'ads.id', '=', 'favorites.ad_id')->where('ads.status', '=', '1')->where('favorites.user_id', '=', Auth::user()->id)->get();

      $reserved_leads = \App\ListingContracts::where('buyer_id', '=', Auth::user()->id)->where('list_status', '=', '1')->get();

      $listed_leads = \DB::table('ads')->join('listing_contracts', 'ads.id', '=', 'listing_contracts.listing_id')->join('media', 'media.ad_id', '=', 'ads.id')->select('ads.*', 'listing_contracts.*', 'media.*')->where('ads.user_id', Auth::user()->id)->where('ads.status', '!=', '0')->get();

      $uploaded_leads = \App\PlanContract::where('user_id', '=', Auth::user()->id)->get();
      @endphp
      <li class="m-menu__item {{ in_array(request()->segment(2), $lead_url) || in_array(request()->segment(4), $lead_url) ? 'm-menu__item--active  m-menu__item--open' : ''}}  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-layers"></i><span
        class="m-menu__link-text">Manage Leads</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
          <ul class="m-menu__subnav">

            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('create_ad') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet m-menu__link-bullet--square" style="width:13px !important;"><span></span></i><span class="m-menu__link-text">Place New Lead</span></a></li>

            @if(count($all_leads) >= 1)
              <li class="m-menu__item {{ request()->segment(4) == 'my-leads' ? 'active-hover' : '' }}" aria-haspopup="true"><a href="{{ route('my-leads') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">My Leads</span></a></li>
            @elseif(count($all_leads) <= 0 && count($reserved_leads) >= 1)
              <li class="m-menu__item {{ request()->segment(2) == 'reserved-leads' ? 'active-hover' : '' }}" aria-haspopup="true" ><a href="{{ route('reserved-leads') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Leads I Reserved</span></a></li>
            @endif




            @if(count($favorites) >= 1)
              <li class="m-menu__item {{ request()->segment(4) == 'favorite-lists' ? 'active-hover' : '' }}" aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('favorite_ads') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Favorite Leads</span></a></li>
            @endif

            @if(count($uploaded_leads) >= 1)
              <li class="m-menu__item {{ Route::currentRouteName() == 'uploaded_leads' ? 'active-hover' : '' }}" aria-haspopup="true">
                <a href="{{ route('uploaded_leads') }}" class="m-menu__link ">
                  <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                  <span class="m-menu__link-text">Uploaded Leads</span></a>
              </li>
            @endif

            @if(count($reserved_leads) >= 1 && count($all_leads) >= 1)
              <li class="m-menu__item {{ request()->segment(2) == 'reserved-leads' ? 'active-hover' : '' }}" aria-haspopup="true" ><a href="{{ route('reserved-leads') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Leads I Reserved</span></a></li>
            @endif

            @if(count($listed_leads) >= 1)
              <li class="m-menu__item {{ request()->segment(2) == 'lead-status' ? 'active-hover' : '' }}" aria-haspopup="true"><a href="{{ route('lead-status') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Leads I Listed</span></a></li>
            @endif

            <li style="display: none !important;" class="m-menu__item {{ request()->segment(2) == 'payments' ? 'active-hover' : '' }}" aria-haspopup="true"><a href="{{ route('payments') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Payments</span></a></li>



            </ul>
          </div>
        </li>
        @endif

        @if(Auth::user()->is_admin())
        @php
        $lead_url = ['approved', 'blocked', 'import-leads', 'featured_ads', 'paused', 'successful_leads', 'vip-leads', 'uploaded_leads', 'uploaded_leads_status'];
        @endphp
        <li class="m-menu__item  m-menu__item--submenu {{ in_array(request()->segment(2), $lead_url) ? 'm-menu__item--active  m-menu__item--open' : ''}}" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-settings"></i><span
          class="m-menu__link-text">Leads</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
          <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
            <ul class="m-menu__subnav">

              <li class="m-menu__item {{ request()->segment(2) == 'approved' ? 'active-hover' : '' }}" aria-haspopup="true"><a href="{{ route('approved_ads') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Active Leads</span></a></li>

              <li class="m-menu__item {{ request()->segment(2) == 'featured_ads' ? 'active-hover' : '' }}" aria-haspopup="true"><a href="{{ route('featured_ads') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Featured Ads</span></a></li>

              <li class="m-menu__item {{ request()->segment(2) == 'paused' ? 'active-hover' : '' }}" aria-haspopup="true"><a href="{{ route('paused') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Paused Leads</span></a></li>

              <li class="m-menu__item {{ request()->segment(2) == 'blocked' ? 'active-hover' : '' }}" aria-haspopup="true"><a href="{{ route('admin_blocked_ads') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Blocked Leads</span></a></li>

              <li class="m-menu__item {{ request()->segment(2) == 'vip-leads' ? 'active-hover' : '' }}" aria-haspopup="true"><a href="{{ route('vip-leads') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">VIP Leads</span></a></li>

              <li style="border-top: 1px solid #333; border-bottom: 1px solid #333;" class="m-menu__item {{ request()->segment(2) == 'successful_leads' ? 'active-hover' : '' }}" aria-haspopup="true"><a href="{{ route('successful_leads') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Reserved Leads</span></a></li>

              <li class="m-menu__item {{ request()->segment(2) == 'uploaded_leads' ? 'active-hover' : '' }}" aria-haspopup="true"><a href="{{ route('uploaded_leads') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Uploaded Leads</span></a></li>

            </ul>
          </div>
        </li>



      </li>

      <li class="m-menu__item {{ request()->segment(2) == 'categories' ? 'm-menu__item--active  m-menu__item--open' : '' }}" aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('parent_categories') }}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-signs-1"></i><span class="m-menu__link-text">Categories</span></a></li>
      @endif

      @if(Auth::user()->user_type == 'admin')
      <li class="m-menu__item {{ request()->segment(2) == 'pages' ? 'm-menu__item--active  m-menu__item--open' : '' }}" aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('pages') }}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-web"></i><span class="m-menu__link-text">Pages</span></a></li>
      @endif


      <li class="m-menu__item {{ request()->segment(2) == 'comments' ? 'm-menu__item--active  m-menu__item--open' : '' }}" aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('comments') }}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-comment"></i><span class="m-menu__link-text">Comments</span></a></li>


      <li class="m-menu__item {{ request()->segment(2) == 'messages' ? 'm-menu__item--active  m-menu__item--open' : '' }}" aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('messages') }}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-comment"></i><span class="m-menu__link-text">Messages</span></a></li>


      @if(Auth::user()->user_type == 'admin')
      <li class="m-menu__item  m-menu__item--submenu {{ request()->segment(2) == 'users' ? 'm-menu__item--active  m-menu__item--open' : '' || request()->segment(2) == 'user-add' ? 'm-menu__item--active  m-menu__item--open' : '' || request()->segment(2) == 'users-info' ? 'm-menu__item--active  m-menu__item--open' : ''}}" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-settings"></i><span
        class="m-menu__link-text">Users</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
          <ul class="m-menu__subnav">

            <li class="m-menu__item {{ request()->segment(2) == 'users' ? 'active-hover' : '' }}" aria-haspopup="true"><a href="{{ route('users') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">All  Users</span></a></li>

            <li class="m-menu__item {{ request()->segment(2) == 'user-add' ? 'active-hover' : '' }}" aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('user-add') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Add User</span></a></li>

          </ul>
        </div>
      </li>
      @endif

      @if(Auth::user()->user_type == 'admin')
      <li class="m-menu__item {{ request()->segment(2) == 'contact-messages' ? 'm-menu__item--active  m-menu__item--open' : '' }}" aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('contact_messages') }}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-email"></i><span class="m-menu__link-text">Contact Messages</span></a></li>
      @endif

      @if(Auth::user()->user_type == 'admin')
      <li style="display:none !important;" class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="monetization.html" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-coins"></i><span class="m-menu__link-text">Monetization</span></a></li>
      @endif

      @if(Auth::user()->user_type == 'admin')
      <li class="m-menu__item  m-menu__item--submenu {{ request()->segment(2) == 'settings' ? 'm-menu__item--active  m-menu__item--open' : '' }}" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-settings"></i><span
        class="m-menu__link-text">Settings</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
          <ul class="m-menu__subnav">

            <li class="m-menu__item {{ request()->segment(3) == 'general' ? 'active-hover' : '' }}" aria-haspopup="true"><a href="{{ route('general_settings') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">General Settings</span></a></li>

            @if(Auth::user()->user_type == 'admin_test')
            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="auction-settings.html" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Auction settings & pricing</span></a></li>

            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="payment-settings.html" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Payment Settings</span></a></li>

            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="social-settings.html" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Social Settings</span></a></li>

            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="recaptcha-settings.html" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">reCAPTCHA Settings</span></a></li>

            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="other-settings.html" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Other Settings</span></a></li>
            @endif
          </ul>
        </div>
      </li>
      @endif

      @if(Auth::user()->user_type == 'admin')
      <li class="m-menu__item {{ request()->segment(2) == 'administrators' ? 'm-menu__item--active  m-menu__item--open' : '' }}" aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('administrators') }}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-user-settings"></i><span class="m-menu__link-text">Administrators</span></a></li>
      @endif

      @if(Auth::user()->user_type == 'admin')
      <li class="m-menu__item  m-menu__item--submenu {{ request()->segment(2) == 'lead-status' || request()->segment(2) == 'reserved-leads' || request()->segment(2) == 'payments'  ? 'm-menu__item--active  m-menu__item--open' : '' }}" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-notepad"></i><span
        class="m-menu__link-text">Transactions</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
          <ul class="m-menu__subnav">


            <li class="m-menu__item {{ request()->segment(2) == 'reserved-leads' ? 'active-hover' : '' }}" aria-haspopup="true"><a href="{{ route('payments') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Payments</span></a></li>

          </ul>
        </div>
      </li>
      @endif

      @if(Auth::user()->user_type == 'user')
      <li class="m-menu__item {{ request()->segment(4) == 'profile' ? 'm-menu__item--active  m-menu__item--open' : '' }}" aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('profile') }}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-profile-1"></i><span class="m-menu__link-text">Profile</span></a></li>
      @endif

      @if(Auth::user()->user_type == 'user_0')
      <li class="m-menu__item {{ request()->segment(4) == 'notification' ? 'm-menu__item--active  m-menu__item--open' : '' }}" aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('notification') }}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-alert"></i><span class="m-menu__link-text">Notification Settings</span></a></li>
      @endif

    </ul>
  </div>

  <!-- END: Aside Menu -->
</div>
