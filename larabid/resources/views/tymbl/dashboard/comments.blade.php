@extends('tymbl.layouts.dashboard')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

  <!-- BEGIN: Subheader -->
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title ">Comments</h3>
      </div>
    </div>
  </div>

  <!-- END: Subheader -->
  <div class="m-content">

    @include('admin.flash_msg')
    <!--begin::Portlet-->
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
              All Comments
            </h3>
          </div>
        </div>
      </div>
      <div class="m-portlet__body">

        <!--begin::Section-->
        <div class="m-section">
          <div class="m-section__content">
            <table class="table table-bordered table-hover table-responsive">
              <tbody>
              </tbody><thead>
              </thead><colgroup>
                <col width="5%">
                <col width="20%">
                <col width="25%">
                <col width="30%">
                <col width="5%">
                <col width="15%">
                <col width="20%">
              </colgroup><tbody><tr>
                <th class="text-center" scope="col">Lead ID</th>
                <th class="text-center" scope="col">Author Name</th>
                <th class="text-center" scope="col">Email</th>
                <th class="text-center" scope="col">Comment</th>
                <th class="text-center" scope="col">Status</th>
                <th class="text-center" scope="col">Date Posted</th>
                <th class="text-center" scope="col">Actions</th>
              </tr>

              @foreach($ads as $ad)
              <tr id="comment-post-id-{{$ad->id}}">
                <td class="text-center"><a href="/listing/{{$ad->ad_id}}/{{$ad->slug}}">{{$ad->ad_id}}</td>
                  <td>{{$ad->author_name}}</td>
                  <td>{{$ad->author_email}}</td>
                  <td>{{$ad->comment}}</td>
                  <td class="text-center">@if($ad->approved == '1')
                    <i data-container="body" data-toggle="m-tooltip" data-original-title="Approved" class="fas fa-circle text-success"></i>
                    @elseif($ad->approved == '0')
                    <i data-container="body" data-toggle="m-tooltip" data-original-title="Pending Approval" class="fas fa-circle text-secondary"></i>
                    @else
                    <i data-container="body" data-toggle="m-tooltip" data-original-title="Blocked" class="fas fa-circle text-danger"></i>
                    @endif</td>
                    <td>{{$ad->created_at}}</td>
                    <td class="text-center">
                      @if(!$user->is_admin())
                      <a href="/listing/{{$ad->ad_id}}/{{$ad->slug}}#comment" rel="{{$ad->id}}" class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-container="body" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Reply">
                        <i class="fa flaticon-reply"></i>
                      </a>
                      @else
                      @if($ad->approved == '0' || $ad->approved == '2')
                      <span rel="{{$ad->id}}" id="approve-comment" class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-container="body" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Approve">
                        <i class="fas fa-check"></i>
                      </span>
                      @else
                      <span rel="{{$ad->id}}" id="approve-revoke" class="btn btn-warning m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-container="body" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Block" style="color: #fff">
                        <i class="fas fa-ban"></i>
                      </span>
                      @endif
                      @endif
                      <span style="margin-top: 10px;" rel="{{$ad->id}}" id="delete-comment" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-container="body" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Delete">
                        <i class="fa flaticon-delete"></i>
                      </span>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>

              {{ $ads->links() }}
            </div>
          </div>

          <!--end::Section-->
        </div>

        <!--end::Form-->
      </div>

      <!--end::Portlet-->
    </div>
  </div>

  @endsection
