@extends('tymbl.layouts.dashboard')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">Page Edit</h3>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->
					<div class="m-content">
            						<!--begin::Portlet-->
						<div class="m-portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<h3 class="m-portlet__head-text">
												Page Edit
											</h3>

										</div>
									</div>
                  <a href="/page/{{$slug}}" target="_blank" class="btn btn-primary btn-create-page pull-right"><i class="far fa-eye"></i> View Page</a>
								</div>
								<div class="m-portlet__body">

									<!--begin::Section-->
									<div class="m-section">
										<div class="m-section__content">
                      <div class="col-xs-12">

                        {{ Form::open(['class' => 'form-horizontal']) }}

                        <div class="form-group {{ $errors->has('title')? 'has-error':'' }}">
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="title" value="{{ old('title')?old('title'): $page->title }}" name="title" placeholder="@lang('app.title')">
                                {!! $errors->has('title')? '<p class="help-block">'.$errors->first('title').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('post_content')? 'has-error':'' }}">
                            <div class="col-sm-12">
                                <textarea name="post_content" id="post_content" class="form-control">{!!  old('post_content')? old('post_content'): $page->post_content !!}</textarea>
                                {!! $errors->has('post_content')? '<p class="help-block">'.$errors->first('post_content').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="show_in_header_menu" class="checkbox-inline">
                                    <input type="checkbox" value="1" id="show_in_header_menu" name="show_in_header_menu" {{ $page->show_in_header_menu? 'checked':'' }}>
                                    @lang('app.show_in_header_menu')
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label for="show_in_footer_menu" class="checkbox-inline">
                                    <input type="checkbox" value="1" id="show_in_footer_menu" name="show_in_footer_menu"  {{ $page->show_in_footer_menu? 'checked':'' }}>
                                    @lang('app.show_in_footer_menu')
                                </label>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary">@lang('app.update_page')</button>
                                &nbsp;&nbsp;
                                <a href="{{ route('pages') }}" style="" title="Back to Pages" class="btn btn-danger">Cancel</a>
                            </div>


                        </div>
                        {{ Form::close() }}

                      </div>
										</div>
									</div>

									<!--end::Section-->
								</div>

								<!--end::Form-->
							</div>

							<!--end::Portlet-->
					</div>
				</div>
@endsection
