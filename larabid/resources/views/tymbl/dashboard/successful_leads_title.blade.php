<div class="jd-success-data">
    <div class="ref-info">
    <div class="drc">
        <h5>
            <a href="/listing/{{ $ads->id }}/{{ $ads->slug }}" target="_blank">{{ safe_output($ads->title) }}</a>
        </h5>
    </div>
    <div class="drc">
        <span class="dbc-reserved">ID #</span>
        {{ safe_output($ads->id) }}
    </div>
        <div class="drc">
            <span class="dbc-reserved">STATUS</span>
            @if(isset($ads['listingContracts']['prospect_contact_status']['current_prospect_contact_status']))
                <span class="text-success font-weight-bold text-uppercase">
                    {{ \App\ProspectContactStatus::CS_DESCRIPTION[$ads['listingContracts']['prospect_contact_status']['current_prospect_contact_status']] }}
                </span>
                <span class="text-info font-weight-bold text-uppercase">
                    {{ date("M d y", strtotime($ads['listingContracts']['prospect_contact_status']['updated_at'])) }}
                </span>
                <i class="fa fa-info-circle status-detail">
                    <span>&bull; {!! implode('<br>&bull; ',array_column($ads['listingContracts']['prospect_contact_status']['prospectContactStatusOptionsSaved']->toArray(), 'option_value')) !!}</span>
                </i>
            @else
                @if($ads['listingContracts']['list_status'] == '0')
                    Agreement Signed
                @elseif($ads['listingContracts']['list_status'] == '1')
                    <span class="text-success font-weight-bold">APPROVED</span>
                @else
                   Not Successful
                @endif
            @endif
        </div>
    <div class="drc">
        <span class="dbc-reserved">Posted By</span>{{ safe_output($ads->seller_name) }}
    </div>
    <div class="drc">
        <span class="dbc-reserved">Payment Method</span>{{ $ads['payment']['payment_method'] ?? '' }}
    </div>
    <div class="drc">
        <span class="dbc-reserved">Payment ID</span> #{{ $ads['payment']['local_transaction_id'] ?? '' }}
    </div>
    <div class="drc">
        <span class="dbc-reserved">Contract ID</span>{{ $ads['listingContracts']['user_contract_id'] ?? '' }}
    </div>
    <div class="drc">
        <span class="dbc-reserved">Date Signed</span>{{ $ads['listingContracts']['updated_at'] ?? '' }}
    </div>
    <div class="drc">
        <span class="dbc-reserved">Reserved To</span>{{ $ads['listingContracts']['buyer']['name'] ?? '' }}
    </div>
    </div>
    <hr style="background-color: black">
    <div class="ref-info">
        <div class="drc">
            <h5 class="dbc-reserved text-info">Referral Info</h5>
        </div>
        <div class="drc">
            <span class="dbc-reserved">Name</span>
            {{ $ads['referral_info']['referral_name'] ?? '' }}
        </div>
        <div class="drc">
            <span class="dbc-reserved">Address</span>
            {{ $ads['referral_info']['referral_contact_address'] ?? '' }}
        </div>
        <div class="drc">
            <span class="dbc-reserved">Email</span>
            {{ $ads['referral_info']['referral_contact_email'] ?? '' }}
        </div>
        <div class="drc">
            <span class="dbc-reserved">Phone</span>
            {{ $ads['referral_info']['referral_contact_phone'] ?? '' }}
        </div>
    </div>
    <hr>
    <div class="notes">
        <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
            <div class="m-accordion__item" style="padding: 1rem;">
                <div class="m-accordion__item-head collapsed" role="tab" data-toggle="collapse" href="#note-accordion-{{ $ads->id }}" aria-expanded="false">
                    <span class="m-accordion__item-icon">
                        <i class="fa flaticon-speech-bubble-1"></i>
                    </span>
                    <span class="m-accordion__item-title">Notes</span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse" id="note-accordion-{{ $ads->id }}" role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                    <div class="m-accordion__item-content">';
                        <form method="POST" action="/lead-note-add" accept-charset="UTF-8" class="m-form">
                            <input type="hidden" name="ad_id" value="{{ $ads->id }}">
                            <div class="form-group">
                                <div class="form-group">
                                    <textarea class="form-control" rows="2" name="lead_note_body" cols="50" style="width: 100%;"></textarea>
                                </div>
                            </div>
                            <input class="btn btn-primary btn-sm float-right" type="submit" value="Add Note">
                        </form>
                        <div class="clearfix"></div>
                        <div class="all-notes" style="padding-top: 2rem;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
