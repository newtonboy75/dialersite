@extends('tymbl.layouts.dashboard')

@section('css')
    <style>
        i.status-detail:hover {
            cursor: help;
            position: relative
        }
        i.status-detail span {
            display: none
        }
        i.status-detail:hover span {
            box-shadow: 0 0 10px;
            border-radius: 5px;
            border: #c0c0c0 1px dotted;
            padding: 10px;
            display: block;
            z-index: 100;
            background: #ffffff;
            margin: 10px;
            width: 250px;
            position: absolute;
            top: 10px;
            text-decoration: none
        }

        .morecontent span {
            display: none;
        }
        .morelink {
            display: block;
        }
        .circle-text {
            display: table-cell;
            height: 80px;
            width: 80px;
            text-align: center;
            vertical-align: middle;
            border-radius: 50%;
            background-color: #007bff;
            border-color: #007bff;
            color: #fff;
            font-size: 10px;
            font-weight: 600;
        }
        .circle-text:hover {
            color: #fff;
            background-color: #0069d9;
            border-color: #0062cc;
        }
        ul.state_container {
            padding: 0;
        }

        ul.state_container > li {
            display: inline-block;
            margin: 10px 10px 0 0;
        }
        a.circle-text, a.circle-text:hover{
            text-decoration: none;
            color: #ffffff;
        }
    </style>
@endsection


@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    @if(Route::currentRouteName() == 'reserved-lead')
                        <h3 class="m-subheader__title" style="text-transform: uppercase;"><a href="{{ route('reserved-leads') }}" style="text-decoration: none">All reserved leads</a> / {{ $header }}</h3>
                    @else
                        <h3 class="m-subheader__title" style="text-transform: uppercase;">{{ $header }}</h3>
                    @endif
                </div>
            </div>
        </div>

        <!-- END: Subheader -->
        <div class="m-content">
            <!--begin::Portlet-->
            <div class="m-portlets">
                <div class="m-portlet__body" >
                    <!--begin::Section-->
                    <div class="m-section">
                        <div class="m-section__content">
                            @if(Route::currentRouteName() == 'reserved-lead')
                                @include('tymbl.dashboard.reserved_lead_content', $list)
                            @endif
                            @if(Route::currentRouteName() == 'reserved-leads')
                                @foreach($lists as $list)
                                    @include('tymbl.dashboard.reserved_lead_content', $list)
                                @endforeach
                            @endif
                        </div>
                    </div>
                    @if(Route::currentRouteName() == 'reserved-leads')
                        {{ $lists->links() }}
                    @endif
                <!--end::Section-->
                </div>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <div id="myModalBox" class="modal fade"></div>
@endsection

@section('js')
    <script>
        $(".prospect-contact-status").on('click', function(e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $.ajax({
                url: url,
                type: 'get',
                success: function(data) {
                    $("#myModalBox").html(data);
                    $('#myModalBox').modal('show');
                },
                error: function (data) {
                    console.log('Errors!');
                }
            });
        });

        $(document).on('click', '#save-contact-status', function () {
            var formData = $('#contact-status-form');
            $.ajax({
                url: '{{ route('reserved-leads-update-status') }}',
                type: 'POST',
                data: formData.serialize(),
                success: function(data) {
                    var obj = JSON.parse(data);
                    if (obj.status == 'ok') {
                        var options = {closeButton: true};
                        $('#myModalBox').modal('hide');
                        toastr.success(obj.message, 'Success!', options);
                        setTimeout(function() {
                            location.reload();
                        }, 2000);
                    } else {
                        var options = {closeButton: true};
                        toastr.error(obj.message, 'Error!', options)
                    }
                },
                error: function (data) {
                    var obj = JSON.parse(data);
                    var options = {closeButton: true};
                    toastr.error(obj, 'Error!', options)
                }
            });
        });
    </script>

    <script>
        $(document).ready(function() {
            var showChar = 125;
            var ellipsestext = "...";
            var moretext = "Show more ";
            var lesstext = "Show less";
            $('.more').each(function() {
                var content = $(this).html();
                if (content.length > showChar) {
                    var c = content.substr(0, showChar);
                    var h = content.substr(showChar, content.length - showChar);
                    var html = c + '<span class="moreellipses">' + ellipsestext +
                        '&nbsp;</span><span class="morecontent"><span>' + h +
                        '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
                    $(this).html(html);
                }
            });

            $(".morelink").click(function(){
                if($(this).hasClass("less")) {
                    $(this).removeClass("less");
                    $(this).html(moretext);
                } else {
                    $(this).addClass("less");
                    $(this).html(lesstext);
                }
                $(this).parent().prev().toggle();
                $(this).prev().toggle();
                return false;
            });
        });
    </script>
@endsection

