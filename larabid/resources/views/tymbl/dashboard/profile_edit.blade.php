@extends('tymbl.layouts.dashboard')
@section('content')

<div class="m-content">
            <div class="mx-auto" style="width:100%;"></div>
						<!--begin::Portlet-->
						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<span class="m-portlet__head-icon m--hide">
											<i class="la la-gear"></i>
										</span>
										<h3 class="m-portlet__head-text">
											Edit Profile
										</h3>
									</div>

                  <div class="success-error"></div>
								</div>
							</div>
							<!--begin::Form-->
              <div class="m-portlet__body">
                <div class="m-form__section m-form__section--first">

                  {!! Form::open(['class'=>'form-horizontal', 'files'=>'true']) !!}
                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">@lang('app.first_name')<span class="required-field">*</span></label>
                    <div class="col-lg-6">
                      <input type="text" class="form-control" id="name" value="{{ old('first_name')? old('first_name') : safe_output($first_name) }}" name="first_name" placeholder="@lang('app.first_name')" required>
                      {!! $errors->has('first_name')? '<p class="help-block">'.$errors->first('first_name').'</p>':'' !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">@lang('app.last_name')<span class="required-field">*</span></label>
                    <div class="col-lg-6">
                      <input type="text" class="form-control" id="name" value="{{ old('last_name')? old('last_name') : safe_output($last_name) }}" name="last_name" placeholder="@lang('app.first_name')" required>
                      {!! $errors->has('last_name')? '<p class="help-block">'.$errors->first('last_name').'</p>':'' !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">@lang('app.email')<span class="required-field">*</span></label>
                    <div class="col-lg-6">
                      <input type="email" class="form-control" id="email" value="{{ old('email')? old('email') : safe_output($user->email) }}" name="email" placeholder="@lang('app.email')" required>
                      {!! $errors->has('email')? '<p class="help-block">'.$errors->first('email').'</p>':'' !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">@lang('app.mobile')<span class="required-field">*</span></label>
                    <div class="col-lg-6">
                      <input type="text" class="form-control" id="mobile" value="{{ old('phone')? old('phone') : $user->phone }}" name="phone" placeholder="@lang('app.mobile') 9999999999" required>
                      {!! $errors->has('phone')? '<p class="help-block">'.$errors->first('phone').'</p>':'' !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">Title</label>
                    <div class="col-lg-6">
                      <select id="title" name="title" class="form-control" style="height: 40px;">
                          <option value="">Select Gender</option>
                          <option value="mr" {{ $user->title == 'mr'?'selected':'' }}>Mr</option>
                          <option value="mrs" {{ $user->title == 'mrs'?'selected':'' }}>Mrs</option>
                          <option value="other" {{ $user->title == 'other'?'selected':'' }}>Other</option>
                      </select>
                      {!! $errors->has('title')? '<p class="help-block">'.$errors->first('title').'</p>':'' !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">Business Address<span class="required-field">*</span></label>
                    <div class="col-lg-6">
                      <input type="text" class="form-control" id="refAdress" value="{{ old('address')? old('address') : safe_output($user->address) }}" name="address" placeholder="@lang('app.address')" required>
                      {!! $errors->has('address')? '<p class="help-block">'.$errors->first('address').'</p>':'' !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">Country<span class="required-field">*</span></label>
                    <div class="col-lg-6">
                      <select name="countryid" class="form-control" id="country_id" style="height: 40px;">
                        <option value="231" {{$user->country_id == '231' ? 'selected' : ''}}>United States</option>
                        <option value="38" {{$user->country_id == '38' ? 'selected' : ''}}>Canada</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">State<span class="required-field">*</span></label>
                    <div class="col-lg-6">
                      @if(isset($states))
                      @if($states->count() > 0)
                        @foreach($states as $state)
                          @if($user->state_id == $state->id)
                            @php $state_name = $state->state_name @endphp
                          @endif
                        @endforeach
                      @endif
                      @endif
                    <input type="text" id="state" class="form-control" name="state_name" value="{{ isset($states) ? $state_name : '' }}">
                      {!! $errors->has('state_id')? '<p class="help-block">'.$errors->first('state_id').'</p>':'' !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">City</label>
                    <div class="col-lg-6">
                      @if(isset($cities))
                        @if($cities->count() > 0)
                          @foreach($cities as $city)
                            @if($user->city_id == $city->id)
                              @php $city_name = $city->city_name; @endphp
                            @endif
                          @endforeach
                        @endif
                        @endif
                        <input type="text" class="form-control" name="city_name" value="{{ isset($city_name) ? $city_name : ''  }}">
                      {!! $errors->has('city_id')? '<p class="help-block">'.$errors->first('city_id').'</p>':'' !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">Zip Code</label>
                    <div class="col-lg-6">
                      <input type="text" class="form-control"  name="zip_code" value="{{ $user->zip_code == '0' ? '' : $user->zip_code }}">
                    </div>
                  </div>

                  <input type="hidden" name="country_id" value="{{ isset($user->country_id) ? $user->country_id : ''}}">
                  <input type="hidden" id="state_id" name="state_id" value="{{ $user->state_id }}">
                  <input type="hidden" id="city_id" name="city_id" value="{{ $user->city_id }}">

                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">@lang('app.change_avatar')</label>
                    <div class="col-lg-6">

                      <p><img style="width: 80px;" src="{{ auth()->user()->get_gravatar() !== null ? auth()->user()->get_gravatar() : '/uploads/avatar/gravatar-default.png' }}" alt=""></p>
                      <input type="file" id="photo" name="photo" class="filestyle" >
                      {!! $errors->has('photo')? '<p class="help-block">'.$errors->first('photo').'</p>':'' !!}
                    </div>
                  </div>
                  <br>
                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">Brokerage Firm<span class="required-field">*</span></label>
                    <div class="col-lg-6">
                      <input type="text" required class="form-control" id="broker_name" value="{{ old('broker_name')? old('broker_name') : (isset($broker->name) ? safe_output($broker->name) : '') }}" name="broker_name" placeholder="Brokerage Firm">
                      {!! $errors->has('broker->name')? '<p class="help-block">'.$errors->first('broker->name').'</p>':'' !!}
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">Brokerage Contact Person<span class="required-field">*</span></label>
                    <div class="col-lg-6">
                      <input type="text" required class="form-control" id="broker_contact_person" value="{{ old('broker_contact_person')? old('broker_contact_person') : (isset($broker->broker_contact_person) ? safe_output($broker->broker_contact_person) : '') }}" name="broker_contact_person" placeholder="Contact Person">
                      {!! $errors->has('broker->broker_contact_person')? '<p class="help-block">'.$errors->first('broker->broker_contact_person').'</p>':'' !!}
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">Brokerage Address<span class="required-field">*</span></label>
                    <div class="col-lg-6">
                      <input type="text" required class="form-control" id="brokerage_address" value="{{ old('brokerage_address')? old('brokerage_address') : (isset($broker->address) ? safe_output($broker->address) : '') }}" name="brokerage_address" placeholder="Broker Address">
                      {!! $errors->has('broker->broker_contact_person')? '<p class="help-block">'.$errors->first('broker->broker_contact_person').'</p>':'' !!}
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">Email<span class="required-field">*</span></label>
                    <div class="col-lg-6">
                      <input type="text" required class="form-control" id="broker_name" value="{{ old('broker_email')? old('broker_email') : (isset($broker->broker_email) ? safe_output($broker->broker_email) : '') }}" name="broker_email" placeholder="Email Address">
                      {!! $errors->has('broker->broker_contact_person')? '<p class="help-block">'.$errors->first('broker->broker_contact_person').'</p>':'' !!}
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">Phone<span class="required-field">*</span></label>
                    <div class="col-lg-6">
                      <input type="text" required class="form-control" id="broker_phone" value="{{ old('broker_phone')? old('broker_phone') : (isset($broker->broker_phone) ? safe_output($broker->broker_phone) : '') }}" name="broker_phone" placeholder="Phone">
                      {!! $errors->has('broker->broker_phone')? '<p class="help-block">'.$errors->first('broker->broker_phone').'</p>':'' !!}
                    </div>
                  </div>


                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">MLS</label>
                    <div class="col-lg-6">
                      <input type="text" class="form-control" id="mls_id" value="{{ old('mls_id')? old('mls_id') : (isset($user_mls->name) ? $user_mls->name : '') }}" name="mls_id" placeholder="Enter MLS or click change to choose from the list">&nbsp;<a id="change_mls" href="javascript::">Change</a>
                      {!! $errors->has('broker->name')? '<p class="help-block">'.$errors->first('broker->name').'</p>':'' !!}
                      <input type="hidden" id="mls_state" name="mls_state" value="">
                    </div>
                  </div>


                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label">Notifications</label>
                    <div class="col-lg-6"><br>
                      @php
                      $areacodes = '';
                      $notified_by = '';
                      $notification = '';
                      @endphp

                      @foreach($notifications as $notification)
                      @if($notification)
                        @if($notification->loc_range == '0')
                          @php
                          $areacodes .= $notification->zip_id.', ';
                          $notified_by = '1';
                          @endphp
                        @else
                          @php
                          $areacodes = $notification->loc_range;
                          $notified_by = '2';
                          @endphp
                        @endif
                      @endif

                      @endforeach
                      <input type="checkbox" name="sms_notify" class="sms_notify" {{ $areacodes == '' ? '' : 'checked' }}> Notify me when there are new leads in my area
                      <div>
                        @if($notified_by == '1')
                        <br>
                          <div style="margin-bottom: 10px; font-weight: 500;">by zip codes: </div>
                          <input class="form-control" type="text" value="@php echo trim($areacodes, ', '); @endphp">
                          &nbsp;
                        @elseif($notified_by == '2')
                        <br>
                        <div style="margin-bottom: 10px; font-weight: 500;">by location range: </div>
                        <input class="form-control" type="text" disabled value="{{$notification->loc_range}} MILES">
                        &nbsp;
                        @endif

                        <a href="javascript::" id="pop-modal" data-toggle="modal" data-target="#profile-modal" style="display: inline-block; margin-top: 10px; text-decoration: none">add or update notification</a>
                      </div>
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-lg-3 col-form-label"></label>
                    <div class="col-lg-6">
                      <p>&nbsp;&nbsp;</p>
                      <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom" >@lang('app.save')</button>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ route('profile') }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">Cancel</a>
                    </div>
                  </div>
                  {!! Form::close() !!}
                  </div>
              </div>
							<!--end::Form-->
						</div>

						<!--end::Portlet-->
					</div>

          <div class="mls_section" id="mls_section">
            <select class="form-control" id="allcountries" style="height: 40px;">
              <option value="" selected>Select Country</option>
              <option value="38">Canada</option>
              <option value="231">United States</option>

            </select><br>
            <select class="form-control allstate" id="allstate" name="allstate" style="height: 40px;">
              <option value="">Select State</option>
              @if(isset($states))
              @foreach($states as $state)
                <option value="{{$state->state_name}}">{{$state->state_name}}</option>
              @endforeach
              @endif
            </select><br>
            <select class="form-control mls" id="mls" name="mls" style="height: 40px;">
              <option value="">Please choose MLS</option>
              @foreach($mls as $ml)
                <option rel="{{$ml->id}}" value="{{$ml->state}}">{{$ml->name}}</option>
              @endforeach
            </select>
            <br>

            <button id="ok-mls" class="btn btn-accent m-btn m-btn--air m-btn--custom">Ok</button>&nbsp;&nbsp;
            <button id="ok-cancel" class="btn btn-secondary m-btn m-btn--air m-btn--custom">Cancel</button>
            <span id="err-mls"></span>
          </div>


          <!-- Button trigger modal -->

          <!-- Modal -->
          <div class="modal fade" id="profile-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Notify me when there are new leads</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                  <ul class="list" style="margin:0px; padding: 0px; list-style: none !important">
                    <li class="list-group-item"><p class="custom-header">BY DISTANCE WITHIN MY AREA</p>
                    <input type="range" min="0" max="100" value="1" class="slider" id="slider" name="distance" class="form-control-range mb-md-4 mr-md-2" onclick="actionClick()"><br><br>
                    AREA: <small class="range-val" id="slider_value">0 MILES</small></li>
                    <li class="list-group-item"><br>
                      <span class="custom-header">BY ZIP CODE (up to 10)</span><br>
                      <div class="zip-input" >
                        <ul id="ul-zips"></ul>
                        <input type="text" id="zip_input" class="form-control w-100" placeholder="type in zip code">
                        <div style="display:inline-block; height: 30px;"><div id="pill"></div></div>
                      </div>
                    </li>
                  </ul>
                  </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" id="profile-save-changes">Save changes</button>
                </div>
              </div>
            </div>
          </div>
          <!--Modal-->

@endsection
