@extends('tymbl.layouts.dashboard')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">Notification Settings</h3>
							</div>
						</div>
					</div>
          @include('admin.flash_msg')
					<!-- END: Subheader -->
					<div class="m-content">


						<!--begin::Portlet-->
						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<span class="m-portlet__head-icon m--hide">
											<i class="la la-gear"></i>
										</span>
										<h3 class="m-portlet__head-text">
											Notification Settings
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<!--begin::Form-->

								<div class="m-portlet__body">
									<div class="m-form__section m-form__section--first">
										<div class="form-group m-form__group row">
											<label class="col-lg-3 col-form-label">Email</label>
											<div class="col-lg-6" id="email" rel="{{$email}}">
												<input type="text" class="form-control m-input m-input--solid editable" value="{{$email}}" id="notification-email" disabled="disabled">
                        <a id="edit_email" class="add_btn" href="#">Edit</a><a href="#" id="btn-save-mail">Save</a>
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label class="col-lg-3 col-form-label">Phone</label>
											<div class="col-lg-6" id="phone" rel="{{$phone}}">
												<input type="text" class="form-control m-input m-input--solid editable" id="notification-phone" disabled="disabled" value="{{$phone}}">
                        <a id="edit_phone" class="add_btn" href="#">Edit</a><a href="#" id="btn-save-phone">Save</a>
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label class="col-lg-3 col-form-label">Country</label>
											<div class="col-lg-6">
                        @if($countries=='')
                          nothing
                        @else
                        <select id="country_ti" style="display:none;" multiple data-role="tagsinput">
                          @foreach($countries as $country)

                            @if($country == '231')
                              <option rel="321" value="United States">United States</option>
                            @else
                              <option rel="38" value="Canada">Canada</option>
                            @endif
                          @endforeach
                        </select><br>
                        <a id="add_country" class="add_btn" href="#">Change country</a>
                        <div id="selector-wrapper-country"><select class="form-control select2" id="country_select" name="country">
                          <option rel="" value="" selected disabled="disabled">Select Country</option>
                          <option value="38">Canada</option>
                          <option value="231">United States</option>
                        </select>
                        </div>
                        @endif
											</div>
										</div>
                    <div class="form-group m-form__group row">
                      <label class="col-lg-3 col-form-label">States</label>
                      <div class="col-lg-6">
                        @if($states != '')
                        <select id="states_ti" style="display:none;" multiple data-role="tagsinput">
                        @foreach($states as $state => $st)
                                @for($i=0; $i < count($st); $i++)
                                  <option rel="{{$st[$i]->id}}" value="{{$st[$i]->state_name}}">{{$st[$i]->state_name}}</option>
                                @endfor
                        @endforeach
                      </select>
                      @endif

                        <a id="add_state" class="add_btn" href="#">Add state</a>
                        <div id="selector-wrapper-state"><select class="form-control select2" id="state_select" name="state"></select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group m-form__group row">
                      <label class="col-lg-3 col-form-label">City</label>
                      <div class="col-lg-6">

                        @if($cities != '')
                        <select id="city_ti" style="display:none;"  multiple data-role="tagsinput">
                          @foreach($cities as $city=> $ct)
                                  @for($i=0; $i < count($ct); $i++)
                                    @if($ct[$i]->city_name != '0')
                                      <option rel="{{$ct[$i]->id}}" value="{{$ct[$i]->city_name}}">{{$ct[$i]->city_name}}</option>
                                    @endif
                                  @endfor
                          @endforeach
                      </select>
                      @endif

                      <a id="add_city" class="add_btn" href="#">Add city</a>
                      <div id="selector-wrapper-city"><select class="form-control select2" id="city_select" name="city"></select></div>
                      </div>
                    </div>

                  </div>
                </div>


							<!--end::Form-->

							<!--end::Form-->
						</div>

						<!--end::Portlet-->
					</div>
				</div>

      @endsection
