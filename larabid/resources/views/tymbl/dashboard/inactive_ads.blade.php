@extends('tymbl.layouts.dashboard')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">Leads</h3>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->
					<div class="m-content">
            						<!--begin::Portlet-->
						<div class="m-portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<h3 class="m-portlet__head-text">
												{{$title}}
											</h3>
										</div>
									</div>
                  <!-- search from -->
                  <form action="{{ Request::url() }}" method="GET" class="user-search-dash">
                    <div class="input-group mb-3">
                      <input class="form-control float-left" type="text" name="q" value="{{ app('request')->input('q') }}">
                      <input type="hidden" name="p" value="{{$adtype}}">
                      <div class="input-group-append">
                        <input class="btn btn-primary float-left" type="submit" value="search">
                      </div>
                    </div>
                  </form>
                  <!-- end search from -->
								</div>
								<div class="m-portlet__body">
									<!--begin::Section-->
									<div class="m-section">
										<div class="m-section__content">
                      @if($ads->total() > 0)
                          <table class="table table-bordered table-striped" id="tbl-admin-user">

                              @foreach($ads as $ad)
                                @php
                                $images = \App\Media::where('ad_id', $ad->id)->orderBy('created_at', 'desc')->where('type', '=', 'image')->first();
                                @endphp
                                  <tr>
                                      <td>
                                          @if($images['media_name'] && $images['media_name'] != 'NULL')
                                            @if(file_exists('uploads/images/thumbs/'.$images['media_name']))
                                              <img class="dashboard-thumbs" src="{{ media_url($ad->feature_img) }}" class="thumb-listing-table" alt="">
                                            @else
                                              <img src="uploads/images/{{$images['media_name']}}" class="dashboard-thumbs" />
                                            @endif
                                          @else
                                            @if($ad->category_type == "selling")
                                              <img src="{{ asset('assets/img/tymbl/house.png') }}" class="dashboard-thumbs" />
                                            @else
                                              <img src="{{ asset('assets/img/tymbl/person.png') }}" class="dashboard-thumbs" />
                                            @endif
                                          @endif
                                      </td>
                                      <td class="ad-desc">
                                          <h5><a href="{{  route('single_ad', [$ad->id, $ad->slug]) }}" target="_blank">{{ safe_output($ad->title) }}</a></h5>
                                          <p class="text-muted">
                                              <i class="fa fa-map-marker"></i> {!! $ad->full_address()  !!} <br />  <i class="fa fa-clock-o"></i> {{ $ad->posting_datetime()  }}
                                          </p>
                                      </td>

                                      <td id="icon-actions">

                                        <div class="f-holder">Featured Ad&nbsp;&nbsp;<label class="switch">
                                          <input rel="{{$ad->id}}" id="f-toggler" type="checkbox" {{ $ad->is_featured == '1' ? 'checked' : '' }}>
                                          <span class="slider round" title="Mark as featured ad"></span>
                                        </label></div>

                                        <a title="Remove this lead" href="javascript:;" class="btn btn-danger deleteAds" data-slug="{{ $ad->slug }}"><i class="fa fa-trash"></i> </a>


                                        <a title="Mark Active" href="#" class="btn btn-success approveAds" data-slug="{{ $ad->slug }}" data-value="1" rel="{{$ad->id}}"><i class="fas fa-check"></i> </a>

                                        @if($ad->status == '3')
                                        <a href="javascript:;" title="Unblock this ad" class="btn btn-warning approveAds" data-slug="{{ $ad->slug }}" data-value="1"><i class="fas fa-circle"></i></a>
                                        @else
                                        <a title="Block lead" href="javascript:;" class="btn btn-warning blockAds" data-slug="{{ $ad->slug }}" data-value="3"><i class="fa fa-ban"></i></a>
                                        <a title="Edit" href="{{ route('edit_ad', $ad->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i> </a>
                                        @endif



                                      </td>
                                  </tr>
                              @endforeach

                          </table>

                      @else
                          <h2>@lang('app.there_is_no_ads')</h2>
                      @endif

                      {!! $ads->links() !!}
										</div>
									</div>

									<!--end::Section-->
								</div>

								<!--end::Form-->
							</div>

							<!--end::Portlet-->
					</div>
				</div>

@endsection
