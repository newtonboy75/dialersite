@extends('tymbl.layouts.dashboard')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">Payments</h3>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->
					<div class="m-content">
            						<!--begin::Portlet-->
						<div class="m-portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<h3 class="m-portlet__head-text">
												Payments
											</h3>

										</div>
									</div>
								</div>
								<div class="m-portlet__body">

									<!--begin::Section-->
									<div class="m-section">
										<div class="m-section__content">
                      <table class="table table-bordered table-striped" id="jDataTable">
                          <thead>
                              <tr>
                                  <th>ID</th>
                                  <th>Lead</th>
                                  <th>@lang('app.user')</th>
                                  <th>@lang('app.amount')</th>
                                  <th>@lang('app.payment_method')</th>
                                  <th>@lang('app.status')</th>
                                  <th>@lang('app.created_at')</th>
                              </tr>
                          </thead>
                      </table>
										</div>
									</div>

									<!--end::Section-->
								</div>

								<!--end::Form-->
							</div>

							<!--end::Portlet-->
					</div>
				</div>

@endsection
