<div class="row">
    <div class="col-sm-6">
        <table class="table">
            <tr>
                <td class="font-weight-bold">User Id</td>
                <td id="uid" data-id="{{ $user->id }}">{{ $user->id }}</td>
            </tr>
            <tr>
                <td class="font-weight-bold">
                    First Name<span style="color:red">*</span>
                </td>
                <td id="fname">
                    <input required class="form-control" type="text" id="first_name" name="first_name" value="{{ $user->first_name }}">
                </td>
            </tr>
            <tr>
                <td class="font-weight-bold">
                    Last Name<span style="color:red">*</span>
                </td>
                <td id="lname">
                    <input  class="form-control"type="text" id="last_name" name="last_name" value="{{ $user->last_name }}">
                </td>
            </tr>
            <tr>
                <td class="font-weight-bold">Title</td>
                <td class="title">
                    <input style="text-transform: capitalize;" class="form-control" type="text" id="title" name="title" value="{{ $user->title }}">
                </td>
            </tr>
            <tr>
                <td class="font-weight-bold">
                    Email<span style="color:red">*</span>
                </td>
                <td class="email">
                    <input class="form-control" type="text" id="email" name="email" value="{{ $user->email }}"></td>
            </tr>
            <tr>
                <td class="font-weight-bold">Phone<span style="color:red">*</span></td>
                <td class="phone">
                    <input class="form-control" type="text" id="phone" name="phone" value="{{ $user->phone }}">
                </td>
            </tr>
            <tr>
                <td class="font-weight-bold">
                    Address<span style="color:red">*</span>
                </td>
                <td class="address">
                    <input class="form-control" type="text" id="address" name="address" value="{{ $user->address }}">
                </td>
            </tr>
            <tr>
                <td class="font-weight-bold">
                    Country<span style="color:red">*</span>
                </td>
                <td class="country">
                    <input class="form-control ui-autocomplete-input" type="text" id="country" name="country" value="{{ isset($user->country) ? $user->country->country_name : '' }}">
                    <input type="hidden" name="country_id" id="country_id" value="{{ isset($user->country) ? $user->country->id : '' }}">
                </td>
            </tr>
            <tr>
                <td class="font-weight-bold">
                    State<span style="color:red">*</span>
                </td>
                <td class="state">
                    <input class="form-control" type="text" id="state" name="state" value="{{ isset($user->state) ? $user->state->state_name : '' }}">
                    <input type="hidden" name="state_id" id="state_id" value="{{ isset($user->state) ? $user->state->id : '' }}">
                </td>
            </tr>
            <tr>
                <td class="font-weight-bold">
                    City<span style="color:red">*</span>
                </td>
                <td class="city">
                    <input class="form-control" type="text" id="city" name="city" value="{{ isset($user->city) ? $user->city->city_name : '' }}">
                    <input type="hidden" name="city_id" id="city_id" value="{{ isset($user->city) ? $user->city->id : '' }}">
                </td>
            </tr>
            <tr>
                <td class="font-weight-bold">Zip Code</td>
                <td class="zip">
                    <input class="form-control" type="text" id="zip" name="zip" value="{{ $user->zip_code }}">
                </td>
            </tr>
        </table>
        <p>&nbsp;</p>
    </div>
    <div class="col-sm-6">
        <table class="table">
            <tr>
                <td class="font-weight-bold">
                    Brokerage Name<span style="color:red">*</span>
                </td>
                <td class="broker">
                    <input class="form-control" type="text" id="broker" name="broker" value="{{ isset($user->broker) ? $user->broker->name : '' }}">
                </td>
            </tr>
            <tr>
                <td class="font-weight-bold">
                    Contact Person<span style="color:red">*</span>
                </td>
                <td class="broker_contact_person">
                    <input class="form-control" type="text" id="broker_contact_person" name="broker_contact_person" value="{{ isset($user->broker) ? $user->broker->broker_contact_person : ''}}">
                </td></tr>
            <tr>
                <td class="font-weight-bold">Contact No.<span style="color:red">*</span></td>
                <td class="broker_contact_num">
                    <input class="form-control" type="text" id="broker_contact_num" name="broker_contact_num" value="{{ isset($user->broker) ? $user->broker->broker_phone : '' }}">
                </td>
            </tr>
            <tr>
                <td class="font-weight-bold">
                    Email Address<span style="color:red">*</span>
                </td>
                <td class="broker_email">
                    <input class="form-control" type="text" id="broker_email" name="broker_email" value="{{ isset($user->broker) ? $user->broker->broker_email : '' }}">
                </td>
            </tr>
            <tr>
                <td class="font-weight-bold">MLS</td>
                <td class="mls">
                    <input class="form-control" type="text" id="mls" name="mls" value="{{ isset($user->mls) ? $user->mls->name : '' }}">
                    <input type="hidden" name="mls_id" id="mls_id" value="{{ isset($user->mls) ? $user->mls->id : '' }}">
                </td>
            </tr>
            <tr>
                <td class="font-weight-bold"></td>
                <td></td>
            <tr>
            <tr>
                <td class="font-weight-bold">Leads Posted</td>
                <td id="num_leads">
                    <a href="{{ route('ads_by_user', $user->id) }}" target="_blank">{{ isset($user->adsStatusOneCount) ? $user->adsStatusOneCount : 0 }} lead/s</a>
                </td>
            </tr>
            <tr>
                <td class="font-weight-bold">User is verified by broker</td>
                <td id="broker_verified">{{ isset($user->broker) && $user->broker->broker_verified == '1' ? 'Yes' : 'No' }}</td>
            </tr>
            <tr>
                <td class="font-weight-bold">Date Joined</td>
                <td id="date_joined">{{ $user->created_at }}</td>
            </tr>
            <tr>
                <td class="font-weight-bold">Status</td>
                <td>
                    <select class="form-control user-ddp" title="Change user status" id="user_status" style="height:40px;">
                        @foreach($statuses as $key => $val)
                            <option value="{{ $key }}" {{ $key == $user->active_status ? 'selected' : '' }}>
                                {{ $val }}
                            </option>
                        @endforeach
                    </select><br>&nbsp;
                </td>
            </tr>
        </table>
    </div>
</div>
