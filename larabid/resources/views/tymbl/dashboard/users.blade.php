@extends('tymbl.layouts.dashboard')
@if(session()->has('message'))
  <div class="alert alert-success">
    {{ session()->get('message') }}
  </div>
@endif
@section('content')
  <div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="m-subheader__title ">Users</h3>
        </div>
      </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
      <!--begin::Portlet-->
      <div class="m-portlet">
        <div class="m-portlet__head">
          <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
              <h3 class="m-portlet__head-text">
                Users
              </h3>

            </div>
          </div>
        </div>
        <div class="m-portlet__body">

          <!--begin::Section-->
          <div class="m-section">
            <div class="m-section__content">
              <div class="table-responsive">
                <table class="table table-bordered table-striped text-center table-responsive" id="jDataTable" data-page-length='25'>
                  <thead>
                  <tr>
                    <th class="text-center" style="width:300px !important;">@lang('app.name')</th>
                    <th class="text-center">@lang('app.status')</th>
                    <th class="text-center" style="width:300px !important;">Location</th>
                    <th class="text-center">Confirmation</th>
                  </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>

          <!--end::Section-->
        </div>

        <!--end::Form-->
      </div>

      <!--end::Portlet-->
    </div>
  </div>

  <div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLabel">User Profile</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="container" id="user_edit"></div>
          <div class="float-left">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="text-danger fas fa-times-circle"></i>&nbsp;
            <a id="remove_user">Delete user</a>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="save_edit">Save</button>&nbsp;&nbsp;
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

@endsection
