@extends('tymbl.layouts.dashboard')
@section('content')
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
  <div class="d-flex align-items-center">
    <div class="mr-auto">
      <h3 class="m-subheader__title ">Dashboard</h3>
    </div>
  </div>
</div>

<!-- END: Subheader -->
<div class="m-content">

  <!--begin:: Widgets/Stats-->
  <div class="m-portlet  m-portlet--unair">
    <div class="m-portlet__body  m-portlet__body--no-padding">
      <div class="row m-row--no-padding m-row--col-separator-xl">
        <div class="col-md-12 col-lg-6 col-xl-3">

          <!--begin::Approved Auction-->
          <div class="m-widget24">
            <div class="m-widget24__item">
              <h4 class="m-widget24__title">
                Active Leads
              </h4><br>
              <span class="m-widget24__stats m--font-brand">
                {{$approved_ads}}
              </span>
              <span class="m-widget24__desc">
                <a href="{{ route('approved_ads') }}">View all</a>
              </span>
              <div class="m--space-10"></div>
            </div>
          </div>
          <!--end::Approved Auction-->
        </div>



        <div class="col-md-12 col-lg-6 col-xl-3">

          <!--begin::Blocked Auction-->
          <div class="m-widget24">
            <div class="m-widget24__item">
              <h4 class="m-widget24__title">
                  Blocked Leads
              </h4><br>
              <span class="m-widget24__desc">
                <a href="{{ route('admin_blocked_ads') }}">View all</a>
              </span>
              <span class="m-widget24__stats m--font-danger">
                {{$blocked_ads}}
              </span>
              <div class="m--space-10"></div>
            </div>
          </div>

          <!--end::Blocked Auction-->
        </div>

        <div class="col-md-12 col-lg-6 col-xl-3">

          <!--begin::Users-->
          <div class="m-widget24">
            <div class="m-widget24__item">
              <h4 class="m-widget24__title">
                Active Users
              </h4><br>
              <span class="m-widget24__desc">
                Total Joined User
              </span>
              <span class="m-widget24__stats m--font-success">
                {{$total_users}}
              </span>
              <div class="m--space-10"></div>
              <span class="m-widget24__change">
                <a href="{{route('users')}}">View all</a>
              </span>
            </div>
          </div>

          <!--end::Users-->
        </div>

        <div class="col-md-12 col-lg-6 col-xl-3">

          <!--begin::New Users-->
          <div class="m-widget24">
            <div class="m-widget24__item">
              <h4 class="m-widget24__title">
                Total Users
              </h4><br>
              <span class="m-widget24__desc">
                Joined New User
              </span>
              <span class="m-widget24__stats m--font-info">
                {{$total_users_all}}
              </span>
              <div class="m--space-10"></div>
              <span class="m-widget24__change">
                  <a href="{{ route('users') }}">View all</a>
              </span>


            </div>
          </div>

          <!--end::New Users-->
        </div>
      </div>
    </div>
  </div>
  <div class="m-portlet  m-portlet--unair">
    <div class="m-portlet__body  m-portlet__body--no-padding">
      <div class="row m-row--no-padding m-row--col-separator-xl">
        <div class="col-md-12 col-lg-6 col-xl-3">

          <!--begin::Reports-->
          <div class="m-widget24">
            <div class="m-widget24__item">
              <h4 class="m-widget24__title">
                  Reports
              </h4><br>
              <span class="m-widget24__desc">
                <a href="#">View all</a>
              </span>
              <span class="m-widget24__stats m--font-warning">
                {{$total_reports}}
              </span>
              <div class="m--space-10"></div>
            </div>
          </div>

          <!--end::Reports-->
        </div>
        <div class="col-md-12 col-lg-6 col-xl-3">

          <!--begin::Successful payments-->
          <div class="m-widget24">
            <div class="m-widget24__item">
              <h4 class="m-widget24__title">
                  Successful payments
              </h4><br>
              <span class="m-widget24__desc">
                <a href="{{ route('payments') }}">View all payments</a>
              </span>
              <span class="m-widget24__stats m--font-success">
                {{$total_payments}}
              </span>
              <div class="m--space-10"></div>
            </div>
          </div>

          <!--end::Successful payments-->
        </div>
        <div class="col-md-12 col-lg-6 col-xl-3">

          <!--begin::Total Payment-->
          <div class="m-widget24">
            <div class="m-widget24__item">
              <h4 class="m-widget24__title">
                  Total Payment
              </h4><br>
              <span class="m-widget24__desc">
                Total Amount
              </span>
              <span class="m-widget24__stats m--font-primary">

              </span>
              <div class="m--space-10"></div>
            </div>
          </div>

          <!--end::Total Payment-->
        </div>
        <div class="col-md-12 col-lg-6 col-xl-3" style="border-left: 0px !important;">

          <!--begin::Total Payment-->
          <div class="m-widget24">
            <div class="m-widget24__item">
              <h4 class="m-widget24__title">

              </h4><br>
              <span class="m-widget24__desc">

              </span>
              <span class="m-widget24__stats m--font-primary">
                {{number_format($total_payments_amount, 2)}}
              </span>
              <div class="m--space-10"></div>
            </div>
          </div>

          <!--end::Total Payment-->
        </div>
      </div>
    </div>
  </div>


  <!--end:: Widgets/Stats-->


  <!--begin::Portlet-->
  <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
                Latest 10 contact messages
            </h3>
          </div>
        </div>
      </div>
      <div class="m-portlet__body">

        <!--begin::Section-->
        <div class="m-section">
          <div class="m-section__content">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>Sender Name</th>
                  <th>Sender Email</th>
                  <th>Message</th>
                </tr>
              </thead>
              <tbody>
                @foreach($ten_contact_messages as $tc)
                <tr>
                  <th scope="row">{{$tc->name}}</th>
                  <td>{{$tc->email}}</td>
                  <td>{{$tc->message}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>

        <!--end::Section-->
      </div>

      <!--end::Form-->
    </div>

    <!--end::Portlet-->
  <!--begin::Portlet-->
  <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
                Latest 10 item reports
            </h3>
          </div>
        </div>
      </div>
      <div class="m-portlet__body">

        <!--begin::Section-->
        <div class="m-section">
          <div class="m-section__content">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>type</th>
                  <th>Price</th>
                  <th>Details</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">Condo</th>
                  <td>Condo</td>
                  <td>$30000</td>
                  <td>Lorem ipsum dolor sit amet consectetur adipisicing elit.</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <!--end::Section-->
      </div>

      <!--end::Form-->
    </div>

    <!--end::Portlet-->
</div>
@endsection
