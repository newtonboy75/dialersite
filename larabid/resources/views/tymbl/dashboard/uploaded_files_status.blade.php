@extends('tymbl.layouts.dashboard')
@section('css')
    <style>
        .circle {
            cursor: pointer;
            border-radius: 200px;
            color: white;
            height: 40px;
            width: 40px;
            font-weight: bold;
            display: table;
            margin: 20px auto;
            text-align: center;
        }
        .circle i {
            vertical-align: middle;
            display: table-cell;
            color: #ffffff
        }

        .start-call {
            background-color: #004680;
        }

        .end-call {
            background-color: red;
        }

        .sms-button {
            text-decoration: none;
            display: table;
            width: 50px;
            text-align: center;
            height: 40px;
            padding: 5px;
            background-color: #007bff;
            border-color: #007bff;
        }
        .sms-button:hover {
            text-decoration: none;
            background-color: #0069d9;
            border-color: #0062cc;
        }
        .sms-button:hover i {
            color: #0062cc;
        }
        .sms-button i {
            vertical-align: middle;
            display: table-cell;
            border-radius: 20px;
            color: #007bff;
            font-weight: bold;
            background-color: #ffffff;
        }
    </style>
@endsection
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

  <!-- BEGIN: Subheader -->
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title ">{{$title}}</h3>
      </div>
    </div>
  </div>

  <!-- END: Subheader -->
  <div class="m-content">
    <!--begin::Portlet-->
    <div class="m-portlet">
      @php
        $file_id = request()->segment(3);
      @endphp
        <div class="m-portlet__head" style="align-items: center; justify-content: space-between;">
            <a href="/dashboard/uploaded_leads" title="back to Uploaded Leads page">
                <i class="fas fa-arrow-left"></i>
            </a>
            <div style="display: flex; align-items: center; justify-content: space-between;">
                <div style="display: flex; align-items: center; justify-content: space-between;">
                    <a class="circle start-call main-call-button" data-isSingleCall="false" title="Start calling">
                        <i class="fa fa-phone" style="transform: scaleX(-1);"></i>
                    </a>
{{--                    <a class="circle" style="margin-left: 10px; color: #ffffff; background-color: #E8E2DF">--}}
{{--                        <i class="fa fa-play" style="color: #53AFDC;padding: 0;margin: 0"></i>--}}
{{--                    </a>--}}
                </div>
                <h4 style="padding: 0; margin: 0 0 0 2em">Campain Status: <span id="call-status">Stop</span></h4>
            </div>
            @if(Auth::user()->is_admin())
                <div id="lead_importer">
                    <a href="{{ route('import-leads', ['file_id' => $file_id]) }}" class="btn btn-primary">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                        <span class="m-menu__link-text">Import Leads</span>
                    </a>
                </div>
            @endif
        </div>
      <div class="m-portlet__body">

        <!--begin::Section-->
        <div class="m-section">
          <div class="m-section__content">
            <div class="messages-wrapper">
              <div class="row" style="overflow-x: hidden; ">
              <table class="table jdata display nowrap" id="jDataTable" data-order='[[ 0, "asc" ]]' data-page-length='25'>
                <thead>
                  <tr>
                    @if(Auth::user()->user_type == 'admin')
                      <th>ID</th>
                      <th>Date Added</th>
                      <th>Status</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Seller</th>
                      <th>Buyer</th>
                      <th>Email</th>
                      <th>Phone</th>
                      <th>Address</th>
{{--                      <th>Original Notes/Tags</th>--}}
{{--                      <th>Prioritized After Scrubbing</th>--}}
{{--                      <th>1st Dial Date</th>--}}
{{--                      <th>1st Dial Outcome</th>--}}
{{--                      <th>1st Dial Note</th>--}}
{{--                      <th>2nd Dial Date</th>--}}
{{--                      <th>2nd Dial Outcome</th>--}}
{{--                      <th>2nd Dial Note</th>--}}
{{--                      <th>SMS Date</th>--}}
{{--                      <th>SMS Outcome</th>--}}
{{--                      <th>SMS Note</th>--}}
{{--                      <th>Email Date</th>--}}
{{--                      <th>Email Outcome</th>--}}
{{--                      <th>Email Note</th>--}}
                      <th>SMS</th>
                      <th>Voice</th>
                    @else
                      <th>ID</th>
                      <th>Added_date</th>
                      <th>Status</th>
                      <th>Buyer</th>
{{--                      <th>1st dial date</th>--}}
{{--                      <th>1st dial outcome</th>--}}
{{--                      <th>1st dial notes</th>--}}
{{--                      <th>2st dial date</th>--}}
{{--                      <th>2st dial outcome</th>--}}
{{--                      <th>2st dial notes</th>--}}
{{--                      <th>Sms date</th>--}}
{{--                      <th>Sms outcome</th>--}}
{{--                      <th>Sms notes</th>--}}
{{--                      <th>Email date</th>--}}
{{--                      <th>Email outcome</th>--}}
{{--                      <th>Email notes</th>--}}
                      <th>SMS</th>
                      <th>Voice</th>
                    @endif
                  </tr>
                </thead>
              </table>
            </div>
            </div>
          </div>
        </div>

        <!--end::Section-->
      </div>

      <!--end::Form-->
    </div>

    <!--end::Portlet-->
  </div>
</div>

<div id="sms-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">SMS message</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body"></div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="send-sms">Send SMS</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript" src="https://media.twiliocdn.com/sdk/js/client/v1.7/twilio.min.js"></script>
    <script>
        var callStatus = $("#call-status");
        var answerButton = $(".answer-button");
        var callSupportButton = $(".call-support-button");
        var hangUpButton = $(".hangup-button");
        var callCustomerButtons = $(".call-customer-button");
        var isSingleCall = true;

        // const device = Twilio.Device;
        var device = new Twilio.Device;
        var phone_numbers = [];
        var pos = 0;

        $(document).ready(function() {
            $.getJSON('https://fallow-cobra-6195.twil.io/capability-token')
                .then(function (data) {
                    device.setup(data.token, {
                        codecPreferences: ['opus', 'pcmu'],
                        fakeLocalDTMF: true,
                        enableRingingState: true
                    });
                });
            {{--$.post("{{ route('twilio.token') }}", {forPage: window.location.pathname}, function(data) {--}}
            {{--    device.setup(data.token, {--}}
            {{--        codecPreferences: ['opus', 'pcmu'],--}}
            {{--        fakeLocalDTMF: true,--}}
            {{--        enableRingingState: true--}}
            {{--    });--}}
            {{--});--}}
        });

        function updateCallStatus(status) {
            callStatus.text(status);
        }

        device.on('ready', function (device) {
            $('.dialer').each(function () {
                phone_numbers.push($(this).data('phone'));
            });
            updateCallStatus("Ready to call");
        });

        device.on('error', function (error) {
            updateCallStatus("ERROR!");
            toastr.error(error.message, 'Error!', {closeButton: true});
            $('.main-call-button').addClass('start-call').removeClass('end-call');
            $('dialer-' + pos).addClass('start-call').removeClass('end-call');
            device.disconnect();
        });

        device.on('disconnect', function(connection) {
            if (isSingleCall) {
                $('.end-call').removeClass('end-call').addClass('start-call');
                updateCallStatus("Ready to call");
            } else {
                if (phone_numbers.length <= pos) {
                    pos = 0;
                    updateCallStatus("The call is complete");
                    $('.main-call-button').removeClass('end-call').addClass('start-call');
                    $('.dialer-' + pos).removeClass('end-call').addClass('start-call');
                } else {
                    var phoneNumber = phone_numbers[pos++];
                    updateCallStatus("Calling " + phone + "...");
                    $('.main-call-button').removeClass('start-call').addClass('end-call');
                    $('.dialer-' + pos).removeClass('start-call').addClass('end-call');
                    var params = {"phoneNumber": phoneNumber};
                    device.connect(params);
                }
            }
        });

        $(document).on('click', '.start-call', function () {
            isSingleCall = Boolean($(this).data('is-single-call'));
            if (isSingleCall) {
                var phoneNumber = $(this).data('phone');
                $('.main-call-button').removeClass('start-call').addClass('end-call');
                $(this).removeClass('start-call').addClass('end-call');
            } else {
                if (phone_numbers.length <= pos) {
                    pos = 0;
                }
                var phoneNumber = phone_numbers[pos++];
                $('.main-call-button').removeClass('start-call').addClass('end-call');
                $('.dialer-' + pos).removeClass('start-call').addClass('end-call');
            }
            updateCallStatus("Calling " + phoneNumber);
            var params = {To: phoneNumber};
            if (device) {
                device.connect(params);
            }
        });

        $(document).on('click', '.end-call', function () {
            if (isSingleCall) {
                $('.main-call-button').removeClass('end-call').addClass('start-call');
                $(this).removeClass('end-call').addClass('start-call');
            } else {
                $('.main-call-button').removeClass('end-call').addClass('start-call');
                $('.dialer-' + pos).removeClass('end-call').addClass('start-call');
            }
            updateCallStatus("Call ended");
            if (device) {
                device.disconnectAll();
            }
        });

        $(document).on('click', '.sms-button', function (e) {
            e.preventDefault();
            $(".modal-body").load($(this).attr('href'));
            $("#sms-modal").modal('show');
        });

        $(document).on('click', '#send-sms', function () {
            $.ajax({
                url: "{{ route('twilio.sms.send') }}",
                type: "POST",
                data: $('#sms-form').serialize(),
                success: function(data) {
                    if (data.status == 'ok') {
                        toastr.success(data.message, 'Success!', {closeButton: true});
                        $("#sms-modal").modal('hide');
                    } else {
                        toastr.error(data.message, 'Error!', {closeButton: true})
                    }
                },
                error: function (data) {
                    alert(JSON.stringify(data));
                }
            });
        });

        $(document).on('click', '#use-default-template', function () {
            if ($(this).is(':checked')) {
                $('#sms-body').hide('500');
                $('#default-template').show('400');
                $('#partner-realtors-container').show('400');
            } else {
                $('#sms-body').show('400');
                $('#default-template').hide('500');
                $('#partner-realtors-container').hide('400');
            }
        });

        $(document).on('click', '#use-default-partner-realtors', function () {
            if ($(this).is(':checked')) {
                $('#partner-realtors').hide('500');
                $('#default-partner-realtors').show('400');
            } else {
                $('#partner-realtors').show('400');
                $('#default-partner-realtors').hide('500');
            }
        });
    </script>
@endsection
