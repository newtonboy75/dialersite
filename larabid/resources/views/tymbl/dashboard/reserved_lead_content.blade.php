<div class="row m-portlet" style="padding: 40px;">
    <!--left-->
    <div class="col-md-7 text-center">
        <div class="info-wrapper" style="display: inline-block; width: 100%; text-align: left; !important">
            <div class="pull-left" style="width: 90px !important; margin-right: 20px;">
                @if(isset($list->img) && $list->img->media_name != 'NULL' && $list->img->media_name != '')
                    @if(file_exists('uploads/images/thumbs/' . $list->img->media_name))
                        <img src="/uploads/images/thumbs/{{ $list->img->media_name }}" class="img-responsive" />
                    @else
                        <img src="/uploads/images/{{ $list->img->media_name }}" class="img-responsive" />
                    @endif
                @else
                    @if($list->category_type == "selling")
                        <img src="{{ asset('assets/img/tymbl/house.png') }}" class="img-responsive" />
                    @else
                        <img src="{{ asset('assets/img/tymbl/person.png') }}" class="img-responsive" />
                    @endif
                @endif
            </div>
            <div class="pull-left">
                <h5>
                    <a href="/listing/{{ $list->listingContracts->listing_id }}/{{ $list->slug }}">
                        {{ safe_output($list->title) }}
                    </a>
                </h5>
                Ad ID: #{{ $list->listingContracts->listing_id }}<br>
                Contract ID: {{ $list->listingContracts->contract_id }}<br>
            </div>
        </div>
        <div style="display: block; max-width: 480px;  margin-top: 20px; text-align: left; border: 1px solid #f0f0f0; -webkit-box-shadow: 4px 10px 11px -2px rgba(0,0,0,0.03); -moz-box-shadow: 4px 10px 11px -2px rgba(0,0,0,0.03); box-shadow: 4px 10px 11px -2px rgba(0,0,0,0.03);">
            <h5 class="text-dark" style="font-weight: 500; background: #fafafa; font-size: 13px; padding-left: 30px; padding-top: 20px;  margin: 0px;">
                PROSPECT INFORMATION
            </h5>
            <div style="font-size: 13px; padding: 20px 30px; background: #fafafa;">
                @if (isset($list->listingContracts->referralContactInfo->referral_first_name) && $list->listingContracts->referralContactInfo->referral_first_name != '0' || isset($list->listingContracts->referralContactInfo->referral_last_name) && $list->listingContracts->referralContactInfo->referral_last_name != '0')
                    @php $referral_complete_name = $list->listingContracts->referralContactInfo->referral_first_name . ' ' . $list->listingContracts->referralContactInfo->referral_last_name; @endphp
                @else
                    @php $referral_complete_name = isset($list->listingContracts->referralContactInfo->referral_name) ? $list->listingContracts->referralContactInfo->referral_name : ''; @endphp
                @endif

                @if($list->listingContracts->referralContactInfo)
                    <span>{{ safe_output($referral_complete_name) }}</span><br>
                    @if($list->listingContracts->referralContactInfo->referral_contact_address)
                        <span>
                            {{ safe_output($list->listingContracts->referralContactInfo->referral_contact_address) }}
                        </span><br>
                    @endif
                    <span>
                        <a href="tel:+{{ $list->listingContracts->referralContactInfo->referral_contact_phone }}">
                            {{ $list->listingContracts->referralContactInfo->referral_contact_phone }}
                        </a>
                    </span><br>
                    <span>
                        <a href="mailto:{{ $list->listingContracts->referralContactInfo->referral_contact_email }}">
                            {{ $list->listingContracts->referralContactInfo->referral_contact_email }}
                        </a>
                    </span><br><br>
                    <span class="more">{{ $list->description }}</span>
                @endif
            </div>
        </div>

        <div class="float-left text-left;" style="display: none">
            @php $reminder = '1'; $ltb_info = '0'; @endphp
            @if(isset($list->listingContracts->titleCompanyInfoLTB) && count($list->listingContracts->titleCompanyInfoLTB))
                @foreach($list->listingContracts->titleCompanyInfoLTB as $tci)
                    @if($tci->ad_id == $list->listingContracts->listing_id )
                        @php $ltb_info = '1'; @endphp
                        @if($tci->company_name == '' || $tci->representative_name == '' || $tci->representative_email == '')
                            @php $reminder = '0'; @endphp
                            <table class="table">
                                <thead>
                                <h5 >Title Company Details</h5>
                                </thead>
                                <tbody>
                                <tr>
                                    <div class="text-warning">
                                        Incomplete Title Company Details
                                    </div><br>
                                    <a href="/referral-document/{{ $tci->ad_id }}/title_company" class="btn btn-primary">
                                        Edit Title Company Details
                                    </a></tr>
                                </tbody>
                            </table>
                        @endif
                    @endif
                @endforeach
            @endif

            @if($selltab == '0')
                @if($reminder == '1')
                    @if($list->list_status == '0')
                        <form action="{{url('status-action')}}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{$ltb_info}}" name="ltb" />
                            <input type="hidden" value="0" name="id" />
                            <input type="hidden" name="contract_id" value="{{$list->listingContracts->contract_id}}" />
                            <input type="submit" value="Resend Referral Request" class="btn btn-primary" disabled />
                        </form>
                    @endif
                @endif
            @endif
        </div>

        <!--Note: Begin-->
        <div class="notes" style="margin-top: 20px; margin-right: 60px;">
            <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
                <div class="m-accordion__item">
                    <div class="m-accordion__item-head collapsed" role="tab" data-toggle="collapse" href="#note-accordion-{{$list->listingContracts->listing_id}}" aria-expanded="false">
                        <span class="m-accordion__item-icon"><i class="fa flaticon-speech-bubble-1" style="font-size: 16px !important;"></i></span>
                        <span class="m-accordion__item-title">Notes</span>
                        <span class="m-accordion__item-mode"></span>
                    </div>
                    <div class="m-accordion__item-body collapse" id="note-accordion-{{ $list->listingContracts->listing_id }}" role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                        <div class="m-accordion__item-content">
                            <form method="post" action="{{url('lead-note-add')}}" class="m-form">
                                <input type="hidden" name="ad_id" value="{{ $list->listingContracts->listing_id }}">
                                <div class="form-group">
                                    {!! Form::textarea('lead_note_body', null, ['class'=>'form-control', 'rows' => 2,]  ) !!}
                                </div>
                                <input type="submit" value="Add Note" class="btn btn-primary btn-sm float-right">
                            </form>
                            <div class="clearfix"></div>

                            <div class="all-notes" style="padding-top: 2rem;">
                                @if(isset($list->listingContracts->leadNote) && count($list->listingContracts->leadNote))
                                    @foreach($list->listingContracts->leadNote as $note)
                                        <div class="single-note">
                                            <div class="m-separator m-separator--space m-separator--dashed"></div>
                                            <div class="note-body" style="text-align: left;">
                                                @if($note)
                                                    <p>{{ $note->lead_note_body }}</p>
                                                @endif
                                            </div>
                                            <div class="single-note-foot row" style="border-bottom:none; text-align: left;">
                                                <div class="col-sm-9">
                                                    <small>By: <strong>{{ $note->user->name }}</strong>&nbsp;&nbsp;|&nbsp;&nbsp;{{ $note->updated_at->format('d M Y' )}}</small>
                                                </div>
                                                <div class="col-sm-3">
                                                    <form method="post" action="{{ url('lead-note-delete') }}" class="m-form">
                                                        <input type="hidden" name="id" value="{{$note->id}}">
                                                        <button class='m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill float-right'
                                                                type='submit' value='Delete'
                                                                data-toggle='m-tooltip'
                                                                title='Delete Note'
                                                                data-original-title='Delete Note'>
                                                            <i class="flaticon-delete" style="font-size: 16px;"></i>
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Note: End-->
    </div>
    <!--right-->
    <div class="col-md-5 text-left">
        <div style="width: 100%;">
            <div class="pull-left">
                <strong>STATUS:&nbsp;&nbsp;</strong>
                @if(isset($list->listingContracts->prospect_contact_status->current_prospect_contact_status))
                    <span class="text-success font-weight-bold text-uppercase">
                        {{ \App\ProspectContactStatus::CS_DESCRIPTION[$list->listingContracts->prospect_contact_status->current_prospect_contact_status] }}
                    </span>
                    <span class="text-info font-weight-bold text-uppercase">
                        {{ date("M d y h:i", strtotime($list->listingContracts->prospect_contact_status->updated_at)) }}
                    </span>
                    <i class="fa fa-info-circle status-detail">
                        <span>&bull; {!! implode('<br>&bull; ',array_column($list->listingContracts->prospect_contact_status->prospectContactStatusOptionsSaved->toArray(), 'option_value')) !!}</span>
                    </i>
                @else
                @if($list->listingContracts->list_status == '0')
                        <span>Agreement Signed</span>
                    @elseif($list->listingContracts->list_status == '1')
                        <span class="text-success font-weight-bold">APPROVED</span>
                    @else
                        <span>Not Successful</span>
                    @endif
                @endif
            </div>
        </div>
        <div class="table-responsive" style="max-height:250px; overflow-x: auto; margin-top: 60px;">
            <table class="status-update">
                @if(isset($list->listingContracts->transactionReports) && count($list->listingContracts->transactionReports))
                    @foreach($list->listingContracts->transactionReports as $tr)
                        <tr>
                            @if($tr->ad_id)
                                @if($tr)
                                    <td>
                                        <small style="font-weight: 500">
                                            {{ date("M d y h:i", strtotime($tr->updated_at)) }}
                                        </small>
                                        <div style="display: inline-block;">{{ $tr->status }}</div>
                                    </td>
                                @endif
                            @endif
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td>
                            <small style="font-weight: 500">
                                {{ date("M d y h:i", strtotime($list->listingContracts->referralContactInfo->created_at)) }}
                            </small>
                            <div style="display:inline-block;">Reservation Successful</div>
                        </td>
                    </tr>
                @endif
                <tr>
                    <td>
                        <ul class="state_container">
                            <li>
                                <a class="circle-text prospect-contact-status"
                                   href="{{ route('reserved-leads-add-status') . "?lead_type={$list->category_type}&lead_id={$list->listingContracts->id}&contact_status=" . App\ProspectContactStatus::CS_CONTACTED_CUSTOMER }}">
                                    I CONTACTED<br> CUSTOMER
                                </a>
                            </li>
                            <li>
                                <a class="circle-text prospect-contact-status"
                                   href="{{ route('reserved-leads-add-status') . "?lead_type={$list->category_type}&lead_id={$list->listingContracts->id}&contact_status=" . App\ProspectContactStatus::CS_MET_CUSTOMER }}">
                                    I MET WITH<br> CUSTOMER
                                </a>
                            </li>
                            <li>
                                <a class="circle-text prospect-contact-status"
                                   href="{{ route('reserved-leads-add-status') . "?lead_type={$list->category_type}&lead_id={$list->listingContracts->id}&contact_status=" . App\ProspectContactStatus::CS_IN_ESCROM }}">
                                    WE ARE<br> WORKING ON<br> THE OFFER
                                </a>
                            </li>
                            <li>
                                <a class="circle-text prospect-contact-status"
                                   href="{{ route('reserved-leads-add-status') . "?lead_type={$list->category_type}&lead_id={$list->listingContracts->id}&contact_status=" . App\ProspectContactStatus::CS_CLOSING }}">
                                    WE ARE IN<br> CLISING!
                                </a>
                            </li>
                            <li>
                                <a class="circle-text prospect-contact-status"
                                   href="{{ route('reserved-leads-add-status') . "?lead_type={$list->category_type}&lead_id={$list->listingContracts->id}&contact_status=" . App\ProspectContactStatus::CS_LONGTERM_NUTURE }}">
                                    MOVED LEAD<br> TO LONG-TERM<br> NURTURE
                                </a>
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
