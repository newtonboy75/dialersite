@extends('tymbl.layouts.dashboard')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

  <!-- BEGIN: Subheader -->
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title ">{{$title}}</h3>
      </div>
    </div>
  </div>

  <!-- END: Subheader -->
  <div class="m-content">
    <!--begin::Portlet-->
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
              {{$title}}
            </h3>
          </div>
        </div>
        <a href="/dashboard/uploaded_leads_status/{{$file_id}}" class="float-right mt-sm-4">Back</a>
      </div>
      <div class="m-portlet__body">

        <!--begin::Section-->
        <div class="m-section">
          <div class="m-section__content">
            <div class="messages-wrapper">
              <div>ID: {{$file_status->id}}</div>
              <div>
                {{ Form::open(array('url' => '/dashboard/uploaded_leads_status')) }}
                <label class="label-sm">Date Added</label>
                <input class="form-control" disabled type="text" style="border:0px; background: #fff" id="date_added" value="{{ isset($date_added) ? $date_added : ''}}">
                <input type="hidden" name="date_added" value="{{ isset($date_added) ? $date_added : ''}}">
                <input type="hidden" name="file_id" value="{{ isset($file_id) ? $file_id : '' }}">
                <div>
                  <div class="modal-body">
                    <div class="row ">
                      <div class="col-sm-6">
                        <label class="label-sm">Status</label>
                        <select class="custom-select form-control" name="status">
                          <option value="New" {{ isset($file_status->status) && $file_status->status == 'New' ? 'selected' : ''}}>New</option>
                          <option value="Needs Follow-up" {{ isset($file_status->status) && $file_status->status == 'Needs Follow-up' ? 'selected' : ''}}>Needs Follow-up</option>
                          <option value="Scrubbed" {{ isset($file_status->status) && $file_status->status == 'Scrubbed' ? 'selected' : ''}}>Scrubbed</option>
                          <option value="Dead/Purged" {{ isset($file_status->status) && $file_status->status == 'Dead/Purged' ? 'selected' : ''}}>Dead/Purged</option>
                        </select>
                      </div>

                      <div class="col-sm-6">
                        <label>Seller</label>&nbsp;&nbsp;<input value="seller" {{isset($file_status->seller) && $file_status->seller == 'yes' ? 'checked' : ''}} type="radio" name="buyer_seller">
                        <label>Buyer</label>&nbsp;&nbsp;<input value="buyer" {{isset($file_status->buyer) && $file_status->buyer == 'yes' ? 'checked' : ''}} type="radio" name="buyer_seller">
                      </div>
                    </div>

                    <div class="row ">
                      <div class="col-sm-6">
                        <label class="label-sm">First Name</label>
                        <input class="form-control" type="text" name="first_name" id="first_name" value="{{ isset($file_status->first_name) ? $file_status->first_name : ''}}">
                      </div>

                      <div class="col-sm-6">
                        <label class="label-sm">Last Name</label>
                        <input class="form-control" type="text" name="last_name" id="last_name" value="{{ isset($file_status->last_name) ? $file_status->last_name : ''}}">
                      </div>
                    </div>

                    <div class="row ">
                      <div class="col-sm-6">
                        <label class="label-sm">Lead Source</label>
                        <input class="form-control" type="text" name="lead_source" id="lead_source" value="{{ isset($file_status->lead_source) ? $file_status->lead_source : ''}}">
                      </div>

                      <div class="col-sm-6">
                        <label class="label-sm">Email</label>
                        <input class="form-control" type="email" name="email" id="email" required value="{{ isset($file_status->email) ? $file_status->email : ''}}">
                      </div>
                    </div>

                    <div class="row ">
                      <div class="col-sm-6">
                        <label class="label-sm">Phone</label>
                        <input class="form-control" type="text" name="phone" id="phone" value="{{ isset($file_status->phone) ? $file_status->phone : ''}}">
                      </div>

                      <div class="col-sm-6">
                        <label class="label-sm">Address</label>
                        <input class="form-control" type="text" name="address" id="address" value="{{ isset($file_status->address) ? $file_status->address : '' }}">
                      </div>
                    </div>

                    <div class="row ">
                      <div class="col-sm-6">
                        <label class="label-sm">Original Notes</label>
                        <textarea class="form-control" name="notes_tags" id="original_notes">{{ isset($file_status->notes_tags) ? $file_status->notes_tags : '' }}</textarea>
                      </div>

                      <div class="col-sm-6">
                        <label class="label-sm">Prioritize after scrubbing?</label>
                        <select class="custom-select form-control" name="prioritize_scrub">
                          <option value="na" {{ isset($file_status->prioritize_scrub) && $file_status->prioritize_scrub == 'na' ? 'selected' : ''}}>Select option</option>
                          <option value="yes" {{ isset($file_status->prioritize_scrub) && $file_status->prioritize_scrub == 'yes' ? 'selected' : ''}}>Yes</option>
                          <option value="no" {{ isset($file_status->prioritize_scrub) && $file_status->prioritize_scrub == 'no' ? 'selected' : ''}}>No</option>
                        </select>
                      </div>
                    </div>

                    <p>&nbsp;</p>
                    <h5>Dials</h5>

                    <div class="row ">
                      <div class="col-sm-6">
                        <label class="label-sm">First Dial Date</label>
                        <input class="form-control" type="text" name="first_dial_date" id="first_dial_date" value="{{ isset($file_status->first_dial_date) ? $file_status->first_dial_date : ''}}">

                        <label class="label-sm">First Dial Outcome</label>
                        <select class="custom-select form-control" name="first_dial_outcome">
                          <option disabled selected value="na" {{ isset($file_status->first_dial_outcome) && $file_status->first_dial_outcome == 'na' ? 'selected' : ''}}>Select option</option>
                          <option value="Answered" {{ isset($file_status->first_dial_outcome) && $file_status->first_dial_outcome == 'Answered' ? 'selected' : ''}}>Answered</option>
                          <option value="VM" {{ isset($file_status->first_dial_outcome) && $file_status->first_dial_outcome == 'VM' ? 'selected' : ''}}>VM</option>
                          <option value="Wrong Number" {{ isset($file_status->first_dial_outcome) && $file_status->first_dial_outcome == 'Wrong Number' ? 'selected' : ''}}>Wrong Number</option>
                          <option value="No Answer-No VM" {{ isset($file_status->first_dial_outcome) && $file_status->first_dial_outcome == 'No Answer-No VM' ? 'selected' : ''}}>No Answer/No VM</option>
                        </select>

                        <label class="label-sm">First Dial Notes</label>
                        <textarea class="form-control" name="first_dial_notes">{{ isset($file_status->first_dial_notes) ? $file_status->first_dial_notes : ''}}</textarea>

                      </div>

                      <div class="col-sm-6">
                        <label class="label-sm">Second Dial Date</label>
                        <input class="form-control" type="text" name="second_dial_date" id="second_dial_date" value="{{ isset($file_status->second_dial_date) ? $file_status->second_dial_date : ''}}">

                        <label class="label-sm">Second Dial Outcome</label>
                        <select class="custom-select form-control" name="second_dial_outcome">
                          <option value="na" disabled selected {{ isset($file_status->second_dial_outcome) && $file_status->second_dial_outcome == 'na' ? 'selected' : ''}}>Select option</option>
                          <option value="Answered" {{ isset($file_status->second_dial_outcome) && $file_status->second_dial_outcome == 'Answered' ? 'selected' : ''}}>Answered</option>
                          <option value="VM" {{ isset($file_status->second_dial_outcome) && $file_status->second_dial_outcome == 'VM' ? 'selected' : ''}}>VM</option>
                          <option value="Wrong Number" {{ isset($file_status->second_dial_outcome) && $file_status->second_dial_outcome == 'Wrong Number' ? 'selected' : ''}}>Wrong Number</option>
                          <option value="No Answer-No VM" {{ isset($file_status->second_dial_outcome) && $file_status->second_dial_outcome == 'No Answer-No VM' ? 'selected' : ''}}>No Answer/No VM</option>
                        </select>

                        <label class="label-sm">Second Dial Notes</label>
                        <textarea class="form-control" name="second_dial_notes">{{ isset($file_status->second_dial_notes) ? $file_status->second_dial_notes : ''}}</textarea>

                      </div>
                    </div>

                    <p>&nbsp;</p>
                    <h5>SMS / EMAIL</h5>

                    <div class="row ">
                      <div class="col-sm-6">
                        <label class="label-sm">SMS Date</label>
                        <input class="form-control" type="text" name="sms_date" id="sms_date" value="{{ isset($file_status->sms_date) ? $file_status->sms_date : ''}}">

                        <label class="label-sm">SMS Outcome</label>
                        <select class="custom-select form-control" name="sms_outcome">
                          <option disabled selected value="na" {{ isset($file_status->sms_outcome) && $file_status->sms_outcome == 'na' ? 'selected' : ''}}>Select option</option>
                          <option value="Answered" {{ isset($file_status->sms_outcome) && $file_status->sms_outcome == 'Answered' ? 'selected' : ''}}>Answered</option>
                          <option value="Bounced" {{ isset($file_status->sms_outcome) && $file_status->sms_outcome == 'Bounced' ? 'selected' : ''}}>Bounced</option>
                          <option value="No Answer" {{ isset($file_status->sms_outcome) && $file_status->sms_outcome == 'No Answer' ? 'selected' : ''}}>No Answer</option>
                        </select>

                        <label class="label-sm">SMS Notes</label>
                        <textarea class="form-control" name="sms_notes">{{ isset($file_status->sms_notes) ? $file_status->sms_notes : ''}}</textarea>

                      </div>

                      <div class="col-sm-6">
                        <label class="label-sm">Email Date</label>
                        <input class="form-control" type="text" name="email_date" id="email_date" value="{{ isset($file_status->email_date) ? $file_status->email_date : ''}}">

                        <label class="label-sm">Email Outcome</label>
                        <select class="custom-select form-control" name="email_outcome">
                          <option value="na" disabled selected {{ isset($file_status->email_outcome) && $file_status->email_outcome == 'na' ? 'selected' : ''}}>Select option</option>
                          <option value="Answered" {{ isset($file_status->email_outcome) && $file_status->email_outcome == 'Answered' ? 'selected' : ''}}>Answered</option>
                          <option value="Bounced" {{ isset($file_status->email_outcome) && $file_status->email_outcome == 'Bounced' ? 'selected' : ''}}>Bounced</option>
                          <option value="No Answer" {{ isset($file_status->email_outcome) && $file_status->email_outcome == 'No Answer' ? 'selected' : ''}}>No Answer</option>
                          <option value="Out-of-office" {{ isset($file_status->email_outcome) && $file_status->email_outcome == 'Out-of-office' ? 'selected' : ''}}>Out-of-Office</option>
                        </select>

                        <label class="label-sm">Email Notes</label>
                        <textarea class="form-control" name="email_notes">{{ isset($file_status->email_notes) ? $file_status->email_notes : ''}}</textarea>

                      </div>

                    </div>

                  </div>
                  <p>&nbsp;</p>

                  <div class="modal-footer">

                    <input type="hidden" name="cid" value="{{request()->segment(3)}}">
                    <input class="btn btn-primary" type="submit" value="Save changes">

                    <div style="position: absolute; left: 340px !important; text-align:left"><a class="pull-left" href="javascript:;" rel="{{request()->segment(3)}}" id="del-lead-entry">Delete lead</a></div>
                  </div>
                  {{ Form::close() }}
                </div>
              </div>
            </div>
          </div>
        </div>

        <!--end::Section-->
      </div>

      <!--end::Form-->
    </div>

    <!--end::Portlet-->
  </div>
</div>

@endsection
