@extends('tymbl.layouts.dashboard')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">Pages</h3>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->
					<div class="m-content">
            						<!--begin::Portlet-->
						<div class="m-portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<h3 class="m-portlet__head-text">
												Pages
											</h3>
										</div>
									</div>
								</div>
								<div class="m-portlet__body">

									<!--begin::Section-->
									<div class="m-section">
										<div class="m-section__content">
                      <div class="col-xs-12">

                          {{ Form::open(['class' => 'form-horizontal']) }}

                          <div class="form-group {{ $errors->has('title')? 'has-error':'' }}">
                              <div class="col-sm-12">
                                  <input type="text" class="form-control" id="title" value="{{ old('title') }}" name="title" placeholder="@lang('app.title')">
                                  {!! $errors->has('title')? '<p class="help-block">'.$errors->first('title').'</p>':'' !!}
                              </div>
                          </div>


                          <div class="form-group {{ $errors->has('post_content')? 'has-error':'' }}">
                              <div class="col-sm-12">
                                  <textarea name="post_content" id="post_content" class="form-control summerNoteEditor" rows="6">{{ old('post_content') }}</textarea>
                                  {!! $errors->has('post_content')? '<p class="help-block">'.$errors->first('post_content').'</p>':'' !!}
                              </div>
                          </div>


                          <div class="form-group">
                              <div class="col-md-6">
                                  <label for="show_in_header_menu" class="checkbox-inline">
                                      <input type="checkbox" value="1" id="show_in_header_menu" name="show_in_header_menu">
                                      @lang('app.show_in_header_menu')
                                  </label>
                              </div>
                              <div class="col-md-6">
                                  <label for="show_in_footer_menu" class="checkbox-inline">
                                      <input type="checkbox" value="1" id="show_in_footer_menu" name="show_in_footer_menu">
                                      @lang('app.show_in_footer_menu')
                                  </label>
                              </div>
                          </div>


                          <div class="form-group">
                              <div class="col-sm-9">
                                  <button type="submit" class="btn btn-primary">@lang('app.save_new_page')</button>
                              </div>
                          </div>
                          {{ Form::close() }}

                      </div>
										</div>
									</div>

									<!--end::Section-->
								</div>

								<!--end::Form-->
							</div>

							<!--end::Portlet-->
					</div>
				</div>
@endsection
