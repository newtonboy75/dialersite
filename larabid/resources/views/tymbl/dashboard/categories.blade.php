@extends('tymbl.layouts.dashboard')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">Page Edit</h3>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->
					<div class="m-content">
            						<!--begin::Portlet-->
						<div class="m-portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<h3 class="m-portlet__head-text">
												Categories
											</h3>

										</div>
									</div>
								</div>
								<div class="m-portlet__body">

									<!--begin::Section-->
									<div class="m-section">
										<div class="m-section__content">
                      {{ Form::open(['class' => 'form-horizontal']) }}

                      <div class="form-group {{ $errors->has('category_name')? 'has-error':'' }}">
                          <label for="category_name" class="col-sm-4 control-label">@lang('app.category_name')</label>
                          <div class="col-sm-8">
                              <input type="text" class="form-control" id="category_name" value="{{ old('category_name') }}" name="category_name" placeholder="@lang('app.category_name')">
                              {!! $errors->has('category_name')? '<p class="help-block">'.$errors->first('category_name').'</p>':'' !!}
                          </div>
                      </div>


                      <div class="form-group {{ $errors->has('description')? 'has-error':'' }}">
                          <label for="description" class="col-sm-4 control-label">@lang('app.description')</label>
                          <div class="col-sm-8">
                              <textarea name="description" id="description" class="form-control" rows="6">{{ old('description') }}</textarea>
                              {!! $errors->has('description')? '<p class="help-block">'.$errors->first('description').'</p>':'' !!}
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="col-sm-offset-4 col-sm-8">
                              <button type="submit" class="btn btn-primary">@lang('app.save_new_category')</button>
                          </div>
                      </div>
                      {{ Form::close() }}

                      <div class="col-xs-12">
                          <table class="table table-bordered">
                              <tr>
                                  <th>@lang('app.category_name') (@lang('app.total_ads')) </th>
                              </tr>
                              @foreach($categories as $category)
                                  <tr>
                                      <td>
                                          <div class="clearfix">
                                              <strong>{{ $category->category_name }} ({{ $category->product_count() }})</strong>
                                              <span class="pull-right">

                                              <a href="{{ route('edit_categories', $category->id) }}" class="btn btn-info btn-xs"><i class="far fa-edit"></i> </a>
                                              <a href="javascript:;" class="btn btn-danger btn-xs" data-id="{{ $category->id }}"><i class="fa fa-trash"></i> </a>
                                              </span>

                                          </div>

                                      </td>
                                  </tr>
                              @endforeach
                          </table>
                      </div>
										</div>
									</div>

									<!--end::Section-->
								</div>

								<!--end::Form-->
							</div>

							<!--end::Portlet-->
					</div>
				</div>
        
@endsection
