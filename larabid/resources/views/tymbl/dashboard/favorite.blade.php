@extends('tymbl.layouts.dashboard')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

  <!-- BEGIN: Subheader -->
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title ">Favourite Items</h3>
      </div>
    </div>
  </div>

  <!-- END: Subheader -->
  <div class="m-content">

    <!--begin::Portlet-->
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
              Favourite Items
            </h3>
          </div>
        </div>
      </div>
      <div class="m-portlet__body">
@include('admin.flash_msg')
        <!--begin::Section-->
        <div class="m-section">
          <div class="m-section__content">
            <div class="responsive-table-div">

              @if($ads->total() > 0)
              @foreach($ads as $ad)
              <div class="row" id="fav-holder-{{$ad->id}}">
                <div class="col-sm-4">
                  <img src="{{ media_url($ad->feature_img) }}" alt="item" class="img-fluid img-thumbnail thumb">
                </div>
                <div class="col-sm-4">
                  <div class="m-list-search">
                    <div class="m-list-search__results">
                      <a href="{{  route('single_ad', [$ad->id, $ad->slug]) }}" class="m-list-search__result-item">
                        <span class="m-list-search__result-item-icon"><i class="flaticon-home-2 m--font-warning"></i></span>
                        <span class="m-list-search__result-item-text ">{{ safe_output($ad->title) }}</span>
                      </a>
                      <div class="m-list-search__result-item">
                        <span class="m-list-search__result-item-icon"><i class="flaticon-map-location m--font-success"></i></span>
                        <span class="m-list-search__result-item-text">{{ safe_output($ad->address) }}</span>
                      </div>
                      <div class="m-list-search__result-item">
                        <span class="m-list-search__result-item-icon"><i class="flaticon-clock-1 m--font-info"></i></span>
                        <span class="m-list-search__result-item-text">{{ $ad->created_at->format('M d Y') }}</span>
                      </div>

                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <a href="#" rel="{{ $ad->id }}" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-container="body" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Delete" id="deleteFav" data-slug="{{ $ad->slug }}">
                    <i class="fa flaticon-delete"></i>
                  </a>
                </div>
              </div>
              @endforeach
              @endif
            </div>
          </div>
        </div>

        <!--end::Section-->
      </div>

      <!--end::Form-->
    </div>

    <!--end::Portlet-->
  </div>
</div>

@endsection
