<form id="sms-form">
    {{ csrf_field() }}
    <input name="phone_number" type="hidden" value="{{ $phone_number }}">
    <div class="form-group" id="default-template">
        <label class="text-uppercase"><strong>Default template message</strong></label>
        <p>{{ $message }}</p>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" value="on" id="use-default-template" name="use_default_template" checked> Use default template
        </label>
    </div>
    <div class="form-group" style="display: none" id="sms-body">
        <textarea name="message" class="form-control" rows="3" placeholder="Message text...">{{ $message }}</textarea>
    </div>
    <div id="partner-realtors-container">
        <div class="form-group" id="default-partner-realtors">
            <label class="text-uppercase"><strong>Default Partner Realtors</strong></label>
            <p>Partner Realtors</p>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" value="on" id="use-default-partner-realtors" name="use_default_partner_realtors" checked> Use default Partner Realtors
            </label>
        </div>
        <div class="form-group" style="display: none" id="partner-realtors">
            <input name="partner_realtors" class="form-control" value="{{ $partner_realtors }}">
        </div>
    </div>
</form>
