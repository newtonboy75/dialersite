@extends('tymbl.layouts.dashboard')
@section('css')
  <style>
    i.status-detail:hover {
      cursor: help;
      position: relative;
    }
    i.status-detail span {
      display: none;
      white-space: normal;
      font-size: 12px;
    }
    i.status-detail:hover span {
      box-shadow: 0 0 10px;
      border-radius: 5px;
      border: #c0c0c0 1px dotted;
      padding: 10px;
      display: block;
      z-index: 100;
      background: #ffffff;
      margin: 10px;
      width: 250px;
      position: absolute;
      top: 10px;
      text-decoration: none;
    }
    </style>
@endsection
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

  <!-- BEGIN: Subheader -->
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title ">{{$title}}</h3>
      </div>
    </div>
  </div>

  <!-- END: Subheader -->
  <div class="m-content">
    <!--begin::Portlet-->
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{$title}}</h3>
          </div>
        </div>
      </div>
      <div class="m-portlet__body">

        <!--begin::Section-->
        <div class="m-section">
          <div class="m-section__content">
            <div class="messages-wrapper table-responsive">
              <table class="table jdata display nowrap" id="jDataTable">
                <thead>
                  <tr>
                    <th style="width: 200px;">&nbsp;</th>
                    <th>TRANSACTION DETAILS</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
        <!--end::Section-->
      </div>

      <!--end::Form-->
    </div>

    <!--end::Portlet-->
  </div>
</div>

@endsection
@section('js')
  <script>
    $(document).on('click', '#send-mail', function (e) {
      e.preventDefault();
      var id = $(this).data('id');
      $.ajax({
        url: '{{ route('send-mail-reservation-info') }}',
        type: "post",
        data: {_token: '{{ csrf_token() }}', id: id},
        success: function (data) {
          var obj = JSON.parse(data);
          if (obj.status == 'ok') {
            toastr.success(obj.message, 'Success!', {closeButton: true});
          } else {
            toastr.error(obj.message, 'Error!', {closeButton: true})
          }
        },
        error: function (data) {
          console.log(data);
        }
      });
    });

    $(document).on('click', '#follow_up', function (e) {
      var id = $(this).data('id');
      var a = $(this);
      $.ajax({
        url: '{{ route('reserved-leads-follow-up') }}',
        type: "post",
        data: {_token: '{{ csrf_token() }}', id: id},
        success: function (data) {
          var obj = JSON.parse(data);
          if (obj.status == 'ok') {
            toastr.success(obj.message, 'Success!', {closeButton: true});
            a.text(obj.value);
          } else {
            toastr.error(obj.message, 'Error!', {closeButton: true})
          }
        },
        error: function (data) {
          toastr.error(data, 'Error!', {closeButton: true})
        }
      });
    });
  </script>
@endsection
