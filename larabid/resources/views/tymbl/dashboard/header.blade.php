<header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
  <div class="m-container m-container--fluid m-container--full-height">
    <div class="m-stack m-stack--ver m-stack--desktop">

      <!-- BEGIN: Brand -->
      <div class="m-stack__item m-brand  m-brand--skin-dark ">
        <div class="m-stack m-stack--ver m-stack--general">
          <div class="m-stack__item m-stack__item--middle m-brand__logo">
            <a href="/" class="m-brand__logo-wrapper">
              <img alt="" src="{{ asset('assets/css/tymbl/dashboard/demo/media/img/logo/logo.png') }}" />
            </a>
          </div>
          <div class="m-stack__item m-stack__item--middle m-brand__tools">

            <!-- BEGIN: Left Aside Minimize Toggle -->
            <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block  ">
              <span></span>
            </a>

            <!-- END -->

            <!-- BEGIN: Responsive Aside Left Menu Toggler -->
            <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
              <span></span>
            </a>

            <!-- END -->

            <!-- BEGIN: Responsive Header Menu Toggler -->
            <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
              <span></span>
            </a>

            <!-- END -->

            <!-- BEGIN: Topbar Toggler -->
            <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
              <i class="flaticon-more"></i>
            </a>

            <!-- BEGIN: Topbar Toggler -->
          </div>
        </div>
      </div>

      <!-- END: Brand -->
      <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">

        <!-- BEGIN: Horizontal Menu -->
        <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
        <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-dark m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark ">
          <ul class="m-menu__nav ">
            <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="false"><a href="/" class="m-menu__link" title="Back to Tymbl"><i class="m-menu__link-icon flaticon-home"></i><span class="m-menu__link-text">Back Home</span></a>
            </li>

            <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="false"><a href="{{ route('create_ad') }}" class="m-menu__link" title="Create a lead"><i
                 class="m-menu__link-icon flaticon-file"></i><span class="m-menu__link-text">Place New Lead</span></a></li>
          </ul>
        </div>

        <!-- END: Horizontal Menu -->

        <!-- BEGIN: Topbar -->
        <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
          <div class="m-stack__item m-topbar__nav-wrapper">
            <ul class="m-topbar__nav m-nav m-nav--inline">
              <li class="m-nav__item m-topbar__notifications m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center 	m-dropdown--mobile-full-width" m-dropdown-toggle="click" m-dropdown-persistent="1">
                <a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">
                  <span class="m-nav__link-icon">
                    <span class="m-nav__link-icon-wrapper"><i class="flaticon-alarm"></i></span>
                    <span class="m-nav__link-badge m-badge m-badge--danger" id="num_messages"></span>
                  </span>
                </a>
                <div class="m-dropdown__wrapper">
                  <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                  <div class="m-dropdown__inner">
                     <div class="m-dropdown__body">
                       <h5>New Messages</h5>
                      <div class="m-dropdown__content">
                        <ul id="messages-content">
                        </ul>
                        <br>
                        <a id="read-more-msg" href="{{ route('messages') }}">Read all</a>
                      </div>
                    </div>
                  </div>
                </div>
              </li>

              <li class="m-nav__item m-topbar__user-profile  m-dropdown m-dropdown--medium m-dropdown--arrow  m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
                <a href="#" class="m-nav__link m-dropdown__toggle">
                  <span class="m-topbar__userpic">

                      <img src="{{ auth()->user()->get_gravatar() !== null ? auth()->user()->get_gravatar() : '/uploads/avatar/gravatar-default.png' }}" class="m--img-rounded m--marginless" alt=""
                  </span>
                  <span class="m-nav__link-icon m-topbar__usericon  m--hide">
                    <span class="m-nav__link-icon-wrapper"><i class="flaticon-user-ok"></i></span>
                  </span>
                  <span class="m-topbar__username m--hide">Nick</span>
                </a>
                <div class="m-dropdown__wrapper">
                  <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                  <div class="m-dropdown__inner">
                    <div class="m-dropdown__header m--align-center">
                      <div class="m-card-user m-card-user--skin-light">
                        <div class="m-card-user__pic">
                            <img src="{{ auth()->user()->get_gravatar() !== null ? auth()->user()->get_gravatar() : '/uploads/avatar/gravatar-default.png' }}" class="m--img-rounded m--marginless"
                        </div>
                        <div class="m-card-user__details">
                          <span class="m-card-user__name m--font-weight-500">{{ auth()->user()->name }}</span>
                          <a href="" class="m-card-user__email m--font-weight-300 m-link">{{ auth()->user()->email }}</a>
                        </div>
                      </div>
                    </div>
                    <div class="m-dropdown__body">
                      <div class="m-dropdown__content">
                        <ul class="m-nav m-nav--skin-light">
                          <li class="m-nav__section m--hide">
                            <span class="m-nav__section-text">Section</span>
                          </li>

                          <li class="m-nav__item ">
                            <a href="{{ route('profile') }}" class="m-nav__links b-left">
                              <i class="m-nav__link-icon flaticon-profile-1"></i>
                              <span class="m-nav__link-text">My Profile</span>
                            </a>
                            <p>&nbsp;</p>
                            <a href="{{ route('messages') }}" class="m-nav__links b-left">
                              <i class="m-nav__link-icon flaticon-chat-1"></i>
                              <span class="m-nav__link-text">Messages</span>
                            </a>
                          </li>
                          <li class="m-nav__separator m-nav__separator--fit">
                          </li>
                          <li class="m-nav__separator m-nav__separator--fit">
                          </li>
                          <li class="m-nav__item">
                            <p>&nbsp;</p><p>&nbsp;</p>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                            </form>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>

        <!-- END: Topbar -->
      </div>
    </div>
  </div>
</header>
