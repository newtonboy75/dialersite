<div class="modal-dialog">
    <div class="modal-content">
        <!-- Modal title -->
        <div class="modal-header">
            <h4 class="modal-title" style="text-transform: uppercase">{{ $modal_title ?? 'Modal title' }}</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
    <!-- < Modal main content> -->
        <div class="modal-body">
            <form action="{{ route('reserved-leads-update-status') }}" method="post" id="contact-status-form">
                {{ csrf_field() }}
                <input type="hidden" name="lead_type" value="{{ $lead_type }}">
                <input type="hidden" name="reserved_lead_id" value="{{ $lead_id }}">
                <input type="hidden" name="current_prospect_contact_status" value="{{ $contact_status }}">
                <div class="form-group">
                    <label>Enter date</label>
                    <input type="text" id="datapicker" name="date" value="{{ \Carbon\Carbon::now()->format('m/d/Y') }}" class="form-control">
                </div>
                @php $counter = 0; @endphp
                @if(isset($options['select']))
                    @foreach($options['select'] as $option)
                        <div class="form-group">
                            <input name="options[{{ ++$counter }}][id]" class="form-control" type="hidden" value="{{ $option->id }}">
                            <select name="options[{{ $counter }}][value]" class="form-control" size="1">
                                @foreach(array_map('trim', explode(',', $option->option_name)) as $val)
                                    <option value="{{ $val }}" {{ (isset($savedOptions[$option->id]) && $savedOptions[$option->id]->option_value == $val) ? 'selected' : '' }}>
                                        {{ $val }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    @endforeach
                @endif
                @if(isset($options['radio']))
                    @foreach($options['radio'] as $option)
                        <div class="form-group">
                            <input name="options[{{ ++$counter }}][id]" type="hidden" value="{{ $option->id }}">
                            <input name="options[{{ $counter }}][value]" type="radio" value="{{ $option->option_name }}" {{ isset($savedOptions[$option->id]) ? 'checked' : '' }}> {{ $option->option_name }}
                        </div>
                    @endforeach
                @endif
                @if(isset($options['checkbox']))
                    @foreach($options['checkbox'] as $option)
                        <div class="form-group">
                            <input name="options[{{ ++$counter }}][id]" type="hidden" value="{{ $option->id }}">
                            <input name="options[{{ $counter }}][value]" type="checkbox" value="{{ $option->option_name }}" {{ isset($savedOptions[$option->id]) ? 'checked' : '' }}> {{ $option->option_name }}
                        </div>
                    @endforeach
                @endif
                @if(isset($options['text']))
                    @foreach($options['text'] as $option)
                        <div class="form-group">
                            <label>{{ $option->option_name }}:</label>
                            <input name="options[{{ ++$counter }}][id]" type="hidden" value="{{ $option->id }}">
                            <input name="options[{{ $counter }}][value]" class="form-control" type="text" value="{{ isset($savedOptions[$option->id]) ? $savedOptions[$option->id]->option_value : '' }}">
                        </div>
                    @endforeach
                @endif
            </form>
        </div>
    <!-- Modal foter -->
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button id="save-contact-status" type="button" class="btn btn-primary">Save</button>
        </div>
    </div>
</div>

<script src="{{ asset('assets/plugins/bootstrap-datepicker-1.6.4/js/bootstrap-datepicker.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#datapicker').datepicker({
            format: "mm/dd/yyyy",
            todayHighlight: true,
            autoclose: true,
            minDate: '-90',
            maxDate: '0',
            defaultDate: new Date(),
        });
    });

    $('#datapicker').on('change', function () {
        var strindDate = $(this).val();
        if (new Date(strindDate) > new Date()) {

        }
    });

</script>