@extends('tymbl.layouts.app')
@section('content')
<section class="page-header2">
  <div class="container smaller-font">
  <i class="fas fa-angle-left"></i>&nbsp;&nbsp;<a href="javascript:history.back()">Back to lead details</a>
  </div>

</section>
<div class="container">
  <div class="row">
    <div class="col-sm-8">
      <div>
        <div class="panel panel-default center-block">

            <div class="panel-body" style="padding: 30px">
              <h5 class="payment-misc">@lang('app.review_your_order') </h5>
                <div class="row" style="font-size: 1.1em; font-weight:400; margin-top: 30px; padding:12px;">
                  <div class="col-sm-8">Reservation fee:</div>
                  <div class="col-sm-4" style="text-align:right; color: #000;">{{ $list_country = $ad->country_id == '231' ? 'USD$' : 'CAD$' }}  {{number_format($ad->escrow_amount, 2, '.', ',')}}</div>
                </div>
                <div class="row" style="font-size: 1.1em; font-weight:400; border-top: 1px solid #f0f0f0; padding:12px;">
                  <div class="col-sm-8">Total:</div>
                  <div class="col-sm-4" style="text-align:right; color: #000;">{{ $list_country = $ad->country_id == '231' ? 'USD$' : 'CAD$' }}  {{number_format($ad->escrow_amount, 2, '.', ',')}}</div>
                </div>

                <div class="row">
                  <div class="col-sm-6">
                    <p>&nbsp;</p><p>&nbsp;</p>
                    <img style="width:30%;" src="{{url('/assets/img/tymbl/logo-paypal.svg')}}" /></div>
                  <div class="col-sm-6">
                    <form action="/payment/paid" method="post">
                      {{ csrf_field() }}
                      <input name="amount" value="{{$amount}}" type="hidden">
                      <input name="transaction_id" value="{{$transaction_id}}" type="hidden">
                      <input name="ad_name" value="{{$ad->title}}" type="hidden">
                      <input name="ad_id" value="{{$ad->id}}" type="hidden">
                        <p>&nbsp;</p>
                        <button class="btn btn-lg btn-primary pp-button"><i class="fab fa-paypal"></i>&nbsp;Pay with Paypal</button>
                        <div style="font-size: 11px">*When the transaction is successful, please note that you will be charged 1% - the escrow transfer fee</div>
                    </form>
                  </div>

                </div>
            </div>
        </div>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="payment-details">
      <p class="payment-misc">Lead</p>
      <p class="misc-headers">{{$ad->title}}</p>
      <div>
        {{ $ad->address }}
      </div>
      <div style="display:none;">
        <span class="listing-price">{{ $list_country = $ad->country_id == '231' ? 'USD$' : 'CAD$' }}  {{number_format($ad->escrow_amount, 2, '.', ',')}}</span><sup class="sup-info"><i class="far fa-question-circle"></i></sup>
      </div>
      <div style="margin-top: 20px;">


        @if(isset($media->media_name) && $media->media_name != 'NULL')
            <img src="/uploads/images/{{$media->media_name}}" class="d-block w-100" />
          @else
            @if($ad->category_type == "selling")
              <img src="{{ asset('assets/img/tymbl/house.png') }}" class="d-block w-100" />
            @else
              <img src="{{ asset('assets/img/tymbl/person.png') }}" class="d-block w-100" />
            @endif
          @endif

      </div>
    </div>
      <div class="payment-details">
        <h5 class="payment-misc">Seller</h5>
        <div><img class="mr-3" style="width:77px; height:75px;" src="{{ $ad->user->get_gravatar() }}" alt="Generic placeholder image">&nbsp;&nbsp;{{$ad->seller_name}}</div>
      </div>
    </div>
  </div>
</div>
<p>&nbsp;</p><p>&nbsp;</p>
@endsection
