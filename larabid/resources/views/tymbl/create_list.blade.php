@extends('tymbl.layouts.app')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('content')
<section class="page-header">
  <h1>Place new lead</h1>
</section>
<main class="post-flow">
  <div class="container">
    @if( ! \Auth::check())
    <div class="alert alert-info no-login-info">
      <div><i class="fa fa-info-circle"></i> @lang('app.no_login_info')</div>
    </div>
    @endif

    <div class="row justify-content-md-center">
      <div class="col-lg-9">
        {{ Form::open(['id'=>'adsPostForm', 'class' => 'form-horizontal', 'files' => true]) }}
        <section class="post-sec post-info">
          <div style="border-bottom: 1px solid #f0f0f0;">
            <h2 style="border: none !important;">INFO</h2>

            @if(\Auth::user()->user_type == 'admin')
            <div class="form-check form-check-inline text-bold float-right" style="margin-top: -30px;">
            <input class="form-check-input" name="is_vip" type="checkbox" id="is_vip">
            <label class="form-check-label" for="lookingToBuy">Mark as VIP&nbsp;&nbsp;<i class="fas fa-eye"></i></label>
            </div>
            @endif

          </div>


          <div class="form-group row" style="margin-top: 30px; margin-right: 0px; margin-left:0px;">
            <label class="col-sm-2 col-form-label p-sm" style="margin-top: -8px !important;">Prospect <span class="required-field">*</span></label>
            <div class="btn-group2 col-sm-10" role="group">
              <!-- <button type="button" class="btn btn-primary btn-lg btn">Looking to buy</button>
              <button type="button" class="btn btn-outline-primary btn-lg btn">Looking to sell</button> -->
              @php
              foreach($categories as $category){
                if($category->category_type == 'buying'){
                  $buying[]= array('category_type' => $category->category_type, 'id' => $category->id, 'name' => $category->category_name);
                }else{
                  $selling[]= array('category_type' => $category->category_type, 'id' => $category->id, 'name' => $category->category_name);
                }
              }
              @endphp
              <div class="form-check form-check-inline text-bold margin20">
                <input class="form-check-input" data-msg="Prospect is required" name="cat_type" type="radio" id="lookingToBuy" value="ltb" required {{ old('cat_type') == 'ltb' ? 'checked' : '' }}>

                <label class="form-check-label" for="lookingToBuy">Looking to buy&nbsp;&nbsp;<i style="font-size: 13px;" class="fas fa-user"></i> </label>
                <div id="ltp">&nbsp;</div>
              </div>
              <div class="form-check form-check-inline text-bold">
                <input class="form-check-input" name="cat_type" type="radio" id="lookingToSell" value="lts" {{ old('cat_type') == 'lts' ? 'checked' : '' }}>
                <label class="form-check-label" for="lookingToSell">Looking to sell&nbsp;&nbsp;<i style="font-size: 13px;" class="fas fa-home"></i> </label>
                <div id="ltp">&nbsp;</div>
              </div>

              <div class="form-check form-check-inline text-bold lbl-sm">
                <input class="form-check-input" name="cat_type" type="radio" id="notSure" value="ns" {{ old('cat_type') == 'ns' ? 'checked' : '' }}>
                <label class="form-check-label" for="notSure">Not sure </label>
                <div id="ltp">&nbsp;</div>
              </div>
            </div>
          </div>

          @php
          $style='none';
          if(old('cat_type')){
            $style = 'block';
          }
          $dstyle = 'none';
          if(old('cat_type_q')){
            $dstyle = 'block';
          }
          @endphp
          <div class="form-group row" id="qq" style="display:{{$style}}">
            <label for="refEscrow" class="col-sm-2 col-form-label" >
            </label>

            <span class="q-text text-bold">Have you qualified this lead?</span>
            <div class="form-check form-check-inline text-bold lbl-sm">
              <input required  data-msg="Let us know if this lead has been qualified" class="form-check-input" name="cat_type_q" type="radio" value="yes" id="ns_qq" {{ old('cat_type_q') == 'yes' ? 'checked' : '' }}>
              <label class="form-check-label" for="notSure">Yes</label>
              <div id="ltq">&nbsp;</div>
            </div>

            <div class="form-check form-check-inline text-bold lbl-sm">
              <input class="form-check-input" name="cat_type_q" id="ns_qq" value="no" type="radio" {{ old('cat_type_q') == 'no' ? 'checked' : '' }}>
              <label class="form-check-label" for="notSure">No</label>
              <div id="ltq">&nbsp;</div>
            </div>
          </div>

          <div class="row" id="dte-list" style="display:{{$dstyle}}">
            <label class="col-sm-2 col-form-label">&nbsp;</label>
            <div class="text-bold lbl-sm col-sm-8">&nbsp;&nbsp;
              Approx. date of last contact &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input value="{{old('date_contacted_qualified')}}" name="date_contacted_qualified" type="text" id="date_list" data-msg="Let us know when this lead qualified">
            </div>
          </div>

          <div class="form-group row" style="margin-top:40px !important;">
            <label for="refEscrow" class="col-sm-2 col-form-label" >Referral Fee%&nbsp;<span class="required-field">*</span>
            </label>
            <div class="col-sm-10">
              <div class="sup-info3" data-toggle="tooltip" title="" data-original-title=""><i class="fas fa-info-circle"></i> ABOUT REFRERRAL FEE</div>
              <input required data-msg="Referral fee is required" type="text" min="0" class="form-control" id="refFee" value="{{ old('referral_fee') ? old('referral_fee') : '25%'  }}" name="referral_fee">

              {!! $errors->has('referral_fee')? '<p class="help-block">'.$errors->first('referral_fee').'</p>':'' !!}
            </div>
          </div>

          <div class="form-group row"id="listing-type">
            <label for="type" class="col-sm-2 col-form-label" id="label-type">Type</label>
            <div class="col-sm-10"  style="width: 100%;">
              <select class="custom-select form-control" id="sub-category" name="category">
                @foreach($buying as $buy)
                <option value="{{ $buy['id'] }}"
                {{ old('category') == $buy['id'] ? 'selected': $buy['id'] == '6' ? 'selected' : '' }}  data-category-type="{{$buy['category_type']}}">{{ $buy['name'] }}
              </option>
              @endforeach
              </select>
            </div>
            {!! $errors->has('category')? '<p class="help-block">'.$errors->first('category').'</p>':'' !!}

        </div>
        <div class="form-group row">
          <label for="title" class="col-sm-2 col-form-label">Title <span class="required-field">*</span></label>
          <div class="col-sm-10 {{ $errors->has('listing_title')? 'has-error':'' }}">
            <input type="text" class="form-control" id="title" name="listing_title" data-msg="Title is required" value="{{ old('listing_title') }}" required>
            <small id="passwordHelpBlock" class="form-text text-muted">Ideally 70-100 characters</small>
            {!! $errors->has('listing_title')? '<p class="help-block">'.$errors->first('listing_title').'</p>':'' !!}
          </div>
        </div>

        <div class="form-group row">
          <label for="" class="col-sm-2 col-form-label">Summary</label>
          <div class="col-sm-10">
            <div class="row">
              <div class="col-sm-4">
                <div class="input-group">
                  <input type="number" min="0" class="form-control feature" name="feature[0]" value="{{ old('feature.0') }}">
                  <div class="input-group-append iga">
                    <span class="input-group-text">Beds</span>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="input-group">
                  <input type="number" min="0" class="form-control feature" id="feature" name="feature[1]" value="{{ old('feature.1') }}">
                  <div class="input-group-append iga">
                    <span class="input-group-text">Baths</span>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="input-group">
                  <input type="number" min="0" class="form-control feature" id="feature" name="feature[2]" value="{{ old('feature.2') }}">
                  <div class="input-group-append iga">
                    <span class="input-group-text">Sqft</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-4">
                <div class="input-group">
                  <input type="number" min="0" class="form-control feature" id="feature" name="feature[3]" value="{{ old('feature.3') }}">
                  <div class="input-group-append iga">
                    <span class="input-group-text">Garages</span>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="input-group">
                  <input type="number" min="0" class="form-control feature" id="feature" name="feature[4]" value="{{ old('feature.4') }}">
                  <div class="input-group-append iga">
                    <span class="input-group-text">Kitchen</span>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="input-group">
                  <input type="number" min="0" class="form-control feature" id="feature" name="feature[5]" value="{{ old('feature.5') }}">
                  <div class="input-group-append iga">
                    <span class="input-group-text">Deck</span>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /.summary -->

        <div class="form-group row" style="margin-top: 30px;">
          <label for="desc" class="col-sm-2 col-form-label">Description <span class="required-field">*</span></label>
          <div class="col-sm-10 {{ $errors->has('listing_description')? 'has-error':'' }}">
            <small class="text-info pti">Please provide some notes about the lead here. Anything you may know about it except for the actual contact info.</small>
            <textarea required class="form-control" rows="5" id="desc" name="listing_description" data-msg="Description is required">{{ old('listing_description') }}</textarea>

            {!! $errors->has('listing_description')? '<p class="help-block">'.$errors->first('listing_description').'</p>':'' !!}
          </div>
        </div>

      </section>
      <!-- /.post-info -->

      <!-- price range -->
      <div class="form-group row">
        <label for="title" class="col-sm-2 col-form-label">Price Range<span class="required-field">*</span></label>
        <div class="col-sm-10 {{ $errors->has('listing_title')? 'has-error':'' }}">
          <select class="custom-select form-control" name="price_range" id="price-range" required data-msg="Choose price range">
            <option selected disabled value="all">Choose Price Range</option>
            <option value="< $50K" {{ old('price_range') == '< $50K' ? 'selected':'' }}>< $50K</option>
            <option value="$50K-$99K" {{ old('price_range') == '$50K-$99K' ? 'selected':'' }}>$50K - $99K</option>
            <option value="$100K-$199K" {{ old('price_range') == '$100K-$199K' ? 'selected':'' }}>$100K - $199K</option>
            <option value="$200K-$299K" {{ old('price_range') == '$200K-$299K' ? 'selected':'' }}>$200K- $299K</option>
            <option value="$300K-$399K" {{ old('price_range') == '$300K-$399K' ? 'selected':'' }}>$300K - $399K</option>
            <option value="$400K-$499K" {{ old('price_range') == '$400K-$499K' ? 'selected':'' }}>$400K - $499K</option>
            <option value="$500K-$599K" {{ old('price_range') == '$500K-$599K' ? 'selected':'' }}>$500K - $599K</option>
            <option value="$600K-$699K" {{ old('price_range') == '$600K-$699K' ? 'selected':'' }}>$600K - $699K</option>
            <option value="$700K-$799K" {{ old('price_range') == '$700K-$799K' ? 'selected':'' }}>$700K - $799K</option>
            <option value="$800K-$899K" {{ old('price_range') == '$800K-$899K' ? 'selected':'' }}>$800K - $899K</option>
            <option value="$900K-$1 million" {{ old('price_range') == '$900-$1 million' ? 'selected':'' }}>$900K - $1M</option>
            <option value="$1million-$2 million" {{ old('price_range') == '$1million-$2 million' ? 'selected':'' }}>$1M - $2M</option>
            <option value="$2million-$3 million" {{ old('price_range') == '$2million-$3 million' ? 'selected':'' }}>$2M- $3M</option>
            <option value="$3million-$4 million" {{ old('price_range') == '$3million-$4 million' ? 'selected':'' }}>$3M - $4M</option>
            <option value="$4million-$5 million" {{ old('price_range') == '$4million-$5 million' ? 'selected':'' }}>$4M - $5M</option>
            <option value="> 5 million" {{ old('price_range') == '> 5 million' ? 'selected':'' }}>5M+</option>
          </select>
          {!! $errors->has('price_range')? '<p class="help-block">'.$errors->first('price_range').'</p>':'' !!}
        </div>
      </div>
      <!-- price range -->
<br>
      <section class="post-sec post-img">
        <h2>Images</h2>

        <div class="form-group row">
          <label for="refName" class="col-sm-2 col-form-label">Image&nbsp;</label>
          <div class="col-sm-10 {{ $errors->has('seller_name')? 'has-error':'' }}">
            <div class="alert alert-danger" role="alert">
              <strong>NEVER add contact info in the images</strong>
            </div>
            <div class="upload-images-input-wrap2">
              <input type="file" name="images[]" id="file" class="inputfile {{ $errors->has('images')? 'has-error':'' }}"/>
            </div>
            <div class="image-ad-more-wrap" style="margin-top:8px;">
              <a href="javascript:;" class="image-add-more"><i class="fa fa-plus-circle"></i> @lang('app.add_more')</a>
            </div>
            {!! $errors->has('images')? '<p class="help-block">'.$errors->first('images').'</p>':'' !!}
          </div>
        </div>

      </section>
      <br>
          <section class="referral post-sec">
            <h2>Lead info</h2>
            <div class="alert alert-info" role="alert">
              <strong>This information will ONLY be revealed to the other party upon the execution of the referral agreement</strong>
            </div>

            <div class="form-group row">
              <label for="refName" class="col-sm-2 col-form-label">Prospect's First Name&nbsp;<span class="required-field">*</span></label>
              <div class="col-sm-10 {{ $errors->has('referral_first_name')? 'has-error':'' }}">
                <input required data-msg="Prospect Name is required" type="text" class="form-control" id="refFName" value="{{ old('referral_first_name') ? old('referral_first_name') : ''}}" name="referral_first_name" >
                {!! $errors->has('referral_first_name')? '<p class="help-block">'.$errors->first('referral_first_name').'</p>':'' !!}
              </div>


              <label for="refName" class="col-sm-2 col-form-label">Last Name&nbsp;<span class="required-field">*</span></label>
              <div class="col-sm-10 {{ $errors->has('referral_last_name')? 'has-error':'' }}">
                <input required data-msg="Last Name is required" type="text" class="form-control" id="refLName" value="{{ old('referral_last_name') ? old('referral_last_name') : ''}}" name="referral_last_name" >
                {!! $errors->has('referral_last_name')? '<p class="help-block">'.$errors->first('referral_last_name').'</p>':'' !!}
              </div>
            </div>
            <div class="form-group row">
              <label for="refEmail" class="col-sm-2 col-form-label">Email</label>
              <div class="col-sm-10">
                <input type="email" class="form-control {{ $errors->has('seller_email')? 'has-error':'' }}" id="refEmail seller_email" name="referral_contact_email" value="{{ old('referral_contact_email') ? old('referral_contact_email') : ''}}" placeholder="@lang('app.email')">
                {!! $errors->has('referral_contact_email')? '<p class="help-block">'.$errors->first('referral_contact_email').'</p>':'' !!}
              </div>
            </div>

            <div class="form-group row {{ $errors->has('seller_phone')? 'has-error':'' }}">
              <label for="refPhone" class="col-sm-2 col-form-label">Phone&nbsp;<span class="required-field">*</span></label>
              <div class="col-sm-10">
                <input type="tel" class="form-control" id="seller_phone" name="referral_contact_phone" data-msg="Prospect Contact Number is required e.g. (444)444-4444" required value="{{ old('referral_contact_phone') ? old('referral_contact_phone') : ''}}">
                {!! $errors->has('seller_phone')? '<p class="help-block">'.$errors->first('seller_phone').'</p>':'' !!}
              </div>
            </div>
            <div class="form-group row {{ $errors->has('seller_phone')? 'has-error':'' }}">
              <label for="refAddress" class="col-sm-2 col-form-label">Address <span class="req-add required-field">*</span></label>
              <div class="col-sm-10">
                <input type="text" data-msg="Address is required" class="form-control refAdress" id="refAdress" value="{{ old('referral_contact_address') ? old('referral_contact_address') : ''}}" name="referral_contact_address" placeholder="">
                {!! $errors->has('referral_contact_address')? '<p class="help-block">'.$errors->first('referral_contact_address').'</p>':'' !!}
              </div>
            </div>

            <div class="form-group row {{ $errors->has('country')? 'has-error':'' }} loc-section">
              <label for="country" class="col-sm-2 col-form-label">Country <span class="required-field">*</span></label>
              <div class="col-sm-10"><input type="text" class="form-control" name="country_name" value="{{ old('country_name') == '' ? 'United States' : 'Canada' }}"></div>
              {!! $errors->has('country')? '<p class="help-block">'.$errors->first('country').'</p>':'' !!}
            </div>
            <div class="form-group row {{ $errors->has('state')? 'has-error':'' }} loc-section">
              <label for="state" class="col-sm-2 col-form-label">State <span class="required-field">*</span></label>
              <div class="col-sm-10"><input type="text" id="state" class="form-control" name="state_name" value="{{old('state_name')}}"></div>
              </div>
              <div class="form-group row {{ $errors->has('city')? 'has-error':'' }} loc-section">
                <label for="city" class="col-sm-2 col-form-label">City <span class="required-field">*</span></label>
                <div class="col-sm-10"><input type="text" class="form-control" name="city_name" value="{{old('city_name')}}"></div>
              </div>

              <div class="form-group row {{ $errors->has('zipcode')? 'has-error':'' }} loc-section">
                <label for="city" class="col-sm-2 col-form-label">Zip Code <span class="required-field">*</span></label>
                <div class="col-sm-10"><input type="text" class="form-control" name="zipcode" value="{{old('zipcode')}}"></div>
              </div>

              <input type="hidden" id="country_id" name="country" value="{{ $current_country == 'United States' ? '231' : '38' }}">
              <input type="hidden" id="state_id" name="state" value="{{old('state_id')}}">
              <input type="hidden" id="city_id" name="city" value="{{old('city_id')}}">


            <div class="form-group row">
              <label for="refFax" class="col-sm-2 col-form-label">Fax</label>
              <div class="col-sm-10">
                <input type="tel" class="form-control" id="refFax" name="referral_contact_fax" value="{{ old('referral_contact_fax') ? old('referral_contact_fax') : ''}}">
              </div>
            </div>

            <div class="form-group row">
              <label for="sumbit" class="col-sm-2 col-form-label"></label>
              <div class="col-sm-10">
                <input type="submit" role="button" class="btn btn-primary" value="POST LEAD" id="post_submit">
              </div>
            </div>
          </section>

          <input type="hidden" class="form-control" id="seller_email" value="{{ old('seller_email')? old('seller_email') : (Auth::check() ? $lUser->email : '' ) }}" name="seller_email">
          <input type="hidden" class="form-control" id="seller_name" value="{{ old('seller_name')? old('seller_name') : (Auth::check() ? $lUser->name : '' )  }}" name="seller_name">
          <input type="hidden" class="form-control" id="seller_phone" value="{{ old('seller_phone') ? old('seller_phone') : (Auth::check() ? $lUser->phone : '' ) }}" name="seller_phone">
          <input type="hidden" class="form-control" id="address" value="{{ old('address')? old('address') : (Auth::check() ? $lUser->address : '' ) }}" name="address" placeholder="@lang('app.address')">
          {{ Form::close() }}
        </div>
      </div>
    </div>
    <!-- /.container -->
  </main>

<!-- Modal -->
<div class="modal fade" id="cityModal" tabindex="-1" role="dialog" aria-labelledby="cityModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="cityModalLabel">Add city</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="text" name="find_city" class="form-control" placeholder="City Name" id="city_name">
        <small>&nbsp;&nbsp;<span style="color: red" id="error">&nbsp;</span></small>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save_change_city">Save changes</button>
      </div>
    </div>
  </div>
</div>


  @endsection
