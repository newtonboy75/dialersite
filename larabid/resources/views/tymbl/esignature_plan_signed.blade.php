@extends('tymbl.layouts.app')
@section('content')
<section class="page-header">
  <h1>Upload Leads File</h1>
</section>
<main class="post-flow">
  <div class="container">
    <div class="row justify-content-md-center">
      <div style="padding-bottom: 100px; padding-left: 10px; padding-right: 10px; display: inline-block; width: 90%;margin-left: auto; margin-right: auto;">
        @if($records)
        <p>&nbsp;</p>
        <h5>Thank you for uploading the leads!</h5>
        <p>Once we process them (1-2 business days), we'll send you an email with instructions on how to track the qualification process.</p>
        @else
        <div  class="section-agent-upload">
          <div class="m-section__content">
            <h5 class="text-success">Thank you for signing the Engagement Agreement</h5>
            <p>you may now upload your leads file </p>
            <p>&nbsp;</p><p>&nbsp;</p>
            <small class="text-secondary">&nbsp;&nbsp;Upload your .csv, .xls or .xlsx here</small>
            <form class="form-horizontal" method="POST" action="{{ route('import-csv') }}" enctype="multipart/form-data">
              {{ csrf_field() }}
              <input id="csv_file" type="file" class="form-control" name="csv_file" required>
              <input type="hidden" name="plan_upload" value="1">
              <input type="hidden" name="contract_id" value="{{ request()->contract_id}}">
              @if ($errors->has('csv_file'))
              <span class="help-block">
                <strong>{{ $errors->first('csv_file') }}</strong>
              </span>
              @endif
              <p>&nbsp;</p>
              <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                  <button type="submit" class="btn btn-primary">
                    Upload File
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div>
</main>
@endsection
