@extends('tymbl.layouts.app')
@section('content')
<section class="page-header">
  <h1>{{ $page->title }} </h1>
</section>
<div class="container">
  <div class="row">
    @if(request()->segment(1) === 'page' && request()->segment(2) != 'how-it-works')
    <div class="col-md-8 col-md-offset-2" style="min-height: 72vh">
      <div class="page_wrapper page-{{ $page->id }}" style="padding-top:40px; padding-left: 10px;padding-bottom:40px; text-align: justify;">
        {!! safe_output($page->post_content) !!}
      </div>
      <div>
      </div>
    </div>


    <div class="col-md-4">
      <section class="share-ad single-section" style="padding-top:40px; padding-left: 20px;">
        <div class="section-head">
          <h2>Share</h2>
        </div>

        <div class="social">
          <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(Request::fullUrl()) }}"><i class="fab fa-facebook-f" data-toggle="tooltip" data-placement="top" title="Facebook">
          </i></a>
          <a href="https://twitter.com/intent/tweet?url={{ urlencode(Request::fullUrl()) }}"><i class="fab fa-twitter" data-toggle="tooltip" data-placement="top" title="Twitter"></i></a>
          <a href="#"><i class="fab fa-instagram" data-toggle="tooltip" data-placement="top" title="Instagram"></i></a>
          <a href="https://plus.google.com/share?url={{ urlencode(Request::fullUrl()) }}"><i class="fab fa-google-plus-g" data-toggle="tooltip" data-placement="top" title="Google plus"></i></a>
          <a href="http://www.linkedin.com/shareArticle?mini=true&url={{ urlencode(Request::fullUrl()) }}&source=tymbl.com"><i class="fab fa-linkedin-in" data-toggle="tooltip" data-placement="top" title="Linkedin"></i></a>
        </div>
      </section>
    </div>
    @else

    <div class="text-center ">
      <div class="row" style="margin: 30px;">
        <div class="col-sm-4 text-center"><img src="/assets/img/tymbl/register.png"><h3><span style="color:#007bff">1</span> Register</h3>Register and sign up for notifications to let you know when there are new referrals available in your area.</div>
        <div class="col-sm-4 text-center"><img src="/assets/img/tymbl/search.png"><h3><span style="color:#007bff">2</span> Pick</h3>Choose a referral using search or notification.</div>
        <div class="col-sm-4 text-center"><img src="/assets/img/tymbl/reserve.png"><h3><span style="color:#007bff">3</span> Reserve</h3> Sign a referral agreement with a single click.
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 center-block" style="padding: 60px;">
          <h5>FAQ</h5>
          <div class="single-faq">
                        <h5>I want to sell a referral, how do I go about it?</h4>
                        <p>You will post a listing on Tymbl, which should only take a few minutes, without disclosing the contact name of the your client. Tymbl will notify real estate agents in the area, that there is a referal available. An agent will "buy" you referral by signing a legally-binding referral agreement with you on Tymbl and we'll notify this agent's title company and send them a copy of the contract.</p>
                    </div>

                    <div class="single-faq">
                        <h5>How do I buy a referral?</h4>
                        <p>Register and select a state or states and cities you are interested in. When there are referrals available in your area, we'll notify you. Then you login and sign a referral agreement and the seller of this referral will send you the contact info of the prospect.</p>
                    </div>

                    <div class="single-faq">
                        <h5>How do I make sure the other agent follows through on the terms of the referral agreement and I actually get paid when the property is sold?</h4>
                        <p>Tymbl establishes trust between parties involved in the transaction by</p>
                        <ul>
                            <li>Ensuring that the electronic referral agreement signed by you and the other party on Tymbl is a legally binding contract.</li>
                            <li>Enabling you to set an escrow fee the other party will have to pay when signing the referral agreement.</li>
                            <li>Holding the fee in the Tymbl escrow until the title company rep of the other party lets Tymbl know whether the referral resulted in a succesful sale or a purchase or not to release or refund the fee accordingly.</li>
                            <li>Collecting ratings and displaying an average score for every user who completed a transaction on Tymbl.</li>
                        </ul>
                    </div>

                    <div class="single-faq">
                        <h5>Is Tymbl for independent brokerages or franchises?</h4>
                        <p>Tymbl is completely brokerage agnostic</p>
                    </div>
				</div>
      </div>

      @endif

          </div>


        </div>

          @if(session()->has('success'))
            <p class="bg-success text-white" style="padding: 14px;">{{session()->get('success')}}y</p>
          @endif
        @include('tymbl.layouts.home_mailer')
        @endsection
