@extends('tymbl.layouts.app')
@section('content')
<section class="page-header2">
  <div class="container smaller-font">
    <i class="fas fa-angle-left"></i>&nbsp;&nbsp;<a href="{{ URL::previous() }}">Back to lead details</a>
  </div>
</section>
<div class="container">
  <div class="row">
    <div class="col-sm-8">
      <div>
        @if($image !='' && $image != 'NULL')

      	@if(file_exists('uploads/images/'.$image))
          <img src="/uploads/images/{{$listing_image->media_name}}" class="d-block w-100" />
        @else
        	@if($validList->category_type == "selling")
            <img src="{{ asset('assets/img/tymbl/house.png') }}" class="d-block w-100" />
          @else
            <img src="{{ asset('assets/img/tymbl/person.png') }}" class="d-block w-100" />
          @endif
        @endif
        @else
          @if($validList->category_type == "selling")
            <img src="{{ asset('assets/img/tymbl/house.png') }}" class="d-block w-100" />
          @else
            <img src="{{ asset('assets/img/tymbl/person.png') }}" class="d-block w-100" />
          @endif
        @endif
      </div>
    </div>
    <div class="col-sm-4">
      <h1 class="misc-header">{{$validList->title}}</h1>
      <div>

        <p class="smaller-font">To reserve this lead, the seller requires the reservation payment of:</p>
        <p class="listing-price">{{ $list_country = $country->id == '231' ? 'USD$' : 'CAD$' }}  {{number_format($validList->escrow_amount, 2, '.', ',')}}<sup class="sup-info"><i class="far fa-question-circle"></i></sup></p>

        <form class="mx-auto text-center" method="POST" id="payment-form"  action="/payment/pay">
          {{ csrf_field() }}
          <input type="hidden" name="referral_id" value="{{$id}}">
          <button class="btn btn-lg btn-block btn-buy btn-primary" style="margin-top:20px; margin-bottom: 30px;">Make a Payment</button></p>
        </form>

        <div class="referral-info">
          <span class="referral-info-left">Availability:</span><span class="referral-info-right">Available now</span>
        </div>
        <div class="referral-info">
          <span class="referral-info-left">Listed On:</span><span class="referral-info-right">{{ date('F d, Y', strtotime($validList->created_at)) }}</span>
        </div>

        <div class="payment-details">
          <p>After you make a payment, your funds will be held in an escrow until the transaction is completed one of the following ways:</p>

          <ul style="padding:0px; margin: 16px;">
            <li>Your title company rep logs in to Tymbl with the code we have sent and lets us know that the property was purchased/sold successfully. We release the funds to the seller.</li>
            <li>Your title company rep logs in to Tymbl with the code we have sent and lets us know that the property was NOT purchased/sold successfully. We release the funds back to you.</li>
          </ul>
        </div>
        <p>&nbsp;</p>
      </div>
    </div>
  </div>
</div>

@endsection
