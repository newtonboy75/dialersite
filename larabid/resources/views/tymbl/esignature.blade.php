@extends('tymbl.layouts.app')
@section('content')
<section class="page-header2">
  <div class="container">
  <i class="fas fa-angle-left"></i>&nbsp;&nbsp;<a href="{{ URL::previous() }}" class="back-to-page">Back to details</a>
  </div>

</section>
<div class="container">
  @if($check_id_error == '')
  <div id="framewrapper" rel="{{$id}}" style="width: 100% !important; min-height: 100vh;">
    <iframe src="{{$contract_url}}" name="esignatures-io-iframe" id="esignature-iframe" frameborder="0" style="width: 100% !important; max-height: 299vh; height: 299vh"></iframe>
  </div>
  @else
    <div class="text-center invalid-id">{{$check_id_error}}
      <p>&nbsp;</p><p><a href="#" onclick="javascript: window.history.back()">Back</a></p>
    </div>
  @endif
</div>
@endsection
