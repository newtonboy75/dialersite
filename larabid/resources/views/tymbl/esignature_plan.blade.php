@extends('tymbl.layouts.app')
@section('content')
<section class="page-header2">
  <br>
  <div class="container">
  <i class="fas fa-angle-left"></i>&nbsp;&nbsp;<a href="{{ URL::previous() }}" class="back-to-page">Back to plans</a>
  </div>
<br>
</section>
<div class="container">

  @if(request()->option == 2 || request()->option == '3')
  <div id="framewrapper text-center" style="width: 100% !important; padding: 100px;">
    <h1 class="text-center" style="color: #007bff;">coming soon...</h1>
  </div>
  @else
  <div id="framewrapper" style="width: 100% !important;">
    <iframe src="{{$contract_url}}" name="esignatures-io-iframe" id="esignature-iframe" frameborder="0" style="width: 100%; max-height: 290vh; height: 1400px"></iframe>
  </div>
  @endif
</div>
@endsection
