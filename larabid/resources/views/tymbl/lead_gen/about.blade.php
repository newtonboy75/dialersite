@extends('tymbl.lead_gen.layout.landing')
@section('content')
<!-- Sdivder
		============================================= -->
		<section id="sdivder" class="sdivder-element clearfix" style="height: 120px; background: url({{ asset('assets/img/tymbl/homeworth/about/hero.jpg') }}) center 20% no-repeat; background-size: cover;">
			<div class="container clearfix">
				<div class="clearfix center divcenter" style="max-width: 700px;">
					<div class="emphasis-title">
						<h2 class="font-secondary" style="color: #222; font-size: 70px; font-weight: 900; text-shadow: 0 7px 5px rgba(0,0,0,0.01), 0 4px 4px rgba(0,0,0,0.1);">How It Works</h2>
					</div>
				</div>

			</div>
		</section>

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap clearfix">

				<div class="container" style="max-width: 700px;">
          @if(session()->has('success'))
            <div class="alert alert-primary text-center">
                {{ session()->get('success') }}
            </div><br>
          @endif

          <div><h4>1. How does Tymbl Home Valuation work?</h4>
            We attempt to come up with an automated estimate based different online sources to establish a baseline.  Whether we can do it or not, we will have a real estate professional from your area get in touch with you to set up a home valuation appointment with you. They will come out and evaluate your property in detail and provide you with a more nuanced assessment of your home. Free of charge.<p>&nbsp;</p>
          </div>
          <div><h4>2. What’s the catch?</h4>
          Being a successful real estate agent is largely about building strong long-term relationships with clients. Good agents are willing to provide this evaluation service at no cost to establish a relationship with you. If they do a good job, you are likely to get in touch with them when you are ready to sell or buy property. There is no catch.<p>&nbsp;</p></div>
          <div><h4>3. How long would it take?</h4>
            An agent would probably need at least 15-20 minutes of your time to view your home, assess the condition and provide you with a more accurate valuation than you would get online from major real estate platforms.<br><br>
            Relationships matter and human touch always wins over technology.<p>&nbsp;</div>

          <span style="font-size: 20px; font-weight: 500">Got more questions?</span><br>
          <span>Shoot us a message and we’ll get back to you.</span>
          <p>
            {{ Form::open(array('url' => '/homeworth/submit-inquiry', 'method' => 'post', 'class' => 'quick-contact-form nobottommargin', 'id' => 'quick-contact-form3', )) }}

              <div class="form-process"></div>

              <input type="text" class="required sm-form-control input-block-level not-dark" id="first-name" name="first_name" value="" placeholder="First Name" /><br>

              <input type="text" class="required sm-form-control input-block-level not-dark" id="last-name" name="last_name" value="" placeholder="Last Name" /><br>

              <input type="email" class="required sm-form-control email input-block-level not-dark" id="quick-contact-form-email" name="email" value="" placeholder="Email Address" /><br>

              <input required type="phone" class="required sm-form-control input-block-level not-dark" id="quick-contact-form-phone" name="phone" value="" placeholder="Phone Number (+1-5555-22211)" /><br>
              <textarea name="message" class="required sm-form-control input-block-level not-dark" placeholder="Message"></textarea><br>

              <div class="mx-auto" style="width: 200px; margin-left: auto; margin-right: auto;">
              <input type="submit" style="width: 200px !important;" id="form-submit" class="button button-md button-rounded button-divght button-dark nomargin" value="submit">
              </div>

            {{ Form::close() }}
          </p>

				</div>
			</div>

		</section><!-- #content end -->
@endsection
