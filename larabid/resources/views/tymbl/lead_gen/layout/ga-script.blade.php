<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127180364-3"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-127180364-3');
</script>

<script>
    @if(Route::currentRouteName() == 'find')
        gtag('event', 'EstimateSuccess', {
            'event_category': 'ESTIMATE SUCCESS'
        });
    @endif

    @if(session()->has('message') && Route::currentRouteName() == 'valuation')
        gtag('event', 'EstimateFail', {
            'event_category': 'ESTIMATE Fail'
        });
    @endif

    @if(session()->has('success') && Route::currentRouteName() == 'homeworth')
        gtag('event', 'HValueCreateSuccess', {
            'event_category': 'CONTACT SUBMITTED'
        });
    @endif

    $(document).ready(function($) {

        $('#widget-subscribe-form-email').on('keyup', function () {
            var value = $(this).val();
            if (value.length == 2) {
                gtag('event', 'EstimateBegin', {
                    'event_category': 'SEARCH'
                });
            }
        });

        $('#first-name').on('keyup', function () {
            var value = $(this).val();
            if (value.length == 2) {
                gtag('event', 'HValueCreateBegin', {
                    'event_category': 'CONTACT START'
                });
            }
        });
    });
</script>