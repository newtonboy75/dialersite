<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="author" content="SemiColonWeb" />
  <!-- Stylesheets
  ============================================= -->
  <link href="//fonts.googleapis.com/css?family=Rubik:300,400,500,700|Playfair+Display:700,700i,900" rel="stylesheet" type="text/css" />
  <link href="//fonts.googleapis.com/css?family=Playfair+Display:400,900" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl/homeworth/bootstrap.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl/homeworth/style.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl/homeworth/dark.css') }}">

  <!-- Home Demo Specific Stylesheet -->
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl/homeworth/theme.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl/homeworth/font-icons.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/css/tymbl/homeworth/animate.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl/homeworth/magnific-popup.css') }}">

  <!-- Reader's Blog Demo Specific Fonts -->
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl/homeworth/fonts.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl/homeworth/responsive.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl/homeworth/colors.css') }}">
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl/homeworth/colors.php?color=1c85e8') }}" type="text/css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <style type="text/css" rel="stylesheet">
  .ui-autocomplete {
    z-index: 215000000 !important;
  }
  </style>

  <link rel="shortcut icon" href="{{ asset('assets/img/tymbl/fav.png') }}">
  <!-- Facebook Pixel Code -->
  <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '819133038464804');
    fbq('track', 'PageView');
  </script>

  <noscript>
    <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=819133038464804&ev=PageView&noscript=1"/>
  </noscript>
  <!-- End Facebook Pixel Code -->

  <!-- Document Title ============================================= -->
  <title>Tymbl</title>
</head>

<body class="stretched side-push-panel">

  @include('tymbl.lead_gen.layout.header')
  @yield('content')
  @include('tymbl.lead_gen.layout.footer')

  <!-- Go To Top ============================================= -->
  <div id="gotoTop" class="icon-angle-up"></div>

  <!-- External JavaScripts ============================================= -->
  <script src="{{ asset('assets/js/tymbl/homeworth/jquery.js') }}"></script>
  <script src="{{ asset('assets/js/tymbl/homeworth/plugins.js') }}"></script>

  <!-- Footer Scripts ============================================= -->

  <script src="{{ asset('assets/js/tymbl/homeworth/functions.js') }}"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>

  @if(request()->segment(2) == '')
    <script>
      $('#location-modal').on('show.bs.modal', function (event) {
        load_location();
      });

      //hide.bs.modal
    $('#location-modal').on('hide.bs.modal', function (event) {
      $('.emphasis-title h1').text('How much is your '+$("#city_id_p").val()+' home worth?');
      $('#widget-subscribe-form-email').attr('placeholder', 'Enter your address in '+$("#city_id_p").val()+' (ex. 123 ABC St Apple Rd)');
      //Kirkland, WA 98033
      var csz = $("#city_id_p").val()+', '+$("#state_id_p").attr('rel')+' '+$("#zip_id_p").val();
      $('#citystatezip').val(csz);
      $('#city_id').val($('#city_id_p').val());
      $('#state_id').val($('#state_id_p').attr('rel'));
      $('#zip_id').val($('#zip_id_p').val());
    });

    function load_location() {
      var country_id = $('#country_id').val();
      var state_id = $('#state_id').val();
      var city_id = $('#city_id').val();
      var zip_id = $('#zip_id').val();

      $.ajax({
        url: '/homeworth/load-location',
        method: 'post',
        data: { country: country_id, state: state_id, city: city_id, zip: zip_id, _token: '{{csrf_token()}}' },
        success: function(data) {
          //console.log(data);
          var state_option = '', city_option = '', zip_option = '';
          var selected_state = '', selected_city = '', selected_zip = '';

          let state_code = data[0];
          let state_name = data[1];
          let city_name = data[2];
          let zip_name = data[3];

          $('#state_id_p').val(state_name);
          $('#state_id_p').attr('rel', state_code);
          $('#city_id_p').val(city_name);
          $('#zip_id_p').val(zip_name);
      }
    });
  }
  </script>
  <script>
  var region = $('[name="country"]').val() == 'United States' ? 'US' : 'CA';
  function initMap() {
    var input = document.getElementById('widget-subscribe-form-email');
    // var input = $('#widget-subscribe-form-email');
    var options = {
      types: [('address')],
      componentRestrictions: {country: ['us', 'ca']}
    };
    var autocomplete = new google.maps.places.Autocomplete(input, options);
    autocomplete.addListener('place_changed', function() {
      let place=autocomplete.getPlace();
      this.pickup=place.formatted_address;
      $('#widget-subscribe-form-email').val(this.pickup);
    });
  }
  //google.maps.event.addDomListener(window, 'load', init);
  </script>

  <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key={{$gapid}}&libraries=places&callback=initMap&language=en&region=US" async defer></script>


  <script>
  var states_array = [];
  var cities_array = [];
  var zips_array = [];
  var stateid='';
  var cityid='';
  var zipid='';

  $( function() {

    var countryid = $('#country_id').val() == 'United States' ? '231' : '38';

    $('#state_id_p').autocomplete({
      source: function(request, response) {
        $.ajax({
          url: '/get-states-for-autocomplete',
          type: "POST",
          data: {state_name: request.term, country_id: countryid, _token: '{{ csrf_token() }}'},
          success: function (data) {
            //console.log(countryid);
            var resp = $.map(data,function(obj){
              //console.log(resp);
              return obj.state_name;
            });

            var states = $.map(data,function(obj){
              return obj;
            });
            states_array = [];
            states_array.push(states);
            response(resp);
            //return resp;
          }
        });
      },
      minLength: 1,
      select: function( event, ui ) {
        //console.log(ui.item.value);
        $.each( states_array, function( id, value ) {
          $.each( value, function( i, v ) {
            if(ui.item.value == v['state_name']){
              state_name = v['state_name'];
              stateid = v['id'];
              //console.log(stateid);
              $('#state_id_p').val(state_name);
              $('#city_id_p').val('');
              $('#zip_id_p').val('');
            }
          });
        });
      }
    });

    $('#city_id_p').autocomplete({
      source: function(request, response) {
        $.ajax({
          url: '/get-cities-for-autocomplete',
          type: "POST",
          data: {city_name: request.term, state_id: stateid, _token: '{{ csrf_token() }}'},
          success: function (data) {
            //console.log(data);
            var resp = $.map(data,function(obj){
              //console.log(resp);
              return obj.city_name;
            });

            var cities = $.map(data,function(obj){
              return obj;
            });
            cities_array = [];
            cities_array.push(cities);

            response(resp);
            //return resp;
          }
        });
      },
      minLength: 1,
      select: function( event, ui ) {

        $.each( cities_array, function( id, value ) {
          $.each( value, function( i, v ) {
            if(ui.item.value == v['city_name']){
              cityid = v['city_name'];
              $('#city_id_p').val(cityid);
              $('#zip_id_p').val('');
            }
          });
        });
      }
    });


    $('#zip_id_p').autocomplete({
      source: function(request, response) {
        $.ajax({
          url: '/get-zips-for-autocomplete',
          type: "POST",
          data: {
            zip_name: request.term,
            country: countryid,
            state: $('#state_id_p').val(),
            city: $('#city_id_p').val(),
            _token: '{{ csrf_token() }}'
          },
          success: function (data) {
            //console.log(data);
            var resp = $.map(data,function(obj){
              //console.log(resp);
              return obj.zip;
            });

            var zips = $.map(data,function(obj){
              return obj;
            });

            zips_array = [];
            zips_array.push(zips);

            response(resp);$('.ui-corner-all').css('padding', '20px');
            //return resp;
          }
        });
      },
      minLength: 1,
      select: function( event, ui ) {
        //console.log(states_array);
        $.each( zips_array, function( id, value ) {
          $.each( value, function( i, v ) {
            //console.log(v['state_code']);
            var state_code = v['state_code'];
            if(ui.item.value == v['zip']){
              $('[name="zip_code"]').val(v['zip']);
              $('#state_id_p').attr('rel', state_code);
            }
          });
        });
      }
    });
    //end here
  });
  </script>


  @endif

  @if(request()->segment(2) == 'find')

  <script>
  var latitude = {{ $lat }};
  var longitude = {{ $lon }};
  var street = '{{ $street }}';
  var map;
  var service;
  var infowindow;
  var marker;

  function initMap() {
    var property_address = new google.maps.LatLng(latitude, longitude);

    infowindow = new google.maps.InfoWindow();

    map = new google.maps.Map(
      document.getElementById('map'), {center: property_address, zoom: 30, mapTypeId: 'satellite'});
      var request = {
        query: street,
        fields: ['name', 'geometry'],
      };

      var service = new google.maps.places.PlacesService(map);
      //alert(service);

      service.findPlaceFromQuery(request, function(results, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          for (var i = 0; i < results.length; i++) {
            createMarker(results[i]);
            createPhotoMarker(results[i]);
          }
          map.setCenter(results[0].geometry.location);
        }
      });
    }

    function createMarker(place) {
      var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location
      });

      google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(place.name);
        infowindow.open(map, this);
      });
    }

    function createPhotoMarker(place) {
      var photos = place.photos;
      if (!photos) {
        return;
      }

      var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location,
        title: place.name,
        icon: photos[0].getUrl({maxWidth: 35, maxHeight: 35})
      });
    }
    </script>
    <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key={{$gapid}}&libraries=places&callback=initMap" async defer></script>
    @endif
  @include('tymbl.lead_gen.layout.ga-script')
  </body>
  </html>
