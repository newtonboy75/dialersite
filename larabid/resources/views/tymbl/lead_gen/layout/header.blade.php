<div id="side-panel">

		<div id="side-panel-trigger-close" class="side-panel-trigger"><a href="#"><i class="icon-line-cross"></i></a></div>

		<div class="side-panel-wrap">

			<div class="widget clearfix">

				<h4 class="t400">Login with Social</h4>

				<a href="{{ route('facebook_redirect') }}" class="button button-rounded t400 btn-block center si-colored noleftmargin si-facebook">Facebook</a>
				<a href="{{ route('google_redirect') }}" class="button button-rounded t400 btn-block center si-colored noleftmargin si-gplus">Google</a>
        <a href="{{ route('twitter_redirect') }}" class="button button-rounded t400 btn-block center si-colored noleftmargin si-twitter">Twitter</a>

				<div class="line line-sm"></div>
				<form id="login-form" name="login-form" class="nobottommargin" action="{{ route('login') }}" method="post">

					<div class="col_full">
						<label for="login-form-username" class="t400">email_address:</label>
						<input type="text" id="login-form-username" name="email" value="" class="form-control" />
					</div>

					<div class="col_full">
						<label for="login-form-password" class="t400">Password:</label>
						<input type="password" id="login-form-password" name="password" value="" class="form-control" />
					</div>

					<div class="col_full nobottommargin">
						<button class="button button-rounded t400 nomargin" id="login-form-submit" name="login-form-submit" value="login">Login</button>
						<a href="#" class="text-muted fright">Forgot Password?</a>
					</div>

				</form>


			</div>

		</div>

	</div>

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<div class="row justify-content-xl-between justify-content-lg-between clearfix">

						<div class="col-lg-2 col-12 d-flex align-self-center">
							<!-- Logo
							============================================= -->
							<div id="logo">
								<a href="/homeworth" class="standard-logo"><img src="{{ asset('assets/img/tymbl/homeworth/logo.png') }}" alt="Tymbl Logo"></a>
								<a href="/homeworth" class="retina-logo"><img src="{{ asset('assets/img/tymbl/homeworth/logo@2x.png') }}" alt="Tymbl Logo"></a>
							</div><!-- #logo end -->
						</div>

						<div class="col-lg-8 col-12 d-xl-flex d-lg-flex justify-content-xl-center justify-content-lg-center">
							<!-- Primary Navigation
							============================================= -->
							<nav id="primary-menu" class="with-arrows fnone clearfix">

								<ul>
									<li><a href="/homeworth/how-it-works"><div>How It Works</div></a></li>
									<li><a href="/contact-us"><div>Contact</div></a></li>
								</ul>
							</nav><!-- #primary-menu end -->
						</div>


				</div>

			</div>

		</header><!-- #header end -->
