<!-- Footer
		============================================= -->
		<footer id="footer" class="noborder" style="background-color: #F5F5F5;">

			<!-- Copyrights
			============================================= -->
			<div id="copyrights" class="" style="background-color: #FFF">

				<div class="container clearfix">

					<div class="col_full center nomargin">
						<span>Copyrights &copy; 2019 All Rights Reserved by Tymbl</span>
					</div>

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->
