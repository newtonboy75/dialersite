@extends('tymbl.lead_gen.layout.landing')
@section('content')
<!-- Page Title
		============================================= -->
		<section id="page-title" class="parallax page-title-dark center" style="background-image: url('images/single/1.jpg'); background-size: cover; padding: 100px 0;" data-100-bottom-top="background-position:0px 200px;" data-top-bottom="background-position:0px -200px;">

			<div class="container clearfix" style="z-index: 2">
				<span class="before-heading" style="color: #FFF;">info@tymbl.com</span>
				<h2 class="font-body nott t500 mb-2" style="color: #FFF; font-size: 54px;">+91-800-9876-221</h2>
				<a href=https://maps.google.com/maps?width=100%&height=600&hl=en&q=4020%20Lake%20Washington%20Blvd.%20Suite%20310%20Kirkland%2C%20WA%2098033+(Tymbl)&ie=UTF8&t=&z=17&iwloc=A" data-lightbox="iframe" class="d-inline-block"><i class="icon-map-marker mb-0 text-white notextshadow i-large i-plain dark divcenter d-block"></i><span class="text-white font-secondary lowercase font-italic ls1 mt-0"><u>Map</u></span></a>
				<span class="t400 text-white font-secondary mt-4">4020 Lake Washington Blvd. Suite 310 Kirkland, WA 98033</span>
			</div>

			<div class="video-overlay" style="background: rgba(28,133,232,0.55); z-index: 1"></div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">


			<div class="content-wrap clearfix">

				<div class="container">

          @if(session()->has('success'))
            <div class="alert alert-success text-center">
                {{ session()->get('success') }}
            </div>
          @endif

					<div class="contact-widget2 mt-5 divcenter" style="max-width: 750px">

						<div class="contact-form-result"></div>

						{{ Form::open(array('url' => '/contact-send', 'method' => 'post', 'class' => 'nobottommargin', 'id' => 'contact')) }}

							<div class="form-process"></div>

							<div class="col_half">
								<label class="nott" for="template-contactform-name">Name <small>*</small></label>
								<input type="text" id="template-contactform-name" name="name" value="" class="sm-form-control required" />
							</div>

							<div class="col_half col_last">
								<label class="nott" for="template-contactform-email">Email <small>*</small></label>
								<input type="email" id="template-contactform-email" name="email" value="" class="required email sm-form-control" />
							</div>

							<div class="clear"></div>

							<div class="col_full">
								<label class="nott" for="template-contactform-phone">Phone</label>
								<input type="text" id="template-contactform-phone" name="phone" value="" class="sm-form-control" />
							</div>

							<div class="clear"></div>

							<div class="col_full">
								<label class="nott" for="template-contactform-message">Message <small>*</small></label>
								<textarea class="required sm-form-control" id="template-contactform-message" name="message" rows="6" cols="30"></textarea>
							</div>


							<div class="col_full">
								<button class="button button-rounded button-large nomargin" type="submit" id="template-contactform-submit" value="submit">Send Message</button>
							</div>

						{{ Form::close() }}
					</div>

				</div>

			</div>

		</section><!-- #content end -->
@endsection
