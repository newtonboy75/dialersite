@extends('tymbl.lead_gen.layout.landing')
@section('content')
<!-- Page Title
		============================================= -->
		<section id="page-title" class="nobg border-top center">

			<div class="container clearfix">
				<h1 class="font-secondary nott mb-3" style="font-size: 54px;">FAQs</h1>
				<span>All your Questions answered in one place</span>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap clearfix">

				<div class="container">

					<div class="row clearfix">

						<div class="col-md-8">
							<div class="toggle toggle-bg" data-animate="fadeIn">
								<div class="togglet rounded-top t400"><strong class="mr-1">Q.</strong>I want to sell a referral, how do I go about it?<i class="toggle-icon icon-line-circle-plus"></i></div>
								<div class="togglec rounded-bottom">You will post a listing on Tymbl, which should only take a few minutes, without disclosing the contact name of the your client. Tymbl will notify real estate agents in the area, that there is a referal available. An agent will "buy" you referral by signing a legally-binding referral agreement with you on Tymbl and we'll notify this agent's title company and send them a copy of the contract.</div>
							</div>

							<div class="toggle toggle-bg" data-animate="fadeIn" data-delay="200">
								<div class="togglet t400 rounded-top"><strong class="mr-1">Q.</strong>How do I buy a referral?<i class="toggle-icon icon-line-circle-plus"></i></div>
								<div class="togglec rounded-bottom">How do I buy a referral?</div>
							</div>

							<div class="toggle toggle-bg" data-animate="fadeIn" data-delay="400">
								<div class="togglet t400 rounded-top"><strong class="mr-1">Q.</strong>How do I make sure the other agent follows through on the terms of the referral <br>agreement and I actually get paid when the property is sold?<i class="toggle-icon icon-line-circle-plus"></i></div>
								<div class="togglec rounded-bottom">Tymbl establishes trust between parties involved in the transaction by

Ensuring that the electronic referral agreement signed by you and the other party on Tymbl is a legally binding contract.
Enabling you to set an escrow fee the other party will have to pay when signing the referral agreement.
Holding the fee in the Tymbl escrow until the title company rep of the other party lets Tymbl know whether the referral resulted in a succesful sale or a purchase or not to release or refund the fee accordingly.
Collecting ratings and displaying an average score for every user who completed a transaction on Tymbl.</div>
							</div>

							<div class="toggle toggle-bg" data-animate="fadeIn" data-delay="600">
								<div class="togglet t400 rounded-top"><strong class="mr-1">Q.</strong>Is Tymbl for independent brokerages or franchises?<i class="toggle-icon icon-line-circle-plus"></i></div>
								<div class="togglec rounded-bottom">Tymbl is completely brokerage agnostic</div>
							</div>


						</div>

						<div class="col-md-4">
							<a href="{{ asset('assets/img/tymbl/homeworth/single/2.jpg') }}" data-lightbox="image"><img src="{{ asset('assets/img/tymbl/homeworth/single/1.jpg') }}" alt=""></a>
							<div class="clear"></div>
							<a href="{{ asset('assets/img/tymbl/homeworth/single/6.jpg') }}" data-lightbox="image"><img class="mt-5" src="{{ asset('assets/img/tymbl/homeworth/single/4.jpg') }}" alt=""></a>
						</div>

					</div>

				</div>

			</div>

		</section><!-- #content end -->
@endsection
