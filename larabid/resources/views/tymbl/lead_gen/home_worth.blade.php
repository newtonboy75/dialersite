@extends('tymbl.lead_gen.layout.landing')
@section('content')
<!-- Slider +============================================ -->
<section id="slider" class="slider-element full-screen clearfix" style="height: 650px; background: url({{ asset('assets/img/tymbl/homeworth/hero.jpg') }}) center center no-repeat;">
    <div class="vertical-middle">
        <div class="container clearfix">
            <div class="clearfix center divcenter" style="max-width: 700px;">
                <div class="emphasis-title">
                    <h1 class="font-secondary" style="color: #FFF; font-size: 70px; line-height: 105%; font-weight: 900; text-shadow: 0 7px 10px rgba(0,0,0,0.07), 0 4px 4px rgba(0,0,0,0.2);">How much is your {{ old('city') ? old('city') : $loc['city'] }} home worth?</h1>
                    <p style="font-weight: 300; opacity: .7; color: #FFF; text-shadow: 0 -4px 20px rgba(0, 0, 0, .25);">Find out the value of your home</p>
                </div>
                <div class="subscribe-widget" data-loader="button">
                    <div class="widget-subscribe-form-result"></div>
                    {{ Form::open(array('url' => '/homeworth/find', 'method' => 'post', 'class' => 'nobottommargin', 'id' => 'find-address', 'data-animate'=>'fadeInUp')) }}
                    <div class="float-right"><a href="javascript::" id="a-change_loc" data-toggle="modal" data-target="#location-modal"><span>CHANGE LOCATION</span></a><br><br></div>
                    <div class="input-group divcenter">
                        <input required type="text" id="widget-subscribe-form-email" name="address" class="form-control form-control-lg not-dark" value="{{ old('address') ? old('address') : '' }}" placeholder="Enter your address (ex. 123 ABC St Beverly Hills CA 90210)"  style="border: 0; box-shadow: none; overflow: hidden;">
                        <input type="hidden" id="citystatezip" name="citystatezip" value="{{ old('city') ? old('city') : $loc['city'] }}, {{ old('state') ? old('state') : $loc['state'] }} {{ old('zip') ? old('zip') : $loc['zip'] }}">
                        <input type="hidden" id="country_id" name="country" value="{{ old('country') ? old('country') : $loc['country'] }}">
                        <input type="hidden" id="city_id" name="city" value="{{ old('city') ? old('city') : $loc['city'] }}">
                        <input type="hidden" id="state_id" name="state" value="{{ old('state') ? old('state') : $loc['state'] }}">
                        <input type="hidden" id="zip_id" name="zip" value="{{ old('zip') ? old('zip') : $loc['zip'] }}">
                        <div class="input-group-append">
                            <input class="button t400" type="submit" style="border-radius: 3px;" value="Submit">
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>

                @if(session()->has('error'))
                    <br><br>
                    <div class="alert alert-danger text-center">
                        {{ session()->get('error') }}
                    </div>
                @endif

                @if(session()->has('success'))
                    <br><br>
                    <div class="alert alert-primary text-center">
                        {{ session()->get('success') }}
                    </div>
                @endif
            </div>
        </div>
    </div>

      <!-- Modal -->
    <div id="location-modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Change Location</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <label for="state_id">State</label>
                    <input class="form-control" type="text" id="state_id_p" value="">
                    <br>
                    <label for="state_id">City</label>
                    <input class="form-control" type="text" id="city_id_p" value="">
                    <br>
                    <label for="state_id">Zip Code</label>
                    <input class="form-control" type="text" id="zip_id_p" value="">
                </div>
                <div class="modal-footer">
                    <button id="save-change" type="button" class="btn btn-default" data-dismiss="modal">Save Changes</button>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection


