@extends('tymbl.lead_gen.layout.landing')
@section('content')
<!-- Slider
		============================================= -->
		<section id="slider" class="slider-element full-screen clearfix" style="height: 650px; background: url({{ asset('assets/img/tymbl/homeworth/hero.jpg') }}) center center no-repeat;">
			<div class="vertical-middle-2">
				<div class="container clearfix">
          <br>
					<div class="row justify-content-center">
                        <div class="col-md-6">

            @php
              $location = explode('|', request()->get('cisict'));
              $city_id = $location[0];
              $state_id = $location[1];
              $country_id = $location[2];
              $zip = $location[3];
            @endphp
            <br>
            @if(session()->has('message'))
            <div class="bg-primary text-white divcenter" style="padding: 10px; font-size: 15px; width: 100%;">
              Sorry, we cannot provide an automated estimate of this property, but our Home Valuation Specialist will be in touch with you to ask a few questions to determine a more Accurate Valuation.
            </div>
            <div class="bg-white text-dark divcenter" style="width: 100%; padding: 20px;margin-bottom: 20px;">

              {{ Form::open(array('url' => '/homeworth/submit', 'method' => 'post', 'class' => 'quick-contact-form nobottommargin', 'id' => 'quick-contact-form3', )) }}

  							<div class="form-process"></div>

  							<input type="text" class="required sm-form-control input-block-level not-dark" id="first-name" name="first_name" value="" placeholder="First Name" /><br>

  							<input type="text" class="required sm-form-control input-block-level not-dark" id="last-name" name="last_name" value="" placeholder="Last Name" /><br>

  							<input type="email" class="required sm-form-control email input-block-level not-dark" id="quick-contact-form-email" name="email" value="" placeholder="Email Address" /><br>

  							<input required type="phone" class="required sm-form-control input-block-level not-dark" id="quick-contact-form-phone" name="phone" value="" placeholder="Phone Number (+1-5555-22211)" /><br>
                <input type="hidden" name="city_id" value="{{ $city_id }}">
                <input type="hidden" name="state_id" value="{{ $state_id }}">
                <input type="hidden" name="country_id" value="{{ $country_id }}">
                <input type="hidden" name="zip_id" value="{{ $zip }}">

  							<input type="submit" id="form-submit" class="button button-small button-rounded button-light button-dark nomargin" value="submit">

  						{{ Form::close() }}
            </div>
            @endif
					</div>
                    </div>
				</div>
			</div>

      <!-- Modal -->
<div id="location-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-dialog-centered">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header"><h4 class="modal-title">Change Location</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>
      <div class="modal-body">
          <label for="state_id">State</label>
          <select class="form-control form-control-md" id="state_id_p"><option>Test</option></select>
          <br>
          <label for="state_id">City</label>
          <select class="form-control form-control-md" id="city_id_p"><option>Test</option></select>
          <br>
          <label for="state_id">Zip Code</label>
          <select class="form-control form-control-md" id="zip_id_p"><option>Test</option></select>
      </div>
      <div class="modal-footer">
        <button id="save-change" type="button" class="btn btn-default" data-dismiss="modal">Save Changes</button>
      </div>
    </div>

  </div>
</div>

		</section>
@endsection
