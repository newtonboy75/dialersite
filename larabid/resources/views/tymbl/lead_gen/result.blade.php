@extends('tymbl.lead_gen.layout.landing')
@section('content')
<!-- Slider============================================= -->
	<div class="row norightmargin bottommargin-lg common-height">
		<div id="headquarters-map" class="col-lg-8 gmap d-md-block" style="height: 500px;">
			@if($protected_details == 'no')
				<div style="width: 100%; height: 100%;"><img src="{{$property_image}}" style="width: 100%; height: 100%;"></div>
			@else
				<div id="map" style="width: 100%; height: 100%;"></div>
			@endif
		</div>
		<div class="col-lg-4 bgcolor" style="height: 500px;">
			<div class="col-padding">
				<div class="quick-contact-widget dark clearfix">
					<div style="max-width: 400px; text-align: right;">
						<div style="font-size: 12px; margin-right: 4px;">
							<a style="color: #fff;" href="{{ $link }}">Zestimate®</a>
						</div>
						<h2 class="capitalize ls1 t400">${{ $zestimate }}</h2>
					</div><br>
					<h4 class="capitalize ls1 t400">
						Please note that this is just an estimate based on recent sales in the neighborhood. However,
						Our home valuation specialist will be in touch with you to ask a few questions to determine a more accurate valuation.
					</h4>
					<div class="quick-contact-form-result"></div>
					{{ Form::open([
							'url' => '/homeworth/submit',
							'method' => 'post',
							'class' => 'quick-contact-form nobottommargin',
							'id' => 'quick-contact-form3',
							'data-animate'=>'fadeInUp'
						])
					}}
						<div class="form-process"></div>
						<input type="text" class="required sm-form-control input-block-level not-dark" id="first-name"
							   name="first_name" value="" placeholder="First Name" />
						<input type="text" class="required sm-form-control input-block-level not-dark" id="last-name"
							   name="last_name" value="" placeholder="Last Name" />
						<input type="email" class="required sm-form-control email input-block-level not-dark"
							   id="quick-contact-form-email" name="email" value="" placeholder="Email Address" />
						<input type="phone" class="required sm-form-control input-block-level not-dark"
							   id="quick-contact-form-phone" name="phone" value="" placeholder="Phone Number (+1-555-22211)" />
						<input type="hidden" name="title" value="{{ $ad_title }}">
						<input type="hidden" name="country_id" value="{{ $country_id }}">
						<input type="hidden" name="city_id" value="{{ $city_id->id }}">
						<input type="hidden" name="state_id" value="{{ $state_id->id }}">
						<input type="hidden" name="zip" value="{{ $zip }}">
					<input type="submit" id="form-submit"
						   class="button button-small button-rounded button-light button-white nomargin" value="submit">
					{{ Form::close() }}<br><br>
					<img alt="Real Estate on Zillow" title="Real Estate on Zillow"
						 src="https://www.zillow.com/widgets/GetVersionedResource.htm?path=/static/logos/Zillowlogo_200x50.gif">
				</div>
			</div>
		</div>
	</div>
@endsection
