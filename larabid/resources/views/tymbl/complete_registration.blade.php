@extends('tymbl.layouts.app')
@section('content')
<div class="container" style="min-height:60px; padding:40px;">
    <div class="row">
        <div class="col">
            <div class="mx-auto register-form-panel">
                <h5 class="panel-heading">@lang('app.complete_profile')</h5>
                <div class="panel-body">

                    @include('admin.flash_msg')

                    <form role="form" method="POST" action="{{ route('register-complete') }}">
                        {{ csrf_field() }}

                        {{--Company name --}}
                        <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
                            <label for="company_name" class="col-md-4 control-label">@lang('app.company_name')</label>

                            <div class="col">
                                <input id="company_name" type="text" class="form-control" name="company_name" value="{{ old('company_name') }}" required autofocus>

                                @if ($errors->has('company_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('company_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{--Representative name --}}
                        <div class="form-group{{ $errors->has('representative_name') ? ' has-error' : '' }}">
                            <label for="representative_name" class="col-md-4 control-label">@lang('app.representative_name')</label>

                            <div class="col">
                                <input id="representative_name" type="text" class="form-control" name="representative_name" value="{{ old('representative_name') }}" required autofocus>

                                @if ($errors->has('representative_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('representative_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{--Representative email --}}
                        <div class="form-group{{ $errors->has('representative_email') ? ' has-error' : '' }}">
                            <label for="representative_email" class="col-md-4 control-label">@lang('app.representative_email')</label>

                            <div class="col">
                                <input id="representative_email" type="text" class="form-control" name="representative_email" value="{{ old('representative_email') }}" required autofocus>

                                @if ($errors->has('representative_email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('representative_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <hr>

                        {{--Address--}}
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label">@lang('app.address')</label>

                            <div class="col">
                                <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" autofocus>

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        {{--Phone--}}
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">@lang('app.phone')</label>

                            <div class="col">
                                <input id="phone" type="tel" class="form-control" name="phone" value="{{ old('phone') }}" autofocus pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}">

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{--Fax--}}
                        <div class="form-group{{ $errors->has('fax') ? ' has-error' : '' }}">
                            <label for="fax" class="col-md-4 control-label">@lang('app.fax')</label>

                            <div class="col">
                                <input id="fax" type="tel" class="form-control" name="fax" value="{{ old('fax') }}" autofocus>

                                @if ($errors->has('fax'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fax') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        {{--RE License Number--}}
                        <div class="form-group{{ $errors->has('re_license_number') ? ' has-error' : '' }}">
                            <label for="re_license_number" class="col-md-4 control-label">@lang('app.re_license_number')</label>

                            <div class="col">
                                <input id="re_license_number" type="text" class="form-control" name="re_license_number" value="{{ old('re_license_number') }}" autofocus>

                                @if ($errors->has('re_license_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('re_license_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        @if(get_option('enable_recaptcha_registration') == 1)
                            <div class="form-group {{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                <div class="col">
                                    <div class="g-recaptcha" data-sitekey="{{get_option('recaptcha_site_key')}}"></div>
                                    @if ($errors->has('g-recaptcha-response'))
                                        <span class="help-block">
                                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                                </span>
                                    @endif
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                            <div class="col mx-auto text-center">
                                <button type="submit" class="btn btn-primary btn-lg">
                                    <i class="fa fa-user-plus"></i> @lang('app.submit')
                                </button>

                                &nbsp;&nbsp;<a class="pull-right" href='{{ route('dashboard') }}'>Finish Later</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
