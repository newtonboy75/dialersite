@extends('tymbl.layouts.app')
@section('content')
<div class="container">
<div class="col text-center" style="height: 70vh; padding: 50px;">

            @include('admin.flash_msg')
              <div class="col text-center">
                <h5>Congratulations! You have successfully reserved a lead on Tymbl!</h5>
                <div class="mx-auto" style="max-width:640px; text-align:left; margin-top: 30px;">
                  <p class="font-weight-bold">IMPORTANT: Please note that:</p><br>
                <p>Tymbl reserves the right to terminate the referral agreement and remove the lead from from any agent in the event:</p>
                <ul>
                    <li>Agent refuses to cooperate with Tymbl, (respond to inquiries, emails and text messaging for follow up)</li>
                    <li>Agent fails to contact the reserved lead within 24 hours after reservation</li>
</ul></div>

              </div>
              <p>&nbsp;</p>
              <p><a class="btn btn-primary" href="{{route('reserved-leads')}}">Proceed to view Prospect's Contact Details</a></p>

</div>
</div> <!-- /#container -->
@endsection
