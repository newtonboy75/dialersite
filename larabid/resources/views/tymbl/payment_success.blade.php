@extends('tymbl.layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
          <div class="payment-success-wrap">
              <h5><i class="fas fa-check-circle" style="color: green;"></i> @lang('app.payment_success_info') </h5>
          </div>

        </div>
    </div>
</div>
@endsection
