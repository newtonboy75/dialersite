@extends('tymbl.layouts.app')
@section('content')
<div class="container">
  <div id="post-new-ad">
      <div class="container">
          <div class="row">
            <div class="thankyou-text">
              When the seller signs the referral agreement, a copy of the contract will be forwarded to <strong>{{$title_company_name}}</strong> representative.
              <p>&nbsp;</p>
              <div class="referral-success-buttons">
                <button id="ok-title-company" type="button" class="btn btn-primary">Ok</button>
                <a href="/dashboard/u/posts/profile/edit#rep" class="btn btn-secondary" type="button" class="btn btn-secondary">Change</a>
              </div>
              <div id="loader" style="margin-top:20px;">&nbsp;</div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
@endsection
