@extends('tymbl.layouts.app')
@section('content')

</section>
<div class="text-center" id="jumper">Go to:&nbsp;&nbsp;&nbsp;
  @if (Auth::check())<a href="javascript:;" id="a-scrollto" rel="my-leads">Leads in your Area</a>&nbsp;&nbsp;|&nbsp;&nbsp;@endif
  <a href="javascript:;" id="a-scrollto" rel="section-qualified">Qualified Leads</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="javascript:;" id="a-scrollto" rel="section-unqualified">Unualified Leads</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="javascript:;" id="a-scrollto" rel="section-reserved">Reserved Leads</a></div>
<main class="main-content">
            @if (Auth::check())
            @include('tymbl.layouts.home_my_location_items')
            @endif
            @include('tymbl.layouts.home_qualified_items')
            @include('tymbl.layouts.home_unqualified_items')
            @include('tymbl.layouts.home_reserved_items')

        </main> <!--/.main-content-->
@endsection
