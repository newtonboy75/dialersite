<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Open Graph data -->
  <meta property="og:description" content="<?php echo trans('app.meta_description') ?>" />
  <meta property="og:title" content="<?php echo trans('app.meta_title') ?>" />
  <meta property="og:url" content="{{ url()->current() }}" />
  <meta property="og:type" content="website" />
  <meta property="og:locale" content="en_US" />
  <meta property="og:locale:alternate" content="en_US" />
  <meta property="og:site_name" content="{{get_option('site_name')}}" />
  <meta property="og:image:url" content="{{URL::asset('assets/img/tymbl/tymbl-post-leads.jpg')}}" />
  <meta property="og:image:size" content="300" />
  <!-- Twitter Card -->
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@tymbl1">
  <meta name="twitter:title" content="<?php echo trans('app.meta_title') ?>">
  <meta name="twitter:description" content="<?php echo trans('app.meta_description') ?>">
  <meta name="twitter:creator" content="@author_handle">
  <meta name="twitter:image:src" content="{{URL::asset('assets/img/tymbl/tymbl-post-leads.jpg')}}">
  <!--Google+ -->
  <meta itemprop="name" content="<?php echo trans('app.meta_title') ?>">
  <meta itemprop="description" content="<?php echo trans('app.meta_description') ?>">
  <meta itemprop="image" content="{{URL::asset('assets/img/tymbl/tymbl-post-leads.jpg')}}">
  <title>@section('title') {{ get_option('site_title') }} @show</title>
  <!-- favicon -->
  <link rel="shortcut icon" href="{{ asset('assets/img/tymbl/fav.png') }}">
  <!-- fontawesome -->

  <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome/css/all.min.css') }}">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl/bootstrap.min.css') }}">
  <!-- Owl Carousel Css -->
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl/front/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl/front/owl.theme.default.min.css') }}">
  <!-- custom css -->
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl/front/style.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl/front/custom.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl/front/vip_v2.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl/front/vip_responsive.css') }}">

  <!-- responsive css -->
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl/front/responsive.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/plugins/toastr/toastr.min.css') }}">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
  <link href="//unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

  @if(request()->segment(1) === 'plans')
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl/front/plans.css') }}">
  @endif

  @if((request()->segment(1) === 'referral-document' && request()->segment(3)) == 'success' || request()->segment(1) === 'dashboard')

  @endif
  @if((request()->segment(1) === 'title-company' && request()->segment(2)) == 'approve')
  <link type="text/css" rel="stylesheet" href="{{ asset('assets/plugins/waitMe/waitMe.css') }}">
  @endif
  <meta name="google-site-verification" content="fa25hLOssKHKzyrF0ilVxhIvfzqZfgtPq2J0aEe0Neo" />
  <style>
  .rating li {
    float: right !important;
    color: #c2c2c2;
    padding: 10px 5px;
  }

  .rating li:hover > span,
  .rating li:hover ~ li > span{
    color: #ffd700;
    cursor:pointer !important;
  }

  li.check > span{
    display: inline-block;
    list-style: none;
    color: #ffd700 !important;
  }
  </style>
</head>
@php
if(request()->segment(1) == '' && !Auth::check()){
  $style = '#E9F4FF url("/assets/img/tymbl/img_hero.svg") center 50px no-repeat fixed;-webkit-background-size: cover;
  background-size: cover;';
}else{
  $style = '';
}

@endphp

<body style="background: {{$style}}">
  <header class="sticky">
    <div class="container">
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        @include('tymbl.layouts.menu_horizontal')
        @include('tymbl.layouts.menu_collapse')
      </nav>
    </div>
  </header>
  @yield('content')
  @include('tymbl.layouts.footer')


  <!-- to top -->
  <a id="back-to-top" href="#" class="btn back-to-top" role="button" data-placement="left"><i class="fas fa-arrow-up"></i></a>
  @if(request()->segment(1) === 'referral-document' && request()->segment(3) === 'success')
  @if($popup == 'no')

  <div id="rating_popup" class="rating_popup">
    <a href="#" id="popup_close" class="alert-link alert alert-secondary" style="display: none; float:right;padding:0 0.4em;font-size:26px;">×</a>
    <div>
      <div class="text-center" id="rating-referral">
        <h4>Thank you for signing the REFERRAL AGREEMENT!</h4>
        <p style="padding-left:12px; padding-right:12px;">This contract has now been returned, and a copy of the document will be sent to you when all parties have signed.</p>

        <strong class="center-btn">Please rate how your transaction went</strong>
        <ul class="inline-rating rating">
          <li id="rating" rel="5"><span class="fa fa-star" title="Rate 5"></span></li>
          <li id="rating" rel="4"><span class="fa fa-star" title="Rate 4"></span></li>
          <li id="rating" rel="3"><span class="fa fa-star" title="Rate 3"></span></li>
          <li id="rating" rel="2"><span class="fa fa-star" title="Rate 2"></span></li>
          <li id="rating" rel="1"><span class="fa fa-star" title="Rate 1"></span></li>
        </ul>
      </div>
      <div id="option">
        <strong class="center-btn">Let us know why you gave this transaction 1 star</strong>
        <div class="input-group">
          @if($type == 'selling')
          <div class="input-groups">
            <div class="form-check form-check-inline search-radio"><input class="form-check-input" name="reason" id="reason" value="Prospect contact info was incorrect" type="radio" id="toggler_option"><label class="form-check-label" for="toggler-distance">Prospect contact info was incorrect</label>
            </div>
          </div>
          <div class="input-groups">
            <div class="form-check form-check-inline search-radio"><input class="form-check-input" name="reason" id="reason" value="Seller didn't provide the prospect contact information" type="radio" id="toggler_option"><label class="form-check-label" for="toggler-distance">Seller didn't provide the prospect contact information</label></div>
          </div>
          <div class="input-groups">

            <div class="form-check form-check-inline search-radio"><input class="form-check-input" name="reason" id="reason" value="Prospect didn't expect to be contacted" type="radio" id="toggler_option"><label class="form-check-label" for="toggler-distance">Prospect didn't expect to be contacted</label></div>

          </div>
          <div class="input-groups">

            <div class="form-check form-check-inline search-radio"><input class="form-check-input" name="reason" id="reason" value="Seller was slow to respond" type="radio" id="toggler_option"><label class="form-check-label" for="toggler-distance">Seller was slow to respond</label></div>

          </div>
          <div class="input-groups">
            <div class="form-check form-check-inline search-radio"><input class="form-check-input" name="reason" id="reason" value="Other" type="radio" id="toggler_option"><label class="form-check-label" for="toggler-distance">Other</label></div>
          </div>
          @else
          <div class="input-groups">
            <div class="form-check form-check-inline search-radio"><input class="form-check-input" name="reason" value="Buyer was slow to respond" type="radio" id="toggler_option"><label class="form-check-label" for="toggler-distance">Buyer was slow to respond</label></div>
          </div>
          <div class="input-groups">
            <div class="form-check form-check-inline search-radio"><input class="form-check-input" name="reason" id="reason" value="Buyer was slow to sign the referral agreement" type="radio" id="toggler_option"><label class="form-check-label" for="toggler-distance">Buyer was slow to sign the referral agreement</label></div>
          </div>
          <div class="input-groups">
            <div class="form-check form-check-inline search-radio"><input class="form-check-input" name="reason" id="reason" value="Other" type="radio" id="toggler_option"><label class="form-check-label" for="toggler-distance">Other</label></div>
          </div>
          @endif
          <div id="reason-details">Please provide details<br>
            <input type="text" class="form-control" id="other-details"><br><br>
            <div class="center-btn"><a id="submit-feedback" class="btn btn-primary active text-center" role="button" href="#">Submit</a></div>
          </div>
        </div>
      </div>
      <div class="text-center text-success" id="thank-you-feedback">Thank you for your feedback.<br><br>
        <button type="button" id="popup_close" class="btn" href="#">Close</button><br><br>
      </div>
      <div class="popup-footer text-center">Signatures are collected and stored by https://esignatures.io</div>&nbsp;
    </div>
  </div>

  @endif
  @endif

  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <!-- jquery -->
  <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
  <!-- jquery ui -->
  <script src="{{ asset('assets/js/jquery-ui.js') }}"></script>
  <script src="{{ asset('assets/js/tymbl/jquery.easing.js') }}"></script>
  <script src="{{ asset('assets/js/tymbl/jquery.easing.compatibility.js') }}"></script>
  <script src="{{ asset('assets/js/popper.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
  <!-- owl carousel -->
  <script src="{{ asset('assets/js/tymbl/owl.carousel.js') }}"></script>
  <script src="{{ asset('assets/plugins/toastr/toastr.min.js') }}"></script>
  <script src="{{ asset('assets/js/tymbl/main.js') }}"></script>
  <script src="{{ asset('assets/js/tymbl/retina.min.js') }}"></script>
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
  <script>
  AOS.init();
  var toastr_options = {closeButton : true};
  </script>
  <!--conditional scripts here -->

  @if(request()->segment(1) === 'listing')
  @include('tymbl.layouts.script_listing')
  @endif

  @if(request()->segment(1) === 'post-new')
  @include('tymbl.layouts.script_newpost')
  <script>
  $(document).on('click', 'input, textarea, radio, select, button', function (e) {
    gtag('event', 'PostAdBegin', {
      'event_category': 'Post Page',
      'event_label': e.target.nodeName,
    });
  });
  </script>
  @endif

  @include('tymbl.layouts.script_search')

  @if(session('error'))
  <script>
  toastr.error('{{ session('error') }}', '{{ trans('app.error') }}', toastr_options);
  </script>
  @endif

  @if(request()->segment(1) === 'contact-us' || Request::is('/'))
  <script>
  @if(session('success'))
  toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
  @endif
  </script>
  @endif

  @if(request()->segment(1) === 'payment' && request()->segment(2) == 'success')
  <script>
  setTimeout(function(){ window.location = '/referral-document/{{$ad_id}}' }, 3000);
  </script>
  @endif

  @if((request()->segment(1) === 'referral-document' && request()->segment(3)) == 'success' || request()->segment(1) === 'dashboard')
  <script src="{{ asset('assets/plugins/jquery-popup-overlay-gh-pages/jquery.popupoverlay.js') }}"></script>
  <script src="{{ asset('assets/plugins/waitMe/waitMe.js') }}"></script>
  @endif

  @if(request()->segment(1) === 'title-company')
  <script src="{{ asset('assets/plugins/waitMe/waitMe.js') }}"></script>
  <script src="{{ asset('assets/plugins/Bootstrap-Confirmation-2/dist/bootstrap-confirmation.js') }}"></script>
  <script>
  if (window.performance && window.performance.navigation.type == window.performance.navigation.TYPE_BACK_FORWARD) {
    location.reload();
  }
  </script>
  @endif

  @if(request()->segment(1) === 'title-company' && request()->segment(2) === 'approve')
  <script>
  $('[data-toggle=sold]').confirmation({
    rootSelector: '[data-toggle=confirmation]',
    onConfirm: function(value) {
      $.ajax({
        url: '/title-approve-click',
        method: 'post',
        data: { id: '{{ $valid_contract->contract_id }}', status: "1", _token: '{{csrf_token()}}' },
        beforeSend: function() {
          $('#loader').waitMe({
            effect : 'bounce',
            text : '',
            color : '#000',
          });
        },
        success: function(data) {
          //console.log(data);
          if(data == '1'){
            window.location = '/title-approve/1';
            //alert(data);
          }else{
            alert('An error occured while proccessing your request. Please try again later')
          }
        }
      });
    }
  });

  $('[data-toggle=not-sold').confirmation({
    rootSelector: '[data-toggle=confirmation]',
    onConfirm: function(value) {
      $.ajax({
        url: '/title-approve-click',
        method: 'post',
        data: { id: '{{ $valid_contract->contract_id }}', status: "2", _token: '{{csrf_token()}}' },
        beforeSend: function() {
          $('#loader').waitMe({
            effect : 'bounce',
            text : '',
            color : '#000',
          });
        },
        success: function(data) {
          //console.log(data);
          if(data == '1'){
            window.location = '/title-approve/2';
          }else{
            alert('An error occured while proccessing your request. Please try again later')
          }
        }
      });
    }
  });
  </script>
  @endif

  @if(request()->segment(1) == 'referral-document')
  <script>

  var toastr_options = {closeButton : true};
  $('#flash-success').delay(5000).fadeOut(400);

  var load_num = 0;

  $('#esignature-iframe').on('load', function(){
    var listid = {!! request()->segment(2); !!}
    load_num += 1;
    if(load_num >= 2){
      window.location = '/dashboard/reserved-leads-check/'+listid;
    }else if(load_num > 2){
      window.location = '/dashboard';
      //alert('reloading');
    }
  });
  </script>
  @endif
  <!-- end listing page -->
  <!-- end conditonal script -->
  <script>
  var windowScale = window.devicePixelRatio;
  if(windowScale > 1){
    $('#logo').attr('src', '/assets/img/tymbl/logo@2x.png');
  }else{
    $('#logo').attr('src', '/assets/img/tymbl/logo.png');
  }
</script>
<script>
if (navigator.geolocation) {
  var timeoutVal = 10 * 1000 * 1000;
  navigator.geolocation.getCurrentPosition(displayPosition, displayError,
    { enableHighAccuracy: true, timeout: timeoutVal, maximumAge: 0 }
  );
}
else {
  alert("Geolocation is not supported by this browser");
}

var current_loc = '';
function displayPosition(position) {
  $('#demo').html("Latitude: " + position.coords.latitude + ", Longitude: " + position.coords.longitude);
  var latitude = position.coords.latitude;
  var longitude = position.coords.longitude;
  //alert(latitude + " - " + longitude);

  $.ajax({
    url: "/get-location",
    type: "POST",
    data: { lat: latitude, long: longitude, _token: '{{csrf_token()}}' },
    success: function(data) {
      if(data.length >=1){
        current_loc = data[0].zip;
      }else{
        current_loc = '{{isset($zip_code) ? $zip_code : '' }}';
      }
    }
  });
}

function displayError(error) {
  var errors = {
    1: 'Permission denied',
    2: 'Position unavailable',
    3: 'Request timeout'
  };
  return;
}
</script>

<script>
$(function(){
  $('#toggler-refine').popover({
    placement: 'top',
    title: 'Refine Search',
    html:true,
    content:  $('#form-refined').html()
  }).on('click', function(){
    // had to put it within the on click action so it grabs the correct info on submit
    $("#f-near").popover('hide');
    $("#f-loc").popover('hide');
  });

  $('#f-near').popover({
    placement: 'bottom',
    title: 'Search Nearby Location',
    html:true,
    content:  '<div class="slidecontainer-dist"><div class="float-left"><small>Enter Zip Code</small></div><input class="form-control" type="zip" id="zipcode" name="zipcode" placeholder="zip code" value=""><div class="float-left"><small>move the slider</small></div><div class="float-right"><small id="distance-value">1 mile/s</small></div><input type="range" min="1" max="200" value="0" class="slider" id="myRange" name="distance_range"><a href="#" role="button" class="btn btn-lg btn-primary" id="ok-dist">Ok</a></div>'
  }).on('click', function(e){
    //alert(current_loc);
    $('#zipcode').val(current_loc);
    $("#toggler-refine").popover('hide');
    $("#f-loc").popover('hide');

  });

  $('#f-loc').popover({
    placement: 'bottom',
    title: 'Search State/City',
    html:true,
    content:  $('#dropdown-loc').html()
  }).on('click', function(e){
    // had to put it within the on click action so it grabs the correct info on submit
    $("#toggler-refine").popover('hide');
    $("#f-near").popover('hide');
  });

  $('html').on('click', function(e) {
    if ($(e.target).is('#ok-dist')) {
      appendVal = $(e.target).parent().find('#myRange').val() + ' miles';
      $('#mileage').text('within ' + appendVal);
      $('#tagval').css('left', '440px');
      $('#tagval').show('fast');
      $('#loc-range').val($(e.target).parent().find('#myRange').val());
      $('#dstate').val('');
      $('#dcity').val('');
      $('#dzip').val($(e.target).parent().find('#zipcode').val());
      $('#toggler').val('distance');
      $("#f-near").popover('hide');
    }
  });

  $('html').on('click', function(e) {
    if ($(e.target).is('#ok-loc')) {
      $(e.target).parent().find('#derror').html('');
      var state = $(e.target).parent().find('#state option:selected').text();
      var city = $(e.target).parent().find('#city option:selected').text();
      if($(e.target).parent().find('#city option:selected').text().trim() == 'Select city' || $(e.target).parent().find('#state option:selected').text().trim() == 'Select state'){
        $(e.target).parent().find('#derror').html('Please select a state or city.');
      }else{
        $('#mileage').text('in ' + city + ', ' + state);
        $('#tagval').css('left', '380px');
        $('#tagval').show('fast');
        $('#loc-range').val('');
        $('#dstate').val($(e.target).parent().find('#state').val());
        $('#dcity').val($(e.target).parent().find('#city').val());
        $('#toggler').val('location');
        $("#f-loc").popover('hide');
      }
    }
  });

  $('#tagclose').click(function() {
    $('#loc-range').val('');
    $('#dstate').val('');
    $('#dcity').val('');
    $('#dzip').val('');
    $('#tagval').hide();
  });

});

$('html').on('click', function(e) {
  if (!$(e.target).is('#toggler-refine') && !$(e.target).is('#f-near') && !$(e.target).is('#f-loc') && $(e.target).closest('.popover').length == 0) {
    $("#toggler-refine").popover('hide');
    $("#f-near").popover('hide');
    $("#f-loc").popover('hide');
  }
});

$('html').on('click', function(e) {
  if ($(e.target).is('#toggler-distance')) {
    $(e.target).parent().parent().find('#zip').val(current_loc);
  }
});
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127180364-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-127180364-1');
</script>

<script>
$('#hero-search-type').click(function() {
  var cat_type = $(this).val();
  var option = $("#toggler_option:checked").val();

  if(option == 'All'){
    option = 'BuyOrSell';
  }

  if(cat_type == 'condo'){
    gaActionClick(option+"Condo");
  }
  else if(cat_type == 'single-family-home'){
    gaActionClick(option+"SFH");
  }
  else if(cat_type == 'multi-family-home'){
    gaActionClick(option+"MFH");
  }
  else if(cat_type == 'commercial'){
    gaActionClick(option+"Comm");
  }
  else if(cat_type == 'vacant-lot'){
    gaActionClick(option+"Other");
  }

});

function gaActionClick(cattype){
  gtag('event', 'SearchCategorySelected', {
    'event_category': 'SEARCH',
    'event_label': cattype,
  });
}

$('#hero-search').click(function() {
  gtag('event', 'SearchCategorySelected', {
    'event_category': 'SEARCH',
    'event_label': 'SearchBegin',
  });
});

$('#hero-search-area').click(function() {
  var selectedArea = $(this).find('option:selected').text();
  gtag('event', 'SearchAreaSelected', {
    'event_category': 'SEARCH',
    'event_label': selectedArea,
  });
});

</script>

@if(request()->segment(1) === 'search')
@if (Auth::check())
<script>
$('[id="btn-user-remind"]').click(function() {
  var user_id = '{{ Auth::user()->id }}';
  var terms = '{{ $_SERVER['REQUEST_URI'] }}';
  var dbtn = $(this);
  $.ajax({
    url: '/save-user-search',
    type: 'post',
    data: { user_id: user_id, terms: terms, _token: '{{csrf_token()}}' },
    success: function(data) {
      console.log(data);
      if(data == '1'){
        dbtn.addClass('btn-user-remind-clicked');
        dbtn.prop('disabled', true);
        $('.chk-remind').show();
        dbtn.after('<p class="d-block">WWe will notify by email when there are leads meeting your criteria in your area</p>');
      }

    }
  });
});
</script>
@endif
@endif

@if(request()->segment(1) === 'listing')
@if (Auth::check())
<script>
$('#save-lead').click(function() {

  var ad_id = {{ $ad->id }};
  var user_id = '{{ Auth::user()->id }}';
  var dbtn = $(this);

  $.ajax({
    url: '/save-lead',
    type: 'post',
    data: { user_id: user_id, ad_id: ad_id, _token: '{{csrf_token()}}' },
    success: function(data) {
      //console.log(data);

      if(data == '1'){
        dbtn.addClass('ad-watching');
        dbtn.html('<i class="far fa-eye"></i>&nbsp;&nbsp;watching');
        toastr.success(data.msg, 'We will notify by email when there are leads meeting your criteria in your area', toastr_options);
      }else{
        dbtn.removeClass('ad-watching');
        dbtn.html('<i class="far fa-bell"></i>&nbsp;&nbsp;notify me of similar leads');
      }
    }
  });

});
</script>
@endif

<script>
$('#save_as_favorite').click(function() {


  var status = $(this).find('i').attr('rel');
  var selector = $(this);
  var slug = selector.data('slug');

  if(status == '1'){
    gtag('event', 'FavoriteAdded', {
      'event_category': 'ViewItem',
      'event_label': 'Listing',
    });
  }else{
    gtag('event', 'FavoriteRemoved', {
      'event_category': 'Home  Page',
      'event_label': slug,
    });
  }

});

$('#reserve_button').click(function() {
  gtag('event', 'ReserveClicked', {
    'event_category': 'Home  Page',
    'event_label': slug,
  });
});
</script>
@endif

@if(request()->segment(1) === 'login')
<script>
$('#email, #password, #submit, .fb, .twitter, .google, .register').click(function(e){
  gtag('event', 'LoginBegin', {
    'event_category': 'Page',
    'event_label': 'Login',
  });
});
</script>

@if(session()->has('register-message'))
<script>
toastr.success('Your account has been created. A verification code has been sent to your email.', 'Registration Success', toastr_options);
</script>
@endif

@endif

@if (session()->has('message'))
@if(session('message') == 'ga_login')
<script>

gtag('event', 'LoginSuccess', {
  'event_category': 'Login Page',
  'event_label': 'Login',
});
</script>
@endif

@endif

@if(request()->segment(1) === 'search')
<script>

$("a[id^='item']").click(function(e){
  gtag('event', 'SearchResultClicked', {
    'event_category': 'Search Page',
    'event_label': $(this).text(),
  });
});
</script>
@endif

@if (session()->has('success'))
@if(session('success') == 'Listing Saved Successfully')
<script>
alert('New listing created successfully');

gtag('event', 'PostAdSuccess', {
  'event_category': 'Post Page',
  'event_label': session('message'),
});

</script>
@endif
@endif

@if(request()->segment(1) === 'referral-document' && request()->segment(3) == 'success')
<!-- edit title company popup -->
<script>
$('#rating_popup').popup('hide');

$('[id="edit-title-company"]').click(function() {
  window.location = '/dashboard/u/posts/profile/edit#rep';
});

$('[id="ok-title-company"]').click(function() {
  //alert('hey');

  $('#loader').waitMe({
    effect : 'bounce',
    text : '',
    color : '#000',
  });
  $.ajax({
    url: '/send-contract-to-tc',
    method: 'post',
    data: { id: '{{ request()->segment(2) }}', code: '{{ $code }}', title_company_name: '{{$title_company_name}}', url: '{{ $url }}', email: '{{ $email }}', _token: '{{csrf_token()}}' },
    success: function(data) {
      // console.log(data);
      if(data == '1'){
        window.location = '/dashboard/reserved-leads';
      }
    }
  });
});

$('[id="ok-title-company-buy"]').click(function() {

});
</script>

<!-- rating popup -->
<script>
$('#rating_popup').popup('show');
$('[id="popup_close"]').click(function() {
  $('#rating_popup').popup('hide');
});

$('[id="rating"]').click(function() {
  var item_rating = $(this).attr('rel');
  var currentListID = {{request()->segment(2)}};
  $(this).addClass('check');
  $(this).nextAll().addClass('check');
  $(this).prevAll().removeClass('check');

  if(item_rating == 1){
    $('#option').fadeIn('slow');
    $('[id="reason"]').on('click', function() {
      var reason = $(this).val();
      if( reason == 'Other'){
        $('#reason-details').fadeIn();
        $('#thank-you-feedback').fadeOut();
        $('#other-details').val('');
        $('#submit-feedback').click(function() {
          if($('#other-details').val() == ''){
            alert('Detail should not be empty.');
          }else{
            submit_rating(item_rating, currentListID, reason, $('#other-details').val());
            $('#other-details').val('');
            $(this).fadeOut();
          }
        });
      }else{
        $('#reason-details').fadeOut('slow');
        submit_rating(item_rating, currentListID, reason, '');
        $('#thank-you-feedback').fadeIn('slow');
      }
    });
  }else{
    submit_rating(item_rating, currentListID, '', '');
    $('#option').fadeOut('slow');
  }
});

function submit_rating(item_rating, list_id, reason, other){
  $.ajax({
    url: '/post-rating',
    type: 'post',
    data: { id: item_rating, list_id: list_id, reason: reason, other: other, _token: '{{csrf_token()}}' },
    success: function(data) {
      $('#thank-you-feedback').fadeIn();
      //console.log(data);
    }
  });
}
</script>
@endif

<script>
$('.itemListView').click(function (e) {
  e.preventDefault();
  switchItemView('itemListView');
});

$('.itemGridView').click(function (e) {
  e.preventDefault();
  switchItemView('itemGridView');
});

$('.itemImageListView').click(function (e) {
  e.preventDefault();
  switchItemView('itemImageListView');
});

function setInitialItemViewMode(isSavedViewMode) {
  switchItemView(isSavedViewMode);
}

//setInitialItemViewMode(isSavedViewMode);

function switchItemView(mode){
  if (mode == 'itemListView'){
    $('.recent-single').removeClass('col-lg-3').addClass('col-lg-12').css('margin-bottom', '30px');
    $('.recent-thumb').addClass('imglist').removeClass('imggrid');
    $('.recent-thumb').find('img').removeClass('imggrid-img').addClass('imglist-img').removeClass('imglv-img');
    $('.recent-body').removeClass('imglv');
    $('.recent-floating-caption').css('display', 'block');
    $('[id="recent-title2"]').hide();
  }else if (mode == 'itemGridView'){
    $('.recent-single').removeClass('col-lg-12').addClass('col-lg-3');
    $('.recent-thumb').addClass('imggrid').removeClass('imglist');
    $('.recent-thumb').find('img').addClass('imggrid-img').removeClass('imglist-img').removeClass('imglv-img');
    $('.recent-body').removeClass('imglv');
    $('.recent-floating-caption').css('display', 'block');
    $('[id="recent-title2"]').hide();
  }else if(mode == 'itemImageListView'){
    $('.recent-single').removeClass('col-lg-3').addClass('col-lg-12').css('margin-bottom', '30px');
    $('.recent-thumb').removeClass('imggrid').removeClass('imglist');
    $('.recent-thumb').find('img').removeClass('imggrid-img').removeClass('imglist-img').addClass('imglv-img');
    $('.recent-body').addClass('imglv');
    $('.recent-floating-caption').css('display', 'none');
    //$( ".recent-title" ).clone().appendTo( ".recent-body" );
    $('[id="recent-title2"]').show();
    $('[id="recent-title2"]').css('position', 'absolute');
    $('[id="recent-title2"]').css('z-index', '9999999');
  }
}
</script>

@if(request()->segment(1) == 'reserved-leads-success')
<script>
var id = '{{request()->segment(2)}}';
//alert('hello there ' + id);
$.ajax({
  url: '/send-referral-success',
  type: 'post',
  data: { id: id, _token: '{{csrf_token()}}' },
  success: function(data) {
  }
});
</script>
@endif

@if(request()->segment(1) == 'leads-by-user')
<script>
switchItemView('itemImageListView');
</script>

@endif
<script>
$('.sup-info').tooltip({title: "<h5 class='htl'>Reservation fee</h5><ul class='tl'><li>The fee is collected and held by Tymbl much like an escrow</li><li>If the referral results in a successful sale, your title company rep will confirm the sale and these funds will transfer to the agent that posted this referral and you will receive your percentage of the commission</li></ul>", html: true, placement: "bottom"});

$('.sup-info2').tooltip({title: "<h5 class='htl'>Reservation fee</h5><div class='tl'>This the amount you want to charge the other party to reserve your referral. You will receive this amount in addition to your percentage of the commissions as set fourth in the referral agreement if the referral results in a successful sale or purchase. If it doesn't, this amount will be release back to the other party</div>", html: true, placement: "top"});

$('.sup-info3').tooltip({title: "<h5 class='htl'>Referral fee</h5><div class='tl'>The Referral fee is the % of the commission you require to refer your client. The % you set will be reflected in the Referral agreement.</div>", html: true, placement: "top"});

$('.sup-desc').tooltip({title: "Please provide some notes about the lead here. Anything you may know about it except for the actual contact info.", html: true, placement: "bottom"});

$('.sup-info-reserved').tooltip({title: "<h5 class='htl'>Reservation Status</h5><ul class='tl'><li>These leads were already reserved by a different agent, but feel free to register to be notified of other opportunities that are added daily.</li></ul>", html: true, placement: "bottom"});

</script>

<script>
if (location.href.indexOf("#comment") != -1) {
  $('html, body').animate({
    scrollTop: $(".comment-section").offset().top - 120
  }, 100);
  $('#comment-area').focus();
}
</script>

<script>
if($(".btn-action-floating-resp").is(":visible")){
  $(window).scroll(function() {
    if ($(window).scrollTop() > 1537) {
      $('.btn-action-floating-resp').slideUp();
    }else{
      $('.btn-action-floating-resp').slideDown();
    }
  });
}
</script>

<!--for the new layout -->
<script>

$('div[id="save-as-favorite"]').click(function (e){
  e.stopPropagation();
  var selector = $(this);
  var slug = selector.data('slug');
  var id = selector.attr('rel');
  $.ajax({
    type : 'POST',
    url : '/save-ad-as-favorite',
    data : { id: id, slug : slug, action: 'add',  _token : '{{ csrf_token() }}' },
    success : function (data) {
      if (data.status == 1){
        //console.log(data.msg);
        if(!data.msg.includes("remove")){
          $(selector).removeClass('recent-favorite-active');
          $(selector).attr('data-original-title', 'Add to Favorites');
          gtag('event', 'FavoriteRemoved', {
            'event_category': 'Home  Page',
            'event_label': slug,
          });

        }else{
          $(selector).addClass('recent-favorite-active');
          $(selector).attr('data-original-title', 'Remove from Favorites');
          gtag('event', 'FavoriteAdded', {
            'event_category': 'Home  Page',
            'event_label': slug,
          });
        }
      }else {
        if (data.redirect_url){
          location.href= data.redirect_url;
        }
      }
    }
  });
});

$('div[id="single-section"]').click(function (){
  var selector = $(this);
  var slug = selector.data('slug');
  var id = selector.attr('rel');
  location.href= '/listing/'+id+'/'+slug;
});

$('[id="a-scrollto"]').click(function() {
  var linkrel = $(this).attr('rel');
  $('html, body').animate({
    scrollTop: $("#"+linkrel).offset().top - 200
  }, 100);
  linkrel = '';
});

</script>

<script>
$(document).ready(function() {

  var $videoSrc;

  $('.video-btn').click(function() {
    $videoSrc = $(this).data( "src" );
  });
  //console.log($videoSrc);

  $('#myModal').on('shown.bs.modal', function (e) {
    $("#video").attr('src',$videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0" );
  })

  $('#myModal').on('hide.bs.modal', function (e) {
    $("#video").attr('src','');
  })

  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
  	$(body).css('background-position', 'center 120px');

  }
  // document ready
});
</script>

@if(request()->segment(1) == 'sign-contract' && request()->segment(2) == 'plan')
<script>

var toastr_options = {closeButton : true};
$('#flash-success').delay(5000).fadeOut(400);

var load_num = 0;

$('#esignature-iframe').on('load', function(){
  var contract_id = '{{ $contract_id }}';
  var user_id = '{{ $user->id }}';
  var plan_option = '{{ request()->option }}';
  load_num += 1;
  if(load_num >= 2){
    window.location = '/sign-contract/check-contract-signed/?id='+contract_id+'&plan_type='+plan_option;
  }else if(load_num > 2){
    window.location = '/dashboard';
  }
});
</script>
@endif

@if(request()->segment(1) == 'referral-vip-sign')
<script>
var load_num = 0;
var id='{{request()->segment(2)}}';


$('#esignature-iframe').on('load', function(){

  load_num += 1;
  if(load_num >= 2){
    $("html, body").animate({
      scrollTop: 0
    }, 100);
    window.location = '/dashboard/reserved-leads-check/'+id;
  }else if(load_num > 2){
    $("html, body").animate({
      scrollTop: 0
    }, 100);
    window.location = '/dashboard/reserved-leads-check/'+id;
  }
});
</script>
@endif

</body>
</html>
