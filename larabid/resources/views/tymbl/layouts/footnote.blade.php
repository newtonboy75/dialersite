<section class="foot-note">
    <div class="container">
        @include('tymbl.layouts.features_horizontal')
        <div class="row">
            <div class="col-sm-12 posting-buttons clearfix text-center">
                <a href="{{ route('search') }}" role="button" id="" class="btn btn-outline-primary btn-lg text-center"><i class="fas fa-search"></i>Browse Leads</a>
                <a href="{{ route('create_ad') }}" role="button" id="" class="btn btn-outline-primary btn-lg text-center"><i class="far fa-save"></i>post new lead</a>
            </div>
        </div>
    </div>
</section <!--/.foot note-->
