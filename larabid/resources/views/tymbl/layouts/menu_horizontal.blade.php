<div class="logo-top">
    <a href="/" class="navbar-brand"><img src="{{ asset('assets/img/tymbl/logo.png') }}" alt="Tymbl Logo" id="logo"></a>
</div>

<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation">
        <span class="icon-bar top-bar"></span>
        <span class="icon-bar middle-bar"></span>
        <span class="icon-bar bottom-bar"></span>
</button>
