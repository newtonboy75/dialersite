@if(count($featured_ads) > 0)
<section class="single-section featured-items">
  <div class="container">
    <div class="section-title">
      <h1>Screened and Qualified Leads</h1>
    </div>

    <div class="featured-property">
      <div class="row owl-carousel owl-theme">

        @foreach($featured_ads as $featured)
        <div class="col featured-property-single item">
          <div class="featured-thumb">

            @if($featured->cat_type_status == 'unqualified')
              <div class="featured-unq">UNQUALIFIED</div>
            @endif

            @php
            $images = \App\Media::where('ad_id', $featured->id)->orderBy('created_at', 'desc')->first();

            @endphp

            @if($images['media_name'] && $images['media_name'] != 'NULL')
              @if(file_exists('uploads/images/thumbs/'.$images['media_name']))
                <img src="/uploads/images/thumbs/{{$images['media_name']}}" class="img-responsive" />
              @else
                <img src="/uploads/images/{{$images['media_name']}}" class="img-responsive" />
              @endif
            @else
              @if($featured->category_type == "selling")
              <img src="{{ asset('assets/img/tymbl/house.png') }}" class="img-responsive" />
              @else
              <img src="{{ asset('assets/img/tymbl/person.png') }}" class="img-responsive" />
              @endif
            @endif

            @php
            $features = unserialize($featured->feature_1);
            $filled_features['Beds'] = $features[0];
            $filled_features['Baths'] = $features[1];
            $filled_features['Sq Ft'] = $features[2];
            $filled_features['Garages'] = $features[3];
            $filled_features['Kitchens'] = $features[4];
            $filled_features['Balconies'] = $features[5];


            @endphp

            <div class="featured-price">
              <div class="price">
                <h2>
                  @if((float)$featured->referral_fee > 0.00)
                    {{ (float)$featured->referral_fee*100 }}%<br><span style="font-size:16px;">referral fee</span>
                  @else
                    @if($featured->country_id=='231')
                      @php
                        setlocale(LC_MONETARY, 'en_US');
                        echo '$'.number_format($featured->escrow_amount,2);
                      @endphp
                    @else
                      @php
                        echo 'CA$'.number_format($featured->escrow_amount,2);
                      @endphp
                    @endif
                    <br><span style="font-size:16px;">reservation fee</span>
                  @endif
              </div>
            </div>

            @if($featured->is_my_favorite())
            <div class="featured-favorite featured-favorite-active" data-toggle="tooltip" data-placement="top" data-slug="{{$featured->slug}}">
              <i class="far fa-heart"></i>
            </div>
            @else
            <div class="featured-favorite" data-toggle="tooltip" data-placement="top" data-slug="{{$featured->slug}}">
              <i class="far fa-heart"></i>
            </div>
            @endif
          </div><!-- /.featured-thumb -->
          <div class="featured-body">
            <a href="listing/{{$featured->id}}/{{$featured->slug}}" class="featured-title" title="{{ safe_output($featured->title) }}"><h4>{{ safe_output(str_limit($featured->title, 60)) }}</h4></a>
            <small><i class="fas fa-map-marker-alt"></i> {{ $featured->city->city_name }}, {{ $featured->state->state_name }}, {{ $featured->country->country_code }}</small>



            <div class="featured-details">
              <div class="row">

                @php $i=0; @endphp
                @foreach($filled_features as $k=>$f)
                @if(!$f == '0' || !empty($f))
                @php $i += 1; @endphp
                <div class="col-sm-4">{{$k}}: {{$f}}</div>
                @endif
                @if($i==3)
                @php break; @endphp
                @endif
                @endforeach
              </div>
              <div class="row">
                <div class="col">
                  {{ $featured->category_name }}
                </div>
              </div>
            </div><!-- /.featured-details -->
            <div class="featured-view-button text-right">
              <a href="listing/{{$featured->id}}/{{$featured->slug}}">View Details <i class="fas fa-long-arrow-alt-right"></i></a>
            </div>
          </div><!-- /.featured-body -->
        </div><!-- /.featured-property-single -->
        @endforeach
      </div>
    </div><!-- /.featured-property -->
  </div><!-- /.container -->
</section>
@endif
<!-- /.featured-items -->
