<footer class="core-features">
    <div class="container">
        <div class="section-title">
            <h1 style="text-align: center">Tymbl is Different</h1>
            <h3 style="text-align: center">With some incredible features</h3>
        </div>
        <div class="row all-features justify-content-md-center">
            <div class="col-sm-3 text-center">
                <img src="{{ asset('assets/img/tymbl/electronic.png') }}" alt="Excellent Value" class="center-block">
                <h4>Electronic Referral Agreement</h4>
            </div>
            <div class="col-sm-3 text-center">
                <img src="{{ asset('assets/img/tymbl/escrow.png') }}" alt="Excellent Value" class="center-block">
                <h4>Free Lead Qualification</h4>
            </div>
            <div class="col-sm-3 text-center">
                <img src="{{ asset('assets/img/tymbl/notification.png') }}" alt="Excellent Value" class="center-block">
                <h4>Referrals-in-your-area Notifications</h4>
            </div>
        </div>
        <div class="clearfix"></div>


        <div class="copyright">
          <ul>
            <li><a href="/page/privacy-policy">Privacy Policy</a></li>
            <li>.</li>
            <li><a href="/page/terms-and-conditions-1">Terms & Conditions</a></li>
            <li>.</li>
            <li><a href="/page/frequently-asked-questions">FAQs</a></li>
            <li>.</li>
            <li><a href="/contact-us">Contact Us</a></li>
          </ul>
          <br><br>
          Copyright 2019, Tymbl All rights reserved</div>
    </div>
</footer>
