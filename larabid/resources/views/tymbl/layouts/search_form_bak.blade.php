<div class="row justify-content-md-center">
  <div class="hero-search-form">
    <form action="{{url('search')}}" method="get"><div class="search-radio-label">&nbsp;&nbsp;PROSPECT LOOKING TO </dive>
      <div class="form-check form-check-inline search-radio"><input class="form-check-input" name="toggler_option" value="Buy" type="radio"  id="toggler_option" {{ Request::get('toggler_option') == 'Buy' ? 'checked' : '' }}><label class="form-check-label" for="toggler-distance">BUY</label></div>

      <div class="form-check form-check-inline search-radio"><input class="form-check-input" name="toggler_option" value="Sell" type="radio" id="toggler_option" {{ Request::get('toggler_option') == 'Sell' ? 'checked' : '' }}><label class="form-check-label" for="toggler-distance">SELL</label></div>

      <div class="form-check form-check-inline search-radio"><input class="form-check-input" name="toggler_option" value="All" type="radio" id="toggler_option" {{ Request::get('toggler_option') == 'All' ? 'checked' : '' }} @php if(Request::get('toggler_option') === null){
         echo 'checked'; } @endphp><label class="form-check-label" for="toggler-distance">BROWSE ALL LEADS</label></div>
      <div class="input-group input-group-sm">
        <div class="input-group-prepend mb-3">
          <select class="custom-select" name="listing_type" id="hero-search-type">
            <option value="all">Type</option>
            <option value="condo" {{ Request::get('listing_type') == 'condo' ? 'selected' : '' }}>Condo</option>
            <option value="single-family-home" {{ Request::get('listing_type') == 'single-family-home' ? 'selected' : '' }}>Single-Family Home</option>
            <option value="multi-family-home" {{ Request::get('listing_type') == 'multi-family-home' ? 'selected' : '' }}>Multi-Family Home</option>
            <option value="commercial" {{ Request::get('listing_type') == 'commercial' ? 'selected' : '' }}>Commercial</option>
            <option value="vacant-lot" {{ Request::get('listing_type') == 'vacant-lot' ? 'selected' : '' }}>Vacant Lot</option>
          </select>
          <select class="custom-select" name="state_id" id="hero-search-area">
            <option value="all">State/Province</option>
            @foreach($states as $state)
            <option value="{{$state->id}}" {{ Request::get('state_id') == $state->id ? 'selected' : '' }}>{{$state->state_name}}</option>
            @endforeach
          </select>
          <input name="hero_search" id="hero-search" class="btn btn-primary" type="submit" value="SEARCH">
          <div class="font-weight-bold refine-search-link"><a role="button" href="#" id="toggler-refine"><i style="color:blue" class="fas fa-filter"></i> REFINE SEARCH</a></div>
        </div>
      </div>
    </form>
  </div>
</div>

<!-- start search popup -->
<div id="form-refined" class="hide">
  <form action="{{url('search')}}" id="popForm" method="get">
    <div style="display:none">Match:
      <div class="form-check form-check-inline">
        <input name="match" class="form-check-input" type="radio" id="match" value="1">
        <label class="form-check-label" for="match">Exact Match</label>
      </div>

      <div class="form-check form-check-inline">
        <input name="match" class="form-check-input" type="radio" id="match" value="2">
        <label class="form-check-label" for="match">Any</label>
      </div>
    </div>
    <input name="q" type="text" class="form-control" value="" placeholder="Enter address, zip code, city or state">
    <div><small>Location</small></div>
    <div>
      <ul id="sliders-container">
        <li>
          <div class="form-check form-check-inline"><input class="form-check-input" name="toggler_location" value="distance" type="radio" id="toggler-distance"><label class="form-check-label" for="toggler-distance">Search Nearby</label></div>
          <div class="slidecontainer-distance">
            <div class="float-left"><small>Enter Zip Code</small></div>
            <input class="form-control" type="zip" name="zipcode" placeholder="zip code" value="{{$zip_code}}">
            <div class="float-left"><small>move the slider</small></div>
            <div class="float-right"><small id="distance-value">1 mile/s</small></div>
            <input type="range" min="1" max="200" value="0" class="slider" id="myRange" name="distance_range">

          </div>
        </li>
        <li>
          <div class="form-check form-check-inline"><input class="form-check-input" name="toggler_location" value="location" type="radio" id="toggler-location"><label class="form-check-label" for="toggler-location">Search by Location</label></div>
          <div class="slidecontainer-location">
            <select class="custom-select" name="state" id="state">
              <option selected disabled value="all">State</option>
              @foreach($states as $state)
              <option value="{{$state->id}}">{{$state->state_name}}</option>
              @endforeach
            </select>
            <select class="custom-select" name="city" id="city" style="margin-top: 6px;">
            </select>
          </div>
        </li>
      </ul>
    </div>
    <br>
    <div><small>Price Range</small></div>
    <div class="form-check form-check-inline display-block">
      <select class="custom-select" name="price" id="price-range" style="width: 250px;">
        <option selected disabled value="all">Choose Price Range</option>
        <option value="0-50">< $50K</option>
        <option value="51-100">$51K - $100K</option>
        <option value="101-200">$101K - $200K</option>
        <option value="201-300">$201K- $300K</option>
        <option value="301-400">$301K - $400K</option>
        <option value="401-500">$401K - $500K</option>
        <option value="501-600">$501K - $600K</option>
        <option value="601-700">$601K - $700K</option>
        <option value="701-800">$701K - $800K</option>
        <option value="801-900">$801K - $900K</option>
        <option value="901-1m">$901K - $1 million</option>
        <option value="1m-2m">$1million - $2 million</option>
        <option value="2m-3m">$2million - $3 million</option>
        <option value="3m-4m">$3million - $4 million</option>
        <option value="4m-5m">$4million - $5 million</option>
        <option value=">5m">> 5 million</option>
      </select>
    </div>
    <br><br>
    <div><small>Looking For</small></div>
    <div class="form-check form-check-inline">
      <input class="form-check-input" name="cat_type" value="buying" type="radio" id="lookingToBuy">
      <label class="form-check-label" for="lookingToBuy">Buy</label>
    </div>

    <div class="form-check form-check-inline">
      <input class="form-check-input" name="cat_type" value="selling" type="radio" id="lookingToSell">
      <label class="form-check-label" for="lookingToSell">Sell</label>
    </div>

    <div class="form-check form-check-inline">
      <input class="form-check-input" name="cat_type" value="all" type="radio" id="lookingAll">
      <label class="form-check-label" for="lookingAll">Both</label>
    </div>
    <div>
<br>
      <div><small>Listing Type</small></div>
      <div class="form-check form-check-inline">
        <input name="listing_type[0]" class="form-check-input" type="checkbox" id="security" value="condo">
        <label class="form-check-label" for="security">Condo</label>
      </div>

      <div class="form-check form-check-inline">
        <input name="listing_type[1]" class="form-check-input" type="checkbox" id="security" value="single-family-home">
        <label class="form-check-label" for="security">Single-Family Home</label>
      </div>

      <div class="form-check form-check-inline">
        <input name="listing_type[2]" class="form-check-input" type="checkbox" id="security" value="multi-family-home">
        <label class="form-check-label" for="security">Multi-Family Home</label>
      </div>

      <div class="form-check form-check-inline">
        <input name="listing_type[3]" class="form-check-input" type="checkbox" id="security" value="commercial">
        <label class="form-check-label" for="security">Commercial</label>
      </div>

      <div class="form-check form-check-inline">
        <input name="feature2[4]" class="form-check-input" type="checkbox" id="security" value="vacant-lot">
        <label class="form-check-label" for="security">Vacant Lot</label>
      </div>
    </div>
<br>
    <div><small>Features</small></div>
    <div id="input-narrow">

      <div class="row">
        <div class="col"><label for="room">Bedrooms</label>&nbsp;<input id="room" type="number" class="form-control input-narrow" name="feature[0]" value=""></div>
        <div class="col"><label for="room">Bath:</label>&nbsp;<input id="room" type="number" class="form-control input-narrow"  name="feature[1]" value=""></div>
      </div>

      <div class="row">
        <div class="col"><label for="room">Sq Feet:</label>&nbsp;<input id="room" type="number" class="form-control input-narrow"  name="feature[2]" value=""></div>
        <div class="col"><label for="room">Garages:</label>&nbsp;<input id="room" type="number" class="form-control input-narrow"  name="feature[3]" value=""></div>
      </div>

      <div class="row">
        <div class="col"><label for="room">Kitchen:</label>&nbsp;<input id="room" type="number" class="form-control input-narrow"  name="feature[4]" value=""></div>
        <div class="col"><label for="room">Balcony:</label>&nbsp;<input id="room" type="number" class="form-control input-narrow"  name="feature[5]" value=""></div>
      </div>
    </div>

    <div class="form-check form-check-inline">&nbsp;&nbsp;
      <input name="feature2[0]" class="form-check-input" type="checkbox" id="security" value="security">
      <label class="form-check-label" for="security">24 hr security</label>
    </div>

    <div class="form-check form-check-inline">&nbsp;&nbsp;
      <input name="feature2[1]" class="form-check-input" type="checkbox" id="laundry" value="laundry">
      <label class="form-check-label" for="laundry">Laundry Services</label>
    </div>

    <div class="form-check form-check-inline">&nbsp;&nbsp;
      <input name="feature2[2]" class="form-check-input" type="checkbox" id="garden" value="garden">
      <label class="form-check-label" for="garden">Garden</label>
    </div>

    <div class="form-check form-check-inline">&nbsp;&nbsp;
      <input name="feature2[3]" class="form-check-input" type="checkbox" id="swimming" value="swimming">
      <label class="form-check-label" for="swimming">Swimmig pool</label>
    </div>

    <div class="form-check form-check-inline">&nbsp;&nbsp;
      <input name="feature2[4]" class="form-check-input" type="checkbox" id="tennis" value="tennis">
      <label class="form-check-label" for="tennis">Tennis Court</label>
    </div>

    <div class="form-check form-check-inline">&nbsp;&nbsp;
      <input name="feature2[5]" class="form-check-input" type="checkbox" id="school" value="school">
      <label class="form-check-label" for="school">Close to Local Schools</label>
    </div>

    <div class="form-check form-check-inline">&nbsp;&nbsp;
      <input name="feature2[6]" class="form-check-input" type="checkbox" id="pet" value="pet">
      <label class="form-check-label" for="pet">Pet Friendly</label>
    </div>

    <div class="form-check form-check-inline">&nbsp;&nbsp;
      <input name="feature2[7]" class="form-check-input" type="checkbox" id="lift" value="lift">
      <label class="form-check-label" for="lift">Lift</label>
    </div>

    <div class="form-check form-check-inline">&nbsp;&nbsp;
      <input name="feature2[8]" class="form-check-input" type="checkbox" id="access" value="access">
      <label class="form-check-label" for="access">Access Controlled</label>
    </div>

    <div class="text-center"><br/>
      <input type="submit" class="btn btn-primary" value="Search">
      <br/><br/>
    </div>
  </form>
</div>
