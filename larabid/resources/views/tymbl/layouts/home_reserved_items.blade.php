@if(count($reserved_ads) > 0)
<section class="single-section recent">
  <div class="container">

    <div class="recent-items mx-auto">
      <a id="section-reserved"></a>
      <h2 class="single-title-title mx-auto" style="width: 65%;">RESERVED LEADS</h2>

      @foreach($reserved_ads as $recent)
      @php
      $images = \App\Media::where('ad_id', $recent->id)->orderBy('created_at', 'desc')->where('type', '=', 'image')->first();

      $recent_features = unserialize($recent->feature_1);

      $filled_features['bedroom'] = $recent_features[0];
      $filled_features['bathroom'] = $recent_features[1];
      $filled_features['sq ft'] = $recent_features[2];
      $filled_features['garage'] = $recent_features[3];
      $filled_features['kitchen'] = $recent_features[4];
      $filled_features['balconies'] = $recent_features[5];

      $features_recent = array_filter($filled_features, function($value) { return $value !== ''; });
      $featured_text = '';

      foreach(array_slice($features_recent, 0, 3) as $k=>$v){
        if($v != ''){
          $featured_text .= $v.' '.$k.' &#183; ';
        }
      }

      @endphp

      <div class="mx-auto row section-single" id="single-section" rel="{{$recent->id}}" data-slug="{{$recent->slug}}">

        <div class="col-sm-12 showonmobile">
          <div  style="display: inline; width: 100px;">@if($recent->category_type == 'buying')
            <img src="{{ asset('assets/img/tymbl/buyer-referral.png') }}" style="width: 50px; vertical-align: top">
            @php $marker_color = '#ffa242'; @endphp
            @elseif($recent->category_type == 'selling')
            <img src="{{ asset('assets/img/tymbl/seller-referral.png') }}" style="width: 50px; vertical-align: top">
            @php $marker_color = '#4285f4'; @endphp
            @else
              @if($recent->cat_type_status == 'qualified')
                <img src="{{ asset('assets/img/tymbl/icon_qualified.png') }}" style="width: 50px; vertical-align: top">
                @php $marker_color = '#ffa242'; @endphp
              @else
                <img src="{{ asset('assets/img/tymbl/icon_unqualified.png') }}" style="width: 50px; vertical-align: top">
                @php $marker_color = '#4285f4'; @endphp
              @endif
            @endif

            @if(!$recent->is_my_favorite())
            <div class="category-like" data-toggle="tooltip" data-placement="top" rel="{{$recent->id}}"
              data-slug="{{$recent->slug}}" id="save-as-favorite" data-original-title="Add to favorites">
              <i class="fas fa-heart"></i>
            </div>
            @else
            <div class="category-like recent-favorite-active" data-toggle="tooltip" data-placement="top" rel="{{$recent->id}}"
              data-slug="{{$recent->slug}}" id="save-as-favorite" data-original-title="Remove from favorite">
              <i class="fas fa-heart"></i>
            </div>
            @endif

          </div>
          <div style="display: inline-block; width: 200px; padding-left: 10px;">
            <div class="address-section" style="font-size: 1.0rem"><i class="fas fa-map-marker-alt" style="color: {{ $marker_color }}"></i>&nbsp;&nbsp;{{ isset($recent->city->city_name) ? $recent->city->city_name .',' : '' }} {{ isset($recent->state->state_name) ? $recent->state->state_name  : '' }}</div>
            <div>Referral Fee:
              @if((float)$recent->referral_fee > 0.00)
              @if($recent->get_ad_status() == 'lead-vip-free-active')
                {{ (float)($recent->referral_fee*100)-10 }}%
              @else
                {{ (float)($recent->referral_fee*100) }}%
              @endif <span class="to-reserve">referral fee</span>
              @else
              @if($recent->country_id=='231')
              @php
              setlocale(LC_MONETARY, 'en_US');
              echo '$'.number_format($recent->escrow_amount,2);
              @endphp
              @else
              @php
              echo 'CA$'.number_format($recent->escrow_amount,2);
              @endphp
              @endif
              <span class="to-reserve">reservation fee</span>
              @endif
            </div>
            <div class="desc">{{ safe_output(str_limit($recent->title, 50)) }}</div>
            <div>{{ html_entity_decode($featured_text) }}</div>
          </div>
        </div>

        <div class="col-sm-2 hideonmobile">
          @if($recent->category_type == 'buying')
          <img src="{{ asset('assets/img/tymbl/buyer-referral.png') }}" class="img-lead-type">
          @php $marker_color = '#ffa242'; @endphp
          @elseif($recent->category_type == 'selling')
          <img src="{{ asset('assets/img/tymbl/seller-referral.png') }}" class="img-lead-type">
          @php $marker_color = '#4285f4'; @endphp
          @else
            @if($recent->cat_type_status == 'qualified')
              <img src="{{ asset('assets/img/tymbl/icon_qualified.png') }}" class="img-lead-type">
              @php $marker_color = '#ffa242'; @endphp
            @else
              <img src="{{ asset('assets/img/tymbl/icon_unqualified.png') }}" class="img-lead-type">
              @php $marker_color = '#4285f4'; @endphp
            @endif
          @endif
        </div>
        <div class="col-sm-10 contents-right hideonmobile">
          @if(!$recent->is_my_favorite())
          <div class="category-like " data-toggle="tooltip" data-placement="top" rel="{{$recent->id}}"
            data-slug="{{$recent->slug}}" id="save-as-favorite" data-original-title="Add to favorites">
            <i class="fas fa-heart"></i>
          </div>
          @else
          <div class="category-like recent-favorite-active" data-toggle="tooltip" data-placement="top" rel="{{$recent->id}}"
            data-slug="{{$recent->slug}}" id="save-as-favorite" data-original-title="Remove from favorite">
            <i class="fas fa-heart"></i>
          </div>
          @endif
          <div class="address-section"><i class="fas fa-map-marker-alt" style="color: {{ $marker_color }}"></i>&nbsp;&nbsp;{{ isset($recent->city->city_name) ? $recent->city->city_name .',' : '' }} {{ isset($recent->state->state_name) ? $recent->state->state_name  : '' }}</div>
          <div>Referral Fee:
            @if((float)$recent->referral_fee > 0.00)
            @if($recent->get_ad_status() == 'lead-vip-free-active')
              {{ (float)($recent->referral_fee*100)-10 }}%
            @else
              {{ (float)($recent->referral_fee*100) }}%
            @endif <span class="to-reserve">referral fee</span>
            @else
            @if($recent->country_id=='231')
            @php
            setlocale(LC_MONETARY, 'en_US');
            echo '$'.number_format($recent->escrow_amount,2);
            @endphp
            @else
            @php
            echo 'CA$'.number_format($recent->escrow_amount,2);
            @endphp
            @endif
            <span class="to-reserve">reservation fee</span>
            @endif
          </div>
          <div class="desc">{{ safe_output(str_limit($recent->title, 50)) }}</div>
          <div>{{ html_entity_decode($featured_text) }}</div>


        </div>
      </div>

      <!-- /.recent-signle -->
      @endforeach
      <div class="clearfix"></div>


    </div>

    <div class="load-more-hp text-right">

      <a href="{{ route('search')}}/?qtype=3&q=" role="button" class="pull-right">see all</a>
      <!-- {{ $recent_ads->links() }} -->

    </div>

  </div>
</div>

</section>
@endif
<!-- /.Recent items -->
