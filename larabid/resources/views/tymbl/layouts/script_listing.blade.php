<script>
$('#save_as_favorite').click(function(){

    var selector = $(this);
    var slug = selector.data('slug');

    $.ajax({
        type : 'POST',
        url : '{{ route('save_ad_as_favorite') }}',
        data : { slug : slug, action: 'add',  _token : '{{ csrf_token() }}' },
        success : function (data) {
          console.log(data);
            if (data.status == 1){

                selector.html(data.msg);
            }else {
                if (data.redirect_url){
                    location.href= data.redirect_url;
                }
            }
        }
    });
});
</script>
