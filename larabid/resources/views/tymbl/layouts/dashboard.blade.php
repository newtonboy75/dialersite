<!DOCTYPE html>

<html lang="en">

<!-- begin::Head -->
<head>
  <meta charset="utf-8" />
  <title>Tymbl | Dashboard</title>
  <meta name="description" content="Latest updates and statistic charts">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
  @yield('css')

  <!--begin::Web font -->
  <script src="//ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
  <script>
  WebFont.load({
    google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
    active: function() {
      sessionStorage.fonts = true;
    }
  });
  </script>

  <!--end::Web font -->

  <!--begin:: Global Mandatory Vendors -->
  <link href="{{ asset('assets/js/tymbl/vendors/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet" type="text/css" />

  <!--end:: Global Mandatory Vendors -->

  <!--begin:: Global Optional Vendors -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="{{ asset('assets/js/tymbl/vendors/tether/dist/css/tether.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/bootstrap-select/dist/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/nouislider/distribute/nouislider.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/owl.carousel/dist/assets/owl.carousel.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/owl.carousel/dist/assets/owl.theme.default.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/ion-rangeslider/css/ion.rangeSlider.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/ion-rangeslider/css/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/dropzone/dist/dropzone.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/summernote/dist/summernote.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/animate.css/animate.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/toastr/build/toastr.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/jstree/dist/themes/default/style.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/morris.js/morris.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/chartist/dist/chartist.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/socicon/css/socicon.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/vendors/line-awesome/css/line-awesome.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/vendors/flaticon/css/flaticon.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/vendors/metronic/css/styles.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/js/tymbl/vendors/vendors/fontawesome5/css/all.min.css') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="{{ asset('assets/css/tymbl/front/owl.theme.default.min.css') }}">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
  <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

  <!--end:: Global Optional Vendors -->

  <!--begin::Global Theme Styles -->
  <link href="{{ asset('assets/css/tymbl/dashboard/demo/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />

  <!--RTL version:<link href="assets/demo/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

  <!--end::Global Theme Styles -->

  <!--begin::Page Vendors Styles -->
  <link href="{{ asset('assets/dashboard/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />

  <!--RTL version:<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

  <!--end::Page Vendors Styles -->
  <link rel="shortcut icon" href="{{ asset('assets/css/tymbl/dashboard/demo/media/img/logo/favicon.png') }}" />
  @if(request()->segment(4) === 'profile')
  <link rel="stylesheet" href="{{ asset("assets/plugins/bootstrap-taginput/src/bootstrap-tagsinput.css") }}">
  <style>
  .selector-wrapper{
    margin-left:70px;
  }
  .selector-wrapper .select2-results__options{
    font-size:15px !important;
  }
  .select2{
    min-width:200px !important;
  }


  </style>
  @endif

  <link href="{{ asset('assets/css/tymbl/dashboard/custom.css') }}" rel="stylesheet" type="text/css" />
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

  <!-- begin:: Page -->
  <div class="m-grid m-grid--hor m-grid--root m-page">

    <!-- BEGIN: Header -->
    @include('tymbl.dashboard.header')
    <!-- END: Header -->

    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

      <!-- BEGIN: Left Aside -->
      @include('tymbl.dashboard.left_menu')
      <!-- END: Left Aside -->

      <!-- start inner -->
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        @yield('content')
      </div>
    </div>
    <!-- end inner -->
    <!-- end:: Body -->
    <!-- begin::Footer -->
    @include('tymbl.dashboard.footer')
    <!-- end::Footer -->
  </div>

  <!-- end:: Page -->

  <!-- begin::Scroll Top -->
  <div id="m_scroll_top" class="m-scroll-top">
    <i class="la la-arrow-up"></i>
  </div>


  @if(request()->segment(1) === 'referral-document' && request()->segment(3) === 'success')
  <div id="rating_popup" class="rating_popup">
    <a href="#" id="popup_close" class="alert-link alert alert-secondary" style="display: none; float:right;padding:0 0.4em;font-size:26px;">×</a>
    <div>
      <div class="text-center" id="rating-referral">
        <h4>Thank you for signing the REFERRAL AGREEMENT!</h4>
        <p style="padding-left:12px; padding-right:12px;">This contract has now been returned, and a copy of the document will be sent to you when all parties have signed.</p>

        <strong class="center-btn">Please rate how your transaction went</strong>
        <ul class="inline-rating rating">
          <li id="rating" rel="5"><span class="fa fa-star" title="Rate 5"></span></li>
          <li id="rating" rel="4"><span class="fa fa-star" title="Rate 4"></span></li>
          <li id="rating" rel="3"><span class="fa fa-star" title="Rate 3"></span></li>
          <li id="rating" rel="2"><span class="fa fa-star" title="Rate 2"></span></li>
          <li id="rating" rel="1"><span class="fa fa-star" title="Rate 1"></span></li>
        </ul>
      </div>
      <div id="option">
        <strong class="center-btn">Let us know why you gave this transaction 1 star</strong>
        <div class="input-group">
          @if($type == 'selling')
          <div class="input-groups">
            <div class="form-check form-check-inline search-radio"><input class="form-check-input" name="reason" id="reason" value="Prospect contact info was incorrect" type="radio" id="toggler_option"><label class="form-check-label" for="toggler-distance">Prospect contact info was incorrect</label>
            </div>
          </div>
          <div class="input-groups">
            <div class="form-check form-check-inline search-radio"><input class="form-check-input" name="reason" id="reason" value="Seller didn't provide the prospect contact information" type="radio" id="toggler_option"><label class="form-check-label" for="toggler-distance">Seller didn't provide the prospect contact information</label></div>
          </div>
          <div class="input-groups">

            <div class="form-check form-check-inline search-radio"><input class="form-check-input" name="reason" id="reason" value="Prospect didn't expect to be contacted" type="radio" id="toggler_option"><label class="form-check-label" for="toggler-distance">Prospect didn't expect to be contacted</label></div>

          </div>
          <div class="input-groups">

            <div class="form-check form-check-inline search-radio"><input class="form-check-input" name="reason" id="reason" value="Seller was slow to respond" type="radio" id="toggler_option"><label class="form-check-label" for="toggler-distance">Seller was slow to respond</label></div>

          </div>
          <div class="input-groups">
            <div class="form-check form-check-inline search-radio"><input class="form-check-input" name="reason" id="reason" value="Other" type="radio" id="toggler_option"><label class="form-check-label" for="toggler-distance">Other</label></div>
          </div>
          @else
          <div class="input-groups">
            <div class="form-check form-check-inline search-radio"><input class="form-check-input" name="reason" value="Buyer was slow to respond" type="radio" id="toggler_option"><label class="form-check-label" for="toggler-distance">Buyer was slow to respond</label></div>
          </div>
          <div class="input-groups">
            <div class="form-check form-check-inline search-radio"><input class="form-check-input" name="reason" id="reason" value="Buyer was slow to sign the referral agreement" type="radio" id="toggler_option"><label class="form-check-label" for="toggler-distance">Buyer was slow to sign the referral agreement</label></div>
          </div>
          <div class="input-groups">
            <div class="form-check form-check-inline search-radio"><input class="form-check-input" name="reason" id="reason" value="Other" type="radio" id="toggler_option"><label class="form-check-label" for="toggler-distance">Other</label></div>
          </div>
          @endif
          <div id="reason-details">Please provide details<br>
            <input type="text" class="form-control" id="other-details"><br><br>
            <div class="center-btn"><a id="submit-feedback" class="btn btn-primary active text-center" role="button" href="#">Submit</a></div>
          </div>
        </div>
      </div>
      <div class="text-center text-success" id="thank-you-feedback">Thank you for your feedback.<br><br>
        <button type="button" id="popup_close" class="btn" href="#">Close</button><br><br>
      </div>
      <div class="popup-footer text-center">Signatures are collected and stored by https://esignatures.io</div>&nbsp;
    </div>
  </div>
  @endif

  @if(request()->segment(1) === 'dashboard' && (request()->segment(2) === 'successful_leads' || request()->segment(2) === 'approved') || request()->segment(2) === 'vip-leads'))
  <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <p>Current Agent: <span id="cur_user"></span></p><br>
          Change agent to:<br><br>
          <select id="agent_select" class="form-control" style="padding: 4px;">
          </select>
          <br><span id="user-change-status"></span>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="btn-change-user">Save changes</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  @endif
  <!-- end::Scroll Top -->

  <!--begin:: Global Mandatory Vendors -->
  <script src="https://code.jquery.com/jquery-3.4.1.min.js" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/popper.js/dist/umd/popper.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/js-cookie/src/js.cookie.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/moment/min/moment.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/tooltip.js/dist/umd/tooltip.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/perfect-scrollbar/dist/perfect-scrollbar.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/wnumb/wNumb.js') }}" type="text/javascript"></script>

  <!--end:: Global Mandatory Vendors -->

  <!--begin:: Global Optional Vendors -->
  <script src="{{ asset('assets/js/tymbl/vendors/jquery.repeater/src/lib.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/jquery.repeater/src/jquery.input.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/jquery.repeater/src/repeater.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/jquery-form/dist/jquery.form.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/block-ui/jquery.blockUI.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/js/framework/components/plugins/forms/bootstrap-datepicker.init.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.j') }}s" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/js/framework/components/plugins/forms/bootstrap-timepicker.init.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/bootstrap-daterangepicker/daterangepicker.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/js/framework/components/plugins/forms/bootstrap-daterangepicker.init.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/bootstrap-maxlength/src/bootstrap-maxlength.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/bootstrap-switch/dist/js/bootstrap-switch.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/js/framework/components/plugins/forms/bootstrap-switch.init.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/bootstrap-select/dist/js/bootstrap-select.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/select2/dist/js/select2.full.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/typeahead.js/dist/typeahead.bundle.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/handlebars/dist/handlebars.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/inputmask/dist/jquery.inputmask.bundle.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/inputmask/dist/inputmask/inputmask.date.extensions.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/inputmask/dist/inputmask/inputmask.numeric.extensions.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/inputmask/dist/inputmask/inputmask.phone.extensions.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/nouislider/distribute/nouislider.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/owl.carousel/dist/owl.carousel.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/autosize/dist/autosize.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/clipboard/dist/clipboard.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/ion-rangeslider/js/ion.rangeSlider.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/dropzone/dist/dropzone.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/summernote/dist/summernote.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/markdown/lib/markdown.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/bootstrap-markdown/js/bootstrap-markdown.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/js/framework/components/plugins/forms/bootstrap-markdown.init.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/jquery-validation/dist/jquery.validate.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/jquery-validation/dist/additional-methods.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/js/framework/components/plugins/forms/jquery-validation.init.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/bootstrap-notify/bootstrap-notify.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/js/framework/components/plugins/base/bootstrap-notify.init.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/toastr/build/toastr.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/jstree/dist/jstree.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/raphael/raphael.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/morris.js/morris.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/chartist/dist/chartist.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/chart.js/dist/Chart.bundle.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/js/framework/components/plugins/charts/chart.init.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/vendors/jquery-idletimer/idle-timer.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/waypoints/lib/jquery.waypoints.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/counterup/jquery.counterup.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/es6-promise-polyfill/promise.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/sweetalert2/dist/sweetalert2.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/tymbl/vendors/js/framework/components/plugins/base/sweetalert2.init.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/plugins/jquery-popup-overlay-gh-pages/jquery.popupoverlay.js') }}"></script>
  <!--end:: Global Optional Vendors -->

  <!--begin::Global Theme Bundle -->
  <script src="{{ asset('assets/css/tymbl/dashboard/demo/base/scripts.bundle.js') }}" type="text/javascript"></script>
  <!--end::Global Theme Bundle -->

  <!--begin::Page Vendors -->
  <script src="{{ asset('assets/css/tymbl/dashboard/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>
  <!--end::Page Vendors -->

  <!--begin::Page Scripts -->
  <script src="{{ asset('assets/dashboard/app/js/dashboard.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/plugins/jquery-popup-overlay-gh-pages/jquery.popupoverlay.js') }}"></script>
  <script src="{{ asset('assets/plugins/waitMe/waitMe.js') }}"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
  <!--end::Page Scripts -->

  <script>
  $(document).ready(function() {
    $('[id="deleteFav"]').on('click', function (e) {
      e.preventDefault;
      if (!confirm('Do you want to remove this item from your list?')) {
        return '';
      }
      var selector = $(this);
      var slug = selector.data('slug');
      var ad_id = selector.attr('rel');
      $.ajax({
        url: '{{ route('delete-favorite') }}',
        type: "POST",
        data: {ad_id: ad_id, _token: '{{ csrf_token() }}'},
        success: function (data) {
          //console.log(data);
          if (data == 1) {
            //selector.closest('tr').hide('slow');
            $('#fav-holder-'+ad_id).hide('slow');
            toastr.success(data.msg, 'Favorite Deleted', toastr_options);
          }
        }
      });
    });
  });
  </script>

  <script>
  @if(session('success'))
  toastr.success('{{ session('success') }}', '{{ trans('app.success') }}', {timeOut: 5000});
  @endif
  @if(session('error'))
  toastr.error('{{ session('error') }}', '{{ trans('app.error') }}', {timeOut: 5000});
  @endif
  </script>


  @if(request()->segment(2) === 'import-leads')
  <script>
  $('#btn-save').click(function(e){
    e.preventDefault();
    $('.err').removeClass('text-danger');
    $('.err').addClass('text-warning').html('processing... please wait');

    var filename = '{{ app('request')->input('lead_file_name') }}';
    var file_id = '{{ app('request')->input('file_id') }}';
    $.ajax({
      type: 'POST',
      url: '{{ route('import-save') }}',
      data: {filename: filename, file_id: file_id, _token: '{{ csrf_token() }}'},
      success: function (data) {
        //console.log(data);
        if(data === '1'){
          location.href = '/dashboard/uploaded_leads_status/'+file_id;
        }else{
          $('.err').removeClass('text-waring');
          $('.err').addClass('text-danger').html('error: ' + data);
        }
        //location.reload();
      }
    });

  });
  </script>
  @endif


  @if(request()->segment(4) === 'profile')
  <script>
  function setCookie(key, value) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (value * 24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
  }

  function getCookie(key) {
    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null;
  }
  </script>

  @if(isset($countries))
  <script src="{{ asset('assets/plugins/bootstrap-taginput/src/bootstrap-tagsinput.js') }}"></script>
  <script>

  $('.nav-item').click(function(e){
    setCookie('tabid', $(this).attr('id'));
  });

  var current_tab = getCookie('tabid');
  if(current_tab === 'myprofile'){
    $('#m_user_profile_tab_2').removeClass('active');
    $('#m_user_profile_tab_1').addClass('active');
    $('#t2').removeClass('active');
    $('#t1').addClass('active');
  }else if(current_tab === 'mynotification'){
    $('#m_user_profile_tab_1').removeClass('active');
    $('#m_user_profile_tab_2').addClass('active');
    $('#t1').removeClass('active');
    $('#t2').addClass('active');
  }else{
    $('#m_user_profile_tab_2').removeClass('active');
    $('#m_user_profile_tab_1').addClass('active');
    $('#t2').removeClass('active');
    $('#t1').addClass('active');
  }

  var old_email = '';
  var new_email = '';
  var old_phone = '';
  var new_phone= '';

  $('#edit_email').click(function(e){

    $('#notification-email').removeAttr('disabled');
    $('#notification-email').focus();
    old_email = $('#notification-email').val();
    $('#edit_email').hide();
    $('#btn-save-mail').show();
  });

  $('#edit_phone').click(function(e){
    $('#notification-phone').removeAttr('disabled');
    $('#notification-phone').focus();
    old_phone = $('#notification-phone').val();
    $('#edit_phone').hide();
    $('#btn-save-phone').show();
  });

  $('#notification-email').on('blur' , function() {

    new_email = $(this).val();
    var n = validateEmail(new_email);
    var id = {{ $user->id }}
    if(n == false){
      alert('Please enter a valid email address.');
    }else{
      $.ajax({
        type: 'POST',
        url: '{{ route('edit-user-notify') }}',
        data: {id: id, old_email: old_email, new_email: new_email, old_phone: $('#notification-phone').val(), new_phone: $('#notification-phone').val(), _token: '{{ csrf_token() }}'},
        success: function (data) {
          //console.log(data);
          //if(data == 1){
          $('#edit_email').show();
          $('#btn-save-mail').hide();
          //}
          //location.reload();
        }
      });
    }
  });

  $('#notification-phone').on('blur' , function() {
    //alert('hey');
    new_phone = $('#notification-phone').val();
    var n = validatePhone(new_phone);
    var id = {{ $user->id }}
    if(n == false){
      alert('Please enter a valid phone number (eg: 0995-064-9875)');
    }else{
      $.ajax({
        type: 'POST',
        url: '{{ route('edit-user-notify') }}',
        data: {id: id, old_email: $('#notification-email').val(), new_email: $('#notification-email').val(), old_phone: old_phone, new_phone: new_phone, _token: '{{ csrf_token() }}'},
        success: function (data) {
          //console.log(data);
          //location.reload();
          //if(data=='1'){
          $('#edit_phone').show();
          $('#btn-save-phone').hide();
          //}
        }
      });
    }
  });

  function validateEmail(email)
  {
    var re = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i;
    return re.test(email);
  }

  function validatePhone(phone){
    var re = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
    return re.test(phone);
  }

  $('#selector-wrapper-state').popup('hide');
  $('#selector-wrapper-state').popup({
    type: 'tooltip',
    transition: 'all 0.3s',
    opacity: 0,
    setzindex: true,
    autofocus: true,
    onclose: function() {

    }
  });

  $('#selector-wrapper-city').popup('hide');
  $('#selector-wrapper-city').popup({
    type: 'tooltip',
    transition: 'all 0.3s',
    opacity: 0,
    setzindex: true,
    autofocus: false,
    onclose: function() {
    }
  });

  $('#selector-wrapper-country').popup('hide');
  $('#selector-wrapper-country').popup({
    type: 'tooltip',
    transition: 'all 0.3s',
    opacity: 0,
    setzindex: true,
    autofocus: false,
    onclose: function() {

    }
  });
  </script>
  @endif
  @endif

  @if(request()->segment(3) == 'posts' && request()->segment(4) == 'edit')
  @include('tymbl.layouts.script_newpost');
  @endif

  @if(request()->segment(1) == 'dashboard')
  <script src="{{ asset('assets/plugins/metisMenu/dist/metisMenu.min.js') }}"></script>
  <!--
  <script>
  $(function() {
  $('#side-menu').metisMenu();
  getElementsByTagName('option')[1].selected;
});
</script>
-->
<script>
$('#remind_me_popup').popup('hide');
$('#remind_me_popup').popup({
  transition: 'all 0.3s',
  opacity: 0.1,
  setzindex: true,
  autofocus: true,
  onclose: function() {
    $.ajax({
      type: 'POST',
      url: '{{ route('save-notification') }}',
      data: { optin: '2', _token: '{{csrf_token()}}' },
      success: function(data) {
        $('#remind_me_popup').popup('hide');
      }
    });
  }
});
$('#remind_me_popup').popup('show');
$('[name="country"]').change(function () {
  var country_id = $(this).val();
  $('#state_loader').show();
  $.ajax({
    type: 'POST',
    url: '{{ route('get_state_by_country') }}',
    data: {country_id: country_id, _token: '{{ csrf_token() }}'},
    success: function (data) {
      generate_option_from_json(data, 'country_to_state');
    }
  });
});

$('[name="state"]').change(function () {
  var state_id = $(this).val();
  $('#city_loader').show();
  $.ajax({
    type: 'POST',
    url: '{{ route('get_city_by_state') }}',
    data: {state_id: state_id, _token: '{{ csrf_token() }}'},
    success: function (data) {
      generate_option_from_json(data, 'state_to_city');
    }
  });
});

$('#poppup-remind-sms').click(function() {
  $('.popup-text-email').fadeOut();
  $('#poppup-remind-email').prop('checked', false);
  $('#poppup-remind-both').prop('checked', false);
  $('.popup-text-phone').fadeIn();
});

$('#poppup-remind-email').click(function() {
  $('.popup-text-phone').fadeOut();
  $('#poppup-remind-sms').prop('checked', false);
  $('#poppup-remind-both').prop('checked', false);
  $('.popup-text-email').fadeIn();
});
$('#poppup-remind-both').click(function() {
  $('#poppup-remind-sms').prop('checked', false);
  $('#poppup-remind-email').prop('checked', false);
  $('.popup-text-phone').fadeIn();
  $('.popup-text-email').fadeIn();
});

$('#popup_close').click(function() {
  $('#remind_me_popup').popup('hide');
});

$('#popup-submit').click(function() {
  $('#popup-error').hide();
  if($('[name="country"]').val()==''){
    $('#popup-error').text('Please choose a country');
    $('#popup-error').fadeIn();
  }else if($('[name="state"]').val()==''){
    $('#popup-error').text('Please choose a state');
    $('#popup-error').fadeIn();
  }else if(!$('#remind-email').is(':checked') && !$('#remind-phone').is(':checked')){
    $('#popup-error').text('Either mobile number or email address is required to send alerts');
    $('#popup-error').fadeIn();
  }else if($('#remind-phone').is(':checked') && $('#popup-text-phone').val() ==''){
    $('#popup-error').text('Mobile number is required');
    $('#popup-error').fadeIn();
  }else if($('#remind-email').is(':checked') && $('#popup-text-email').val()==''){
    $('#popup-error').text('Email is required');
  }else{
    $.ajax({
      type: 'POST',
      url: '{{ route('save-notification') }}',
      data: { country_id: $('[name="country"]').val(), state_id: $('[name="state"]').val(), city_id: $('[name="city"]').val(), phone: $('#popup-text-phone').val(), email: $('#popup-text-email').val(), _token: '{{csrf_token()}}' },
      success: function(data) {
        $('#popup-error').html('<span style="color: #333 !important;">Notification preferences saved.</span>');
        $('#popup-error').fadeIn();
        setTimeout(function () {$('#remind_me_popup').popup('hide');}, 3000);
        //console.log(data);
      }
    });
  }
});
</script>
<script>
function generate_option_from_json(jsonData, fromLoad) {
  //Load Category Json Data To Brand Select

  if (fromLoad === 'country_to_state') {
    var option = '';
    if (jsonData.length > 0) {
      option += '<option value="0" selected> @lang('app.select_state') </option>';
      for (i in jsonData) {
        option += '<option value="' + jsonData[i].id + '"> ' + jsonData[i].state_name + ' </option>';
      }
      $('#state_select').html(option);
      $('#state_select').select2();
    } else {
      $('#state_select').html('');
      $('#state_select').select2();
    }
    $('#state_loader').hide('slow');

  } else if (fromLoad === 'state_to_city') {
    var option = '';
    if (jsonData.length > 0) {
      option += '<option value="0" selected> @lang('app.select_city') </option>';
      for (i in jsonData) {
        option += '<option value="' + jsonData[i].id + '"> ' + jsonData[i].city_name + ' </option>';
      }
      $('#city_select').html(option);
      $('#city_select').select2();
    } else {
      $('#city_select').html('');
      $('#city_select').select2();
    }
    $('#city_loader').hide('slow');
  } else if(fromLoad === 'state_with_cities') {
    //console.log(jsonData.length);
    var option = '';
    if (jsonData.length > 0) {
      //console.log(jsonData);
      option += '<option value="0" selected> @lang('app.select_city') </option>';

      for (var i=0; i<jsonData.length; i++) {
        let statename = jsonData[i][0];
        let cityarray = jsonData[i][1];
        option += '<option disabled="disabled" value="' + statename + '"> <div class="background:#333 !important; font-size:10px !important;">' + statename + '</div></option>';

        for(var j=0; j<cityarray.length; j++){
          option += '<option value="' + cityarray[j]['id'] + '">&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp' + cityarray[j]['city_name'] + ' </option>';
        }
      }

      $('#city_select').html(option);
      $('#city_select').select2();
    } else {
      $('#city_select').html('');
      $('#city_select').select2();
    }
  }
}
</script>
@endif

<script>
$('[id="delete-comment"]').on('click', function(event) {
  event.preventDefault;
  var cid = $(this).attr('rel');
  if (confirm('Delete this comment?')) {
    $.ajax({
      type: 'POST',
      url: '{{ route('delete-comment') }}',
      data: {comment_id: cid, _token: '{{ csrf_token() }}'},
      success: function (data) {
        //console.log(data);
        if(data == '1'){
          location.reload();
          //$('#comment-post-id-'+cid).hide();
          toastr.success("Comment has been deleted");
        }else{
          toastr.error("An error occured while performing this action. Please try again later.");
        }
      }
    });
  } else {
    return;
  }
});
$('[id="approve-comment"]').on('click', function(event) {
  event.preventDefault;
  var cid = $(this).attr('rel');
  $.ajax({
    type: 'POST',
    url: '{{ route('approve-comment') }}',
    data: {comment_id: cid, _token: '{{ csrf_token() }}'},
    success: function (data) {
      //console.log(data);
      if(data == '1'){
        location.reload();
      }else{
        toastr.error("An error occured while performing this action. Please try again later.");
      }
    }
  });
});
$('[id="approve-revoke"]').on('click', function(event) {
  event.preventDefault;
  var cid = $(this).attr('rel');
  $.ajax({
    type: 'POST',
    url: '{{ route('approve-block') }}',
    data: {comment_id: cid, _token: '{{ csrf_token() }}'},
    success: function (data) {
      //console.log(data);
      if(data == '1'){
        location.reload();
      }else{
        toastr.error("An error occured while performing this action. Please try again later.");
      }
    }
  });
});
</script>

<script>
$('body').on('click', '.imgDeleteBtn', function(){
  //Get confirm from user
  if ( !confirm('Are you sure want to delete this image?')){
    return '';
  }

  var current_selector = $(this);
  var img_id = $(this).closest('.img-action-wrap').attr('id');
  $.ajax({
    url : '/dashboard/u/posts/delete-media',
    type: "POST",
    data: { media_id : img_id, _token : 'UBCkpTsVV7hsISJmGV83XO2AdskuhofV1zR29boK' },
    success : function (data) {
      if (data.success == 1){
        current_selector.closest('.creating-ads-img-wrap').hide('slow');
        toastr.success(data.msg, 'Success!', toastr_options);
      }
    }
  });
});


$(document).on('click', '.image-add-more', function (e) {
  e.preventDefault();
  $('.upload-images-input-wrap').append('<input type="file" name="images[]" class="form-control" />');
});
</script>

<script>
var ml_state="";
var ml_name="";

$('#change_mls').click(function(event) {
  event.preventDefault;

  $('#mls_section').popup({
    tooltipanchor: event.target,
    autoopen: true,
    type: 'overlay',
    onclose: function() {
      $('#mls_id').val(ml_name);
      $('#mls_state').val(ml_state);
    }
  });

  $('#mls option').each(function() {
    $(this).hide();
  });
});

$('#ok-cancel').click(function(event) {
  event.preventDefault;
  $('#mls_section').popup('hide');
});

$('#allcountries').on('change', function() {
  var country_id = $(this).val();
  //alert(id);
  $.ajax({
    type: 'POST',
    url: '{{ route('get_state_by_country') }}',
    data: {country_id: country_id, _token: '{{ csrf_token() }}'},
    success: function (data) {
      generate_option_from_json(data, 'country_to_state');
    }
  });

});



$('#allstate').on('change', function() {
  var state_id = $(this).val();
  $.ajax({
    type: 'POST',
    url: '{{ route('get_mls_by_state') }}',
    data: {state_id: state_id, _token: '{{ csrf_token() }}'},
    success: function (data) {
      $('#mls').html('');
      var option = '<option selected>Select MLS</option>';
      for(i in data){
        option += '<option>'+data[i].name+'</option>';
      }
      $('#mls').html(option);
    }
  });
});

$('#mls').on('change', function() {
  ml_name = $(this).find('option:selected').text();
});


$('#ok-mls').click(function(event) {
  $('#mls_section').popup('hide');
});
</script>

@if(request()->segment(1) == 'dashboard')
<script src="{{ asset('assets/plugins/metisMenu/dist/metisMenu.min.js') }}"></script>

<script>
$('#remind_me_popup').popup('hide');
$('#remind_me_popup').popup({
  transition: 'all 0.3s',
  opacity: 0.1,
  setzindex: true,
  autofocus: true,
  onclose: function() {
    $.ajax({
      type: 'POST',
      url: '{{ route('save-notification') }}',
      data: { optin: '2', _token: '{{csrf_token()}}' },
      success: function(data) {
        $('#remind_me_popup').popup('hide');
      }
    });
  }
});
$('#remind_me_popup').popup('show');
$('[name="country"]').change(function () {
  var country_id = $(this).val();
  $('#state_loader').show();
  $.ajax({
    type: 'POST',
    url: '{{ route('get_state_by_country') }}',
    data: {country_id: country_id, _token: '{{ csrf_token() }}'},
    success: function (data) {
      generate_option_from_json(data, 'country_to_state');
    }
  });
});

$('[name="state"]').change(function () {
  var state_id = $(this).val();
  $('#city_loader').show();
  $.ajax({
    type: 'POST',
    url: '{{ route('get_city_by_state') }}',
    data: {state_id: state_id, _token: '{{ csrf_token() }}'},
    success: function (data) {
      generate_option_from_json(data, 'state_to_city');
    }
  });
});

$('#poppup-remind-sms').click(function() {
  $('.popup-text-email').fadeOut();
  $('#poppup-remind-email').prop('checked', false);
  $('#poppup-remind-both').prop('checked', false);
  $('.popup-text-phone').fadeIn();
});

$('#poppup-remind-email').click(function() {
  $('.popup-text-phone').fadeOut();
  $('#poppup-remind-sms').prop('checked', false);
  $('#poppup-remind-both').prop('checked', false);
  $('.popup-text-email').fadeIn();
});
$('#poppup-remind-both').click(function() {
  $('#poppup-remind-sms').prop('checked', false);
  $('#poppup-remind-email').prop('checked', false);
  $('.popup-text-phone').fadeIn();
  $('.popup-text-email').fadeIn();
});

$('#popup_close').click(function() {
  $('#remind_me_popup').popup('hide');
});

$('#popup-submit').click(function() {
  $('#popup-error').hide();
  if($('[name="country"]').val()==''){
    $('#popup-error').text('Please choose a country');
    $('#popup-error').fadeIn();
  }else if($('[name="state"]').val()==''){
    $('#popup-error').text('Please choose a state');
    $('#popup-error').fadeIn();
  }else if(!$('#remind-email').is(':checked') && !$('#remind-phone').is(':checked')){
    $('#popup-error').text('Either mobile number or email address is required to send alerts');
    $('#popup-error').fadeIn();
  }else if($('#remind-phone').is(':checked') && $('#popup-text-phone').val() ==''){
    $('#popup-error').text('Mobile number is required');
    $('#popup-error').fadeIn();
  }else if($('#remind-email').is(':checked') && $('#popup-text-email').val()==''){
    $('#popup-error').text('Email is required');
  }else{
    $.ajax({
      type: 'POST',
      url: '{{ route('save-notification') }}',
      data: { country_id: $('[name="country"]').val(), state_id: $('[name="state"]').val(), city_id: $('[name="city"]').val(), phone: $('#popup-text-phone').val(), email: $('#popup-text-email').val(), _token: '{{csrf_token()}}' },
      success: function(data) {
        $('#popup-error').html('<span style="color: #333 !important;">Notification preferences saved.</span>');
        $('#popup-error').fadeIn();
        setTimeout(function () {$('#remind_me_popup').popup('hide');}, 3000);
        //console.log(data);
      }
    });
  }
});
</script>
<script>
function generate_option_from_json(jsonData, fromLoad) {
  //Load Category Json Data To Brand Select
  //alert('here ba?');
  if (fromLoad === 'country_to_state') {
    var option = '';
    if (jsonData.length > 0) {
      option += '<option value="0" selected> @lang('app.select_state') </option>';
      for (i in jsonData) {
        option += '<option value="' + jsonData[i].id + '"> ' + jsonData[i].state_name + ' </option>';
      }
      $('#state_select').html(option);
      $('#state_select').select2();

      if($('#allstate').length){
        $('#allstate').html('');
        $('#allstate').html(option);
      }

    } else {
      $('#state_select').html('');
      $('#state_select').select2();
    }
    $('#state_loader').hide('slow');

  } else if (fromLoad === 'state_to_city') {
    var option = '';
    if (jsonData.length > 0) {
      option += '<option value="0" selected> @lang('app.select_city') </option>';
      for (i in jsonData) {
        option += '<option value="' + jsonData[i].id + '"> ' + jsonData[i].city_name + ' </option>';
      }

      $('#city_select').html(option);
      $('#city_select').select2();
    } else {
      $('#city_select').html('');
      $('#city_select').select2();
    }
    $('#city_loader').hide('slow');
  } else if(fromLoad === 'state_with_cities') {
    //console.log(jsonData.length);
    var option = '';
    if (jsonData.length > 0) {
      //console.log(jsonData);
      option += '<option value="0" selected> @lang('app.select_city') </option>';

      for (var i=0; i<jsonData.length; i++) {
        let statename = jsonData[i][0];
        let cityarray = jsonData[i][1];
        option += '<option disabled="disabled" value="' + statename + '"> <div class="background:#333 !important; font-size:10px !important;">' + statename + '</div></option>';

        for(var j=0; j<cityarray.length; j++){
          option += '<option value="' + cityarray[j]['id'] + '">&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp' + cityarray[j]['city_name'] + ' </option>';
        }
      }

      $('#city_select').html(option);
      $('#city_select').select2();
    } else {
      $('#city_select').html('');
      $('#city_select').select2();
    }
  }
}
</script>
@endif

@if(request()->segment(2) == 'leads-admin')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
$(document).ready(function() {
  $('#jDataTable').DataTable({
    processing: true,
    serverSide: true,
    ajax: '{{ route('leads-data') }}',
    "aaSorting": []
  });
});
</script>
@endif

@if(request()->segment(2) == 'uploaded_leads')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
$(document).ready(function() {
  $('#jDataTable').DataTable({
    processing: true,
    serverSide: true,
    ajax: '{{ route('user_leads_uploads') }}',
    "aaSorting": []
  });
});
</script>
@endif

@if(request()->segment(2) == 'successful_leads')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
$(document).ready(function() {
  $('#jDataTable').DataTable({
    processing: true,
    serverSide: true,
    ajax: '{{ route('successful_leads_data') }}',
    "aaSorting": []
  });
});


$('body').on('click', '#relist-ad', function (e) {
  var id = $(this).attr('rel');
  $('#wait-notice').text('please wait...').css('color', 'red');

  if(confirm("This ad will be reposted and all transactions involving this ad will be null and void")){
    $.ajax({
      type: 'POST',
      url: '/dashboard/relist_ad',
      data: {id: id, _token: '{{csrf_token()}}'},
      success: function (data) {
        //console.log(data);
        if (data == '1') {
          //var options = {closeButton: true};
          //toastr.success(data.msg, 'Success!', options);
          $('#wait-notice').text('');
          location.reload();
        }
      }
    });
  }
  else{
    $('#wait-notice').text('');
    return false;
  }



});
</script>
@endif

@if(request()->segment(2) == 'leads-blocked-admin')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
$(document).ready(function() {
  $('#jDataTable').DataTable({
    processing: true,
    serverSide: true,
    ajax: '{{ route('leads-blocked-data') }}',
    "aaSorting": []
  });
});
</script>
@endif

@if(request()->segment(2) == 'pages' && request()->segment(3) == '')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
$(document).ready(function() {
  $('#jDataTable').DataTable({
    processing: true,
    serverSide: true,
    ajax: '{{ route('pages_data') }}',
    "aaSorting": []
  });
});
</script>
<script>
$(document).ready(function() {
  $('body').on('click', '.btn-danger', function (e) {
    if (!confirm("Are you sure want to delete this page?")) {
      e.preventDefault();
      return false;
    }

    var selector = $(this);
    var slug = $(this).data('slug');

    $.ajax({
      type: 'POST',
      url: '/dashboard/pages/delete',
      data: {slug: slug, _token: '{{csrf_token()}}'},
      success: function (data) {
        if (data.success == 1) {
          selector.closest('tr').hide('slow');
          var options = {closeButton: true};
          toastr.success(data.msg, 'Success!', options);
        }
      }
    });
  });
});
</script>
@endif

@if(request()->segment(2) == 'pages' && request()->segment(3) == 'create' || request()->segment(2) == 'pages' && request()->segment(3) == 'edit')
<script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>
<script>
// Replace the <textarea id="editor1"> with a CKEditor
// instance, using default configuration.
CKEDITOR.replace( 'post_content' );
</script>
@endif

@if(request()->segment(1) == 'dashboard' && request()->segment(2) == 'categories' && request()->segment(3) == '')
@section('page-js')
<script>
$(document).ready(function() {
  $('.btn-danger').on('click', function (e) {
    if (!confirm("Delete this category?")) {
      e.preventDefault();
      return false;
    }

    var selector = $(this);
    var data_id = $(this).data('id');

    $.ajax({
      type: 'POST',
      url: '{{ route('delete_categories') }}',
      data: {data_id: data_id, _token: '{{ csrf_token() }}'},
      success: function (data) {
        if (data.success == 1) {
          selector.closest('div').hide('slow');
        }
      }
    });
  });
});
</script>
@endif

@if(request()->segment(1) == 'dashboard' && request()->segment(2) == 'users' && request()->segment(3) == '')
@section('page-js')
@include('tymbl.layouts.script_user_admin_edit')
@endif

@if(request()->segment(1) == 'dashboard' && request()->segment(2) == 'users-info')
<script>
$(document).ready(function() {
  var user_id = '{{ $user->id }}';
  $('#user_status').on('change', function (e) {
    var status = $(this).val();
    $.ajax({
      type: 'POST',
      url: '{{ route('user-status-edit') }}',
      data: {user_id: user_id, status: status,_token: '{{ csrf_token() }}'},
      success: function (data) {
        var options = {closeButton: true};
        toastr.success('User status changed', 'Success!', options)
      }
    });
  });


  $('#remove_user').click(function(){
    var id = $(this).attr('rel');
    if (confirm('Are you sure you want to delete this user?')) {
      $.ajax({
        type: 'POST',
        url: '{{ route('user-delete') }}',
        data: {id: id, _token: '{{ csrf_token() }}'},
        success: function (data) {
          if(data == '1'){
            var options = {closeButton: true,
              fadeIn: 300,
              fadeOut: 300,
            };
            toastr.options.onHidden = function() {
              window.location.href = '/dashboard/users';
            }
            toastr.options.onCloseClick = function() {
              window.location.href = '/dashboard/users';
            }
            toastr.success('User deleted. Redirecting... please wait', 'Success!', options);
          }else{
            var options = {closeButton: true};
            toastr.error('An error occured while processing this request. Please try again later', 'Error', options);
          }
        }
      });
    }
  });


});
</script>
@endif

@if(request()->segment(1) == 'dashboard' && request()->segment(2) == 'contact-messages')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
$(document).ready(function() {
  $('#jDataTable').DataTable({
    processing: true,
    serverSide: true,
    ajax: '{{ route('contact_messages_data') }}',
    "aaSorting": []
  });
});
</script>
@endif


@if(request()->segment(1) == 'dashboard' && request()->segment(2) == 'administrators')
<script type="text/javascript">

$(document).ready(function() {
  /**
  * Blck User
  */
  $('body').on('click','.blockAdministrator',function (e) {
    e.preventDefault();
    var this_btn = $(this);
    var user_id = this_btn.data('id');
    $.ajax({
      url: '{{ route('administratorBlockUnblock') }}',
      type: "POST",
      data: { user_id : user_id, status : 'block', _token : '{{csrf_token()}}' },
      success: function (data) {
        if (data.success == 1) {
          this_btn.hide();
          this_btn.closest('tr').find('.unblockAdministrator').show();
          var options = {closeButton: true};
          toastr.success('Lead administrator has been blocked', '@lang('app.success')', options);
        }
      }
    });
  });
  $('body').on('click','.unblockAdministrator',function (e) {
    e.preventDefault();
    var this_btn = $(this);
    var user_id = this_btn.data('id');
    $.ajax({
      url: '{{ route('administratorBlockUnblock') }}',
      type: "POST",
      data: { user_id : user_id, status : 'unblock', _token : '{{csrf_token()}}' },
      success: function (data) {
        if (data.success == 1) {
          this_btn.hide();
          var options = {closeButton: true};
          this_btn.closest('tr').find('.blockAdministrator').show();
          toastr.success('Lead administrator has been unblocked', '@lang('app.success')', options);
        }
      }
    });
  });
});

</script>
@endif

@if(request()->segment(1) == 'dashboard' && request()->segment(2) == 'settings' && request()->segment(3) == 'general')
@section('page-js')
<script>
$(document).ready(function(){

  $('input[type="checkbox"], input[type="radio"]').click(function(){
    var input_name = $(this).attr('name');
    var input_value = 0;
    if ($(this).prop('checked')){
      input_value = $(this).val();
    }
    $.ajax({
      url : '{{ route('save_settings') }}',
      type: "POST",
      data: { [input_name]: input_value, '_token': '{{ csrf_token() }}' },
    });
  });


  $('input[name="date_format"]').click(function(){
    $('#date_format_custom').val($(this).val());
  });
  $('input[name="time_format"]').click(function(){
    $('#time_format_custom').val($(this).val());
  });

  /**
  * Send settings option value to server
  */
  $('#settings_save_btn').click(function(e){
    e.preventDefault();

    var this_btn = $(this);
    this_btn.attr('disabled', 'disabled');

    var form_data = this_btn.closest('form').serialize();
    $.ajax({
      url : '{{ route('save_settings') }}',
      type: "POST",
      data: form_data,
      success : function (data) {
        var toastr_options = {closeButton: true};
        if (data.success == 1){
          toastr.success(data.msg, '@lang('app.success')', toastr_options);
        }else {
          toastr.warning(data.msg, '@lang('app.error')', toastr_options);
        }
        this_btn.removeAttr('disabled');
      }
    });
  });

});
</script>
@endif

@if(request()->segment(1) == 'dashboard' && request()->segment(4) == 'my-leads')
<script>

$('[id="deleteAds"]').on('click', function () {
  if (!confirm('Are you sure you want to delete this lead?')) {
    return '';
  }
  var selector = $(this);
  var slug = selector.data('slug');
  $.ajax({
    url: '{{ route('delete_ads') }}',
    type: "POST",
    data: {slug: slug, _token: '{{ csrf_token() }}'},
    success: function (data) {
      if (data.success == 1) {
        //selector.closest('tr').hide('slow');
        toastr.success('Lead has been deleted', '@lang('app.success')');
        selector.parent().parent().css('display', 'none');
      }
    }
  });
});
</script>
@endif

@php
$dlinks = array('approved', 'blocked', 'paused', 'featured_ads', 'vip-leads');
@endphp
@if(request()->segment(1) == 'dashboard' && in_array(request()->segment(2), $dlinks) )
@section('page-js')

<script>
$(document).ready(function() {
  $('.deleteAds').on('click', function () {
    if (!confirm('{{ trans('app.are_you_sure') }}')) {
      return '';
    }
    var selector = $(this);
    var slug = selector.data('slug');
    $.ajax({
      url: '{{ route('delete_ads') }}',
      type: "POST",
      data: {slug: slug, _token: '{{ csrf_token() }}'},
      success: function (data) {
        if (data.success == 1) {
          selector.closest('tr').hide('slow');
          toastr.success(data.msg, '@lang('app.success')', toastr_options);
        }
      }
    });
  });

  $('[id="send-vip-lead"]').on('click', function () {
    alert('to be implemented');
  });

  $('.approveAds, .blockAds, .pauseAds, .vipAds, .regularAds').on('click', function () {
    var selector = $(this);
    var slug = selector.data('slug');
    var value = selector.data('value');
    //alert(slug);
    $.ajax({
      url: '{{ route('ads_status_change') }}',
      type: "POST",
      data: {slug: slug, value: value, _token: '{{ csrf_token() }}'},
      success: function (data) {
        //console.log(data);
        if (data.success == 1) {
          selector.closest('tr').hide('slow');
          toastr.success(data.msg, '@lang('app.success')', toastr_options);
        }
      }
    });
  });
});

$('[id="f-toggler"]').on('click', function () {
  var checked = '';
  var id = $(this).attr('rel');

  if(!$(this).is(':checked')){
    checked = '0'
  }else{
    checked = '1';
  }

  $.ajax({
    url: '{{ route('ads_is_featured') }}',
    type: "POST",
    data: {id: id, checked: checked, _token: '{{ csrf_token() }}'},
    success: function (data) {
      console.log(data);

    }
  });
});
</script>

<script>
@if(session('success'))
toastr.success('{{ session('success') }}', '{{ trans('app.success') }}', toastr_options);
@endif
@if(session('error'))
toastr.error('{{ session('error') }}', '{{ trans('app.success') }}', toastr_options);
@endif
</script>
@endif

@if(request()->segment(1) == 'dashboard' && request()->segment(2) == 'user-add')
<script>

$('[id="user_type"]').change(function () {
  if($(this).val() == 'user'){
    $('.user-hidden').show();
  }else{
    $('.user-hidden').hide();
  }
});

$('[name="city"]').change(function () {
  var state_id = $('[name="state"] option:selected').text();
  var city_id = $(this).find('option:selected').text();
  var country = $('[name="country"]').val();

  $('[name="zipcode"]').html('<option disabled selected>Select Zip Code</option>');
  $.ajax({
    type: 'POST',
    url: '{{ route('get_zip_by_city') }}',
    data: {country_id: country, state_id: state_id, city_id: city_id, _token: '{{ csrf_token() }}'},
    success: function (data) {
      //generate_option_from_json(data, 'city_to_zip');
      //console.log(data);
      var option = '';
      if (data.length > 0) {
        option += '<option value="0" selected>Select Zip Code</option>';
        for (i in data) {
          option += '<option value="' + data[i].zip + '"> ' + data[i].zip + ' </option>';
        }
        $('[name="zipcode"]').html(option);
        //$('#zipcode_select').select2();
      } else {
        $('[name="zipcode"]').html('<option value="" selected>(Zip Code not found)</option>');
        //$('#zipcode_select').select2();
      }

    }
  });
});
</script>
@endif

@if(request()->segment(1) == 'dashboard' && request()->segment(2) == 'payments')
@section('page-js')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
$(document).ready(function() {
  $('#jDataTable').DataTable({
    processing: true,
    serverSide: true,
    ajax: '{{ route('get_payments_data') }}',
    "aaSorting": []
  });
});
</script>
@endif

@if(request()->segment(1) == 'dashboard' && request()->segment(2) == 'reserved-leads')
<script>
@if(session('success'))
toastr.success('{{ session('success') }}', 'Message has been sent', toastr_options);
@endif
@if(session('error'))
toastr.error('{{ session('error') }}', '{{ trans('app.success') }}', toastr_options);
@endif
</script>
@endif


@if(request()->segment(1) == 'dashboard' && request()->segment(2) == 'messages')
@section('page-js')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
$(document).ready(function() {
  $('#jDataTable').DataTable({
    processing: true,
    serverSide: true,
    ajax: '{{ route('get_messages_data') }}',
    "aaSorting": []
  });

  $('.messages-wrapper').css('overflow-x', 'scroll');
  $('.messages-wrapper').css('overflow-y', 'hidden');
});
</script>
@endif



<script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>
<script>
// Replace the <textarea id="editor1"> with a CKEditor
// instance, using default configuration.
if($('#content_editor').length){
  CKEDITOR.replace('content_editor');
}

</script>

@if(request()->segment(1) == 'dashboard')
<script>
$('#num_messages').hide();
var user_id = {{ Auth::user()->id }};
$.ajax({
  url: '{{ route('get-user-messages') }}',
  type: "POST",
  data: {id: user_id, _token: '{{ csrf_token() }}'},
  success: function (data) {
    //console.log(data.length);
    var option = '';
    if(data.length >= 1){
      $('#num_messages').show();
      var mail_num = 0;
      $.each(data, function (i, elem) {
        if(elem.user_read == '1'){
          option += '<li><i class="far fa-envelope-open"></i>&nbsp;&nbsp;<a href="/dashboard/message/'+elem.id+'">'+elem.subject+'</a></li>';
        }else{
          mail_num += 1;
          option += '<li><i class="far fa-envelope"></i>&nbsp;&nbsp;<a href="/dashboard/message/'+elem.id+'">'+elem.subject+'</a></li>';
        }
      });
      if(mail_num >= 1){
        $('#num_messages').html(mail_num);
      }else{
        $('#num_messages').hide();
      }

      $('#messages-content').html(option);
    }else{
      $('#num_messages').hide();
      $('#read-more-msg').html('No message').attr('disabled', 'disabled');
    }
  }
});

</script>
@endif

@if(request()->segment(4) === 'my-leads')
<script>
$('.approveAds, .pauseAds').on('click', function () {

  var selector = $(this);
  var slug = selector.data('slug');
  var value = selector.data('value');
  //alert(slug);
  $.ajax({
    url: '{{ route('status-change-user') }}',
    type: "POST",
    data: {value: value, slug: slug, _token: '{{ csrf_token() }}'},
    success: function (data) {
      toastr.success('{{ session('success') }}', data.msg);
      location.reload();
    }
  });
});
</script>
@endif


@php
$cid = '231';
if(isset($user_country)){
  if($user_country->country_name != 'United States'){
    $cid = '38';
  }
}

@endphp

@if(request()->segment(4) === 'profile' && request()->segment(5) === 'edit')
<script>
$(document).on('input change', '#slider', function() {
  $('#slider_value').html( $(this).val() + " MILES");
  $('.custom-pill').remove();
  $('#zip_input').val('');
  pills_ar = [];
});

$('#zip_input').on('keyup', function() {
  $('#slider').val('0');
  $('#slider_value').text('AREA: 0 MILES');
  $('#ul-zips').show();
  var country = {{$cid}};
  var textsearch = $(this).val();
  $('#ul-zips').html('');
  $.ajax({
    url: '/searchzips',
    type:"GET",
    data:{'zip':textsearch, 'country':country},
    success:function (data) {
      //console.log(data);
      $('#ul-zips').html(data);
    }
  })
});

$(document).on('click', '.zip_item', function(){
  var zip_val = $(this).text();
  $('#ul-zip').hide();

  //alert(zip_val);

});

$('[name="sms_notify"]').change(function(){
  if ($(this).is(':checked')) {
    $('#profile-modal').modal('show');
  };
});

$(document).on('click', '#profile-save-changes', function(){
  var distance = $('[name="distance"]').val();
  var notival = '';
  if(distance >= 1){
    notival = distance;
    notification_type = '1';
  }else{
    $(".custom-pill").each(function( index ) {
      notival += $( this ).text() + ',';
      notification_type = '2';
    });
  }

  if(notival != ''){
    $.ajax({
      url: '/notication-user-change',
      type: "POST",
      data: {type: notification_type, notification_value: notival, _token: '{{ csrf_token() }}'},
      success: function (data) {
        if(data == '1'){
          $('#profile-modal').modal('toggle');
          toastr.success(
            'success',
            'Notifications have been changed',
            {
              timeOut: 1000,
              fadeOut: 1000,
              onHidden: function () {
                window.location.reload();
              }
            }
          );
        }else{
          $('#profile-modal').modal('toggle');
          toastr.error('Notifications have been changed', 'error');
        }
      }
    });
  }

});

var pills_ar = new Array();
var pills_ar2 = new Array();
$(document).on('click', '.zip-item', function(){

  var zip_val = $(this).text();
  var num_pill = $('div.custom-pill').length;

  $('#zip_input').val('');
  $('#ul-zips').hide();

  if(num_pill < 10){
    if(pills_ar.indexOf(zip_val) === -1){
      pills_ar.push(zip_val);
      //console.log(zip_val + ' added');
      $('#pill').append('<div class="custom-pill">'+zip_val+' <span title="remove item" id="remove">x</span></div>');
    }else{
      alert("Zip code already added");
    }
  }else{
    alert('Up to 10 zip codes only')
  }
});

$(document).on('click', '#remove', function(){
  var item_remove = $(this).parent().text().replace(' x', '');
  if (confirm('Remove ' + item_remove + '?')) {
    $(this).parent().remove();
    pills_ar.splice(pills_ar.indexOf(item_remove),1);
  }
});

</script>
@if($errors->any())
<script>
toastr.error('{{ session('error') }}', '{{ trans('app.error') }}');
</script>
@endif

@php $current_countryid = isset($user->country_id) ? $user->country_id : '231' @endphp

@if($current_countryid != '')
@php $current_country = $current_countryid == '231' ? 'us' : 'ca'; @endphp
@else
@php $current_country = '231'; @endphp
@endif
<script>

function initMap() {

  var options = {
    types: [('address')],
    componentRestrictions: {country: ['us', 'ca']}
  };

  var input = document.getElementById('refAdress');
  var autocomplete = new google.maps.places.Autocomplete(input, options);
  autocomplete.addListener('place_changed', function() {

    var place=autocomplete.getPlace();
    var zips = '';
    this.pickup=place.address_components;
    //console.log(this.pickup);
    //alert(zips);

    var street_number = '';
    var street = '';
    var city = '';
    var state = '';
    var zip = '';
    var cities = [];

    $.each(this.pickup, function( index, value ) {
      //console.log(value);

      if(value['types'][0]=='country'){
        //console.log(value['long_name']);
        var mycountry = value['long_name'];
        if(mycountry == 'United States'){
          $('[name="countryid"]').val('231').change();
          $('[name="country_id"]').val('231');
        }else{
          $('[name="countryid"]').val('38').change();
          $('[name="country_id"]').val('38');
        }
      }

      if(value['types'][0]=='street_number'){
        //console.log(value['long_name']);
        street_number = value['long_name'];
      }

      if(value['types'][0]=='route'){
        //console.log(value['long_name']);
        street = value['long_name'];
      }

      if(value['types'][0]=='administrative_area_level_1'){
        //console.log(value['long_name']);
        state = value['long_name'];

        //$('[name="state"] option:contains(' + $.trim(state) + ')').attr('selected', 'selected').change();
        setTimeout(function(){
          $.ajax({
            url: '/get-state-by-country',
            type: "POST",
            data: {country_id: $('[name="country_id"]').val(), 'city_name': city, _token: '{{ csrf_token() }}'},
            success: function (data) {
              var resp = $.map(data,function(obj){
                //console.log(resp);
                if(obj.state_name == state){
                  //console.log(obj.id);
                  $('[name="state_id"]').val(obj.id);
                  $('[name="state_name"]').val(obj.state_name);
                  $('[name="mls_id"]').val('');
                  $('[name="zip_code"]').val('');
                }
              });
            }
          });
        }, 1000);
      }

      if(value['types'][0]=='locality'){
        //console.log(value['long_name']);
        city = value['long_name'];
        setTimeout(function(){
          $.ajax({
            url: '/get-city-by-state',
            type: "POST",
            data: {state_id: $('[name="state_id"]').val(), 'city_name': city, _token: '{{ csrf_token() }}'},

            success: function (data) {
              //console.log(data);
              var resp = $.map(data,function(obj){
                //console.log(resp);
                if(obj.city_name == city){
                  //console.log(obj.id);
                  $('[name="city_id"]').val(obj.id);
                  $('[name="city_name"]').val(obj.city_name);
                  $('[name="zip_code"]').val('');
                }
              });
            }
          });
        }, 2000);
      }

      if(value['types'][0]=='postal_code'){
        zip = (value['long_name'].length && value['long_name'].charAt(0) == '0') ? value['long_name'].slice(1) : value['long_name'];

        setTimeout(function(){
          $.ajax({
            url: '/get_zip_by_city',
            type: "POST",
            data: {country_id: $('[name="country_id"]').val(), state_id: $('[name="state_name"]').val(), city_id: $('[name="city_name"]').val(), _token: '{{ csrf_token() }}'},
            success: function (data) {
              //console.log(data);
              var resp = $.map(data,function(obj){
                if(obj.zip == zip){
                  //console.log(obj.id);
                  $('[name="zip_code"]').val(obj.zip);
                }
              });
            }
          });
        }, 2500);

      }$('.ui-corner-all').css('padding', '20px');
    });

    //console.log( city );

    input_street_number = street_number.length ? street_number + ' ' : '';
    input_street = street.length ? street + ', ' : '';
    input_city = city.length ? city + ', ' : '';

    $('#refAdress').val(input_street_number + input_street + input_city + state);

  });

}
</script>
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyAodEhDsLe3d03Rca427tRdlDTPM6g0fyk&libraries=places&callback=initMap&language=en&region=US" async defer></script>

<script>
var states_array = [];
var cities_array = [];
var zips_array = [];
var stateid='';
var cityid='';
var zipid='';
var ctrval = '';


if($('[name="country_id"]').val() == ''){
  ctrval = '231';
}else{
  ctrval = $('[name="country_id"]').val();
}

$( function() {
  $('.ui-corner-all').css('padding', '20px');
  $('[name="state_name"]').autocomplete({
    source: function(request, response) {
      $.ajax({
        url: '/get-states-for-autocomplete',
        type: "POST",
        data: {state_name: request.term, country_id: ctrval, _token: '{{ csrf_token() }}'},
        success: function (data) {
          //console.log(data);
          var resp = $.map(data,function(obj){
            //console.log(resp);
            return obj.state_name;
          });

          var states = $.map(data,function(obj){
            return obj;
          });
          states_array = [];
          states_array.push(states);
          response(resp);
          //return resp;
        }
      });
    },
    minLength: 1,
    select: function( event, ui ) {
      //console.log(ui.item.value);
      $.each( states_array, function( id, value ) {
        $.each( value, function( i, v ) {
          if(ui.item.value == v['state_name']){
            stateid = v['state_name'];
            $('[name="state_id"]').val(v['id']);
            $('[name="city_name"]').val('');
            $('[name="city_id"]').val('');
            $('[name="zip_code"]').val('');
            $('[name="mls_id"]').val('');
          }
        });
      });
    }
  });

  $('[name="city_name"]').autocomplete({
    source: function(request, response) {
      $.ajax({
        url: '/get-cities-for-autocomplete',
        type: "POST",
        data: {city_name: request.term, state_id:  $('[name="state_id"]').val(), _token: '{{ csrf_token() }}'},
        success: function (data) {
          //console.log($('#state_id').val());
          var resp = $.map(data,function(obj){
            //console.log(resp);
            return obj.city_name;
          });

          var cities = $.map(data,function(obj){
            return obj;
          });
          cities_array = [];
          cities_array.push(cities);

          response(resp);
          //return resp;
        }
      });
    },
    minLength: 1,
    select: function( event, ui ) {

      $.each( cities_array, function( id, value ) {
        $.each( value, function( i, v ) {
          if(ui.item.value == v['city_name']){
            cityid = v['city_name'];
            $('[name="city_id"]').val(v['id']);
            $('[name="zipcode"]').val('');
          }
        });
      });
    }
  });


  $('[name="zip_code"]').autocomplete({
    source: function(request, response) {
      $.ajax({
        url: '/get-zips-for-autocomplete',
        type: "POST",
        data: {zip_name: request.term, country: ctrval, state: $('[name="state_name"]').val(), city: $('[name="city_name"]').val(), _token: '{{ csrf_token() }}'},
        success: function (data) {
          //console.log(data);
          var resp = $.map(data,function(obj){
            //console.log(resp);
            return obj.zip;
          });

          var zips = $.map(data,function(obj){
            return obj;
          });

          zips_array = [];
          zips_array.push(zips);

          response(resp);$('.ui-corner-all').css('padding', '20px');
          //return resp;
        }
      });
    },
    minLength: 1,
    select: function( event, ui ) {
      //console.log(states_array);
      $.each( zips_array, function( id, value ) {
        $.each( value, function( i, v ) {
          //console.log(v['id']);
          if(ui.item.value == v['zip']){
            $('[name="zip_code"]').val(v['zip']);
          }
        });
      });
    }
  });
  //end here
});
</script>

<script>
var country = "{{ isset($user->country_id) ? $user->country_id : '32' }}";

$('#country_id').on('change', function () {
  country = $(this).val();
  $('[name="country_id"]').val(country);
});

$('#state_id').on('change', function () {
  //alert($(this).val());
  var state_id = $(this).val();
  $('#city_id').html('<option value="0" selected disabled>Select City</option>');
  $('#zip_code').html('<option value="0" selected disabled>Select Zip</option>');
  $('#mls_id').val('');
  $.ajax({
    url: '/get-city-by-state',
    type: "POST",
    data: {state_id: state_id, _token: '{{ csrf_token() }}'},
    success: function (data) {
      //console.log(data);
      for(i=0; i<data.length; i++){
        $('#city_id').append('<option value="'+data[i].id+'">'+data[i].city_name+'</option>');
      }
    }
  });
});

$('#city_id').on('change', function () {
  var city_id = $('#city_id :selected').text();
  var country_id = country;
  var state_id = $('#state_id :selected').text();
  $('#mls_id').val('');
  $('#zip_code').html('<option value="0" selected disabled>Select Zip</option>');
  $.ajax({
    url: '/get_zip_by_city',
    type: "POST",
    data: {country_id: country_id, state_id: state_id, city_id: city_id, _token: '{{ csrf_token() }}'},
    success: function (data) {
      //console.log(data);
      for(i=0; i<data.length; i++){
        $('#zip_code').append('<option value="'+data[i].zip+'">'+data[i].zip+'</option>');
      }
    }
  });
});

</script>
@endif

@if(request()->segment(1) === 'dashboard' && (request()->segment(2) === 'successful_leads' || request()->segment(2) === 'approved' || request()->segment(2) === 'vip-leads'))
<script>
var reload = '0';

$('#myModal').on('shown.bs.modal', function (e) {

  var ad_id = e.relatedTarget.getAttribute('rel');

  $.ajax({
    url: '/get-all-users',
    type: "POST",
    data: { id: ad_id,_token: '{{ csrf_token() }}'},
    success: function (data) {
      $('#cur_user').text(data[0]);
      var option = '<option value="na" style="padding: 4px;" disabled selected>Select user</option>';

      $.each( data[1], function( key, value ) {
        option += '<option style="padding: 4px;" rel="'+ad_id+'" value="'+value['id']+'">'+value['name']+'</option>';
      });

      $('#agent_select').html(option);
    }
  });
});

$('#btn-change-user').click(function(){

  if(confirm("This ad will be reposted and all transactions involving this ad will be null and void")){
    var selected_user = $( "#agent_select option:selected" ).val();
    var ad_id = $( "#agent_select option:selected" ).attr('rel');

    if($( "#agent_select option:selected" ).val() == 'na'){
      $('#user-change-status').text('Please select user').css('color', 'red');
    }else{
      $.ajax({
        url: '/save-new-agent',
        type: "POST",
        data: {id: ad_id, user: selected_user, _token: '{{ csrf_token() }}'},
        success: function (data) {
          //console.log(data);
          if(data == '1'){
            $('#user-change-status').text('Lead is reassigned successfully').css('color', 'green');
            reload = '1';

            window.location.href = '/dashboard/u/posts/edit/' + ad_id + '/?agent_change=1';
          }else{
            $('#user-change-status').text('An error occured while processing request. Contact Newton.').css('color', 'red');
          }
        }
      });
    }
  }
});

$('#myModal').on('hidden.bs.modal', function (e) {
  if(reload == '1'){
    location.reload();
  }
})

</script>
@endif

<script type="text/javascript">
$('#first_dial_date, #second_dial_date, #sms_date, #email_date').datepicker({
  format: "mm/dd/yyyy",
  todayHighlight: true,
  autoclose: true,
  minDate: '-90',
  maxDate: '0',
  defaultDate: new Date(),
});
</script>


@if(request()->segment(2) == 'uploaded_leads_status')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>


<script>
$(document).ready(function() {

  var file_id = '{{ request()->segment(3) }}';
  var user =  '{{ Auth::user()->user_type }}';
  var stat = (user == 'admin' ? 'No data found. Please import the lead file (.csv only)' : 'No data found');

  var dtable = $('#jDataTable').DataTable({
    processing: false,
    serverSide: true,
    scrollY: "500px",
    scrollX:  true,
    scrollCollapse: true,
    columnDefs: [{ width: '20%', targets: 0 }],
    responsive: true,
    select: true,
    scroller: true,
    ajax:
    {
      "url": "{{ route('uploads_leads_data') }}",
      "data": function ( d ) {
        d.fid = file_id;
      }
    },

    "language": {
      "emptyTable": stat
    },

  });

});

@if(Auth::user()->user_type == 'admin')
$('#lead_importer').show();
@endif

$(window).load(function(){
  $('td.dataTables_empty').css('text-align', 'left');
});

</script>
@endif

<script>
$('#del-lead-entry').click(function(){
  if(confirm("Are you sure you want to delete this?")){
    //alert($(this).attr('rel'));
    var id = $(this).attr('rel');
    $.ajax({
      url: '/del-lead-entry',
      type: "POST",
      data: {id: id, _token: '{{ csrf_token() }}'},
      success: function (data) {
        //console.log(data);
        if(data != '0'){
          alert('Lead deleted');
          window.location.href = '/dashboard/uploaded_leads_status/'+data;
        }else{
          alert('an error occured during the process. please contact Newton/')
        }
      }
    });
  }
  else{
    return false;
  }
});
</script>

@yield('js')
</body>

<!-- end::Body -->
</html>
