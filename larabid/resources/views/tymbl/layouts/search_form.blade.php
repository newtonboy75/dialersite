<div class="row justify-content-md-center">
  <div>
    <form action="{{url('search')}}" method="get">
      {{ csrf_field() }}
      <div class="input-group input-group-sm"><div class="refine-search-link"><a role="button" href="javascript::" id="toggler-refine"><i style="color:blue; font-size: 10px !important;" class="fas fa-filter"></i> REFINE SEARCH</a></div>
      <div class="input-group-prepend mb-3"><div id="tagval"><i id="tagclose" title="remove" class="fas fa-times"></i>&nbsp;&nbsp;&nbsp;<span id="mileage">1 miles</span></div>
      <input class="q-search form-input" type="text" name="q" id="q-input" placeholder="Enter address, zip code, city or state" value="<?php if(isset($_GET['q'])){ echo $_GET['q']; }  ?>">
      <input type="hidden" id="loc-range" name="distance_range" value="">
      <input type="hidden" id="dstate" name="state" value="">
      <input type="hidden" id="dcity" name="city" value="">
      <input type="hidden" id="dzip" name="zipcode" value="">
      <input type="hidden" id="toggler" name="toggler_location" value="">
      <input name="hero_search" id="hero-search" class="btn btn-lg btn-primary" type="submit" value="SEARCH">
    </div>
  </div>

  </form>

</div>
</div>

<!-- start search popup -->
<div id="form-refined" class="hide">
  <form action="{{url('search')}}" id="popForm" method="get">
    <div><input name="q" type="text" class="form-control" value="" placeholder="Search Tymbl"></div>
    <div>
      <div><small>Location</small></div>
      <ul id="sliders-container">
        <li>
          <div class="form-check form-check-inline"><input class="form-check-input" name="toggler_location" value="distance" type="radio" id="toggler-distance"><label class="form-check-label" for="toggler-distance">Search Nearby</label></div>
          <div class="slidecontainer-distance">
            <div class="float-left"><small>Enter Zip Code</small></div>
            <input class="form-control" type="zip" name="zipcode" id="zip" placeholder="zip code" value="{{$zip_code}}">
            <div class="float-left"><small>move the slider</small></div>
            <div class="float-right"><small id="distance-value">1 mile/s</small></div>
            <input type="range" min="1" max="200" value="0" class="slider" id="myRange" name="distance_range">
          </div>
        </li>
        <li>
          <div class="form-check form-check-inline"><input class="form-check-input" name="toggler_location" value="location" type="radio" id="toggler-location"><label class="form-check-label" for="toggler-location">Search by Location</label></div>
          <div class="slidecontainer-location">
            <select class="custom-select" name="state" id="state">
              <option selected disabled value="all">State</option>
              @foreach($states as $state)
              <option value="{{$state->id}}">{{$state->state_name}}</option>
              @endforeach
            </select>
            <select class="custom-select" name="city" id="city" style="margin-top: 6px;">
            </select>
          </div>
        </li>
      </ul>
    </div>
    <br>
    <div class="p-range"><small>Price Range</small></div>
    <div class="form-check form-check-inline">
      <select class="custom-select" name="price" id="price-range" style="width: 250px;">
        <option selected disabled value="all">Choose Price Range</option>
        <option value="< $50K">< $50K</option>
        <option value="$50K-$99K">$50K - $99K</option>
        <option value="$100K-$199K">$100K - $199K</option>
        <option value="$200K-$299K">$200K - $299K</option>
        <option value="$300K-$399K">$300K - $399K</option>
        <option value="$400K-$499K">$400K - $499K</option>
        <option value="$500K-$599K">$500K - $599K</option>
        <option value="$600K-$699K">$600K - $699K</option>
        <option value="$700K-$799K">$700K - $799K</option>
        <option value="$800K-$899K">$800K - $899K</option>
        <option value="$900K-$1 million">$900K - $1 million</option>
        <option value="$1million-$2 million">$1million - $2 million</option>
        <option value="$2million-$3 million">$2million - $3 million</option>
        <option value="$3million-$4 million">$3million - $4 million</option>
        <option value="$4million-$5 million">$4million - $5 million</option>
        <option value="> 5 million">> 5 million</option>
      </select>
    </div>
    <br><br>
    <div><small>Looking For</small></div>
    <div class="form-check form-check-inline">
      <input class="form-check-input" name="cat_type" value="buying" type="radio" id="lookingToBuy">
      <label class="form-check-label" for="lookingToBuy">Buy</label>
    </div>

    <div class="form-check form-check-inline">
      <input class="form-check-input" name="cat_type" value="selling" type="radio" id="lookingToSell">
      <label class="form-check-label" for="lookingToSell">Sell</label>
    </div>

    <div class="form-check form-check-inline">
      <input class="form-check-input" name="cat_type" value="all" type="radio" id="lookingAll">
      <label class="form-check-label" for="lookingAll">Both</label>
    </div>

    <br><br>
    <div><small>Listing Type</small></div>

    <ul class="ul-ltype">
      <li>
        <div class="form-check form-check-inline">
          <input name="listing_type[0]" class="form-check-input" type="checkbox" id="security" value="condo">
          <label class="form-check-label" for="security">Condo</label>
        </div>
      </li>

      <li>
        <div class="form-check form-check-inline">
          <input name="listing_type[1]" class="form-check-input" type="checkbox" id="security" value="single-family-home">
          <label class="form-check-label" for="security">Single-Family Home</label>
        </div>
      </li>

      <li>
        <div class="form-check form-check-inline">
          <input name="listing_type[2]" class="form-check-input" type="checkbox" id="security" value="multi-family-home">
          <label class="form-check-label" for="security">Multi-Family Home</label>
        </div>
      </li>

      <li>
        <div class="form-check form-check-inline">
          <input name="listing_type[3]" class="form-check-input" type="checkbox" id="security" value="commercial">
          <label class="form-check-label" for="security">Commercial</label>
        </div>
      </li>

      <li>
        <div class="form-check form-check-inline">
          <input name="feature2[4]" class="form-check-input" type="checkbox" id="security" value="vacant-lot">
          <label class="form-check-label" for="security">Vacant Lot</label>
        </div>
      </li>

    </ul>

    <div><small>Features</small></div>
    <div id="input-narrow">

      <div class="row">
        <div class="col"><label for="room">Bedrooms</label>&nbsp;<input id="room" type="number" class="form-control input-narrow" name="feature[0]" value=""></div>
        <div class="col"><label for="room">Bath:</label>&nbsp;<input id="room" type="number" class="form-control input-narrow"  name="feature[1]" value=""></div>
      </div>

      <div class="row">
        <div class="col"><label for="room">Sq Feet:</label>&nbsp;<input id="room" type="number" class="form-control input-narrow"  name="feature[2]" value=""></div>
        <div class="col"><label for="room">Garages:</label>&nbsp;<input id="room" type="number" class="form-control input-narrow"  name="feature[3]" value=""></div>
      </div>

      <div class="row">
        <div class="col"><label for="room">Kitchen:</label>&nbsp;<input id="room" type="number" class="form-control input-narrow"  name="feature[4]" value=""></div>
        <div class="col"><label for="room">Deck:</label>&nbsp;<input id="room" type="number" class="form-control input-narrow"  name="feature[5]" value=""></div>
      </div>
    </div>

    <div>
      <ul class="ul-ltype">
        <li>
          <div class="form-check form-check-inline">
            <input name="feature2[0]" class="form-check-input" type="checkbox" id="security" value="security">
            <label class="form-check-label" for="security">24 hr security</label>
          </div>
        </li>

        <li>
          <div class="form-check form-check-inline">
            <input name="feature2[1]" class="form-check-input" type="checkbox" id="laundry" value="laundry">
            <label class="form-check-label" for="laundry">Laundry Services</label>
          </div>
        </li>

        <li>
          <div class="form-check form-check-inline">
            <input name="feature2[2]" class="form-check-input" type="checkbox" id="garden" value="garden">
            <label class="form-check-label" for="garden">Garden</label>
          </div>
        </li>

        <li>
          <div class="form-check form-check-inline">
            <input name="feature2[3]" class="form-check-input" type="checkbox" id="swimming" value="swimming">
            <label class="form-check-label" for="swimming">Swimmig pool</label>
          </div>
        </li>

        <li>
          <div class="form-check form-check-inline">
            <input name="feature2[4]" class="form-check-input" type="checkbox" id="tennis" value="tennis">
            <label class="form-check-label" for="tennis">Tennis Court</label>
          </div>
        </li>

        <li>
          <div class="form-check form-check-inline">
            <input name="feature2[5]" class="form-check-input" type="checkbox" id="school" value="school">
            <label class="form-check-label" for="school">Close to Local Schools</label>
          </div>
        </li>

        <li>
          <div class="form-check form-check-inline">
            <input name="feature2[6]" class="form-check-input" type="checkbox" id="pet" value="pet">
            <label class="form-check-label" for="pet">Pet Friendly</label>
          </div>
        </li>

        <li>
          <div class="form-check form-check-inline">
            <input name="feature2[7]" class="form-check-input" type="checkbox" id="lift" value="lift">
            <label class="form-check-label" for="lift">Lift</label>
          </div>
        </li>

        <li>
          <div class="form-check form-check-inline">
            <input name="feature2[8]" class="form-check-input" type="checkbox" id="access" value="access">
            <label class="form-check-label" for="access">Access Controlled</label>
          </div>
        </li>
      </ul>
    </div>

    <div class="text-center display-block">
      <input type="submit" class="btn btn-primary" value="Search">
      <br/><br/>
    </div>
  </form>
</div>


<div id="dropdown-loc">
  <div class="slidecontainer-loc">
    <select class="custom-select" name="state" id="state">
      <option selected disabled value="all">Select state</option>
      @foreach($states as $state)
      <option value="{{$state->id}}">{{$state->state_name}}</option>
      @endforeach
    </select>
    <select class="custom-select" name="city" id="city" style="margin-top: 6px;">
    </select>
    <div id="derror" style="color:red; font-size: 11px;padding:4px;"></div>
    <a href="#" role="button" class="btn btn-lg btn-primary" id="ok-loc">Ok</a>
    <br>
  </div>
</div>
