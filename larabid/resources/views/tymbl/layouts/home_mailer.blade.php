<section class="single-section homepage-contact">
    <div class="container">
        <div class="section-title">
            <h3>Didn't find what you were looking for?</h3>
            <h1>CONTACT US</h1>
        </div>

        <div class="homepage-form">
            {{ Form::open(array('url' => 'home-contact-send')) }}
              {!! csrf_field() !!}
                <div class="form-row">
                    <div class="col-md-3 col-sm-12">
                        <input type="text" name="name" id="name" placeholder="Your Name" class="form-control" required>
                        {!! $errors->has('name')? '<small class="help-block">'.$errors->first('name').'</small>':'' !!}
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <input type="email" name="email" id="email" placeholder="Your Email" class="form-control" data-toggle="tooltip" data-placement="top" title="Your email addess will be secured with us. required">
                        {!! $errors->has('email')? '<small class="help-block">'.$errors->first('email').'</small>':'' !!}
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <input type="text" name="message" id="subject" placeholder="I would like to discuss" class="form-control" required>
                        {!! $errors->has('message')? '<small class="help-block">'.$errors->first('message').'</small>':'' !!}
                    </div>
                    <input type="hidden" name="to" value="info@tymbl.com">
                    <div class="col-md-3 col-sm-12" style="margin-top:10px;">
                        <input type="submit" class="btn btn-primary btn-block btn-lg" value="SEND">
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</section>
