<section class="single-section core-features">
    <div class="container">
        <div class="section-title">
            <h1>Tymbl is Different</h1>
            <h3>With some incredible features</h3>
        </div>
        <div class="row all-features text-center">
            <div class="col-sm-2"></div>
            <div class="col-sm-3 text-center">
                <img src="{{ asset('assets/img/tymbl/electronic.png') }}" alt="Excellent Value" class="center-block">
                <h4>Electronic Referral Agreement</h4>
            </div>
            <div class="col-sm-3 text-center">
                <img src="{{ asset('assets/img/tymbl/escrow.png') }}" alt="Excellent Value" class="center-block">
                <h4>Escrow deposits to establish trust</h4>
            </div>
            <div class="col-sm-3 text-center">
                <img src="{{ asset('assets/img/tymbl/notification.png') }}" alt="Excellent Value" class="center-block">
                <h4>Title company rep notifications</h4>
            </div>
            <div class="col-sm-2"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>
<!-- /.core-features -->
