<div class="collapse navbar-collapse" id="menu">
  <ul class="navbar-nav ml-auto">
    <li class="{{ request()->segment(1) === 'home'? "active": ""}} nav-item">
      <a href="/" class="nav-link">Home</a>
    </li>
    <li class="{{ request()->segment(2) === 'about-us'? "active": "" }} nav-item">
      <a href="/page/about-us" class="nav-link">TEAM</a>
    </li>
    <li class="{{ request()->segment(2) === 'how-it-works'? "active": "" }} nav-item">
      <a href="/page/how-it-works" class="nav-link">HELP</a>
    </li>
    <li class="{{ request()->segment(1) === 'post-new'? "active": "" }} nav-item">
      <a href="/post-new" class="nav-link">REFER A CLIENT</a>
    </li>
    <li class="{{ request()->segment(1) === 'plans'? "active": "" }} nav-item">
      <a href="/plans" class="nav-link">UPLOAD LEADS</a>
    </li>

    @if (!Auth::check())
    <li class="{{ request()->segment(1) === 'login'? "active": "" }} nav-item">
      <a href="{{ route('login') }}" class="nav-link">Login</a>
    </li>
    <li class="nav-item">
      <a href="{{ route('register') }}" class="btn btn-primary btn-lg">Register</a>
      <!-- <a href="#" class="nav-link btn btn-primary" style="color: white;">register</a> -->
    </li>
    @else
    <li class="dropdown nav-item">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="display:block; padding: .5rem 1rem;">
        @php
        $user_id = auth()->user()->id;
        $broker = \App\Broker::where('user_id', '=', $user_id)->first();
        @endphp
        {{ safe_output(auth()->user()->first_name) }}
        @if($broker)
        @if($broker->broker_verified == '1')
        <i class="text-primary fas fa-check-circle" title="Vefiried"></i>
        @endif
        @endif
        &nbsp;&nbsp;<span class="headerAvatar">

          <img style="width:20px;" src="{{ auth()->user()->get_gravatar() !== null ? auth()->user()->get_gravatar() : '/uploads/avatar/gravatar-default.png' }}" />

        </span> <span class="caret"></span>
      </a>

      <ul class="dropdown-menu" role="menu" style="position:relative;  width: 80px; margin-top: -46px; margin-bottom: -40px !important; padding:10px;font-size:12px;">
        <li style="font-size: 12px;padding:3px;"><a href="{{route('dashboard')}}"> @lang('app.dashboard') </a> </li>
        <li style="font-size: 12px;padding:3px;">
          <a href="{{ route('logout') }}"
          onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">
          @lang('app.logout')
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
        </form>
      </li>
    </ul>
  </li>

  @endif
</ul>

</div>
