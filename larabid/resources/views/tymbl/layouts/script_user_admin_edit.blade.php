<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<script>
    $(document).ready(function() {
        $('#jDataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('get_users_data') }}',
            "aaSorting": []
        });
    });
</script>

<script>

    $('#userModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var userid = button.data('userid');
        $.ajax({
            type: 'POST',
            url: '{{ route('get-user-info') }}',
            data: {userid: userid, _token: '{{ csrf_token() }}'},
            success: function (data) {
                //console.log(data);
                $('#user_edit').html(data);
            },
            error: function (data) {
                //console.log(data);
                $('#user_edit').html(data);
            }
        });
    });


    $('#remove_user').on('click', function() {
        var id = $('#uid').data('id');
        if (confirm('Are you sure you want to delete user #' + id + '?')) {
            $.ajax({
                type: 'POST',
                url: '{{ route('user-delete') }}',
                data: {id: id, _token: '{{ csrf_token() }}'},
                success: function (data) {
                    if (data == '1') {
                        location.reload();
                    }
                }
            });
        }
    });



    $('#save_edit').click(function() {
        var required = $('input,select').filter('[required]:visible');
        var continue_save = false;
        required.each(function() {
            var element = $(this);
            if(element.attr('id') != 'zip' || element.attr('id') != 'mls') {
                if (element.val().length <= 0) {
                    continue_save = false;
                    element.css('border', '1px solid red');
                } else {
                    element.css('border', '1px solid #f0f0f0');
                    continue_save = true;
                }
            }
        });


        if (continue_save === true) {
            var id = $('#uid').data('id');
            $.ajax({
                type: 'POST',
                url: '/admin-user-save',
                data: {
                    id: id,
                    name: $('#first_name').val() + ' ' + $('#last_name').val() ,
                    first_name: $('#first_name').val(),
                    last_name: $('#last_name').val(),
                    title: $('#title').val(),
                    email: $('#email').val(),
                    phone: $('#phone').val(),
                    address: $('#address').val(),
                    country_id: $('#country_id').val(),
                    state_id: $('#state_id').val(),
                    city_id: $('#city_id').val(),
                    zip: $('#zip').val(),
                    brokerage_name: $('#broker').val(),
                    broker_contact_person: $('#broker_contact_person').val(),
                    broker_contact_num: $('#broker_contact_num').val(),
                    broker_email: $('#broker_email').val(),
                    mls_id: $('#mls_id').val(),
                    active_status: $('#user_status').val(),
                    _token: '{{ csrf_token() }}'
                },
                success: function (data) {
                    if (data == '1') {
                        var options = {closeButton: true};
                        toastr.success('User details have been updated', 'Success!', options);
                    } else {
                        var options = {closeButton: true};
                        toastr.error(data, 'Error!', options)
                    }
                }
            });
        } else {
            alert('Validation Error: Missing required fields');
        }
    });

    //autocompletes

    var auto_titles = [
        "Mr",
        "Mrs",
        "Others"
    ];

    var auto_countries = [
        "United States",
        "Canada"
    ];

    $(document).on('keyup', '#title', function () {
        $('#title').autocomplete({
            source: auto_titles
        });
    });

    $(document).on('keyup', '#country', function () {
        $('#country').autocomplete({
            source: auto_countries,
            select: function(event, ui) {
                $("#state").val('');
                $("#state_id").val('');
                $("#city").val('');
                $("#city_id").val('');
                $("#zip").val('');
                $("#mls_id").val('');
                if (ui.item.value == 'United States') {
                    $('#country_id').val('231');
                } else {
                    $('#country_id').val('38');
                }
            }
        });
    });


    $(document).on('keyup', '#state', function () {
        $("#state").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/get-states-for-autocomplete',
                    type: "POST",
                    data: {
                        state_name: request.term,
                        country_id: $("#country_id").val(),
                        _token: '{{ csrf_token() }}'
                    },
                    success: function (data) {
                        var resp = $.map(data, function (obj) {
                            return obj.state_name;
                        });

                        var states = $.map(data, function (obj) {
                            return obj;
                        });
                        states_array = [];
                        states_array.push(states);
                        response(resp);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                $.each(states_array, function (id, value) {
                    $.each(value, function (i, v) {
                        if (ui.item.value == v['state_name']) {
                            stateid = v['state_name'];
                            $("#state_id").val(v['id']);
                            $("#city").val('');
                            $("#city_id").val('');
                            $("#zip").val('');
                            $("#mls_id").val('');
                            $("#mls").val('');
                        }
                    });
                });
            }
        });
    });

    $(document).on('keyup', '#city', function () {
        $("#city").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/get-cities-for-autocomplete',
                    type: "POST",
                    data: {city_name: request.term, state_id: $("#state_id").val(), _token: '{{ csrf_token() }}'},
                    success: function (data) {
                        var resp = $.map(data, function (obj) {
                            return obj.city_name;
                        });

                        var cities = $.map(data, function (obj) {
                            return obj;
                        });
                        cities_array = [];
                        cities_array.push(cities);
                        response(resp);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                $.each(cities_array, function (id, value) {
                    $.each(value, function (i, v) {
                        if (ui.item.value == v['city_name']) {
                            cityid = v['city_name'];
                            $("#city_id").val(v['id']);
                            $("#zip").val('');
                        }
                    });
                });
            }
        });
    });

    $(document).on('keyup', '#zip', function () {
        $('#zip').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/get-zips-for-autocomplete',
                    type: "POST",
                    data: {
                        zip_name: request.term,
                        country: $("#country_id").val(),
                        state: $("#state").val(),
                        city: $("#city").val(),
                        _token: '{{ csrf_token() }}'
                    },
                    success: function (data) {
                        var resp = $.map(data, function (obj) {
                            return obj.zip;
                        });
                        var zips = $.map(data, function (obj) {
                            return obj;
                        });
                        zips_array = [];
                        zips_array.push(zips);
                        response(resp);
                        $('.ui-corner-all').css('padding', '20px');
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                $.each(zips_array, function (id, value) {
                    $.each(value, function (i, v) {
                        if (ui.item.value == v['zip']) {
                            $("#zip").val(v['zip']);
                        }
                    });
                });
            }
        });

    });

    $(document).on('keyup', '#mls', function () {
        $("#mls").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/get-mls-for-autocomplete',
                    type: "POST",
                    data: {name: request.term, state: $("#state").val(), _token: '{{ csrf_token() }}'},
                    success: function (data) {
                        var resp = $.map(data, function (obj) {
                            return obj.name;
                        });

                        var mls = $.map(data, function (obj) {
                            return obj;
                        });

                        mls_array = [];
                        mls_array.push(mls);

                        response(resp);
                        $('.ui-corner-all').css('padding', '20px');
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {

                $.each(mls_array, function (id, value) {
                    $.each(value, function (i, v) {
                        if (ui.item.value == v['name']) {
                            $("#mls_id").val(v['id']);
                        }
                    });
                });
            }
        });
    });
</script>
