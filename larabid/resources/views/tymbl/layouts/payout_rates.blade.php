<section class="single-section core-features">
    <div class="container">
        <div class="section-title">
            <h1>How much does it cost?</h1>
        </div>
        <div class="row all-features text-center">

            <div class="col-sm-6 text-center">
                <h4>Qualified Referrals</h4>
                <img src="{{ asset('assets/img/tymbl/contract.png') }}" style="width:50px;" alt="Excellent Value" class="center-block">
                <h4>35% of your portion of <br>the commission</h4>
            </div>
            <div class="col-sm-6 text-center">
              <h4>Non-qualified Leads</h4>
                <img src="{{ asset('assets/img/tymbl/document.png') }}" style="width:50px;" alt="Excellent Value" class="center-block">
                <h4>15% of your portion of <br>the commission</h4>
            </div>

        </div>
        <div class="clearfix"></div>
    </div>
</section>
<!-- /.core-features -->
