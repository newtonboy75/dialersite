<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 footer-col">
                <div class="footer-logo">

                    <a href="/"><img src="{{ asset('assets/img/tymbl/logo-white.png') }}" alt="Tymbl Logo"></a>
                </div>
            </div>
            <div class="col-sm-4 footer-col">
                <div class="footer-links">
                    <div class="link-item"><a href="/page/privacy-policy">Privacy Policy</a></div>
                    <div class="link-item"><a href="/page/terms-and-conditions-1">Terms & Conditions</a></div>
                    <div class="link-item"><a href="/page/frequently-asked-questions">FAQs</a></div>
                    <div class="link-item"><a href="/contact-us">Contact Us</a></div>
                </div>
            </div>
            <div class="col-sm-4 footer-col">
                @include('tymbl.layouts.social_sharelinks')
            </div>
        </div>
        <div class="copyright">Copyright 2018, Tymbl All rights reserved</div>
    </div>
</footer>
