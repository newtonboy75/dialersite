@section('page-js')
<script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>

<script>
// Replace the <textarea id="editor1"> with a CKEditor
// instance, using default configuration.
CKEDITOR.replace('desc');
</script>
<script src="{{asset('assets/plugins/bootstrap-datepicker-1.6.4/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript">
$('#application_deadline, #bid_deadline, #date_list, #date_contacted_qualified').datepicker({
  format: "mm/dd/yyyy",
  todayHighlight: true,
  autoclose: true,
  minDate: '-90',
  maxDate: '0',
  defaultDate: new Date(),
});
$('#build_year').datepicker({
  format: "yyyy",
  viewMode: "years",
  minViewMode: "years",
  autoclose: true
});

</script>

<script>

@if(isset($current_country))
var current_country = "{{$current_country}}";

if(current_country == '{{$current_country}}'){
  $('[name=country] option').filter(function() {
    return ($(this).text() == '{{$current_country}}');
  }).prop('selected', true);
}
@endif

$('.req-add').hide();
$('.refAdress').removeAttr('required');

$('#lookingToBuy').click(function(){

  $('[name="cat_type_q"]').each(function(){
    if($(this).prop("checked") == true){
      $(this).prop("checked", false);
    }
  });
  loadBuy();

});

$('#lookingToSell').click(function(){
  loadSell();
  $('[name="cat_type_q"]').each(function(){
    if($(this).prop("checked") == true){
      $(this).prop("checked", false);
    }
  });
  loadSell();
});


$('#notSure').click(function(){
  $('[name="cat_type_q"]').each(function(){
    if($(this).prop("checked") == true){
      $(this).prop("checked", false);
    }
  });
  $('.req-add').hide();
  $('.refAdress').removeAttr('required');
  openSubCat();
  $('#sub-category').append($("<option></option>").attr({'value' : '11', 'data-category-type' :  'other'}).text('Unqualified')).prop("selected", true);
});

$('[id="ns_qq"]').click(function(){
  var percentage = $(this).val() == 'yes' ? '25%' : '10%';
  $('#refFee').val(percentage);

  if($(this).val() == 'no'){
    $('.req-add').hide();
    $('.refAdress').removeAttr('required');
    $('#sub-category').empty();
    $('#sub-category').append($("<option></option>").attr({'value' : '11', 'data-category-type' :  'other'}).text('Unqualified')).prop("selected", false);
    $('#date_list').val('');
    $('#dte-list').hide();
    $('#dt_list').hide();

  }else{
    $('#dt_list').show();
    $('#dte-list').show();
    $('#date_list').val('');
    if($('[name="cat_type"]:checked').attr('id') == 'lookingToBuy'){
      loadBuy();
    }else if($('[name="cat_type"]:checked').attr('id') == 'lookingToSell'){
      loadSell();
    }
  }
});


function openSubCat(){
  $('#sub-category').empty();
  //$('#sub-category').append($("<option></option>").attr({'value' : '11', 'data-category-type' :  'unqualified'}).text('Unqualified')).prop("selected", false);
  $('#qq').show();
}

function loadSell(){

  $('.req-add').show();
  $('#qq').hide();
  $('.refAdress').attr('required', 'required');
  var sell = <?php echo json_encode($selling); ?>;
  var old_cats = $('#sub-category option:selected').text();
  openSubCat();

  for(var i=0; i<=5; i++){
    $('#sub-category').append($("<option></option>").attr({'value' : sell[i]['id'], 'data-category-type' :  'selling'}).text($.trim(sell[i]['name'])));
    if($.trim(sell[i]['name']) == $.trim(old_cats)){
      $('#sub-category').val(sell[i]['id']).prop("selected", true);
    }else{
      $('#sub-category').val('1').prop("selected", true);
    }
  }
}

function loadBuy(){

  $('.req-add').hide();
  $('#qq').hide();
  $('.refAdress').removeAttr('required');
  var buy = <?php echo json_encode($buying); ?>;
  var old_catb = $('#sub-category option:selected').text();
  openSubCat();

  for(var i=0; i<=5; i++){
    $('#sub-category').append($("<option></option>").attr({'value' : buy[i]['id'], 'data-category-type' :  'buying'}).text($.trim(buy[i]['name'])));
    if($.trim(buy[i]['name']) == $.trim(old_catb)){
      $('#sub-category').val(buy[i]['id']).prop("selected", true);
    }else{
      $('#sub-category').val('6').prop("selected", true);
    }
  }
}

</script>


<script>
$('[name="country"]').change(function () {
  //alert('hey');
  var country_id = $(this).val();
  //$('#state_loader').show();
  $('[name="zipcode"]').html('<option disabled selected>Select Zip Code</option>');
  $.ajax({
    type: 'POST',
    url: '{{ route('get_state_by_country') }}',
    data: {country_id: country_id, _token: '{{ csrf_token() }}'},
    success: function (data) {
      generate_option_from_json_body(data, 'country_to_state');
      //console.log(data);
    }
  });
});

$('[name="state"]').change(function () {
  var state_id = $(this).val();
  //$('#city_loader').show();
  select_state = state_id;
  $.ajax({
    type: 'POST',
    url: '{{ route('get_city_by_state') }}',
    data: {state_id: state_id, _token: '{{ csrf_token() }}'},
    success: function (data) {
      //console.log(data);
      generate_option_from_json_body(data, 'state_to_city');
    }
  });
});

$('body').on('click', '.imgDeleteBtn', function () {
  //Get confirm from user
  if (!confirm('{{ trans('app.are_you_sure') }}')) {
    return '';
  }

  var current_selector = $(this);
  var img_id = $(this).closest('.img-action-wrap').attr('id');
  $.ajax({
    url: '{{ route('delete_media') }}',
    type: "POST",
    data: {media_id: img_id, _token: '{{ csrf_token() }}'},
    success: function (data) {
      if (data.success == 1) {
        current_selector.closest('.creating-ads-img-wrap').hide('slow');
        toastr.success(data.msg, '@lang('app.success')', toastr_options);
      }
    }
  });
});
/**
* Change ads price by urgent or premium
*/

$(document).on('click', '.image-add-more', function (e) {
  e.preventDefault();
  $('.upload-images-input-wrap2').append('<br /><br /><input type="file" name="images[]" id="file" class="inputfile" />');
});



</script>

<script>
function generate_option_from_json_body(jsonData, fromLoad) {

  //Load Category Json Data To Brand Select
  if (fromLoad === 'country_to_state') {
    $('[name="state"]').html('<option disabled selected>Select State</option>');
    $('[name="zipcode"]').html('<option disabled selected>Select Zip Code</option>');
    var option = '';

    if (jsonData.length > 0) {
      option += '<option value="0" selected>Select State</option>';
      for (i in jsonData) {
        option += '<option value="' + jsonData[i].id + '"> ' + jsonData[i].state_name + ' </option>';
      }
      $('[name="state"]').html(option);
      //$('#state_select').select2();
    } else {
      $('[name="state"]').html('<option disabled selected>Select State</option>');
      //$('#state_select').select2();
    }
    //$('#state_loader').hide('slow');

  } else if (fromLoad === 'state_to_city') {

    $('[name="zipcode"]').html('<option disabled selected">Select Zip Code</option>');
    $('[name="city"]').html('<option disabled selected>Select City</option>');

    var option = '';
    if (jsonData.length > 0) {

      for (i in jsonData) {
        option += '<option value="' + jsonData[i].id + '">' + jsonData[i].city_name + '</option>';
      }

      //return option;

      $('#city_select').append(option);
      $('[name="zipcode"]').html('<option disabled selected>Select Zip Code</option>');
      //$('#city_select').select2();
    } else {
      $('[name="city"]').html('<option disabled selected>Select City</option>');
      $('[name="zipcode"]').html('<option disabled selected>Select Zip Code</option>');
      //$('#city_select').select2();
    }
  }
}
</script>



<script>
$('[name="city"]').change(function () {
  var state_id = $('[name="state"] option:selected').text();
  var city_id = $(this).find('option:selected').text();
  var country = $('[name="country"]').val();
  select_city = city_id;

  $('[name="zipcode"]').html('<option disabled selected>Select Zip Code</option>');
  $.ajax({
    type: 'POST',
    url: '{{ route('get_zip_by_city') }}',
    data: {country_id: country, state_id: state_id, city_id: city_id, _token: '{{ csrf_token() }}'},
    success: function (data) {
      //generate_option_from_json(data, 'city_to_zip');
      //console.log(data);
      var option = '';
      if (data.length > 0) {
        option += '<option value="0" selected>Select Zip Code</option>';
        for (i in data) {
          option += '<option value="' + data[i].zip + '" >' + data[i].zip + '</option>';
        }
        $('[name="zipcode"]').html(option);
        //$('#zipcode_select').select2();
      } else {
        $('[name="zipcode"]').html('<option value="" selected>(Zip Code not found)</option>');
        //$('#zipcode_select').select2();
      }

    }
  });
});


</script>

@if(request()->segment(1) === 'post-new')

<script>
var old_zip = "{{ old('zipcode') }}";
var select_state = '';
var select_city = '';
var select_zip = '';

if(old_zip != ''){
  $('[name=zipcode] option').filter(function() {
    return ($(this).text() == '{{ old('zipcode') }}');
  }).prop('selected', true);
}

$(document).on('click', '[name=cat_type], [name=cat_type_q]', function (e) {
  $('[id=ltp]').hide();
  $('[id=ltq]').hide();
});

$(document).on('click', '#post_submit', function (e) {

  $('input, textarea, select').each(function() {
    $(this).removeClass('check-error');
    $('.cke_inner').removeClass('check-error');
    $('[id=ltp]').hide();
    $('[id=ltq]').hide();
  });

  var ckdata = CKEDITOR.instances.desc.getData();
  var messageLength = CKEDITOR.instances['desc'].getData().replace(/<[^>]*>/gi, '').length;

  if (!$("input[name='cat_type']:checked").val()) {
    toastr.error($('#lookingToBuy').data('msg'), 'Error');
    $('html, body').animate({
      scrollTop: 40
    },200);
    $('[id=ltp]').show();
    return false;
  }else if(!$("input[name='cat_type_q']:checked").val()){
    toastr.error($('#ns_qq').data('msg'), 'Error');
    $('html, body').animate({
      scrollTop: 40
    },200);
    $('[id=ltq]').show();
    return false;
  }else if($("input[name='cat_type_q']:checked").val() == 'yes'){
    if($('#date_list').val() == ''){
      toastr.error($('#date_list').data('msg'), 'Error');
      $('html, body').animate({
        scrollTop: 40
      },200);
      $('#date_list').addClass('check-error');
      return false;
    }
  }else if($('#refFee').val() == ''){
    toastr.error($('#refFee').data('msg'), 'Error');
    $('html, body').animate({
      scrollTop: 40
    },200);
    $('#refFee').addClass('check-error');
    return false;
  }

  if($('#title').val() == ''){
    toastr.error($('#title').data('msg'), 'Error');
    $('html, body').animate({
      scrollTop: 100
    },200);
    $('#title').addClass('check-error');
    return false;

  }else if(messageLength < 1){
    toastr.error($('#desc').data('msg'), 'Error');
    $('html, body').animate({
      scrollTop: 500
    },200);
    $('.cke_inner').addClass('check-error');
    return false;
  }else if($('#price-range').find('option:selected').text() == 'Choose Price Range'){
    toastr.error($('#price-range').data('msg'), 'Error');
    $('html, body').animate({
      scrollTop: $("#price-range").offset().top - 200
    }, 200);
    $('#price-range').addClass('check-error');
    return false;
  }else if($('#refFName').val() == ''){
    toastr.error($('#refFName').data('msg'), 'Error');
    $('html, body').animate({
      scrollTop: $("#refFName").offset().top - 200
    }, 200);
    $('#refFName').addClass('check-error');
    return false;
  }else if($('#refLName').val() == ''){
    toastr.error($('#refLName').data('msg'), 'Error');
    $('html, body').animate({
      scrollTop: $("#refLName").offset().top - 200
    }, 200);
    $('#refLName').addClass('check-error');
    return false;
  }else if($('#seller_phone').val() == '' || $('#seller_phone').val().length < 10){
    toastr.error($('#seller_phone').data('msg'), 'Error');
    $('html, body').animate({
      scrollTop: $("#seller_phone").offset().top - 200
    }, 200);
    $('#seller_phone').addClass('check-error');
    return false;
  }else if($('#refAdress').prop('required') && $('#refAdress').val() == ''){
    toastr.error($('#refAdress').data('msg'), 'Error');
    $('html, body').animate({
      scrollTop: $("#seller_phone").offset().top - 200
    }, 200);
    $('#refAdress').addClass('check-error');
    return false;
  }else if($('[name=country_name]').val() == ''){
    toastr.error('Country is required', 'Error');
    $('html, body').animate({
      scrollTop: $('[name=state_name]').offset().top - 200
    }, 200);
    $('[name=state]').addClass('check-error');
    return false;
  }else if($('[name=state_name]').val() == ''){
    toastr.error('State is required', 'Error');
    $('html, body').animate({
      scrollTop: $('[name=state_name]').offset().top - 200
    }, 200);
    $('[name=state]').addClass('check-error');
    return false;
  }else if($('[name=state_name]').val().length < 4){
    toastr.error('State is invalid', 'Error');
    $('html, body').animate({
      scrollTop: $('[name=state_name]').offset().top - 200
    }, 200);
    $('[name=state]').addClass('check-error');
    return false;
  }else if($('[name=city_name]').val() == ''){
    toastr.error('City is required', 'Error');
    $('html, body').animate({
      scrollTop: $("[name=state_name]").offset().top - 200
    }, 200);
    $('[name=city]').addClass('check-error');
    return false;
  }else if($('[name=city_name]').val().length < 4){
    toastr.error('City is required', 'Error');
    $('html, body').animate({
      scrollTop: $("[name=state_name]").offset().top - 200
    }, 200);
    $('[name=city]').addClass('check-error');
    return false;
  }
  return true;
});
</script>

@endif

@if(request()->segment(3) == 'posts' && request()->segment(4) == 'edit')

<script>
var old_zip = "{{ old('zipcode') }}";
var select_state = '';
var select_city = '';
var select_zip = '';


$(document).on('click', '[name=cat_type], [name=cat_type_q]', function (e) {
  $('[id=ltp]').hide();
  $('[id=ltq]').hide();
});

$(document).on('click', '#post_submit', function (e) {
  $('input, textarea, select').each(function() {
    $(this).removeClass('check-error');
    $('.cke_inner').removeClass('check-error');
    $('[id=ltp]').hide();
    $('[id=ltq]').hide();
  });

  var ckdata = CKEDITOR.instances.desc.getData();
  var messageLength = CKEDITOR.instances['desc'].getData().replace(/<[^>]*>/gi, '').length;

  if (!$("input[name='cat_type']:checked").val()) {
    toastr.error($('#lookingToBuy').data('msg'), 'Error');
    $('html, body').animate({
      scrollTop: 40
    },200);
    $('[id=ltp]').show();
    return false;
  }else if(!$("input[name='cat_type_q']:checked").val()){
    toastr.error($('#ns_qq').data('msg'), 'Error');
    $('html, body').animate({
      scrollTop: 40
    },200);
    $('[id=ltq]').show();
    return false;
  }else if($("input[name='cat_type_q']:checked").val() == 'yes'){
    if($('#date_list').val() == ''){
      toastr.error($('#date_list').data('msg'), 'Error');
      $('html, body').animate({
        scrollTop: 40
      },200);
      $('#date_list').addClass('check-error');
      return false;
    }
  }else if($('#refFee').val() == ''){
    toastr.error($('#refFee').data('msg'), 'Error');
    $('html, body').animate({
      scrollTop: 40
    },200);
    $('#refFee').addClass('check-error');
    return false;
  }

  if($('#ad_title').val() == ''){
    toastr.error($('#ad_title').data('msg'), 'Error');
    $('html, body').animate({
      scrollTop: 100
    },200);
    $('#ad_title').addClass('check-error');
    return false;

  }else if(messageLength < 1){
    toastr.error($('#desc').data('msg'), 'Error');
    $('html, body').animate({
      scrollTop: 500
    },200);
    $('.cke_inner').addClass('check-error');
    return false;
  }else if($('#price-range').find('option:selected').text() == 'Choose Price Range'){
    toastr.error($('#price-range').data('msg'), 'Error');
    $('html, body').animate({
      scrollTop: $("#price-range").offset().top - 200
    }, 200);
    $('#price-range').addClass('check-error');
    return false;
  }else if($('#refFName').val() == ''){
    toastr.error($('#refFName').data('msg'), 'Error');
    $('html, body').animate({
      scrollTop: $("#refFName").offset().top - 200
    }, 200);
    $('#refFName').addClass('check-error');
    return false;
  }else if($('#refLName').val() == ''){
    toastr.error($('#refLName').data('msg'), 'Error');
    $('html, body').animate({
      scrollTop: $("#refLName").offset().top - 200
    }, 200);
    $('#refLName').addClass('check-error');
    return false;
  }else if($('#seller_phone').val() == '' || $('#seller_phone').val().length < 10){
    toastr.error($('#seller_phone').data('msg'), 'Error');
    $('html, body').animate({
      scrollTop: $("#seller_phone").offset().top - 200
    }, 200);
    $('#seller_phone').addClass('check-error');
    return false;
  }else if($('#refAdress').prop('required') && $('#refAdress').val() == ''){
    toastr.error($('#refAdress').data('msg'), 'Error');
    $('html, body').animate({
      scrollTop: $("#seller_phone").offset().top - 200
    }, 200);
    $('#refAdress').addClass('check-error');
    return false;
  }else if($('[name=country_name]').val() == ''){
    toastr.error('Country is required', 'Error');
    $('html, body').animate({
      scrollTop: $('[name=state_name]').offset().top - 200
    }, 200);
    $('[name=state]').addClass('check-error');
    return false;
  }else if($('[name=state_name]').val() == ''){
    toastr.error('State is required', 'Error');
    $('html, body').animate({
      scrollTop: $('[name=state_name]').offset().top - 200
    }, 200);
    $('[name=state]').addClass('check-error');
    return false;
  }else if($('[name=city_name]').val() == ''){
    toastr.error('City is required', 'Error');
    $('html, body').animate({
      scrollTop: $("[name=state_name]").offset().top - 200
    }, 200);
    $('[name=city]').addClass('check-error');
    return false;
  }
  return true;
});
</script>

@endif

@php
$current_country = isset($current_country) ? $current_country : '';

if($current_country != ''){
  if($current_country == 'United States'){
    $cur_country ='us';
  }else{
    $cur_country = 'ca';
  }
}else{
  $country = $ad->country_id;
  if($country == '231'){
    $cur_country = 'us';
  }else{
    $cur_country = 'ca';
  }
}

@endphp

<script>

function initMap() {

  var options = {
    types: [('address')],
    componentRestrictions: {country: "{{$cur_country}}"}
  };

  var input = document.getElementById('refAdress');
  var autocomplete = new google.maps.places.Autocomplete(input, options);

  autocomplete.addListener('place_changed', function() {

    var place=autocomplete.getPlace();
    var zips = '';
    this.pickup=place.address_components;
    //console.log(this.pickup);
    //alert(zips);

    var street_number = '';
    var street = '';
    var city = '';
    var state = '';
    var zip = '';
    var cities = [];

    $.each(this.pickup, function( index, value ) {
      //console.log(value);

      if(value['types'][0]=='country'){
        //console.log(value['long_name']);
        if(value['long_name'] == 'United States'){
          $('[name="country"]').val('231');
        }else{
          $('[name="country"]').val('38');
        }
        $('[name="country_name"]').val(value['long_name']);
      }

      if(value['types'][0]=='street_number'){
        //console.log(value['long_name']);
        street_number = value['long_name'];
      }

      if(value['types'][0]=='route'){
        //console.log(value['long_name']);
        street = value['long_name'];
      }

      if(value['types'][0]=='locality'){
        //console.log(value['long_name']);
        city = value['long_name'];
        setTimeout(function(){
          $.ajax({
            url: '/get-city-by-state',
            type: "POST",
            data: {state_id: $('[name="state"]').val(), 'city_name': city, _token: '{{ csrf_token() }}'},
            success: function (data) {

              //console.log(data);
                var resp = $.map(data,function(obj){
                //console.log(resp);
                if(obj.city_name == city){
                  //console.log(obj.id);
                  $('[name="city"]').val(obj.id);
                  $('[name="city_name"]').val(obj.city_name);
                }
              });
            }
          });
        }, 2000);
      }


      if(value['types'][0]=='administrative_area_level_1'){
        //console.log(value['long_name']);
        state = value['long_name'];
        //$('[name="state"] option:contains(' + $.trim(state) + ')').attr('selected', 'selected').change();

        setTimeout(function(){
          $.ajax({
            url: '/get-state-by-country',
            type: "POST",
            data: {country_id: $('[name="country"]').val(), _token: '{{ csrf_token() }}'},
            success: function (data) {
                var resp = $.map(data,function(obj){
                //console.log(resp);
                if(obj.state_name == state){
                  //console.log(obj.id);
                  $('[name="state"]').val(obj.id);
                  $('[name="state_name"]').val(obj.state_name);
                }
              });
            }
          });
        }, 1000);

      }

      if(value['types'][0]=='postal_code'){
        zip = (value['long_name'].length && value['long_name'].charAt(0) == '0') ? value['long_name'].slice(1) : value['long_name'];

        setTimeout(function(){
          $.ajax({
            url: '/get_zip_by_city',
            type: "POST",
            data: {country_id: $('[name="country"]').val(), state_id: $('[name="state_name"]').val(), city_id: $('[name="city_name"]').val(), _token: '{{ csrf_token() }}'},
            success: function (data) {
              //console.log(data);
                var resp = $.map(data,function(obj){
                if(obj.zip == zip){
                  //console.log(obj.id);
                  $('[name="zipcode"]').val(obj.zip);
                }
              });
            }
          });
        }, 2500);

      }
    });

    //console.log( city );

    input_street_number = street_number.length ? street_number + ' ' : '';
    input_street = street.length ? street + ', ' : '';
    input_city = city.length ? city + ', ' : '';

    $('#refAdress').val(input_street_number + input_street + input_city + state);

  });

}
</script>
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyAodEhDsLe3d03Rca427tRdlDTPM6g0fyk&libraries=places&callback=initMap&language=en&region=US" async defer></script>

<script>
var states_array = [];
var cities_array = [];
var zips_array = [];
var stateid='';
var cityid='';
var zipid='';
var countryid='';

@if(request()->segment(1) === 'new-post')
  countryid={{ isset($current_country) && $current_country == 'United States' ? '231' : '38' }};
@else
  countryid = $('[name="country"]').val();
@endif

$( function() {

  var auto_countries = [
    "United States",
    "Canada"
  ];

  $('[name="country_name"]').autocomplete({
    source: auto_countries,
    select: function( event, ui ) {
      //console.log(ui);
      if(ui.item.value=='United States'){
        countryid = '231';
      }else{
        countryid = '38';
      }

      $('[name="country"]').val(countryid);
      $('[name="state_name"]').val('');
      $('[name="state"]').val('');
      $('[name="city_name"]').val('');
      $('[name="city"]').val('');
      $('[name="zipcode"]').val('');
    }
  });

  $('[name="state_name"]').autocomplete({
    source: function(request, response) {
      $.ajax({
        url: '/get-states-for-autocomplete',
        type: "POST",
        data: {state_name: request.term, country_id: countryid, _token: '{{ csrf_token() }}'},
        success: function (data) {
          //console.log(data);
          var resp = $.map(data,function(obj){
            //console.log(resp);
            return obj.state_name;
          });

          var states = $.map(data,function(obj){
            return obj;
          });
          states_array = [];
          states_array.push(states);
          response(resp);
          //return resp;
        }
      });
    },
    minLength: 1,
    select: function( event, ui ) {
      //console.log(ui.item.value);
      $.each( states_array, function( id, value ) {
        $.each( value, function( i, v ) {
          if(ui.item.value == v['state_name']){
            stateid = v['state_name'];
            $('[name="state"]').val(v['id']);
            $('[name="city_name"]').val('');
            $('[name="city"]').val('');
            $('[name="zipcode"]').val('');
          }
        });
      });
    }
  });

  $('[name="city_name"]').autocomplete({
    source: function(request, response) {
      $.ajax({
        url: '/get-cities-for-autocomplete',
        type: "POST",
        data: {city_name: request.term, state_id:  $("#state_id").val(), _token: '{{ csrf_token() }}'},
        success: function (data) {
          //console.log($('#state_id').val());
          var resp = $.map(data,function(obj){
            //console.log(resp);
            return obj.city_name;
          });

          var cities = $.map(data,function(obj){
            return obj;
          });
          cities_array = [];
          cities_array.push(cities);

          response(resp);
          //return resp;
        }
      });
    },
    minLength: 1,
    select: function( event, ui ) {

      $.each( cities_array, function( id, value ) {
        $.each( value, function( i, v ) {
          if(ui.item.value == v['city_name']){
            cityid = v['city_name'];
            $('[name="city"]').val(v['id']);
            $('[name="zipcode"]').val('');
          }
        });
      });
    }
  });


  $('[name="zipcode"]').autocomplete({
    source: function(request, response) {
      $.ajax({
        url: '/get-zips-for-autocomplete',
        type: "POST",
        data: {zip_name: request.term, country: $('[name="country"]').val(), state: $('[name="state_name"]').val(), city: $('[name="city_name"]').val(), _token: '{{ csrf_token() }}'},
        success: function (data) {
          //console.log(data);
          var resp = $.map(data,function(obj){
            //console.log(resp);
            return obj.zip;
          });

          var zips = $.map(data,function(obj){
            return obj;
          });

          zips_array = [];
          zips_array.push(zips);

          response(resp);
          //return resp;
        }
      });
    },
    minLength: 1,
    select: function( event, ui ) {
      //console.log(states_array);
      $.each( zips_array, function( id, value ) {
        $.each( value, function( i, v ) {
          //console.log(v['id']);
          if(ui.item.value == v['zip']){
            $('[name="zipcode"]').val(v['zip']);
          }
        });
      });
    }
  });
  //end here
});
</script>
