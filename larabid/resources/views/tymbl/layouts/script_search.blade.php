<script>

    function generate_option_from_json(jsonData, fromLoad) {
        //Load Category Json Data To Brand Select
        if (fromLoad === 'country_to_state') {
            var option = '';
            if (jsonData.length > 0) {
                option += '<option value="0" selected> @lang('app.select_state') </option>';
                for (i in jsonData) {
                    option += '<option value="' + jsonData[i].id + '"> ' + jsonData[i].state_name + ' </option>';
                }
                $('#state_select').html(option);
                $('#state_select').select2();
            } else {
                $('#state_select').html('');
                $('#state_select').select2();
            }
            $('#state_loader').hide('slow');

        } else if (fromLoad === 'state_to_city') {

            var option = '';
            if (jsonData.length > 0) {

                option += '<option value="0" selected> @lang('app.select_city') </option>';
                for (i in jsonData) {
                    option += '<option value="' + jsonData[i].id + '"> ' + jsonData[i].city_name + ' </option>';
                }

                return option;

                //$('#city').append(option);

                //$('#city_select').select2();
            } else {
                $('#city').html('');
                //$('#city_select').select2();
            }
        }
    }



    $(document).on('click', '#state', function () {
      var state_id = $(this).val();
      var next_select = $(this).next('select');
      //$('#city_loader').show();
      $.ajax({
          type: 'POST',
          url: '{{ route('get_city_by_state') }}',
          data: {state_id: state_id, _token: '{{ csrf_token() }}'},
          success: function (data) {
              //console.log(data);
              next_select.empty();
              var option = generate_option_from_json(data, 'state_to_city');
              next_select.append(option);
          }
      });
    });

    $(document).on('click', '#toggler-distance', function () {
      $(this).parent('div').next('div').show('slow');
      $(this).parent('div').parent('li').next('li').find('.slidecontainer-location').hide('slow');
    });

    $(document).on('click', '#toggler-location', function () {
      $(this).parent('div').next('div').show('slow');
      $(this).parent('div').parent('li').prev('li').find('.slidecontainer-distance').hide('slow');
    });

</script>

<script>
$(document).on('change', '#myRange', function() {
  // Does some stuff and logs the event to the console
  $(this).parent('div').find('#distance-value').html($(this).val()+' mile/s');
});
</script>
