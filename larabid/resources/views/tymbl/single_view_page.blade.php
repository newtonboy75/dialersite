@extends('tymbl.layouts.app')
@section('content')
<main class="main-content">
  <div class="container mx-auto">

    <div class="row row-header section-row">
      <!-- left -->
      <div class="col-sm-8 section-right">
        <div class="lead-type-head">{{$ad->cat_type_status}} LEAD</div>
        <h1 class="view-item-title">{{ safe_output($ad->title) }}</h1>
      </div>

      <!-- right -->
      <div class="col-sm-4">
        <div class="text-right">
          <a href="javascript:;" class="mini-btn" id="save_as_favorite" data-slug="{{ $ad->slug }}">
            @if($ad->is_my_favorite())
            <i rel="2" class="fas fa-heart"></i> remove from favorite
            @else
            <i rel="1" class="fas fa-heart-o"></i> add to favorite
            @endif
          </a>
          <a href="javascript: window.print();" class="mini-btn"><i class="fas fa-print"></i></a>
        </div>
      </div>
    </div>
    <!-- row1 -->

    <!-- row2 -->
    <div class="row section-row">
      <!-- info -->
      <div class="col-sm-8">

        <i class="fas fa-map-marker-alt"></i>&nbsp;@if($ad->city_id != '0' && isset($ad->city_id))
        <a class="location" href="{{ route('search', [$ad->country->country_code, 'city' => 'city-'.$ad->city->id]) }}">{{ isset($ad->city->city_name) ? $ad->city->city_name : '' }}</a>,
        @endif
        <a class="location" href="{{ route('search', [$ad->country->country_code, 'state' => 'state-'.$ad->state->id]) }}">{{ $ad->state->state_name }}</a> @php $features = unserialize($ad->feature_1); @endphp
        <br><span class="sqft">
          <i class="fas fa-expand-arrows-alt"></i>&nbsp;&nbsp;&nbsp;&nbsp;{{isset($features[2]) && !empty($features[2])  ? $features[2] : '0'}} sq ft</span>
          <!--features -->
          <p>&nbsp;</p>
          <!-- features -->
          <div class="row specs-section">
            <div class="col-sm-6">
              <table>
                <tr><td>Type</td><td class="text-right font-weight-bold">

                  @if($ad->category_type == 'buying')
                    Prospect looking to buy (PLB)
                  @elseif($ad->category_type == 'selling')
                    Prospect looking to sell (PLS)
                  @else
                    @if($ad->cat_type_status == 'qualified')
                    Qualified
                    @else
                    Unqualified
                    @endif

                  @endif</td></tr>
                  <tr><td>Posted on</td><td class="text-right font-weight-bold">{{ $ad->created_at->diffForHumans() }}</td></tr>
                  <tr><td>Desired Timeframe</td><td class="text-right font-weight-bold">3-6 months</td></tr>
                  <tr><td>Date of last contact</td><td class="text-right font-weight-bold">@if(isset($ad->date_contacted_qualified))
                    @php $c_year = date('Y', strtotime($ad->date_contacted_qualified)); @endphp
                    @if($c_year !== '1970')
                    {{ date('M d, Y', strtotime($ad->date_contacted_qualified)) }}
                    @endif
                    @endif</td></tr>
                  </table>
                </div>
                <div class="col-sm-6">
                  <table>
                    <tr><td>Price Range</td><td class="text-right font-weight-bold">{{$ad->price_range}}</td></tr>

                    <tr><td>City</td><td class="text-right font-weight-bold">@if($ad->city_id != '0')
                      {{ isset($ad->city->city_name) ? $ad->city->city_name : ''}}
                      @endif</td></tr>
                      <tr><td>State</td><td class="text-right font-weight-bold">{{ $ad->state->state_name }}</td></tr>
                      <tr><td>Address</td><td class="text-right font-weight-bold">*****</td></tr>
                    </table>
                  </div>
                </div>

                <p>&nbsp;</p>

                <div class="row list-featured">

                  <ul class="feature-list-2">
                    <li><i class="fa fa-bed" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;bedroom &nbsp;{{isset($features[0]) && !empty($features[0]) ? ': '.$features[0] : ''}}</li>
                    <li><i class="fa fa-bath" aria-hidden="true"></i>&nbsp;&nbsp;bath&nbsp;{{isset($features[1]) && !empty($features[1])  ? ': '.$features[1] : ''}}</li>

                    <li><i class="fa fa-car" aria-hidden="true"></i>&nbsp;&nbsp;garage&nbsp;{{isset($features[3]) && !empty($features[3])  ? ': '.$features[3] : ''}}</li>
                    <li><i class="fa fa-columns" aria-hidden="true"></i>&nbsp;&nbsp;kitchen&nbsp;{{isset($features[4]) && !empty($features[4])  ? ': '.$features[4] : ''}}</li>
                    <li><i class="far fa-clone"></i>&nbsp;&nbsp;balconies&nbsp;{{isset($features[5]) && !empty($features[5])  ? ': '.$features[5] : ''}} </li>
                  </ul>
                </div>


                <div class="description">
                  <h4 class="section-header-title">Description</h4>
                  {{ html_entity_decode(strip_tags(safe_output($ad->description)), ENT_QUOTES) }}
                </div>

                <!--user mobile -->
                <div class="user-section-sm">
                  <div class="media">
                    @php
                    $ad_poster = \App\User::where('id', '=', $ad->user_id)->first();

                    @endphp
                    <img class="mr-3" src="{{ $ad_poster ? $ad_poster->get_gravatar() : '/uploads/avatar/gravatar-default.png' }}" alt="{{ $ad_poster ? $ad_poster->name : '' }}">
                    @if($poster_broker)
                    @if($poster_broker->broker_verified == '1')
                    <div style="position: absolute; margin-left: 64px; margin-top: 60px;" title="Verified"><i class="text-primary fas fa-check-circle" style="font-size: 13px; color: #d50000;"></i></div>
                    @endif
                    @endif
                    <div class="media-body">
                      {{ $ad->user ? substr($ad->user->name, 0, 2) : '' }}<span class="conceal"><i class="fas fa-asterisk"></i> <i class="fas fa-asterisk"></i> <i class="fas fa-asterisk"></i> <i class="fas fa-asterisk"></i> <i class="fas fa-asterisk"></i> <i class="fas fa-asterisk"></i> <i class="fas fa-asterisk"></i> <i class="fas fa-asterisk"></i> <i class="fas fa-asterisk"></i></span>{{ $ad->user ? substr($ad->user->name, -3, 4) : '' }}


                      <div class="stars">
                        <?php
                        $wholenum1 =  array(1, 2);
                        $halfnum = array(3, 4, 5, 6);
                        $wholenum2 =  array(7, 8, 9);
                        ?>

                        <div class="user_rating" title="{{$final_user_rating}} user rating">
                          <?php
                          $wholenum1 =  array(1, 2);
                          $halfnum = array(3, 4, 5, 6);
                          $wholenum2 =  array(7, 8, 9);
                          ?>

                          @if((int)$final_user_rating >= 1)
                          <span class="fas fa-star full"></span>
                          @else
                          @if(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum1) && (int)$final_user_rating == 0)
                          <i class="fas fa-star"></i>
                          @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum2) && (int)$final_user_rating == 0)
                          <span class="fas fa-star full"></span>
                          @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $halfnum) && (int)$final_user_rating == 0)
                          <span class="fas fa-star half"></span>
                          @else
                          <span class="fas fa-star"></span>
                          @endif
                          @endif

                          @if((int)$final_user_rating >= 2)
                          <span class="fas fa-star full"></span>
                          @else
                          @if(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum1) && (int)$final_user_rating == 1)
                          <span class="fas fa-star"></span>
                          @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum2) && (int)$final_user_rating == 1)
                          <span class="fas fa-star full"></span>
                          @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $halfnum) && (int)$final_user_rating == 1)
                          <span class="fas fa-star half"></span>
                          @else
                          <span class="fas fa-star"></span>
                          @endif
                          @endif

                          @if((int)$final_user_rating >= 3)
                          <span class="fas fa-star full"></span>
                          @else
                          @if(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum1) && (int)$final_user_rating == 2)
                          <span class="fas fa-star"></span>
                          @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum2) && (int)$final_user_rating == 2)
                          <span class="fas fa-star full"></span>
                          @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $halfnum) && (int)$final_user_rating == 2)
                          <span class="fas fa-star half"></span>
                          @else
                          <span class="fas fa-star"></span>
                          @endif
                          @endif

                          @if((int)$final_user_rating >= 4)
                          <span class="fas fa-star full"></span>
                          @else
                          @if(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum1) && (int)$final_user_rating == 3)
                          <span class="fas fa-star"></span>
                          @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum2) && (int)$final_user_rating == 3)
                          <span class="fas fa-star full"></span>
                          @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $halfnum) && (int)$final_user_rating == 3)
                          <span class="fas fa-star half"></span>
                          @else
                          <span class="fas fa-star"></span>
                          @endif
                          @endif

                          @if((int)$final_user_rating >= 5)
                          <span class="fas fa-star full"></span>
                          @else
                          @if(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum1) && (int)$final_user_rating == 4)
                          <span class="fas fa-star"></span>
                          @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum2) && (int)$final_user_rating == 4)
                          <span class="fas fa-star full"></span>
                          @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $halfnum) && (int)$final_user_rating == 4)
                          <span class="fas fa-star half"></span>
                          @else
                          <span class="fas fa-star"></span>
                          @endif
                          @endif
                        </div>
                      </div>
                      <a href="/leads-by-user/{{$ad->user ? $ad->user->id : ''}}">view leads from this seller</a>
                    </div>
                  </div>
                </div> <!--/.user-info-->
                <!-- -->

                <div class="features-list-2">
                  <h4 class="section-header-title">General Amenities</h4>
                  @php $features2 = unserialize($ad->feature_2); @endphp
                  <ul class="feature-list clearfix">

                    <li><i class="fas fa-check-circle {{isset($features2[2]) ? '': 'grey-f'}}"></i>Garden</li>
                    <li><i class="fas fa-check-circle {{isset($features2[0]) ? '': 'grey-f'}}"></i>24hr Security</li>
                    <li><i class="fas fa-check-circle {{isset($features2[1]) ? '': 'grey-f'}}"></i>Laundry Services</li>
                    <li><i class="fas fa-check-circle {{isset($features2[3]) ? '': 'grey-f'}}"></i>Swimming Pool</li>
                    <li><i class="fas fa-check-circle {{isset($features2[4]) ? '': 'grey-f'}}"></i>Tennis Court</li>
                    <li><i class="fas fa-check-circle {{isset($features2[5]) ? '': 'grey-f'}}"></i>Close to local Schools</li>
                    <li><i class="fas fa-check-circle {{isset($features2[6]) ? '': 'grey-f'}}"></i>Pet friendly</li>
                    <li><i class="fas fa-check-circle {{isset($features2[7]) ? '': 'grey-f'}}"></i>Lift</li>
                    <li><i class="fas fa-check-circle {{isset($features2[8]) ? '': 'grey-f'}}"></i>Access controlled</li>
                  </ul>
                </div>
                <div class="comment-section single-section">
                  <h4 class="section-header-title">Comments</h4>
                  <div class="comment-form">

                    {{ Form::open(['route'=> ['post_comments', $ad->id], 'class' => 'form-horizontal']) }}
                    <div class="form-row">
                      @if( ! auth()->check())
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="name author_name" value="@if(auth()->check() ) {{auth()->user()->name}}@else{{old('author_name')}}@endif" name="author_name" placeholder="@lang('app.author_name')">
                        {{ $errors->has('author_name')? '<p class="help-block">'.$errors->first('author_name').'</p>':'' }}
                      </div>
                      <div class="col-sm-6">
                        <input type="email" class="form-control" id="email author_email" value="@if(auth()->check() ) {{auth()->user()->email}}@else{{old('author_email')}}@endif" name="author_email" placeholder="@lang('app.author_email')">
                        {{ $errors->has('author_email')? '<p class="help-block">'.$errors->first('author_email').'</p>':'' }}
                        <small id="email" class="form-text text-muted">
                          Your email is secure with us and never show publicly.
                        </small>
                      </div>
                      @endif
                      <div class="col-sm-12">
                        <textarea name="comment" id="comment-area" cols="30" rows="3" class="form-control" style="min-width: 100%" placeholder="Write your comment here..." required></textarea>
                      </div>
                    </div>
                    <input type="submit" class="btn btn-primary clearfix" value="POST COMMENT">
                    <br><br>
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif
                    {{ Form::close() }}
                  </div> <!--/.comment-form-->

                  @if(get_option('enable_comments') == 1)

                  @php $comments = \App\Comment::approved()->parent()->whereAdId($ad->id)->with('childs_approved')->orderBy('id', 'desc')->get();
                  $comments_count = \App\Comment::approved()->whereAdId($ad->id)->count();
                  @endphp

                  @if($comments_count < 1)
                  <h5>@lang('app.no_comment_found')</h5>
                  @elseif($comments_count == 1)
                  <h5>{{$comments_count}} @lang('app.comment_found')</h5>
                  @elseif($comments_count > 1)
                  <h5>{{$comments_count}} @lang('app.comments_found')</h5>
                  @endif
                  @endif

                  @if($comments->count())

                  @foreach($comments as $comment)
                  <div class="media other-comments">

                    @if($comment->user_id)
                    <img class="mr-3" src="{{$comment->author->get_gravatar() !== null ? $comment->author->get_gravatar() : 'none'}}" alt="{{$comment->author_name}}">
                    @else
                    <img class="mr-3" src="{{avatar_by_email($comment->author_email)}}" alt="{{$comment->author_name}}">
                    @endif

                    <div class="media-body">
                      <a href="#"><h4 class="section-header-title">{{$comment->author_name}}</h4></a>
                      <small>{{$comment->created_at->diffForHumans()}}</small>
                      <i class="fa fa-reply"></i>
                      <br><br>
                      {{ safe_output(nl2br($comment->comment)) }}
                    </div>
                  </div>

                  @if($comment->childs_approved)
                  @foreach($comment->childs_approved as $childComment)
                  <!-- Respuestas de los comentarios -->
                  <ul class="comments-list reply-list">
                    <li id="comment-{{$childComment->id}}">
                      <!-- Avatar -->
                      <div class="comment-avatar">
                        @if($childComment->user_id)
                        <img src="{{$childComment->author->get_gravatar()}}" alt="{{$childComment->author_name}}">
                        @else
                        <img src="{{avatar_by_email($childComment->author_email)}}" alt="{{$childComment->author_name}}">
                        @endif
                      </div>
                      <!-- Contenedor del Comentario -->
                      <div class="comment-box" data-comment-id="{{$comment->id}}">
                        <div class="comment-head">
                          <h6 class="comment-name by-author">{{$childComment->author_name}}</h6>
                          <span>{{$childComment->created_at->diffForHumans()}}</span>
                          <i class="fa fa-reply"></i>
                        </div>
                        <div class="comment-content">
                          {{ safe_output(nl2br($childComment->comment)) }}
                        </div>
                        <div class="reply_form_box" style="display: none;"></div>
                      </div>
                    </li>

                  </ul>

                  @endforeach
                  @endif
                  @endforeach
                  @else

                  @endif

                </div>

                <div class="row section-row">
                  <div class="social">
                    <h4 class="section-header-title">Share this lead</h4><br>
                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(Request::fullUrl()) }}"><i class="fab fa-facebook-f" data-toggle="tooltip" data-placement="top" title="Facebook">
                    </i></a>
                    <a href="https://twitter.com/intent/tweet?url={{ urlencode(Request::fullUrl()) }}"><i class="fab fa-twitter" data-toggle="tooltip" data-placement="top" title="Twitter"></i></a>
                    <a href="#"><i class="fab fa-instagram" data-toggle="tooltip" data-placement="top" title="Instagram"></i></a>
                    <a href="https://plus.google.com/share?url={{ urlencode(Request::fullUrl()) }}"><i class="fab fa-google-plus-g" data-toggle="tooltip" data-placement="top" title="Google plus"></i></a>
                    <a href="http://www.linkedin.com/shareArticle?mini=true&url={{ urlencode(Request::fullUrl()) }}&source=tymbl.com"><i class="fab fa-linkedin-in" data-toggle="tooltip" data-placement="top" title="Linkedin"></i></a>
                  </div>

                </div>

              </div>

              <!-- button action -->
              <div class="col-sm-4">
                <div class="btn-action-floating">
                  <div class="referral-btn-section">
                    <div class="text-center" id="referral_fee">you pay <strong id='text-percent'>
                      @if((float)$ad->referral_fee > 0.00)
                        {{ (float)($ad->referral_fee*100) }}%
                      <div style="font-size: 16px;margin-right:16px;"></div>
                    @endif
                    </strong><p>referral fee</p></div>
                    <div class="text-center img-holder">

                      @if($ad->category_type == "selling")
                      <img class="image-lead-type" src="{{ asset('assets/img/tymbl/seller-referral.png') }}" alt="Excellent Value" class="center-block">
                      @elseif($ad->category_type == "buying")
                      <img class="image-lead-type" src="{{ asset('assets/img/tymbl/buyer-referral.png') }}" alt="Excellent Value" class="center-block">
                      @else
                        @if($ad->cat_type_status == 'qualified')
                        <img class="image-lead-type" src="{{ asset('assets/img/tymbl/icon_qualified.png') }}" alt="Excellent Value" class="center-block">
                        @else
                        <img class="image-lead-type" src="{{ asset('assets/img/tymbl/icon_unqualified.png') }}" alt="Excellent Value" class="center-block">
                        @endif
                      @endif


                    </div>

                    @php
                    $date_signed = array();
                    $list_status = 'No';
                    $relisted = 'No';
                    if($agreement_signed){
                      $date_signed = explode(' ', $agreement_signed->created_at);
                      $list_status = $agreement_signed->list_status == '0' ? 'No' : 'Yes';
                    }

                    $relisted_ad = \App\ListingContracts::where('listing_id', '=', $ad->id)->first();

                    if($relisted_ad){
                      if($relisted_ad->list_status == '3'){
                        $relisted = 'Yes';
                      }
                    }

                    @endphp

                    @if($ad->category_type == 'selling' || $ad->category_type == 'buying' || $ad->category_type == 'other')

                    @if($ad->status == '1' || $ad->status == '7')
                    @if(round($ad->escrow_amount) == 0)

                    <form action="{{route('no-payment')}}" method="GET">
                      <input type="hidden" name="ref_id" value="{{$ad->id}}">
                      <button class="btn btn-lg btn-primary btn-sign" style="width: 100%;">
                        <img src="/assets/img/tymbl/button-reserve.png" style="width: 100%; height: 100%;">
                      </button>
                    </form>

                    @else
                    <form>
                      <button disabled class="btn btn-lg btn-primary btn-block">RESERVED</button>
                    </form>
                    @endif
                    @elseif($ad->status == '5' || $ad->status == '8')
                    <form>
                      <button disabled class="btn btn-lg btn-secondary btn-block" id="btn-reserved">RESERVED</button>
                      @if (Auth::check())
                      <div style="display: inline-block; width: 100%; text-align:center; margin-top: 10px;">
                        <a href="javascript:;" title="Receive notification when this lead becomes available" class="mini-btn-2 save-lead {{ $ad_watching ? 'ad-watching' : '' }}" id="save-lead">
                          @if($ad_watching)
                          <i class="far fa-eye"></i>&nbsp;&nbsp;watching
                          @else
                          <i class="far fa-bell"></i>&nbsp;&nbsp;notify me of similar leads
                          @endif
                        </a>
                        @else
                        <a href="{{ route('login') }}" title="Receive notification when this lead becomes available" class="mini-btn save-lead"><i class="far fa-bell"></i>&nbsp;&nbsp;notify me of similar leads</a>
                        @endif
                      </div>
                      @endif

                    </form>
                    @endif

                  </div>
                  <div class="div-info">
                    <div class="tbl-cell">
                      <div class="icon-info"><i class="fas fa-info-circle"></i> </div>
                      <div class="incon-info-text">The Referral fee is the % of the commission you require to refer your client. The % you set will be reflected in the Referral agreement.</div>
                    </div>
                  </div>
                  <div class="user-section">
                    <div class="media">
                      @php
                      $ad_poster = \App\User::where('id', '=', $ad->user_id)->first();

                      @endphp
                      <img class="mr-3" src="{{ $ad_poster ? $ad_poster->get_gravatar() : '/uploads/avatar/gravatar-default.png' }}" alt="{{ $ad_poster ? $ad_poster->name : '' }}">
                      @if($poster_broker)
                      @if($poster_broker->broker_verified == '1')
                      <div style="position: absolute; margin-left: 64px; margin-top: 60px;" title="Verified"><i class="text-primary fas fa-check-circle" style="font-size: 13px; color: #d50000;"></i></div>
                      @endif
                      @endif
                      <div class="media-body">
                        {{ $ad->user ? substr($ad->user->name, 0, 2) : '' }}<span class="conceal"><i class="fas fa-asterisk"></i> <i class="fas fa-asterisk"></i> <i class="fas fa-asterisk"></i> <i class="fas fa-asterisk"></i> <i class="fas fa-asterisk"></i> <i class="fas fa-asterisk"></i> <i class="fas fa-asterisk"></i> <i class="fas fa-asterisk"></i> <i class="fas fa-asterisk"></i></span>{{ $ad->user ? substr($ad->user->name, -3, 4) : '' }}


                        <div class="stars">
                          <?php
                          $wholenum1 =  array(1, 2);
                          $halfnum = array(3, 4, 5, 6);
                          $wholenum2 =  array(7, 8, 9);
                          ?>

                          <div class="user_rating" title="{{$final_user_rating}} user rating">
                            <?php
                            $wholenum1 =  array(1, 2);
                            $halfnum = array(3, 4, 5, 6);
                            $wholenum2 =  array(7, 8, 9);
                            ?>

                            @if((int)$final_user_rating >= 1)
                            <span class="fas fa-star full"></span>
                            @else
                            @if(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum1) && (int)$final_user_rating == 0)
                            <i class="fas fa-star"></i>
                            @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum2) && (int)$final_user_rating == 0)
                            <span class="fas fa-star full"></span>
                            @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $halfnum) && (int)$final_user_rating == 0)
                            <span class="fas fa-star half"></span>
                            @else
                            <span class="fas fa-star"></span>
                            @endif
                            @endif

                            @if((int)$final_user_rating >= 2)
                            <span class="fas fa-star full"></span>
                            @else
                            @if(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum1) && (int)$final_user_rating == 1)
                            <span class="fas fa-star"></span>
                            @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum2) && (int)$final_user_rating == 1)
                            <span class="fas fa-star full"></span>
                            @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $halfnum) && (int)$final_user_rating == 1)
                            <span class="fas fa-star half"></span>
                            @else
                            <span class="fas fa-star"></span>
                            @endif
                            @endif

                            @if((int)$final_user_rating >= 3)
                            <span class="fas fa-star full"></span>
                            @else
                            @if(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum1) && (int)$final_user_rating == 2)
                            <span class="fas fa-star"></span>
                            @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum2) && (int)$final_user_rating == 2)
                            <span class="fas fa-star full"></span>
                            @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $halfnum) && (int)$final_user_rating == 2)
                            <span class="fas fa-star half"></span>
                            @else
                            <span class="fas fa-star"></span>
                            @endif
                            @endif

                            @if((int)$final_user_rating >= 4)
                            <span class="fas fa-star full"></span>
                            @else
                            @if(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum1) && (int)$final_user_rating == 3)
                            <span class="fas fa-star"></span>
                            @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum2) && (int)$final_user_rating == 3)
                            <span class="fas fa-star full"></span>
                            @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $halfnum) && (int)$final_user_rating == 3)
                            <span class="fas fa-star half"></span>
                            @else
                            <span class="fas fa-star"></span>
                            @endif
                            @endif

                            @if((int)$final_user_rating >= 5)
                            <span class="fas fa-star full"></span>
                            @else
                            @if(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum1) && (int)$final_user_rating == 4)
                            <span class="fas fa-star"></span>
                            @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $wholenum2) && (int)$final_user_rating == 4)
                            <span class="fas fa-star full"></span>
                            @elseif(in_array(explode('.', number_format($final_user_rating, 1))[1], $halfnum) && (int)$final_user_rating == 4)
                            <span class="fas fa-star half"></span>
                            @else
                            <span class="fas fa-star"></span>
                            @endif
                            @endif
                          </div>
                        </div>
                        <a href="/leads-by-user/{{$ad->user ? $ad->user->id : ''}}">view leads from this seller</a>
                      </div>
                    </div>
                  </div> <!--/.user-info-->
                </div>

              </div>

            </div>
          </div>
          <!-- row2 -->


          <div class="btn-action-floating-resp">

                  you pay <strong id='text-percent'>
                    @if($ad->get_ad_status() == 'lead-vip-free-active')
                      {{ (float)($ad->referral_fee*100)-10 }}%
                    @else
                      {{ (float)($ad->referral_fee*100) }}%
                    @endif<div style="font-size: 16px;margin-right:16px;"></div>
                  </strong><p>referral fee</p>



                  @php
                  $date_signed = array();
                  $list_status = 'No';
                  $relisted = 'No';
                  if($agreement_signed){
                    $date_signed = explode(' ', $agreement_signed->created_at);
                    $list_status = $agreement_signed->list_status == '0' ? 'No' : 'Yes';
                  }

                  $relisted_ad = \App\ListingContracts::where('listing_id', '=', $ad->id)->first();

                  if($relisted_ad){
                    if($relisted_ad->list_status == '3'){
                      $relisted = 'Yes';
                    }
                  }

                  @endphp

                  @if($ad->category_type == 'selling' || $ad->category_type == 'buying' || $ad->category_type == 'other')

                  @if($ad->status == '1' || $ad->status == '7')
                  @if(round($ad->escrow_amount) == 0)
                  <form action="{{route('no-payment')}}" method="GET">
                    <input type="hidden" name="ref_id" value="{{$ad->id}}">
                    <button class="btn btn-lg btn-primary">Sign a Referral Agreement</button>
                  </form>

                  @else
                  <form>
                    <button disabled class="btn btn-lg btn-primary">RESERVED</button>
                  </form>
                  @endif
                  @elseif($ad->status == '5' || $ad->status == '8')
                  <form>
                    <button disabled class="btn btn-lg btn-primary">RESERVED</button>
                    @if (Auth::check())
                    <div>
                      <a href="javascript:;" title="Receive notification when this lead becomes available" class="mini-btn-0 save-lead {{ $ad_watching ? 'ad-watching' : '' }}" id="save-lead-0">
                        @if($ad_watching)
                        <i class="far fa-eye"></i>&nbsp;&nbsp;watching
                        @else
                        <i class="far fa-bell"></i>&nbsp;&nbsp;notify me of similar leads
                        @endif
                      </a>
                      @else
                      <a href="{{ route('login') }}" title="Receive notification when this lead becomes available" class="mini-btn save-lead"><i class="far fa-bell"></i>&nbsp;&nbsp;notify me of similar leads</a>
                      @endif
                    </div>
                    @endif

                  </form>
                  @endif



          </div>



        </div> <!--/.container-->
      </main> <!--/.main-content-->




      @endsection
