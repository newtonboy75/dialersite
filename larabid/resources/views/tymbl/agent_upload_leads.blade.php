@extends('tymbl.layouts.app')
@section('content')
<section class="page-header">
  <h1>Upload Leads File</h1>
</section>
<main class="post-flow">
  <div class="container">
    <div class="row justify-content-md-center">
      <div style="padding-bottom: 100px; display: inline-block; width: 90%;">
        @if($records)
        <div class="bg-info text-white p-2">*Thank you! File uploaded successfully.</div>
        <br><br>

        <table>
          <tr>

            <td><p>&nbsp;</p><p>&nbsp;</p><a href="{{ route('import-reset') }}" class="btn btn-primary" id="btn-reset">Back</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="font-weight-bold err">&nbsp;</span></td>
          </tr>
        </table>
        @else
        <div  class="section-agent-upload">
          <div class="m-section__content">
            @if(Session::has('success'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('success') }}</p>
            @endif
            <p>&nbsp;</p>
            <small class="text-secondary">&nbsp;&nbsp;Upload your .csv, .xls or .xlsx here</small>
            <form class="form-horizontal" method="POST" action="{{ route('import-csv') }}" enctype="multipart/form-data">
              {{ csrf_field() }}
              <input id="csv_file" type="file" class="form-control" name="csv_file" required>
              @if ($errors->has('csv_file'))
              <span class="help-block">
                <strong>{{ $errors->first('csv_file') }}</strong>
              </span>
              @endif
              <p>&nbsp;</p>
              <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                  <button type="submit" class="btn btn-primary">
                    Upload File
                  </button>
                </div>
              </div>
            </form>

          </div>
        </div>
        @endif
      </div>
    </div>
  </div>
</main>
@endsection
