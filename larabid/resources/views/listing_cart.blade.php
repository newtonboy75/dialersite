@extends('layouts.app')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div style="margin-top:30px;">
                    <div class="pull-left">
                      <div style="max-width: 300px; min-height: 300px;">
                        <div style="float:left; font-weight: 400; font-size: 1.2em;">{{$validList->title}}</div><br><br>
                          @if($image != '')
                            <img style="width: 600px" src="{{url('/uploads/images/'.$listing_image->media_name)}}" /><br><br>
                          @else
                            <img style="width: 600px" src="{{url('/assets/img/classified-placeholder.png')}}" /><br><br>
                          @endif
                      </div>
                    </div>
                    <div class="pull-right">
                      <div style="max-width: 500px; min-height: 460px; text-align:left; margin-top: 36px;">
                      <p>The seller escrow amount for this listing to:</p>
                      <p style="font-size:28px; font-weight: 400;">{{ $list_country = $country->id == '231' ? 'USD$' : 'CAD$' }}  {{number_format($validList->escrow_amount, 2, '.', ',')}}</p>
                      <p>After you make a payment, your funds will be held in an escrow until the transaction is completed one of the following ways:</p>

                      <ul style="padding:0px; margin: 16px;">
                        <li>Your title company rep logs in to Tymbl with the code we have sent and lets us know that the property was purchased/sold successfully. We release the funds to the seller.</li>
                        <li>Your title company rep logs in to Tymbl with the code we have sent and lets us know that the property was NOT purchased/sold successfully. We release the funds to back to you.</li>
                      </ul>
                      <p>&nbsp;</p>

                      <form class="w3-container w3-display-middle w3-card-4 " method="POST" id="payment-form"  action="/payment/pay">
                        {{ csrf_field() }}
                        <input type="hidden" name="referral_id" value="{{$id}}">
                        <input type="hidden" name="amount" value="{{$list_country}}  {{number_format($validList->escrow_amount, 2, '.', ',')}}">
                        <button class="btn btn-info">Make a Payment</button></p>
                    </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
