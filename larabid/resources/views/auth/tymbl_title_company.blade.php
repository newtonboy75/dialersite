@extends('tymbl.layouts.app')
@section('content')

<div class="container">
  <div class="row">
          <div class="mx-auto" style="width:50%; padding:30px; width: 50%;">
          <h5>Please enter your code</h5><br>
          {{ Form::open(['url' => 'title-company/validate']) }}
          <input type="hidden" name="id" value="{{ $id }}">
          <input id="title-company-id"  class="form-control text-center" type="text" name="code" value="{{old('code')}}"><br>
          <button id="title-company-submit" type="submit" class="btn btn-lg btn-primary btn-block btn-buy">Submit</button>
          {{ Form::close() }}

          <p style="text-align:left !important; font-size: 12px; padding-top:20px;">This login page is for the title company representatives only. If you want to create an account to buy and sell leads and referrals, please visit the <a href="{{ route('register') }}">registration</a> or <a href="{{ route('login') }}">login</a> page.</p>
        </div>
        <p>&nbsp;</p><p>&nbsp;</p>
</div>
</div>

@endsection
