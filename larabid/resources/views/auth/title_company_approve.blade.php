@extends('layouts.app')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('content')


<div id="post-new-ad">
    <div class="container">
        <div class="row">

            <div style="width:600px; margin-top:40px; margin-bottom: 50px; margin-left:auto; margin-right:auto;">
              <table class="table">
                  <tr>
                    <td id="left-th">Contract ID</td> <td>{{ $valid_contract->contract_id }}</td>
                  </tr>
                  <tr>
                    <td id="left-th">Date Signed</td> <td><?php echo date('Y-m-d h:i:s', strtotime($valid_contract->created_at)); ?></td>
                  </tr>
                  <tr>
                    <td id="left-th">Lead Name</td> <td><a target="_blank" href="/ad/{{$ad->id}}/{{$ad->slug}}">{{ $ad->title }}</a></td>
                  </tr>
                  <tr>
                    <td id="left-th">Address</td> <td>{{ $ad->address }}</td>
                  </tr>
                  <tr>
                    <td id="left-th">State</td> <td>{{ $state }}</td>
                  </tr>
                  <tr>
                    <td id="left-th">City</td> <td>{{ $city }}</td>
                  </tr>
                  <tr>
                    <td id="left-th">List Owner</td> <td>{{ $user->name }}</td>
                  </tr>
                  <tr>

                    <td id="left-th">Contract (PDF)</td> <td>
                      <a href="/download-contract/{{$valid_contract->contract_id}}" rel="nofollow">Download</a>
                    </td>
                  </tr>

              </table>
              <div class="text-center">
                <div id="loader" style="margin-top:20px;margin-bottom: 10px;">&nbsp;</div>
                <button id="btn-sold" data-value="1" class="btn btn-default" data-toggle="sold" data-singleton="true" data-btn-ok-class="btn-success" data-btn-ok-icon-content="check" data-content="Confirm that the referral agreement resulted in a successful sale of the property?" data-btn-ok-label="Confirm" data-btn-cancel-label="Cancel" data-btn-cancel-class="btn-danger" data-title="Property Sold">PROPERTY SOLD</button>

                <button id="btn-cancel" data-value="2" class="btn btn-default" data-toggle="not-sold" data-singleton="true" data-btn-ok-class="btn-success" data-btn-ok-icon-content="check" data-btn-ok-label="Confirm"  data-content="Confirm that the property was not sold and it is OK to release funds if any, held by Tymbl back to all parties involve." data-btn-cancel-label="Cancel" data-btn-cancel-class="btn-danger" data-title="Property Not Sold">PROPERTY NOT SOLD</button>
              </div>
            </div>

      </div>
  </div>
</div>

@endsection
