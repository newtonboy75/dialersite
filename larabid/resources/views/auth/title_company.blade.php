@extends('layouts.app')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('content')



<div id="post-new-ad">
    <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
              <div class="panel panel-default login-form">
                  <div class="panel-heading"><a href="{{ route('login') }}">Login</a> <a class="pull-right reg" href="{{ route('register') }}">Register</a></div>
                  <div class="panel-body">
                      @include('admin.flash_msg')
                      @include('auth.social_login')


                      <div class="text-center">

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <h3>Please enter your code</h3>
                        <br>
                        {{ Form::open(['url' => 'title-company/validate']) }}
                        <input type="hidden" name="id" value="{{ $id }}">
                        <input id="title-company-id"  type="text" name="code" value="{{old('code')}}"><br><br>
                        <button id="title-company-submit" type="submit" class="btn">Submit</button>
                        {{ Form::close() }}

                        <p style="text-align:left !important; font-size: 16px; padding:30px;">This login page is for the title company representatives only. If you want to create an account to buy and sell leads and referrals, please visit the <a href="{{ route('register') }}">registration</a> or <a href="{{ route('login') }}">login</a> page.</p>


                      </div>





                  </div>
                  </div>
              </div>
            </div>












        </div>
      </div>
  </div>
</div>

@endsection
