@extends('tymbl.layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default login-form">
        <div class="panel-body">
          <div class="float-left col-md-1"></div>

          <div class="float-left col-md-5">
            <div style="font-size:1.4em;color:#000;margin-top:20px;"><strong>Login</strong></div><p>&nbsp;</p>
            <form class="form-horizontal"  style="width: 100%;" role="form" method="POST" action="{{ route('login') }}">
              {{ csrf_field() }}
              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="control-label">E-Mail Address</label>

                <div>
                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                  @if ($errors->has('email'))
                  <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="control-label">Password</label>

                <div>
                  <input id="password" type="password" class="form-control" name="password" required>

                  @if ($errors->has('password'))
                  <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
                  @endif
                </div>
              </div>

              @if(get_option('enable_recaptcha_login') == 1)
              <div class="form-group {{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                <div>
                  <div class="g-recaptcha" data-sitekey="{{get_option('recaptcha_site_key')}}"></div>
                  @if ($errors->has('g-recaptcha-response'))
                  <span class="help-block">
                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
              @endif

              <div class="form-group">
                <div>
                  <div class="checkbox">
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div>
                  <button type="submit" class="btn btn-primary" id="submit">
                    Login
                  </button>
                  <a class="btn btn-link" href="{{ route('password.request') }}">Forgot Your Password?</a>
                </div>
              </div>
            </form>
          </div>

          <div class="float-left col-md-1 mid-text">
            <div class="vl">
              <div class="or-text">OR</div>
            </div>
          </div>

          <div class="float-left col-md-5">
            <div id="social_buttons">
              <a href="/login/facebook" class="fb btn">
                <i class="fab fa-facebook-f fa-fw"></i>  Login with Facebook
              </a>
              <a href="/login/twitter" class="twitter btn">
                <i class="fab fa-twitter fa-fw"></i>  Login with Twitter
              </a>
              <a href="/login/google" class="google btn">
                <i class="fab fa-google-plus fa-fw"></i>  Login with Google
              </a>
              <br><br>
              <a href="{{ route('register') }}" class="btn register">
                <i class="fas fa-user-plus"></i>  Register
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<p>&nbsp;</p><p>&nbsp;</p>
@endsection
