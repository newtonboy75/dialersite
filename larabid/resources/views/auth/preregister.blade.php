@extends('layouts.app')

@section('content')
<div class="container">

@include('admin.flash_msg')
  <form method="POST" action="preregister">
  {!! csrf_field() !!}
  <div><input type="text" name="name" value="{{ old('name') }}" />
    {!! $errors->has('name')? '<p class="help-block">'.$errors->first('name').'</p>':'' !!}
  </div>
  <div><input type="email" name="email" value="{{ old('email') }}" />
    {!! $errors->has('email')? '<p class="help-block">'.$errors->first('email').'</p>':'' !!}
  </div>

  <br><br>
  @php $country_usage = get_option('countries_usage'); @endphp
  <legend>@lang('app.location_info')</legend>
  <div class="form-group  {{ $errors->has('country')? 'has-error':'' }}">
      <label class="col-sm-4 control-label">@lang('app.country')</label>
      <div class="col-sm-8">
          <select class="form-control select2" name="country">
              <option value="">@lang('app.select_a_country')</option>
//show only Canada and USA
              @foreach($countries as $country)
                  //@if($country->country_code_number=='124' || $country->country_code_number=='840')
                    <option value="{{ $current_country->id }}" {{ old('country') == $country->id ? 'selected' : '' }} {{ old('country') == '' && $country->country_code_number=='840' ? 'selected' : '' }}>{{ $country->country_name }}</option>
                  //@endif
              @endforeach
          </select>
          {!! $errors->has('country')? '<p class="help-block">'.$errors->first('country').'</p>':'' !!}
      </div>
  </div>
  <div class="form-group  {{ $errors->has('state')? 'has-error':'' }}">
      <label for="state_select" class="col-sm-4 control-label">@lang('app.state')</label>
      <div class="col-sm-8">
          <select class="form-control select2" id="state_select" name="state">
          <option value="">@lang('app.select_a_state')</option>
              //@if($previous_states->count() > 0)
                  @foreach($previous_states as $state)
                      <option value="{{ $state->id }}" {{ old('state') == $state->id ? 'selected' : '' }}>{{ $state->state_name }}</option>
                  @endforeach
              //@endif
          </select>
          <p class="text-info">
              <span id="state_loader" style="display: none;"><i
                          class="fa fa-spin fa-spinner"></i> </span>
          </p>
      </div>
  </div>

  <div class="form-group  {{ $errors->has('city')? 'has-error':'' }}">
      <label for="city_select" class="col-sm-4 control-label">@lang('app.city')</label>
      <div class="col-sm-8">
          <select class="form-control select2" id="city_select" name="city">
          <option value="">@lang('app.select_a_state')</option>
              @if($previous_cities->count() > 0)
                  @foreach($previous_cities as $city)
                      <option value="{{ $city->id }}" {{ old('city') == $city->id ? 'selected':'' }}>{{ $city->city_name }}</option>
                  @endforeach
              @endif
          </select>
          <p class="text-info">
              <span id="city_loader" style="display: none;"><i
                          class="fa fa-spin fa-spinner"></i> </span>
          </p>
      </div>
  </div><br><br>
<input type="submit" name="submit" value="Submit" />
  </form>
  <br><br><br>

</div>
@endsection

@section('page-js')
    <script>

        function generate_option_from_json(jsonData, fromLoad){
            //Load Category Json Data To Brand Select
            if (fromLoad === 'category_to_sub_category'){
                var option = '';
                if (jsonData.length > 0) {
                    option += '<option value="" selected> <?php echo trans('app.select_a_sub_category') ?> </option>';
                    for ( i in jsonData){
                        option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].category_name +' </option>';
                    }
                    $('#sub_category_select').html(option);
                    $('#sub_category_select').select2();
                }else {
                    $('#sub_category_select').html('<option value="">@lang('app.select_a_sub_category')</option>');
                    $('#sub_category_select').select2();
                }
                $('#loaderListingIcon').hide('slow');
            }else if (fromLoad === 'category_to_brand'){
                var option = '';
                if (jsonData.length > 0) {
                    option += '<option value="" selected> <?php echo trans('app.select_a_brand') ?> </option>';
                    for ( i in jsonData){
                        option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].brand_name +' </option>';
                    }
                    $('#brand_select').html(option);
                    $('#brand_select').select2();
                }else {
                    $('#brand_select').html('<option value="">@lang('app.select_a_brand')</option>');
                    $('#brand_select').select2();
                }
                $('#loaderListingIcon').hide('slow');
            }else if(fromLoad === 'country_to_state'){
                var option = '';
                if (jsonData.length > 0) {
                    option += '<option value="" selected> @lang('app.select_state') </option>';
                    for ( i in jsonData){
                        option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
                    }
                    $('#state_select').html(option);
                    $('#state_select').select2();
                }else {
                    $('#state_select').html('<option value="" selected> @lang('app.select_state') </option>');
                    $('#state_select').select2();
                }
                $('#loaderListingIcon').hide('slow');

            }else if(fromLoad === 'state_to_city'){
                var option = '';
                if (jsonData.length > 0) {
                    option += '<option value="" selected> @lang('app.select_city') </option>';
                    for ( i in jsonData){
                        option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].city_name +' </option>';
                    }
                    $('#city_select').html(option);
                    $('#city_select').select2();
                }else {
                    $('#city_select').html('<option value="" selected> @lang('app.select_city') </option>');
                    $('#city_select').select2();
                }
                $('#loaderListingIcon').hide('slow');
            }
        }

        $(function(){
            $('[name="category"]').change(function(){
                var category_id = $(this).val();
                $('#loaderListingIcon').show();
                //window.history.pushState("", "", 'newpage');
                $.ajax({
                    type : 'POST',
                    url : '{{ route('get_sub_category_by_category') }}',
                    data : { category_id : category_id,  _token : '{{ csrf_token() }}' },
                    success : function (data) {
                        generate_option_from_json(data, 'category_to_sub_category');
                    }
                });
            });

            $('[name="sub_category"]').change(function(){
                var category_id = $(this).val();
                $('#loaderListingIcon').show();

                $.ajax({
                    type : 'POST',
                    url : '{{ route('get_brand_by_category') }}',
                    data : { category_id : category_id,  _token : '{{ csrf_token() }}' },
                    success : function (data) {
                        generate_option_from_json(data, 'category_to_brand');
                    }
                });
            });

            $('[name="country"]').change(function(){
                var country_id = $(this).val();
                $('#loaderListingIcon').show();
                $.ajax({
                    type : 'POST',
                    url : '{{ route('get_state_by_country') }}',
                    data : { country_id : country_id,  _token : '{{ csrf_token() }}' },
                    success : function (data) {
                        generate_option_from_json(data, 'country_to_state');
                    }
                });
            });
            $('[name="state"]').change(function(){
                var state_id = $(this).val();
                $('#loaderListingIcon').show();
                $.ajax({
                    type : 'POST',
                    url : '{{ route('get_city_by_state') }}',
                    data : { state_id : state_id,  _token : '{{ csrf_token() }}' },
                    success : function (data) {
                        generate_option_from_json(data, 'state_to_city');
                    }
                });
            });
        });

        $(document).ready(function(){
            @if($country_usage == 'single_country')
                $('#loaderListingIcon').show();
            var country_id = {{get_option('usage_single_country_id')}};
            $.ajax({
                type : 'POST',
                url : '{{ route('get_state_by_country') }}',
                data : { country_id : country_id,  _token : '{{ csrf_token() }}' },
                success : function (data) {
                    generate_option_from_json(data, 'country_to_state');
                }
            });
            @endif
        });

        $(function(){
            $('#showGridView').click(function(){
                $('.ad-box-grid-view').show();
                $('.ad-box-list-view').hide();
                $.ajax({
                    type : 'POST',
                    url : '{{ route('switch_grid_list_view') }}',
                    data : { grid_list_view : 'grid',  _token : '{{ csrf_token() }}' },
                });
            });
            $('#showListView').click(function(){
                $('.ad-box-grid-view').hide();
                $('.ad-box-list-view').show();
                $.ajax({
                    type : 'POST',
                    url : '{{ route('switch_grid_list_view') }}',
                    data : { grid_list_view : 'list',  _token : '{{ csrf_token() }}' },
                });
            });
        });
    </script>


    <script>
        @if(session('success'))
            toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
        @endif
    </script>
@endsection
