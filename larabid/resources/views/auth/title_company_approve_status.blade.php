@extends('layouts.app')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('content')


<div id="post-new-ad">
    <div class="container">
        <div class="row">
          <div style="min-height: 300px; width:600px; margin-top:40px; margin-bottom: 50px; margin-left:auto; margin-right:auto;">
            {{$status_text}}
          </div>
        </div>
    </div>
</div>

@endsection
