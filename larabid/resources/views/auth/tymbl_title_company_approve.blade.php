@extends('tymbl.layouts.app')
@section('content')
<div class="container">
    <div class="row">
      <div class="col-sm-8">
        <div class="mx-auto" style="margin: 30px;">
          <table class="table approve-tb">
              <tr>
                <td id="left-th">Contract ID</td> <td>{{ $valid_contract->contract_id }}</td>
              </tr>
              <tr>
                <td id="left-th">Date Signed</td> <td><?php echo date('Y-m-d h:i:s', strtotime($valid_contract->created_at)); ?></td>
              </tr>
              <tr>
                <td id="left-th">Lead Name</td> <td><i class="fas fa-external-link-alt"></i>&nbsp;&nbsp;<a target="_blank" href="/listing/{{$ad->id}}/{{$ad->slug}}">{{ $ad->title }}</a></td>
              </tr>

              <tr>
                <td id="left-th">List Owner</td> <td>{{ $user->name }}</td>
              </tr>
              <tr>

                <td id="left-th">Contract (PDF)</td> <td><i class="fas fa-file-download"></i>&nbsp;&nbsp;<a title="Download Contract" href="/download-contract/{{$valid_contract->contract_id}}" rel="nofollow">{{$valid_contract->contract_id}}.pdf</a>
                </td>
              </tr>

          </table>
          <div>
            <div id="loader" style="margin-top:20px;margin-bottom: 10px;">&nbsp;</div>
            <button id="btn-sold" data-value="1" class="btn btn-primary btn-lg btn-buy" data-toggle="sold" data-singleton="true" data-btn-ok-class="btn-success" data-content="Confirm that the referral agreement resulted in a successful sale of the property?" data-btn-ok-label="Confirm" data-btn-cancel-label="Cancel" data-btn-cancel-class="btn-danger" data-title="Property Sold">PROPERTY SOLD</button>

            <button id="btn-cancel" data-value="2" class="btn btn-danger btn-lg btn-buy" data-toggle="not-sold" data-singleton="true" data-btn-ok-class="btn-success" data-btn-ok-label="Confirm"  data-content="Confirm that the property was not sold and it is OK to release funds if any, held by Tymbl back to all parties involve." data-btn-cancel-label="Cancel" data-btn-cancel-class="btn-danger" data-title="Property Not Sold">PROPERTY NOT SOLD</button>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        @if(isset($media->media_name))
        <img style="width:100%;" src="{{url('/uploads/images/'.$media->media_name)}}" /><br><br>
        @else
        <img style="width:100%;" src="{{url('/assets/img/classified-placeholder.png')}}" /><br><br>
        @endif
      </div>
  </div>
</div>
@endsection
