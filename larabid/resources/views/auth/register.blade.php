@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default register-form">
                <div class="panel-heading">@lang('app.register')</div>
                <div class="panel-body">

                    @include('admin.flash_msg')
                    @include('auth.social_login')


                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        {{--First name --}}
                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="first_name" class="col-md-4 control-label required">@lang('app.first_name')</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus>

                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{--Last name --}}
                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name" class="col-md-4 control-label required">@lang('app.last_name')</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus>

                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{--<div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
                            <label for="user_name" class="col-md-4 control-label required">@lang('app.user_name')</label>

                            <div class="col-md-6">
                                <input id="user_name" type="text" class="form-control" name="user_name" value="{{ old('user_name') }}" required autofocus>

                                @if ($errors->has('user_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>--}}

                        {{--Email--}}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label required">@lang('app.email_address')</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label required">@lang('app.password')</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label required">@lang('app.confirm_password')</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <hr>

                        {{--Company name --}}
                        <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
                            <label for="company_name" class="col-md-4 control-label required">@lang('app.company_name')</label>

                            <div class="col-md-6">
                                <input id="company_name" type="text" class="form-control" name="company_name" value="{{ old('company_name') }}" required autofocus>

                                @if ($errors->has('company_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('company_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{--Representative name --}}
                        <div class="form-group{{ $errors->has('representative_name') ? ' has-error' : '' }}">
                            <label for="representative_name" class="col-md-4 control-label required">@lang('app.representative_name')</label>

                            <div class="col-md-6">
                                <input id="representative_name" type="text" class="form-control" name="representative_name" value="{{ old('representative_name') }}" required autofocus>

                                @if ($errors->has('representative_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('representative_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{--Representative email --}}
                        <div class="form-group{{ $errors->has('representative_email') ? ' has-error' : '' }}">
                            <label for="representative_email" class="col-md-4 control-label required">@lang('app.representative_email')</label>

                            <div class="col-md-6">
                                <input id="representative_email" type="text" class="form-control" name="representative_email" value="{{ old('representative_email') }}" required autofocus>

                                @if ($errors->has('representative_email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('representative_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <hr>

                        {{--Address--}}
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label">@lang('app.address')</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" autofocus>

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        {{--Phone--}}
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">@lang('app.phone')</label>

                            <div class="col-md-6">
                                <input id="phone" type="tel" class="form-control" name="phone" value="{{ old('phone') }}" autofocus placeholder="995-064-9875" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}">

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{--Fax--}}
                        <div class="form-group{{ $errors->has('fax') ? ' has-error' : '' }}">
                            <label for="fax" class="col-md-4 control-label">@lang('app.fax')</label>

                            <div class="col-md-6">
                                <input id="fax" type="tel" class="form-control" name="fax" value="{{ old('fax') }}" autofocus>

                                @if ($errors->has('fax'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fax') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        {{--RE License Number--}}
                        <div class="form-group{{ $errors->has('re_license_number') ? ' has-error' : '' }}">
                            <label for="re_license_number" class="col-md-4 control-label">@lang('app.re_license_number')</label>

                            <div class="col-md-6">
                                <input id="re_license_number" type="text" class="form-control" name="re_license_number" value="{{ old('re_license_number') }}" autofocus>

                                @if ($errors->has('re_license_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('re_license_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        @if(get_option('enable_recaptcha_registration') == 1)
                            <div class="form-group {{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="g-recaptcha" data-sitekey="{{get_option('recaptcha_site_key')}}"></div>
                                    @if ($errors->has('g-recaptcha-response'))
                                        <span class="help-block">
                                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                                </span>
                                    @endif
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-user-plus"></i> @lang('app.register')
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@if(get_option('enable_recaptcha_registration') == 1)
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endif
