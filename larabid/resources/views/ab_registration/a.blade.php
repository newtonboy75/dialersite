@extends('ab_registration.layouts.register')
@section('content')
<div class="wrapper">
  <form id="wizard" enctype="multipart/form-data" name="form">
    <!-- SECTION 1 -->
    {!! csrf_field() !!}
    <h2>@include('admin.flash_msg')</h2>

    <section>
      <div class="inner">
        <div class="image-holder">
          <img src="{{ asset('assets/img/a/signup-1.jpg') }}" alt="">
        </div>
        <div class="form-content" >
          <div class="form-header">
            <h3>Registration</h3>
          </div>
          <div class="form-row frow">
            <div class="form-holder">
              <small>FIRST NAME *</small>
              <input type="text" name="first_name" placeholder="First Name*" class="form-control" id="first_name" required>
              <span class="small-error" id="fname-error"></span>
              @if ($errors->has('fname'))
              <span class="help-block">
                <strong>{{ $errors->first('fname') }}error</strong>
              </span>
              @endif
            </div>

            <div class="form-holder">
              <small>LAST NAME *</small>
              <input type="text" name="last_name" placeholder="Last Name*" class="form-control" id="last_name" required>
              <span class="small-error" id="lname-error"></span>
              @if ($errors->has('lname'))
              <span class="help-block">
                <strong>{{ $errors->first('lname') }}error</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-row">
            <div class="form-holder">
              <small>EMAIL *</small>
              <input type="email" name="email" placeholder="Email Address*" class="form-control required" id="email" required>
              <span class="small-error" id="email-error"></span>
              @if ($errors->has('email'))
              <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
              @endif
            </div>

            <div class="form-holder">
              <small>PASSWORD *</small>
              <input type="password" name="password" id="password" placeholder="Password*" class="form-control required">
              <div id="show-pass"><small><i style="font-size: 16px !important; color: #9c9c9c" class="fas fa-eye">&nbsp;</i></small></div>
              <span class="small-error" id="password-error"></span>
              @if ($errors->has('password'))
              <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-row">
            <div class="form-holder">
              <small>PHONE *</small>
              <input type="text" name="phone" placeholder="Phone* ex. 9999999999" class="form-control" id="phone" pattern="^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$" title="Enter valid mobile number ex.9999999999" required>
              <span class="small-error" id="phone-error"></span>
            </div>

            <div class="form-holder">
              <small>&nbsp;</small>
                <div><input type="checkbox" name="sms_notify" id="sms_notify"><small> TEXT ME MY ACCONT CONFIRMATION</small></div>
                <div><input type="checkbox" name="lead_notify" id="lead_notify"><small> NOTIFY ME WHEN THERE NEW LEADS IN MY AREA</small></div>
            </div>
          </div>
          <div class="form-row">
            <div class="form-holder">
              <div class="range-slider"> <div class="or-ds"><strong>OR</strong></div>
                <small>SELECT RANGE OF YOUR AREA</small>
                <input type="range" min="0" max="100" value="1" class="slider" id="slider" name="distance" class="form-control-range mb-md-4 mr-md-2" onclick="actionClick()"><br>
                <small class="range-val" id="slider_value">AREA: 0 MILES</small>
              </div>
              <div class="or-mobile"><strong>OR</strong></div>
            </div>

            <div class="form-holder">
              <div class="ui-widget-zip">
              <small>ENTER SOME ZIP CODES (Up to 10)</small>
              <div class="zip-input">
                <ul id="ul-zips"></ul>
                <input type="text" id="zip_input" class="select-control w-100">
                <div id="pill"></div>
              </div>
              </div>
            </div>
          </div>
          <div class="form-row">
            <div class="form-holder2">
              <input style="float:left;" type="checkbox" name="authorized-to-sign" checked>&nbsp;&nbsp;I am authorized by my broker to sign referral agreements
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- SECTION 2 -->
    <h2></h2>
    <section>
      <div class="inner" id="ctr" rel="{{ $country_code}}">
        <div class="image-holder">
          <img src="{{ asset('assets/img/a/signup-1.jpg') }}" alt="">
        </div>
        <div class="form-content">
          <div class="form-header">
            <h3>Registration</h3>
          </div>
          <div class="form-row frow">
            <div class="form-holder">
              <small>BROKERAGE FIRM</small><div class="info" style="position:relative; margin-right: -16px;"><i style="font-size: 12px !important;" class="fas fa-info-circle" data-toggle="tooltip" ></i></div>
              <input type="text" name="brokerage_name" id="brokerage_name" placeholder="Brokerage Firm*" class="form-control" id="brokName" required>
              <span class="small-error" id="broker-name-error"></span>
            </div>
            <div class="form-holder">
              <small>BROKER NAME</small><div class="info" style="position:relative; margin-right: -16px;"><i style="font-size: 12px !important;" class="fas fa-info-circle" data-toggle="tooltip" ></i></div>
              <input type="text" name="brokerage_contact_person" id="brokerage_person" placeholder="Brokerage Name*" class="form-control" id="brokName" required>
              <span class="small-error" id="broker-contact-error"></span>
            </div>
          </div>

          <div class="form-row frow" style="margin-top: 16px !important;">
            <div class="form-holder">
              <small>BROKERAGE EMAIL</small><div class="info" style="position:relative; margin-right: -16px;"><i style="font-size: 12px !important;" class="fas fa-info-circle" data-toggle="tooltip" ></i></div>
              <input type="text" name="brokerage_email" id="brokerage_email" placeholder="Brokerage Email*" class="form-control" id="brokName" required>
              <span class="small-error" id="broker-email-error"></span>
            </div>
            <div class="form-holder">
              <small>BROKER PHONE</small><div class="info" style="position:relative; margin-right: -16px;"><i style="font-size: 12px !important;" class="fas fa-info-circle" data-toggle="tooltip" ></i></div>
              <input type="text" name="brokerage_phone" id="brokerage_phone" placeholder="Brokerage Phone*" class="form-control" id="brokName" required>
              <span class="small-error" id="broker-phone-error"></span>
            </div>
          </div>


          <div class="form-row">
            <div class="form-holder">
              <small>ADDRESS *</small>
              <input type="text" name="address" id="refAdress" placeholder="Address*" class="form-control" id="address">
              <span class="small-error" id="add-error"></span>
            </div>

            <div class="select">
              <div class="form-holder w-100">
                <small>STATE *</small>
                <input class="form-control w-100" type="text" id="state_name" name="state" value="">
                <input class="form-control w-100" type="hidden" id="state_i" name="state_i" value="">
              </div>
              <span class="small-error" id="state-error"></span>
            </div>
          </div>
          <div class="form-row">
            <div class="select">
              <div class="form-holder w-100">
                <small>CITY *</small>
                <input class="form-control w-100" type="text" id="city_name" name="city" value="">
                <input type="hidden" id="city_i" name="city_i" value="">
              </div>
              <span class="small-error" id="city-error"></span>
            </div>

            <div class="form-holder">
              <small>ZIP CODE *</small>
              <ul id="ul-zip"></ul>
              <input class="form-control" type="text" id="zip" name="zip" value="" placeholder="Zip Code">
              <span class="small-error" id="zip-error"></span>
            </div>

          </div>

          <div class="form-row">
            <div class="form-holder">
              <label style="font-size: 14px !important;">
                <input type="checkbox" id="tc">  I accept the <a href="#">terms and conditions</a>
                <span class="checkmark"></span>
              </label>
            </div>
          </div>
          <div class="form-row">
            <div class="wrapper-impt"><small><strong>IMPORTANT:</strong>&nbsp;&nbsp;Please note that if the information is incorrect, your Tymbl account privileges may be suspended and any referral agreement signed is subject to termination until the information is verified.</small></div>
          </div>
      </div>
    </section>
    <div id="error" style="color:red; font-size: 15px; float: right; margin-right:35%;"></div>
  </form>
</div>

<!-- JQUERY -->
<script src="{{ asset('assets/js/a/jquery-3.3.1.min.js') }}"></script>
<script>
var mls = <?php echo json_encode($mls_client) ?>;
var mls_id = '';

</script>
<!-- JQUERY STEP -->

<script src="{{ asset('assets/js/a/jquery.steps.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
<script src="{{ asset('assets/js/a/main.js') }}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>


<!-- Template created and distributed by Colorlib -->
<script>


function submitEntries(){

  $('#error').html('submitting... please wait...');

  var zip_pills = new Array();

  $('.custom-pill').each(function( index ) {
    zip_pills.push($(this).text().replace(' x', ''));

  });

  //console.log(zip_pills);

  $('#broker-name-error').html('');
  $('#broker-contact-error').html('');
  $('#broker-email-error').html('');
  $('#broker-phone-error').html('');
  $('#add-error').html('');
  $('#phone-error').html('');
  $('#zip-error').html('');
  $('#state-error').html('');
  $('#city-error').html('');
  var regEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  var validnum = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/;

  if($('#brokerage_name').val() == ''){
    $('#broker-name-error').html('*brokerage name is required');
    errors = '1';
    return false;
  }else if($('#brokerage_person').val() == ''){
    $('#broker-contact-error').html('*contact person is required');
    errors = '1';
    return false;
  }else if($('#brokerage_email').val() == ''){
    $('#broker-email-error').html('*email is required');
    errors = '1';
    return false;
  }else if(!regEmail.test($('#brokerage_email').val())){
    $('#broker-email-error').html('*email is invalid');
    errors = '1';
    return false;
  }else if($('#brokerage_phone').val() == ''){
    $('#broker-phone-error').html('*phone is required');
    errors = '1';
    return false;
  }else if(!$('#brokerage_phone').val().match(validnum)){
    $('#broker-phone-error').html('*enter valid mobile number ex.9999999999');
    errors = '1';
    return false;
  }else if($('#refAdress').val() == ''){
    $('#add-error').html('*address is required');
    errors = '1';
    return false;
  }else{
    //errors = '';
    //return true;
    var tc_checked = '0';

    $('#error').html("please wait..");
    var cb = '0';
    if ($('#sms_notify').prop('checked')) {
      cb = '1';
    }

    var cb1 = '0';
    if ($('#lead_notify').prop('checked')) {
      cb1 = '1';
    }

    if(mls_id == "other"){
      mls_id = $('#other_mls').val();
    }

    if($("#tc").prop('checked') == true){
      tc_checked = '1';
    }

    $.ajax({
      type: 'post',
      url: '/register',
      data: {
        first_name: $('#first_name').val(),
        last_name: $('#last_name').val(),
        email: $('#email').val(),
        password: $('#password').val(),
        password_confirmation: $('#password_confirmation').val(),
        brokerage_name: $('#brokerage_name').val(),
        brokerage_contact_person: $('#brokerage_person').val(),
        brokerage_email: $('#brokerage_email').val(),
        brokerage_phone: $('#brokerage_phone').val(),
        zip_code: $('#zip').val(),
        address: $('#refAdress').val(),
        phone: $('#phone').val(),
        state: $('#state_name').val(),
        state_id: $('[name="state_i"]').val(),
        city: $('[name="city_i"]').val(),
        from: 'a',
        sms_notify: cb1,
        send_account_info: cb,
        range: $('#slider').val(),
        country: {{ $country_code}},
        zip_pills: zip_pills,
        city_name: $('#city_name').val(),
        tc_checked: tc_checked,
        _token: '{{csrf_token()}}'
      },

      success: function(data) {

        $('#error').html('');
        if(data == 'Email already exists '+ $('#email').val()){
          $("#wizard").steps('previous');
          $("#wizard").steps('previous');
          $('#error').html(data);
        }else if(data == 'Password is too short.'){
          $("#wizard").steps('previous');
          $("#wizard").steps('previous');
          $('#password-error').html(data);
          $('#error').html('');
        }else if(data == 'Either zip code or state is incorrect.'){
          $("#wizard").steps('previous');
          $('#zip-error').html(data);
          $('#error').html('');
        }else if(data == 'on_state'){
          $('#state-error').html('*state is invalid');
        }else if(data == 'on_city'){
          $('#city-error').html('*city is invalid');
        }else if(data == 'on_zip'){
          $('#zip-error').html('*zip code is invalid');
        }else if(data == 'on_tc'){
          alert('Please accept Terms and Conditions.');
        }else{
          //console.log(data);
          if (cb == '0') {
            window.location.href = "/login/";
          }else{
            window.location.href = "/login/verify";
          }
        }
      },
    });
  }
}
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125959247-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-125959247-1');
</script>

<script>

function initMap() {

  var options = {
    types: [('address')],
    componentRestrictions: {country: "us"}
  };

  var input = document.getElementById('refAdress');
  var autocomplete = new google.maps.places.Autocomplete(input, options);

  autocomplete.addListener('place_changed', function() {

    var place=autocomplete.getPlace();
    var zips = '';
    this.pickup=place.address_components;
    //console.log(this.pickup);
    //alert(zips);

    var street_number = '';
    var street = '';
    var city = '';
    var state = '';
    var zip = '';
    var cities = [];
    var cid = {{$country_code}};

    $.each(this.pickup, function( index, value ) {
      //console.log(value);
      if(value['types'][0]=='street_number'){
        //console.log(value['long_name']);
        street_number = value['long_name'];
      }

      if(value['types'][0]=='route'){
        //console.log(value['long_name']);
        street = value['long_name'];
      }

      if(value['types'][0]=='locality'){
        //console.log(value['long_name']);
        city = value['long_name'];
        setTimeout(function(){
          $.ajax({
            url: '/get-city-by-state',
            type: "POST",
            data: {state_id: $('[name="state_i"]').val(), 'city_name': city, _token: '{{ csrf_token() }}'},

            success: function (data) {
              //console.log(data);
                var resp = $.map(data,function(obj){
                //console.log(resp);
                if(obj.city_name == city){
                  //console.log(obj.id);
                  $('[name="city_i"]').val(obj.id);
                  $('[name="city"]').val(obj.city_name);
                }
              });
            }
          });
        }, 2000);
      }


      if(value['types'][0]=='administrative_area_level_1'){
        //console.log(value['long_name']);
        state = value['long_name'];

        //$('[name="state"] option:contains(' + $.trim(state) + ')').attr('selected', 'selected').change();
        setTimeout(function(){
          $.ajax({
            url: '/get-state-by-country',
            type: "POST",
            data: {country_id: cid, _token: '{{ csrf_token() }}'},
            success: function (data) {
              //console.log(data);
                var resp = $.map(data,function(obj){
                //console.log(resp);
                if(obj.state_name == state){
                  //console.log(obj.id);
                  $('[name="state_i"]').val(obj.id);
                  $('[name="state"]').val(obj.state_name);

                  $.each(mls, function (key, i) {
                    if(key == obj.state_name){
                      //console.log(key);
                      $('#mls li').remove();
                      $.each(i, function (key2, val) {
                        //console.log(val.id);
                        $('#mls').append('<li id="mlsid" rel="'+val.id+'">'+val.name+'</li>');
                      });
                      $('#mls').append('<li id="mlsid" rel="other">Other MLS</li>');
                    }
                  });
                }
              });
            }
          });
        }, 1000);

      }

      if(value['types'][0]=='postal_code'){
        zip = (value['long_name'].length && value['long_name'].charAt(0) == '0') ? value['long_name'].slice(1) : value['long_name'];

        setTimeout(function(){
          $.ajax({
            url: '/get_zip_by_city',
            type: "POST",
            data: {country_id: {{$country_code}}, state_id: $('[name="state"]').val(), city_id: $('[name="city"]').val(), _token: '{{ csrf_token() }}'},
            success: function (data) {
              //console.log(data);
                var resp = $.map(data,function(obj){
                if(obj.zip == zip){
                  //console.log(obj.id);
                  $('[name="zip"]').val(obj.zip);
                }
              });
            }
          });
        }, 2500);

      }
    });

    //console.log( city );

    input_street_number = street_number.length ? street_number + ' ' : '';
    input_street = street.length ? street + ', ' : '';
    input_city = city.length ? city + ', ' : '';

    $('#refAdress').val(input_street_number + input_street + input_city + state);

  });

}
</script>
<script>
var states_array = [];
var cities_array = [];
var zips_array = [];
var stateid='';
var cityid='';
var zipid='';
var countryid='{{$country_code}}';

$( function() {

  $('.actions').css({'bottom':'-6%'});

  $('[id="show-pass"]').on('mouseup touchend', function() {
    $('#password').prop('type', 'password');
  }).on('mousedown touchstart', function() {
    $('#password').prop('type', 'text');
  });

  $('[data-toggle="tooltip"]').tooltip({title: "<h5 class='htl'>Why do we need this info?</h5><ul class='tl'><li>This information is required for the contract you will sign in order to reserve a referral</li></ul>", html: true, placement: "bottom"});

  var auto_countries = [
    "United States",
    "Canada"
  ];

  $('[name="state"]').autocomplete({
    source: function(request, response) {
      $.ajax({
        url: '/get-states-for-autocomplete',
        type: "POST",
        data: {state_name: request.term, country_id: countryid, _token: '{{ csrf_token() }}'},
        success: function (data) {
          //console.log(data);
          var resp = $.map(data,function(obj){
            //console.log(resp);
            return obj.state_name;
          });

          var states = $.map(data,function(obj){
            return obj;
          });
          states_array = [];
          states_array.push(states);
          response(resp);
          //return resp;
        }
      });
    },
    minLength: 1,
    select: function( event, ui ) {
      //console.log(ui.item.value);
      $.each( states_array, function( id, value ) {
        $.each( value, function( i, v ) {
          if(ui.item.value == v['state_name']){
            stateid = v['state_name'];
            $('[name="state_i"]').val(v['id']);
            $('[name="city_i"]').val('');
            $('[name="city"]').val('');
            $('[name="zip"]').val('');

            $.each(mls, function (key, i) {
              if(key == v['state_name']){
                //console.log(key);
                $('#mls li').remove();
                $.each(i, function (key2, val) {
                  //console.log(val.id);
                  $('#mls').append('<li id="mlsid" rel="'+val.id+'">'+val.name+'</li>');
                });
                $('#mls').append('<li id="mlsid" rel="other">Other MLS</li>');
              }
            });
          }
        });
      });
    }
  });

  $('[name="city"]').autocomplete({
    source: function(request, response) {
      $.ajax({
        url: '/get-cities-for-autocomplete',
        type: "POST",
        data: {city_name: request.term, state_id:  $("#state_i").val(), _token: '{{ csrf_token() }}'},
        success: function (data) {
          //console.log($('#state_id').val());
          var resp = $.map(data,function(obj){
            //console.log(resp);
            return obj.city_name;
          });

          var cities = $.map(data,function(obj){
            return obj;
          });
          cities_array = [];
          cities_array.push(cities);

          response(resp);
          //return resp;
        }
      });
    },
    minLength: 1,
    select: function( event, ui ) {

      $.each( cities_array, function( id, value ) {
        $.each( value, function( i, v ) {
          if(ui.item.value == v['city_name']){
            $('[name="city_i"]').val(v['id']);
            $('[name="city"]').val(v['city_name']);
            $('[name="zipcode"]').val('');
          }
        });
      });
    }
  });


  $('[name="zip"]').autocomplete({
    source: function(request, response) {
      $.ajax({
        url: '/get-zips-for-autocomplete',
        type: "POST",
        data: {zip_name: request.term, country: {{$country_code}}, state: $('#state_name').val(), city: $('#city_name').val(), _token: '{{ csrf_token() }}'},
        success: function (data) {
          //console.log(data);
          var resp = $.map(data,function(obj){
            //console.log(resp);
            return obj.zip;
          });

          var zips = $.map(data,function(obj){
            return obj;
          });

          zips_array = [];
          zips_array.push(zips);

          response(resp);
          //return resp;
        }
      });
    },
    minLength: 1,
    select: function( event, ui ) {
      //console.log(states_array);
      $.each( zips_array, function( id, value ) {
        $.each( value, function( i, v ) {
          //console.log(v['id']);
          if(ui.item.value == v['zip']){
            $('[name="zipcode"]').val(v['zip']);
          }
        });
      });
    }
  });
  //end here
});
</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

@endsection
