
@extends('ab_registration.layouts.register')
@section('content')
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-4 signup-img-content">
        <h1>Welcome!</h1>
        <h5>Register to access all the features of our Service. It’s free!</h5>
      </div>
      <div class="col-md-1">&nbsp;</div>
      <div class="clearfix"></div>
      <div class="col-md-7">
        @include('admin.flash_msg')
        <h1>Create a new account</h1>
        <p>Please provide us these following information</p>
        <form action="/register" method="POST">
          {!! csrf_field() !!}
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6 col-sm-12">
                <input class="form-control" type="text" name="name" id="name" placeholder="Full Name*" required value="{{ old('name') }}">
                <input class="form-control" type="email" name="email" id="email" placeholder="Email Address*" required value="{{ old('email') }}">
                @if ($errors->has('email'))
                <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
                <input class="form-control" type="password" name="password" id="pass" placeholder="Password*" required>
                @if ($errors->has('password'))
                <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
                <input class="form-control" type="password" name="password_confirmation" id="re-pass" placeholder="Repeat Password*" required>
                <input class="form-control" type="text" name="address" id="address" placeholder="Address*" required value="{{ old('address') }}">
                <input class="form-control" type="tel" name="phone" id="phone" placeholder="Phone* ex. 9999999999" required value="{{ old('phone') }}">
                @if ($errors->has('phone'))
                <span class="help-block">
                  <strong>{{ $errors->first('phone') }}</strong>
                </span>
                @endif
                <select class="form-control custom-select" name="state" id="state" style="display:block;">
                  <option value="opt1" selected disabled>State/Province*</option>
                  @foreach($states as $state)
                  @if($state->country_id == $country_code)
                  <option value="{{ $state->id }}" {{ old('state') == $state->id ? 'selected':'' }}>{{ $state->state_name }}</option>
                  @endif
                  @endforeach
                </select>
              </div>
              <div class="col-md-6 col-sm-12">
                <input class="form-control" type="text" name="company_name" id="title" placeholder="Title Company Name*" required value="{{ old('company_name') }}">
                <input class="form-control" type="text" name="representative_name" id="rep-name" placeholder="Representative Name*" required value="{{ old('representative_name') }}">
                <input class="form-control" type="email" name="representative_email" id="rep-email" placeholder="Representative Email*" required value="{{ old('representative_email') }}">
                <input class="form-control" type="text" name="brokerage_name" id="brokerage_name" placeholder="Brokerage Name*" required value="{{ old('brokerage_name') }}">
                <input class="form-control" type="text" name="brokerage_addr" id="brokerage_addr" placeholder="Brokerage Address" value="{{ old('brokerage_addr') }}">
                <input class="form-control" type="text" name="re_license_number" id="license" placeholder="RE License Number*" required value="{{ old('re_license_number') }}">
                <select class="form-control custom-select" name="mls" id="mls" style="display:block;">
                  <option value="opt1" value="" selected disabled>MLS*</option>
                </select>
                @if ($errors->has('mls'))
                <span class="help-block">
                  <strong>{{ $errors->first('mls') }} Please reselect State.</strong>
                </span>
                @endif
                <input type="hidden" name="from" value="b">
                <input type="submit" class="btn btn-primary float-right" value="REGISTER">



            </div>
          </div>
        </form>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>

  <div class="btn-social">
    <div class="col">
            <strong>or register using:</strong>
        <div class="social">
        <a href="{{ route('facebook_redirect') }}" alt="Facebook"><i class="fab fa-facebook-f" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
            </i></a>
        <a href="{{ route('twitter_redirect') }}"><i class="fab fa-twitter" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter"></i></a>
        <a href="{{ route('google_redirect') }}"><i class="fab fa-google" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google plus"></i></a>
       </div>
    </div>
  </div>



</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

<script>
var mls = <?php echo json_encode($mls_client) ?>;
$("#state").change(function(){
  var selected_state = $(this).find("option:selected").text();
  $.each(mls, function (key, i) {
    if(key == selected_state){
      //console.log(key);
      $('#mls option').remove();
      $('#mls').prepend('<option value="0">MLS*</option>');
      $.each(i, function (key2, val) {
        //console.log(val.id);
        $('#mls').append('<option value="'+val.id+'">'+val.name+'</option>');
      });
    }
  });
});
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125959247-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-125959247-1');
</script>

@endsection
