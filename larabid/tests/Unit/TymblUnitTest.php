<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\BrowserKitTesting\TestCase as BaseTestCase;
use App\User;
use App\UserTitleCompanyInfo;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Twilio\Rest\Client;
use App\Mls;
use App\State;
use App\Broker;
use App\Notification;
use Artisan;
use App\Ad;
use App\NotificationTask;



class TymblUnitTest extends TestCase
{
  /**
  * A basic functional test example.
  *
  * @return void
  */

  protected function &getSharedVar()
  {
    static $res = null;
    return $res;
  }

  //test the homepage
  public function testBasicExample(){
    echo 'Testing homepage...'. PHP_EOL;
    $this->withSession(['testcaller' => '1'])
    ->visit('/')->see('Enables You To');
    echo 'Homepage is up and running'. PHP_EOL;
    echo ' '.PHP_EOL;
  }

  //test link from the homepage
  public function testAllLinks(){
    echo 'Testing Link'. PHP_EOL;
    $this->withSession(['testcaller' => '1'])
    ->visit('/')
    ->click('TEAM')
    ->seePageIs('/page/about-us');
    echo ' '.PHP_EOL;

    //$this->visit('/page/about-us')
    //->click('Privacy Policy')
    //->seePageIs('/page/privacy-policy');
    //echo 'Privacy Policy page link is working'. PHP_EOL;
    //echo ' '.PHP_EOL;

    //this->visit('/page/about-us')
    //->click('Terms & Conditions')
    //->seePageIs('/page/terms-and-conditions-1');
    //echo 'Terms and Conditions link is working'. PHP_EOL;
    //echo ' '.PHP_EOL;

    //$this->visit('/page/about-us')
    //->click('Contact Us')
    //-//>seePageIs('/contact-us');
    //echo 'Contact Us link is working'. PHP_EOL;
    //echo ' '.PHP_EOL;

    $this->visit('/page/about-us')
    ->click('Login')
    ->seePageIs('/login');
    echo 'Login link is working'. PHP_EOL;
    echo ' '.PHP_EOL;

    $this->visit('/page/about-us')
    ->click('TEAM')
    ->seePageIs('/page/about-us');
    echo 'About Us link is working'. PHP_EOL;
    echo ' '.PHP_EOL;

    $this->visit('/page/about-us')
    ->click('REFER A CLIENT')
    ->seePageIs('/login');
    echo 'Post New Listing redirected to login page'. PHP_EOL;
    echo ' '.PHP_EOL;

    echo 'Testing search form'. PHP_EOL;
    $this->withSession(['testcaller' => '1'])
    ->visit('/search')
    ->press('hero-search')
    ->see('Total of');
    echo 'Search form is working';
    echo ' '.PHP_EOL;
    echo ' '.PHP_EOL;

    echo 'All links are working'. PHP_EOL;
    echo ' '.PHP_EOL;
  }

  //test registration from landing page
  public function noTestLandingPageForm(){
    echo 'Testing Landing page registration form...'. PHP_EOL;
    $this->visit('/')
    ->withSession(['testcaller' => "reg", 'testuserid' => '0'])
    ->type('Test User', 'name')
    ->type('testuser1@test.com', 'email')
    ->select('3919', 'state')
    ->select('42594', 'city')
    ->select('6501', 'zipcode')
    ->press('NOTIFY ME')
    ->see('Thank you. We will inform you when there are new referrals or leads in your area.');
    echo 'User testuser1@test.com registered using landing page form'. PHP_EOL;
    echo 'Welcome email was sent to user'. PHP_EOL;
    echo ' '.PHP_EOL;
  }


  //test login unverified users
  public function testLoginPageUnverified(){
    echo 'Testing unverified user'.PHP_EOL;
    $this->visit('/login')
    ->type('testuser2@test.com', 'email')
    ->type('testpass', 'password')
    ->press('Login')
    ->seePageIs('/login');
    echo 'User is redirected to login page'.PHP_EOL;
    echo ' '.PHP_EOL;
  }

  //test login existing verified users
  public function testLoginPageVerified(){
    echo 'Testing existing verified user pabeboy10@gmail.com'. PHP_EOL;
    echo 'User logging in'. PHP_EOL;
    $this->visit('/login')
    ->type('pabeboy10@gmail.com', 'email')
    ->type('renton75', 'password')
    ->press('Login')
    ->seePageIs('/dashboard');
    echo 'User is redirected to the dashboard'. PHP_EOL;
    echo ' '.PHP_EOL;
  }

  /*
  - create account using faker and log in user automatically,
  - simulate user going to register route
  - user should be redirected to the dashboard
  */
  public function testLoggedUser(){
    echo 'Creating new user...'. PHP_EOL;
    $user = factory(User::class)->create();
    $this->actingAs($user)
    ->visit('/register')
    ->seePageIs('/dashboard');
    $res = &$this->getSharedVar();
    $res = $user;
    echo 'User '.$user->email.' created successfully'. PHP_EOL;
    echo ' '.PHP_EOL;
  }

  public function testCheckUserCreated(){
    echo 'Verifying user creation...'. PHP_EOL;
    $res = &$this->getSharedVar();
    $user = \App\User::where('email', $res->email)->first();

    if($res->email == $user->email){
      echo 'User '.$user->email.' exists in the user database'. PHP_EOL;
    }
    echo ' '.PHP_EOL;
  }

  public function testPostNewList(){
    //$this->withoutMiddleware();

    echo 'Testing Post New Listing...'. PHP_EOL;
    $res = &$this->getSharedVar();
    echo 'with user '.$res->email;
    echo ' '.PHP_EOL;

    $this->actingAs($res)
    ->withSession(['testcaller' => "1", 'testuserid' => $res->id])
    ->visit('/post-new')
    ->seePageIs('/post-new');
    echo 'User is logged in and clicked post-new...'. PHP_EOL;
    echo 'Start filling out listing form...'. PHP_EOL;

    $this->select('ns', 'cat_type');
    $this->select('yes', 'cat_type_q');
    $this->type('09/13/2019', 'date_contacted_qualified');
    $this->type('25%', 'referral_fee');
    $this->select('9', 'category');
    $this->type('Buying Sample Condo', 'listing_title');
    $this->type('1', 'feature[0]');
    $this->type('2', 'feature[1]');
    $this->type('3', 'feature[2]');
    $this->type('4', 'feature[3]');
    $this->type('5', 'feature[4]');
    $this->type('6', 'feature[5]');
    $this->type('Sample description test unit', 'listing_description');
    $this->select('< $50K', 'price_range');
    $this->select('231', 'country');
    $this->select('3924', 'state');
    $this->select('Select City', 'city');
    $this->type('90210',  'zipcode');
    $this->type('Modioboy', 'referral_first_name');
    $this->type('Tugadi', 'referral_last_name');
    $this->type('referralname@sample.com', 'referral_contact_email');
    $this->type('12345 Alabaster Street', 'referral_contact_address');
    $this->type('995-064-9875', 'referral_contact_phone');
    $this->type('995-064-9875', 'referral_contact_fax');
    $this->select('$50K-$99K', 'price_range');
    $this->type('995-064-9875', 'referral_contact_fax');
    $this->press('POST LEAD');

    echo 'Listing created successfully'.PHP_EOL;
    echo 'Test link of newly created listing'.PHP_EOL;
    echo 'New listing single page is working'.PHP_EOL;
    echo ' '.PHP_EOL;

  }

  public function testRemoveTestData(){

    $res = &$this->getSharedVar();
    $users = [$res->email, 'testuser2@test.com'];
    echo 'Removing test data...'. PHP_EOL;

    foreach($users as $user=>$u){
      $user = \App\User::where('email', $u)->first();
      if($user){
        $user_id = $user->id;
        $user->delete();
        $broker = \App\Broker::where('user_id', $user_id);
        $broker->delete();
        $notification = \App\Notification::where('user_id', $user_id);
        $notification->delete();
        $title_company = \App\UserTitleCompanyInfo::where('user_id', $user_id);
        $title_company->delete();
        $ad = \App\Ad::where('user_id', $user_id);
        //$notification_task = \App\NotificationTask::where('listing_id', $ad->id);
        $new_reg = \App\NotificationNewRegistration::where('user_id', $user_id);
        $ad->delete();
      }

      //$notification_task->delete();
    }

    echo 'Test data removed...'. PHP_EOL;
  }

}
