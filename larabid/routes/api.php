<?php

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;
use App\ReferralContactInfo;
use App\State;
use App\Ad;
use App\Category;
use App\City;
use App\Media;
use App\User;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
  return $request->user();
});

Route::post('import_csv', function(Request $request) {
  if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
    $user = Auth::user();
    if($user->user_type == 'admin'){
      $path = $request->file('csv_file')->getRealPath();
      $file = file($path);

      //remove header
      $data = array_slice($file, 1);

      $parts = (array_chunk($data, 100));
      $i = 1;
      foreach($parts as $line) {
        $filename = storage_path('csv_uploads/'.date('y-m-d-H-i-s').$i.'.csv');
        file_put_contents($filename, $line);
        $i++;
      }

      $newpath = storage_path('csv_uploads');

      $files = glob("$newpath/*.csv");

      foreach($files as $file) {

          $ads = array();
          if (($handle = fopen($file, "r")) !== FALSE) {
              while (($data = fgetcsv($handle, 4096, ",")) !== FALSE) {

                $post_title = $data[0];
                $description = $data[1];
                $category_id = $data[18];
                $name = $data[2];
                $email = $data[3];
                $phone = $data[4];
                $country = $data[5];
                $state_name = $data[6];
                $city_name = $data[7];
                $zipcode = $data[8];
                $address = $data[9];
                $vid_url = $data[11];
                $feature1 = $data[14];
                $feature2 = $data[15];
                $escrow_amount = $data[12];
                $referral_fee = $data[13];
                $price_range = $data[17];
                $referral_name_info = $data[22];
                $referral_email = $data[23];
                $referral_phone = $data[24];
                $referral_fax = $data[25];
                $referral_address = $data[26];
                $image_name = $data[10];
                $lead_status = $data[16];
                $category_type = $data[19];
                $cat_type_status = $data[20];
                $contract_signed = $data[21];


                $feature1_data = [];
                $feature1_data = array();
                $active = 0;

                $feature1_array = explode(',', $feature1);

                foreach($feature1_array as $n=>$v){
                  $d = explode(':', $v);
                  $feature1_data[$n] = $d[1];
                }

                $feature2_array = explode(',', $feature2);
                foreach($feature2_array as $k=>$fs2){
                  $d2 = explode(':', $fs2);
                    if($d2[1] != '0'){
                      $feature2_data[] = $d2[0];
                    }
                }

                if($country == 'Canada'){
                  $ctr = '38';
                }else{
                  $ctr = '231';
                }

                $state = State::where('state_name', '=', trim($state_name))->where('country_id', '=', $ctr)->first();
                if(!$state){
                  return 'State is not found '.$state_name;
                  exit;
                }
                $stid = $state->id;
                $city = City::where('city_name', 'LIKE', trim($city_name))->where('state_id', '=', $stid)->first();
                if(!$city){
                  return 'City is not found: '.trim($city_name).', '.trim($state_name);
                  exit;
                }

                $user = User::where('email', '=', $email)->first();
                $mycity = ($city) ? $city->id : '';

                if($user){
                  $user_id = $user->id;
                  $active = '1';
                }else{
                //  $user_name_exp = explode(' ', $data[3]);
                  $new_user = [
                    'name' => $name,
                    //'first_name' => $user_name_exp[0],
                    //'last_name'  => $user_name_exp[1],
                    'email' => $email,
                    'phone' => $phone,
                    'sms_activation_code' => ''
                  ];

                  $new_usr = User::create($new_user);
                  $user_id = $new_usr->id;
                  $active = '0';
                }

                $category = Category::whereId($category_id)->first();

                $ads = [
                  'title' => $post_title,
                  'slug' => unique_slug($post_title),
                  'description' => $description,
                  'sub_category_id' => $category_id,
                  'seller_name' => $name,
                  'seller_email' => $email,
                  'seller_phone' => $phone,
                  'country_id'  => $ctr,
                  'state_id'  => $stid,
                  'city_id' => $mycity,
                  'zipcode' => $zipcode,
                  'address' => $address,
                  'video_url' => $vid_url,
                  'escrow_amount' => $escrow_amount,
                  'referral_fee' => $referral_fee,
                  'feature_1' =>  serialize($feature1_data),
                  'feature_2' => serialize($feature2_data),
                  'price_range' => $price_range,
                  'status' => $lead_status,
                  'user_id' => $user_id,
                  'category_type' => $category_type,
                  'cat_type_status' => $cat_type_status
                ];

                $ad = Ad::create($ads);

                if($ad){
                  $refs = [
                    'ad_id' => $ad->id,
                    'referral_name' => $referral_name_info,
                    'referral_contact_email' => $referral_email,
                    'referral_contact_phone' => $referral_phone,
                    'referral_contact_fax' => $referral_fax,
                    'referral_contact_address' => $referral_address,
                  ];

                  $title_company = ReferralContactInfo::create($refs);
                  $image = [
                    'user_id' => $user_id,
                    'ad_id' => $ad->id,
                    'media_name' => $image_name,
                    'type'  => 'image',
                    'storage' => 'public',
                    'ref' => 'ad'
                  ];
                  $media = Media::create($image);
                }
              }

              fclose($handle);
          } else {
              echo "Could not open file: " . $file;
          }
      }

      //end for
      $rfile = new Filesystem;
      $rfile->cleanDirectory($newpath);
    }

    return 200;
  }
  else{
    return response()->json(['error'=>'Unauthorised'], 401);
  }

});
