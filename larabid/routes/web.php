<?php
//Disable for custom registration AB pages
Auth::routes();

// Registration Routes...
Route::get('register/a', ['uses' => 'Auth\RegisterController@showRegistrationFormA']);
Route::get('register/b', ['uses' => 'Auth\RegisterController@showRegistrationFormB']);
Route::post('register', ['uses' => 'Auth\RegisterController@register']);
Route::get('regactivate', ['uses' => 'NotificationTaskController@sendRegistrationActivation']);
Route::get('sendtransact', ['uses' => 'TitleCompanyController@sendApprovalNotification']);
Route::get('signers-signed', ['uses' => 'ContractSigned@signerSinged']);
Route::post('check-email', ['uses' => 'Auth\RegisterController@registerCheckEmail']);
Route::post('get-city', ['uses' => 'AdsController@getCity']);
Route::post('del-lead-entry', ['uses' => 'DashboardController@deleteLeadEntry']);

Route::post('submit-inquiry-sm', ['uses' => 'DashboardController@tempInquirySubmit']);
Route::get('pricing', ['uses' => 'HomeController@renderNonUserHomePagePricing']);

Route::group(['prefix'=>'sign-contract'], function(){
    Route::get('plan', ['as' => 'plan', 'uses' => 'ReferralAgreementController@signContractFreeUpload']);
    Route::get('check-contract-signed/', ['as' => 'check-contract-signed', 'uses' => 'PaymentController@checkContractAgreementSigned']);
    Route::get('agreement-signed', ['as' => 'agreement-signed', 'uses' => 'PaymentController@displayThankYouAgreementSigned']);
});

//InMan2019
Route::get('inman2019', ['as'=>'inman2019', 'uses' => 'HomeController@inMan2019']);
Route::get('InMan2019', ['as'=>'InMan2019', 'uses' => 'HomeController@inMan2019']);

//BrokerVerification
Route::get('verifyagent/{id}', ['uses' => 'BrokerController@verifyAgent']);
Route::post('verified-agent', ['as' => 'verified-agent', 'uses' => 'BrokerController@verifiedAgent']);

//LeadNotes
Route::post('lead-note-add', ['uses' => 'NoteController@store']);
Route::post('lead-note-delete', ['uses' => 'NoteController@destroy']);

//check users with incomplete broker info
Route::get('check-users-with-incomplete-broker', ['uses' => 'NotificationTaskController@checkExistingUsersBrokerInfo']);
Route::get('follow-up-broker-after-2days', ['uses' => 'NotificationTaskController@followUpBrokerAfterThreeDays']);
Route::get('reserve-no-broker', ['as'=>'reserve-no-broker', 'uses' => 'AdsController@reserveNoBroker']);
//Route::post('delete_lead_', ['as'=>'delete_lead_', 'uses' => 'AdsController@destroy']);

Route::get('leads', ['as'=>'leads', 'uses' => 'HomeController@leads']);
Route::get('plans', ['as'=>'plans', 'uses' => 'AdsController@plans']);

//Homeworth
Route::group(['prefix'=>'homeworth'], function(){
    Route::get('/', ['as' => 'homeworth', 'uses' => 'HomeController@homeWorth']);
    Route::post('find', ['as' => 'find','uses' => 'HomeController@homeWorthFindHouse']);
    Route::post('submit', ['uses' => 'HomeController@homeWorthSubmitHouse']);
    Route::post('submit-inquiry', ['uses' => 'HomeController@homeWorthSubmitHouseInquiry']);
    Route::get('how-it-works', ['as' => 'how-it-works', 'uses' => 'HomeController@homeWorthAbout']);
    Route::get('faq', ['uses' => 'HomeController@homeWorthFaq']);
    Route::get('contact', ['uses' => 'HomeController@homeWorthContact']);
    Route::post('load-location', ['uses'=>'HomeController@loadLocation']);
    Route::get('valuation', ['as' => 'valuation', 'uses'=>'HomeController@loadNoZpid']);
});



Route::post('upload-image', ['uses' => 'Auth\RegisterController@uploadImages']);
//testing
Route::get('testsendnotification', ['uses' => 'NotificationTaskController@notificationTask']);
Route::get('sample', ['as' => 'sample', 'uses'=>'TitleCompanyController@sendApprovalNotification']);
Route::get('overdue', ['as' => 'overdue', 'uses'=>'TitleCompanyController@sendEmailOverdue']);
Route::post('get_post_by_id', ['as' => 'get_post_by_id', 'uses' => 'AdministrativeController@getPostById']);
Route::post('test_post_local', ['as' => 'test_post_local', 'uses' => 'AdministrativeController@testPostLocal']);

//E2E test
Route::post('testpost', ['uses'=>'AdministrativeController@testPost']);
Route::get('svcstatus', ['as' => 'svcstatus', 'uses'=>'AdministrativeController@checkServerStatus']);
Route::get('checkdbstatus', ['as' => 'checkdbstatus', 'uses'=>'AdministrativeController@checkDbServerStatus']);
Route::get('general_mailer', ['as' => 'checkdbstatus', 'uses'=>'AdministrativeController@generalMailer']);
Route::get('checkentries', ['as' => 'checkentries', 'uses'=>'AdministrativeController@checkAllEntries']);
Route::get('test_remind_buyer', ['as' => 'test_remind_buyer', 'uses'=>'ContractSigned@sendTitleCompanyReminderTest']);
Route::post('test_reassign_user', ['uses'=>'AdministrativeController@testReassignUser']);


Route::post('preregister', ['uses'=>'UserController@preRegisterUser']);
Route::post('landing-mail', ['uses'=>'UserController@sendEmailFromPrereg']);
Route::get('preregistration', ['as' => 'preregistration', 'uses'=>'UserController@preRegistration']);
Route::get('check-email-current', ['as' => 'check-email-current', 'uses'=>'UserController@checkEmailCurrent']);
Route::post('get-zipcode', ['uses'=>'Auth\RegisterController@getZipState']);

Route::get('installation', ['as' => 'installation', 'uses'=>'HomeController@installation']);
Route::post('installation', [ 'uses'=>'HomeController@installationPost']);

//Route::get('/home', ['as' => 'home', 'uses'=>'HomeController@index']);
//Route::get('/', ['as' => 'home', 'uses'=>'UserController@index']);
Route::get('/', ['as' => 'home', 'uses'=>'HomeController@index']);
Route::get('LanguageSwitch/{lang}', ['as' => 'switch_language', 'uses'=>'HomeController@switchLang']);
Route::post('home-contact-send', ['uses'=>'UserController@sendContact']);

//landing page temporary pages
Route::get('terms-and-conditions', ['as' => 'terms-and-condition', 'uses'=>'UserController@termsAndConditions']);
Route::get('privacy-policy', ['as' => 'privacy-policy', 'uses'=>'UserController@privacyPolicy']);
Route::get('faq', ['as' => 'faq', 'uses'=>'UserController@frequentlyAskedQuestions']);
Route::get('contact', ['as' => 'contact', 'uses'=>'UserController@contact']);
Route::post('contact-send', ['uses'=>'UserController@sendContact']);
Route::post('get-user-info', ['as' => 'get-user-info', 'uses'=>'UserController@getUserInfo']);
Route::post('notication-user-change', ['as' => 'notication-user-change', 'uses'=>'UserController@updateUserNotification']);



//Account activating
Route::get('account/activating/{activation_code}', ['as' => 'email_activation_link', 'uses'=>'UserController@activatingAccount']);

//Listing page
Route::get('contact-us', ['as' => 'contact_us_page', 'uses'=>'HomeController@contactUs']);
Route::post('contact-us', ['uses'=>'HomeController@contactUsPost']);
Route::get('task', ['uses'=>'NotificationTaskController@notificationTask']);
Route::get('how-it-works', ['as' => 'how-it-works', 'uses'=>'PostController@howItWorks']);
Route::get('page/{slug}', ['as' => 'single_page', 'uses'=>'PostController@showPage']);

Route::get('category/{cat_id?}', ['uses'=>'CategoriesController@show'])->name('category');
Route::get('countries/{country_code?}', ['uses'=>'LocationController@countriesListsPublic'])->name('countries');
Route::get('set-country/{country_code}', ['uses'=>'LocationController@setCurrentCountry'])->name('set_country');
Route::get('searchCityJson', ['uses'=>'LocationController@searchCityJson'])->name('searchCityJson');
Route::get('searchzip', ['uses'=>'LocationController@searchZipJson'])->name('searchzip');
Route::get('searchzips', ['uses'=>'LocationController@searchZipsJson'])->name('searchzip');
Route::get('searchzip_single', ['uses'=>'LocationController@searchZipJsonSingle'])->name('searchzip_single');

Route::get('search/{country_code?}/{state_id?}/{city_id?}/{category_slug?}/{brand_slug?}', ['as' => 'search', 'uses'=>'AdsController@search']);
Route::get('search-redirect', ['as' => 'search_redirect', 'uses'=>'AdsController@searchRedirect']);
Route::get('search-listings', ['as' => 'search_listings', 'uses'=>'AdsController@searchListings']);
Route::get('map-to-search', ['as' => 'map_to_search', 'uses'=>'AdsController@mapToSearch']);
Route::post('get-location', ['uses'=>'AdsController@detectLocation']);
Route::post('save-user-search', ['uses'=>'AdsController@saveUserSearch']);
Route::post('save-lead', ['uses'=>'AdsController@saveLead']);
Route::get('save-search', ['as' => 'save-search', 'uses'=>'NotificationTaskController@notifyUserSavedSearch']);
Route::get('leads-by-user/{id?}', ['as' => 'ads_by_user', 'uses'=>'AdsController@adsByUser']);

//Listings
Route::get('listing/{id}/{slug?}', ['as' => 'single_ad', 'uses'=>'AdsController@singleAd']);

Route::get('embedded/{slug}', ['as' => 'embedded_ad', 'uses'=>'AdsController@embeddedAd']);
Route::get('docusign-local', ['as' => 'docusign-local', 'uses'=>'AdsController@embedDocusign']);

Route::post('save-ad-as-favorite', ['as' => 'save_ad_as_favorite', 'uses'=>'AdsController@saveAdAsFavorite']);
Route::post('report-post', ['as' => 'report_ads_pos', 'uses'=>'AdsController@reportAds']);
Route::post('post-rating', ['as' => 'post-rating', 'uses'=>'AdsController@postRating']);
Route::post('reply-by-email', ['as' => 'reply_by_email_post', 'uses'=>'UserController@replyByEmailPost']);
Route::post('post-comments/{id}', ['as' => 'post_comments', 'uses'=>'CommentController@postComments']);
Route::post('delete-comment', ['as' => 'delete-comment', 'uses'=>'CommentController@deleteComment']);
Route::post('approve-comment', ['as' => 'approve-comment', 'uses'=>'CommentController@approveComment']);
Route::post('approve-block', ['as' => 'approve-block', 'uses'=>'CommentController@blockComment']);
Route::post('edit-user-notify', ['as' => 'edit-user-notify', 'uses' => 'UserController@editEmailOrPhone']);

Route::get('apply_job', function (){
    return redirect(route('home'));
});
Route::post('apply_job', ['as' => 'apply_job', 'uses'=>'AdsController@applyJob']);

// Password reset routes...
Route::post('send-password-reset-link', ['as' => 'send_reset_link', 'uses'=>'Auth\PasswordController@postEmail']);
//Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
//Route::post('password/reset', ['as'=>'password_reset_post', 'uses'=>'Auth\PasswordController@postReset']);

Route::post('get-sub-category-by-category', ['as'=>'get_sub_category_by_category', 'uses' => 'AdsController@getSubCategoryByCategory']);
Route::post('get-brand-by-category', ['as'=>'get_brand_by_category', 'uses' => 'AdsController@getBrandByCategory']);
Route::post('get-category-info', ['as'=>'get_category_info', 'uses' => 'AdsController@getParentCategoryInfo']);
Route::post('get-state-by-country', ['as'=>'get_state_by_country', 'uses' => 'AdsController@getStateByCountry']);
Route::post('get-city-by-state', ['as'=>'get_city_by_state', 'uses' => 'AdsController@getCityByState']);
Route::post('get_zip_by_city', ['as'=>'get_zip_by_city', 'uses' => 'AdsController@getZipByCity']);
Route::post('get-cities-by-states', ['as'=>'get-cities-by-states', 'uses' => 'AdsController@getCitiesByStates']);
Route::post('save-notification-db', ['as'=>'save-notification-db', 'uses' => 'AdsController@saveNotification']);
Route::post('remove-by-country', ['as'=>'remove-by-country', 'uses' => 'AdsController@removeNotification']);
Route::post('remove-by-state', ['as'=>'remove-by-state', 'uses' => 'AdsController@removeByState']);
Route::post('remove-by-city', ['as'=>'remove-by-city', 'uses' => 'AdsController@removeByCity']);
Route::post('switch/product-view', ['as'=>'switch_grid_list_view', 'uses' => 'AdsController@switchGridListView']);
Route::post('get-mls-by-state', ['as'=>'get_mls_by_state', 'uses' => 'AdsController@getMlsByState']);
Route::post('get-states-for-autocomplete', ['as'=>'get-states-for-autocomplete', 'uses' => 'LocationController@getStatesByName']);
Route::post('get-cities-for-autocomplete', ['as'=>'get-cities-for-autocomplete', 'uses' => 'LocationController@getCitiesByName']);
Route::post('get-zips-for-autocomplete', ['as'=>'get-zips-for-autocomplete', 'uses' => 'LocationController@getZipsByName']);
Route::post('get-mls-for-autocomplete', ['as'=>'get-mls-for-autocomplete', 'uses' => 'UserController@getMlsAutocomplete']);
Route::post('admin-user-save', ['as'=>'admin-user-save', 'uses' => 'UserController@adminUserEditSave']);
Route::post('get-all-users', ['as'=>'get-all-users', 'uses' => 'AdministrativeController@getAllUsers']);
Route::post('save-new-agent', ['as'=>'save-new-agent', 'uses' => 'AdministrativeController@saveNewAgent']);

//GET and POST new Ads

Route::get('post-new', ['as'=>'create_ad', 'uses' => 'AdsController@create']);
Route::post('post-new', ['uses' => 'AdsController@store']);
Route::get('docusign-callback', ['uses' => 'AdsController@callback']);
Route::get('agent-upload-csv', ['as'=>'agent-upload-csv', 'uses' => 'AdsController@agentUploadCsv']);
Route::get('uploader', ['as'=>'uploader', 'uses' => 'AdsController@agentUploadCsv']);
Route::post('import-images', ['as'=>'import-images', 'uses' => 'AdministrativeController@importImages']);
Route::post('import-save', ['as'=>'import-save', 'uses' => 'AdministrativeController@importSaveAll']);
Route::get('import-reset', ['as'=>'import-reset', 'uses' => 'AdministrativeController@importReset']);
Route::post('import-csv', ['as'=>'import-csv', 'uses' => 'AdministrativeController@importCsv']);

Route::get('download/{csv}',function($file=null){
    $csv_file = public_path()."/downloads/".$file;
    $headers = array('Content-Type: text/csv');
    return Response::download($csv_file, 'importer.csv', $headers);
});

//Create contract and show signer page
Route::get('referral-document/{id}', ['as' => 'referral-document', 'uses'=>'AdsController@loadReferralAgreement']);
Route::get('referral/{id}', ['as' => 'referral', 'uses'=>'AdsController@loadListingCart']);
Route::get('referral-document/{id}/success', ['as' => 'referral', 'uses'=>'AdsController@loadReferralAgreementSuccess']);
Route::get('referral-document/{id}/title_company', ['as' => 'referral', 'uses'=>'AdsController@loadReferralAgreementSuccessTc']);
Route::get('referral-agreement/id', ['as' => 'referral-agreement', 'uses'=>'AdsController@createContract']);
Route::get('contract-signed', ['as' => 'contract-signed', 'uses'=>'ContractSigned@index']);
Route::get('referral-thankyou', ['as' => 'referral-signed', 'uses'=>'ContractSigned@blankPage']);
Route::post('referral-signed', ['as' => 'referral-signed', 'uses'=>'ContractSigned@referralSigned']);
Route::get('download-contract/{id}', ['as' => 'download-contract', 'uses'=>'ContractSigned@downloadContract']);
Route::post('send-contract-to-tc', ['as' => 'send-contract-to-tc', 'uses'=>'ContractSigned@sendContractToTitleCompany']);
Route::post('status-change-user', ['as'=>'status-change-user', 'uses' => 'AdsController@adStatusChange']);
Route::post('title_company_lbt', ['as'=>'title_company_lbt', 'uses' => 'TitleCompanyController@titleCompanyLbt']);
Route::get('reserved-leads-success/{id}', ['as' => 'reserved-leads-success', 'uses'=>'DashboardController@reservedLeadSuccess']);
Route::post('send-referral-success', ['as'=>'send-referral-success', 'uses' => 'DashboardController@sendReferralNotification']);
Route::get('referral-vip-sign/{id}', ['as'=>'referral-vip-sign', 'uses' => 'PaymentController@signReferral']);

//get envelope status
Route::get('envelope-status/{id}', ['uses' => 'AdsController@getEnvelopeStatus']);

//listing category_type
Route::get('listing-cart/{id}', ['uses' => 'AdsController@loadListingCart']);

//Title company
Route::get('title-company/{id}', ['uses' => 'TitleCompanyController@loginByCode']);
Route::post('title-company/validate', ['uses' => 'TitleCompanyController@validateCode']);
Route::get('title-company/approve/{id}', ['as' => 'title-company', 'uses'=>'TitleCompanyController@approveContract']);
Route::post('title-approve-click', ['as' => 'title-approve-click', 'uses'=>'TitleCompanyController@approveContractByTitle']);
Route::get('title-approve/{id}', ['as' => 'title-company', 'uses'=>'TitleCompanyController@approveContractStatus']);
//Save notification
Route::post('save-notification', ['as' => 'save-notification', 'uses' => 'UserController@saveNotification']);

//Route::get('post-new2', ['as'=>'create_ad', 'uses' => 'ListingsController@create']);
//Route::post('post-new2', ['uses' => 'ListingsController@store']);
//Route::get('docusign-callback', ['uses' => 'ListingsController@callback']); t

//Post bid
Route::post('{id}/post-new', ['as' => 'post_bid','uses' => 'BidController@postBid']);

Route::group(['prefix'=>'checkout'], function(){
    //Checkout payment
    Route::get('{transaction_id}', ['as'=>'payment_checkout', 'uses' => 'PaymentController@checkout']);
    Route::post('{transaction_id}', ['uses' => 'PaymentController@chargePayment']);
    //Payment success url
    Route::any('{transaction_id}/payment-success', ['as'=>'payment_success_url','uses' => 'PaymentController@paymentSuccess']);
    Route::any('{transaction_id}/paypal-notify', ['as'=>'paypal_notify_url','uses' => 'PaymentController@paypalNotify']);
});

Route::post('status-action', ['uses' => 'TitleCompanyController@sendReminder']);
Route::get('no-payment', ['as'=>'no-payment', 'uses' => 'PaymentController@processNoPayment']);
Route::get('no-payment-test', ['as'=>'no-payment-test', 'uses' => 'PaymentController@processNoPaymentTest']);

//Payment
Route::group(['prefix'=>'payment'], function(){
    Route::post('pay', ['uses' => 'AdsController@displayCart']);
    Route::post('paid', ['uses' => 'PaypalGatewayController@makePaypalPayment']);
    Route::get('success', ['as'=>'success', 'uses' => 'PaypalGatewayController@paymentSuccess']);
    Route::get('success-vip', ['as'=>'success-vip', 'uses' => 'PaypalGatewayController@paymentSuccessVip']);
    Route::get('fail', ['as'=>'fail', 'uses' => 'PaypalGatewayController@paymentFailed']);
});

//PayPal
Route::get('status', ['as'=>'status', 'uses' => 'PaypalGatewayController@paymentStatus']);
Route::get('cancel', ['as'=>'cancel', 'uses' => 'PaypalGatewayController@paymentCancel']);

Route::group(['prefix'=>'login'], function(){
    //Social login route

    Route::get('facebook', ['as' => 'facebook_redirect', 'uses'=>'SocialLogin@redirectFacebook']);
    Route::get('facebook-callback', ['as' => 'facebook_callback', 'uses'=>'SocialLogin@callbackFacebook']);

    Route::get('google', ['as' => 'google_redirect', 'uses'=>'SocialLogin@redirectGoogle']);
    Route::get('google-callback', ['as' => 'google_callback', 'uses'=>'SocialLogin@callbackGoogle']);

    Route::get('twitter', ['as' => 'twitter_redirect', 'uses'=>'SocialLogin@redirectTwitter']);
    Route::get('twitter-callback', ['as' => 'twitter_callback', 'uses'=>'SocialLogin@callbackTwitter']);

    //Complete registration
    Route::get('complete-registration', ['as' => 'complete-registration', 'uses'=>'CompleteRegistration@stepTwo']);

    //Register complete
    Route::post('register-complete', ['as' => 'register-complete', 'uses'=>'RegisterComplete@addInfo']);

    //Verify complete
    Route::post('verify-complete', ['as' => 'verify-complete', 'uses'=>'RegisterComplete@verifySmsCode']);

    //Redirect to defaul thank you page
    Route::get('verify', ['as' => 'verify', 'uses'=>'CompleteRegistration@verify']);
});

Route::resource('user', 'UserController');

//Dashboard Route
Route::group(['prefix'=>'dashboard', 'middleware' => 'dashboard'], function(){
    Route::get('/', ['as'=>'dashboard', 'uses' => 'DashboardController@dashboard']);
    Route::get('comments', ['as'=>'comments', 'uses' => 'CommentController@userComments']);
    Route::get('leads-admin', ['as'=>'leads-admin', 'uses' => 'DashboardController@allLeads']);
    Route::get('leads-blocked-admin', ['as'=>'leads-blocked-admin', 'uses' => 'DashboardController@allLeads']);
    Route::get('leads-data', ['as'=>'leads-data', 'uses' => 'DashboardController@leadsData']);
    Route::get('leads-blocked-data', ['as'=>'leads-blocked-data', 'uses' => 'DashboardController@leadsBlockedData']);
    Route::get('import-leads', ['as'=>'import-leads', 'uses' => 'DashboardController@importLeads']);
    Route::get('messages', ['as'=>'messages', 'uses' => 'DashboardController@allUserMessages']);
    Route::get('get_messages_data', ['as'=>'get_messages_data', 'uses' => 'DashboardController@getUserMessagesData']);
    Route::post('get-user-messages', ['as'=>'get-user-messages', 'uses' => 'UserController@getUserNotifications']);
    Route::get('message/{id}', ['as'=>'message', 'uses' => 'DashboardController@getUserMessage']);
    Route::get('featured_ads', ['as'=>'featured_ads', 'uses'=>'DashboardController@featuredAds']);
    Route::get('paused', ['as'=>'paused', 'uses'=>'DashboardController@pausedAds']);
    Route::post('ads_is_featured', ['as'=>'ads_is_featured', 'uses' => 'DashboardController@isFeaturedAd']);
    Route::get('successful_leads', ['as'=>'successful_leads', 'uses'=>'DashboardController@successfulLeads']);
    Route::get('successful_leads_data', ['as'=>'successful_leads_data', 'uses' => 'DashboardController@successLeadsData']);
    Route::post('relist_ad', ['as'=>'relist_ad', 'uses' => 'DashboardController@relistAd']);
    Route::get('uploaded_leads', ['as'=>'uploaded_leads', 'uses' => 'DashboardController@uploadedLeads']);
    Route::get('user_leads_uploads', ['as'=>'user_leads_uploads', 'uses' => 'DashboardController@userLeadsUploadsData']);
    Route::get('uploaded_leads_status/{id}', [
        'as' => 'uploaded_leads_status',
        'uses' => 'DashboardController@uploadedLeadsStatus'
    ]);
    Route::get('uploaded_leads_status_edit/{id}', [
        'as' => 'uploaded_leads_status_edit',
        'uses' => 'DashboardController@uploadedLeadsStatusEdit'
    ]);

    Route::post('twilio_token', ['as' => 'twilio.token', 'uses' => 'DashboardController@twilioToken']);
    Route::post('twilio_sms', ['as' => 'twilio.sms', 'uses' => 'DashboardController@twilioSms']);
    Route::post('twilio_call', ['as' => 'twilio.call', 'uses' => 'DashboardController@twilioCall']);
    Route::get('twilio_sms_create', ['as' => 'twilio.sms.create', 'uses' => 'DashboardController@twilioSmsCreate']);
    Route::post('twilio_sms_send', ['as' => 'twilio.sms.send', 'uses' => 'DashboardController@twilioSmsSend']);
    Route::post('twilio_sms_status', ['as' => 'twilio.sms.status', 'uses' => 'DashboardController@uploaded_leads']);

    Route::get('uploaded_leads_status_voice', [
        'as' => 'uploaded_leads_status_voice',
        'uses' => 'DashboardController@uploadedLeadsStatusVoice'
    ]);
    Route::post('uploaded_leads_status', ['uses' =>  'DashboardController@uploadedLeadsStatusAction']);
    Route::get('uploads_leads_data', ['as'=>'uploads_leads_data', 'uses' => 'DashboardController@uploadLeadsData']);

    Route::group(['middleware' => 'only_admin_access'], function() {
        Route::post('follow-up', [
            'as' => 'reserved-leads-follow-up',
            'uses' => 'DashboardController@followUp'
        ]);
        Route::post('send-mail-reservation-info', [
            'as' => 'send-mail-reservation-info',
            'uses' => 'DashboardController@sendMailReservationInfo'
        ]);
        Route::group(['prefix'=>'settings'], function() {
            Route::get('theme-settings', ['as'=>'theme_settings', 'uses' => 'SettingsController@ThemeSettings']);
            Route::get('modern-theme-settings', ['as'=>'modern_theme_settings', 'uses' => 'SettingsController@modernThemeSettings']);
            Route::get('social-url-settings', ['as'=>'social_url_settings', 'uses' => 'SettingsController@SocialUrlSettings']);
            Route::get('general', ['as'=>'general_settings', 'uses' => 'SettingsController@GeneralSettings']);
            Route::get('payments', ['as'=>'payment_settings', 'uses' => 'SettingsController@PaymentSettings']);
            Route::get('ad', ['as'=>'ad_settings', 'uses' => 'SettingsController@AdSettings']);
            Route::get('languages', ['as'=>'language_settings', 'uses' => 'LanguageController@index']);
            Route::post('languages', ['uses' => 'LanguageController@store']);
            Route::post('languages-delete', ['as'=>'delete_language', 'uses' => 'LanguageController@destroy']);
            Route::get('storage', ['as'=>'file_storage_settings', 'uses' => 'SettingsController@StorageSettings']);
            Route::get('social', ['as'=>'social_settings', 'uses' => 'SettingsController@SocialSettings']);
            Route::get('blog', ['as'=>'blog_settings', 'uses' => 'SettingsController@BlogSettings']);
            Route::get('other', ['as'=>'other_settings', 'uses' => 'SettingsController@OtherSettings']);
            Route::post('other', ['as'=>'other_settings', 'uses' => 'SettingsController@OtherSettingsPost']);
            Route::get('recaptcha', ['as'=>'re_captcha_settings', 'uses' => 'SettingsController@reCaptchaSettings']);

            //Save settings / options
            Route::post('save-settings', ['as'=>'save_settings', 'uses' => 'SettingsController@update']);
            Route::get('monetization', ['as'=>'monetization', 'uses' => 'SettingsController@monetization']);
        });

        Route::group(['prefix'=>'location'], function(){
            Route::get('country', ['as'=>'country_list', 'uses' => 'LocationController@countries']);
            Route::get('country-data', ['as'=>'get_countries_data', 'uses' => 'LocationController@getCountriesData']);
            Route::get('states', ['as'=>'state_list', 'uses' => 'LocationController@stateList']);
            Route::post('states', [ 'uses' => 'LocationController@saveState']);
            Route::get('states/{id}/edit', ['as'=>'edit_state', 'uses' => 'LocationController@stateEdit']);
            Route::post('states/{id}/edit', ['uses' => 'LocationController@stateEditPost']);
            Route::post('states/delete', ['as'=>'delete_state', 'uses' => 'LocationController@stateDestroy']);
            Route::get('state-data', ['as'=>'get_state_data', 'uses' => 'LocationController@getStatesData']);
            Route::get('cities', ['as'=>'city_list', 'uses' => 'LocationController@cityList']);
            Route::post('cities', ['uses' => 'LocationController@saveCity']);
            Route::get('city-data', ['as'=>'get_city_data', 'uses' => 'LocationController@getCityData']);
            Route::get('cities/{id}/edit', ['as'=>'edit_city', 'uses' => 'LocationController@cityEdit']);
            Route::post('cities/{id}/edit', ['uses' => 'LocationController@cityEditPost']);
            Route::post('city/delete', ['as'=>'delete_city', 'uses' => 'LocationController@cityDestroy']);
        });

        Route::group(['prefix'=>'categories'], function(){
            Route::get('/', ['as'=>'parent_categories', 'uses' => 'CategoriesController@index']);
            Route::post('/', ['uses' => 'CategoriesController@store']);
            Route::get('edit/{id}', ['as'=>'edit_categories', 'uses' => 'CategoriesController@edit']);
            Route::post('edit/{id}', ['uses' => 'CategoriesController@update']);
            Route::post('delete-categories', ['as'=>'delete_categories', 'uses' => 'CategoriesController@destroy']);
        });

        Route::group(['prefix'=>'distances'], function(){
            Route::get('/', ['as'=>'admin_brands', 'uses' => 'BrandsController@index']);
            Route::post('/', ['uses' => 'BrandsController@store']);
            Route::get('edit/{id}', ['as'=>'edit_brands', 'uses' => 'BrandsController@edit']);
            Route::post('edit/{id}', ['uses' => 'BrandsController@update']);
            Route::post('delete-distances', ['as'=>'delete_brands', 'uses' => 'BrandsController@destroy']);
        });

        Route::group(['prefix'=>'posts'], function(){
            Route::get('/', ['as'=>'posts', 'uses' => 'PostController@posts']);
            Route::get('data', ['as'=>'posts_data', 'uses' => 'PostController@postsData']);

            Route::get('create', ['as'=>'create_new_post', 'uses' => 'PostController@createPost']);
            Route::post('create', ['uses' => 'PostController@storePost']);
            Route::post('delete', ['as'=>'delete_post','uses' => 'PostController@destroyPost']);

            Route::get('edit/{slug}', ['as'=>'edit_post', 'uses' => 'PostController@editPost']);
            Route::post('edit/{slug}', ['uses' => 'PostController@updatePost']);
        });

        Route::group(['prefix'=>'pages'], function(){
            Route::get('/', ['as'=>'pages', 'uses' => 'PostController@index']);
            Route::get('data', ['as'=>'pages_data', 'uses' => 'PostController@pagesData']);

            Route::get('create', ['as'=>'create_new_page', 'uses' => 'PostController@create']);
            Route::post('create', ['uses' => 'PostController@store']);
            Route::post('delete', ['as'=>'delete_page','uses' => 'PostController@destroy']);

            Route::get('edit/{slug}', ['as'=>'edit_page', 'uses' => 'PostController@edit']);
            Route::post('edit/{slug}', ['uses' => 'PostController@updatePage']);
        });
        Route::group(['prefix'=>'admin_comments'], function(){
            Route::get('/', ['as'=>'admin_comments', 'uses' => 'CommentController@index']);
            Route::get('data', ['as'=>'admin_comments_data', 'uses' => 'CommentController@commentData']);
            Route::post('action', ['as'=>'comment_action', 'uses' => 'CommentController@commentAction']);
        });

        Route::get('/confirm-user/{id}', ['as' => 'confirm_user', 'uses' => 'UserController@confirmUser']);

        Route::get('approved', ['as'=>'approved_ads', 'uses' => 'AdsController@index']);
        Route::get('pending', ['as'=>'admin_pending_ads', 'uses' => 'AdsController@adminPendingAds']);
        Route::get('blocked', ['as'=>'admin_blocked_ads', 'uses' => 'AdsController@adminBlockedAds']);
        Route::get('vip-leads', ['as'=>'vip-leads', 'uses' => 'AdsController@adminSpecialAds']);
        Route::post('status-change', ['as'=>'ads_status_change', 'uses' => 'AdsController@adStatusChange']);
        Route::get('reports', ['as'=>'ad_reports', 'uses' => 'AdsController@reports']);
        Route::get('users', ['as'=>'users', 'uses' => 'UserController@index']);
        Route::get('users-data', ['as'=>'get_users_data', 'uses' => 'UserController@usersData']);
        Route::get('users-info/{id}', ['as'=>'user_info', 'uses' => 'UserController@userInfo']);
        Route::get('user-add', ['as'=>'user-add', 'uses' => 'UserController@userAdd']);
        Route::post('user-add', ['as'=>'user-add', 'uses' => 'UserController@addUser']);
        Route::post('user-status-edit', ['as'=>'user-status-edit', 'uses' => 'UserController@userEditStatus']);
        Route::post('user-delete', ['as'=>'user-delete', 'uses' => 'UserController@destroy']);
        Route::post('change-user-status', ['as'=>'change_user_status', 'uses' => 'UserController@changeStatus']);
        Route::post('change-user-feature', ['as'=>'change_user_feature', 'uses' => 'UserController@changeFeature']);
        Route::post('delete-reports', ['as'=>'delete_report', 'uses' => 'AdsController@deleteReports']);
        Route::post('ads_status_reserve', ['as'=>'ads_status_reserve', 'uses' => 'AdsController@adminReserveAd']);

        Route::get('contact-messages', ['as'=>'contact_messages', 'uses' => 'HomeController@contactMessages']);
        Route::get('contact-messages-data', ['as'=>'contact_messages_data', 'uses' => 'HomeController@contactMessagesData']);

        Route::group(['prefix'=>'administrators'], function(){
            Route::get('/', ['as'=>'administrators', 'uses' => 'UserController@administrators']);
            Route::get('create', ['as'=>'add_administrator', 'uses' => 'UserController@addAdministrator']);
            Route::post('create', ['uses' => 'UserController@storeAdministrator']);
            Route::post('block-unblock', ['as'=>'administratorBlockUnblock','uses' => 'UserController@administratorBlockUnblock']);

        });
    });

    //All user can access this route
    Route::get('payments', ['as'=>'payments', 'uses' => 'PaymentController@index']);
    Route::get('payments-data', ['as'=>'get_payments_data', 'uses' => 'PaymentController@paymentsData']);
    Route::get('payments-info/{trand_id}', ['as'=>'payment_info', 'uses' => 'PaymentController@paymentInfo']);

    Route::get('reserved-leads-check/{id}', ['as'=>'reserved-leads-check', 'uses' => 'DashboardController@transactionsSettingsBuyingCheck']);

    Route::get('reserved-lead/{id}', [
            'as'    =>  'reserved-lead',
            'uses'  =>  'DashboardController@transactionSettingsBuying'
        ]);
    Route::group(['prefix' => 'reserved-leads'], function() {
        Route::get('/', [
            'as'    =>  'reserved-leads',
            'uses'  =>  'DashboardController@transactionsSettingsBuying'
        ]);
        Route::get('add-status', [
            'as'    =>  'reserved-leads-add-status',
            'uses'  =>  'DashboardController@transactionsSettingsBuyingAddStatus'
        ]);
        Route::post('update-status', [
            'as'    =>  'reserved-leads-update-status',
            'uses'  =>  'DashboardController@transactionsSettingsBuyingUpdateStatus'
        ]);
    });

    Route::get('lead-status', ['as'=>'lead-status', 'uses' => 'DashboardController@transactionsSettingsSelling']);
    //End all users access

    Route::get('transactions', ['as' => 'transactions', 'uses' => 'SettingsController@transactionsSettings']);

    Route::group(['prefix'=>'u'], function(){
        Route::group(['prefix'=>'posts'], function(){
            Route::get('/', ['as'=>'my_ads', 'uses' => 'AdsController@myAds']);
            Route::post('delete', ['as'=>'delete_ads', 'uses' => 'AdsController@destroy']);
            Route::get('edit/{id}', ['as'=>'edit_ad', 'uses' => 'AdsController@edit']);
            Route::post('edit/{id}', ['uses' => 'AdsController@update']);
            Route::get('my-leads', ['as'=>'my-leads', 'uses' => 'AdsController@myLists']);
            Route::get('favorite-lists', ['as' => 'favorite_ads', 'uses' => 'AdsController@favoriteAds']);
            Route::post('delete-favorite', ['as'=>'delete-favorite', 'uses' => 'AdsController@deleteFavoriteList']);
            //Upload ads image
            Route::post('upload-a-image', ['as'=>'upload_ads_image', 'uses' => 'AdsController@uploadAdsImage']);
            Route::post('upload-post-image', ['as'=>'upload_post_image', 'uses' => 'PostController@uploadPostImage']);
            //Delete media
            Route::post('delete-media', ['as'=>'delete_media', 'uses' => 'AdsController@deleteMedia']);
            Route::post('feature-media-creating', ['as'=>'feature_media_creating_ads', 'uses' => 'AdsController@featureMediaCreatingAds']);
            Route::get('append-media-image', ['as'=>'append_media_image', 'uses' => 'AdsController@appendMediaImage']);
            Route::get('append-post-media-image', ['as'=>'append_post_media_image', 'uses' => 'PostController@appendPostMediaImage']);
            Route::get('pending-lists', ['as'=>'pending_ads', 'uses' => 'AdsController@pendingAds']);
            Route::get('archive-lists', ['as'=>'favourite_ad', 'uses' => 'AdsController@create']);

            Route::get('reports-by/{slug}', ['as'=>'reports_by_ads', 'uses' => 'AdsController@reportsByAds']);
            Route::get('profile', ['as'=>'profile', 'uses' => 'UserController@profile']);
            Route::get('profile/edit', ['as'=>'profile_edit', 'uses' => 'UserController@profileEdit']);
            Route::post('profile/edit', ['uses' => 'UserController@profileEditPost']);
            Route::get('profile/change-avatar', ['as'=>'change_avatar', 'uses' => 'UserController@changeAvatar']);
            Route::post('upload-avatar', ['as'=>'upload_avatar',  'uses' => 'UserController@uploadAvatar']);

            Route::get('notification', ['as'=>'notification', 'uses' => 'UserController@notification']);
            Route::get('job_applicants/{ad_id}', ['as'=>'job_applicants', 'uses' => 'AdsController@jobApplicants']);
            Route::get('job_applicant_view/{applicant_id}', ['as'=>'job_applicant_view', 'uses' => 'AdsController@jobApplicantView']);

            //bids
            Route::get('bids/{ad_id}', ['as'=>'auction_bids', 'uses' => 'BidController@index']);
            Route::post('bids/action', ['as'=>'bid_action', 'uses' => 'BidController@bidAction']);
            Route::get('bidder_info/{bid_id}', ['as'=>'bidder_info', 'uses' => 'BidController@bidderInfo']);

            /**
             * Change Password route
             */
            Route::group(['prefix' => 'account'], function() {
                Route::get('change-password', ['as' => 'change_password', 'uses' => 'UserController@changePassword']);
                Route::post('change-password', 'UserController@changePasswordPost');
            });

        });
    });
    //Route::get('logout', ['as'=>'logout', 'uses' => 'DashboardController@logout']);
});
