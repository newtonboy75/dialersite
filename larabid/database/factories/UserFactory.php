<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
      'name' => $faker->name,
      'email' => $faker->unique()->safeEmail,
      'password' => $faker->password,
      'address' => $faker->address,
      'phone' => $faker->phoneNumber,
      're_license_number' => $faker->randomLetter,
      'active_status' => '1',
      'is_email_verified' => '1',
      'mls_id' => $faker->randomNumber(6),
      'sms_activation_code' => '1',
      'remember_token' => str_random(10),
    ];
});
