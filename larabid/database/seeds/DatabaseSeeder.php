<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(AdsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(BidsTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(MediaTableSeeder::class);
        $this->call(OptionsTableSeeder::class);
        $this->call(PaymentsTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(StatesTableSeeder::class);
    }
}
