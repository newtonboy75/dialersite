<?php

namespace Newtonboy\LaravelEsignatureWrapper;

use Illuminate\Support\ServiceProvider;

class LaravelEsignatureWrapperServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'newtonboy');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'newtonboy');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {

            // Publishing the configuration file.
            $this->publishes([
                __DIR__.'/../config/laravelesignaturewrapper.php' => config_path('laravelesignaturewrapper.php'),
            ], 'laravelesignaturewrapper.config');

            // Publishing the views.
            /*$this->publishes([
                __DIR__.'/../resources/views' => base_path('resources/views/vendor/newtonboy'),
            ], 'laravelesignaturewrapper.views');*/

            // Publishing assets.
            /*$this->publishes([
                __DIR__.'/../resources/assets' => public_path('vendor/newtonboy'),
            ], 'laravelesignaturewrapper.views');*/

            // Publishing the translation files.
            /*$this->publishes([
                __DIR__.'/../resources/lang' => resource_path('lang/vendor/newtonboy'),
            ], 'laravelesignaturewrapper.views');*/

            // Registering package commands.
            // $this->commands([]);
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/laravelesignaturewrapper.php', 'laravelesignaturewrapper');

        // Register the service the package provides.
        $this->app->singleton('laravelesignaturewrapper', function ($app) {
            return new LaravelEsignatureWrapper;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['laravelesignaturewrapper'];
    }
}